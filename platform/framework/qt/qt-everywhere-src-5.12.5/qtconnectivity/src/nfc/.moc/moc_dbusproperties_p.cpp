/****************************************************************************
** Meta object code from reading C++ file 'dbusproperties_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../neard/dbusproperties_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dbusproperties_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface_t {
    QByteArrayData data[15];
    char stringdata0[231];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface_t qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface = {
    {
QT_MOC_LITERAL(0, 0, 37), // "OrgFreedesktopDBusPropertiesI..."
QT_MOC_LITERAL(1, 38, 17), // "PropertiesChanged"
QT_MOC_LITERAL(2, 56, 0), // ""
QT_MOC_LITERAL(3, 57, 9), // "interface"
QT_MOC_LITERAL(4, 67, 18), // "changed_properties"
QT_MOC_LITERAL(5, 86, 22), // "invalidated_properties"
QT_MOC_LITERAL(6, 109, 3), // "Get"
QT_MOC_LITERAL(7, 113, 31), // "QDBusPendingReply<QDBusVariant>"
QT_MOC_LITERAL(8, 145, 4), // "name"
QT_MOC_LITERAL(9, 150, 6), // "GetAll"
QT_MOC_LITERAL(10, 157, 30), // "QDBusPendingReply<QVariantMap>"
QT_MOC_LITERAL(11, 188, 3), // "Set"
QT_MOC_LITERAL(12, 192, 19), // "QDBusPendingReply<>"
QT_MOC_LITERAL(13, 212, 12), // "QDBusVariant"
QT_MOC_LITERAL(14, 225, 5) // "value"

    },
    "OrgFreedesktopDBusPropertiesInterface\0"
    "PropertiesChanged\0\0interface\0"
    "changed_properties\0invalidated_properties\0"
    "Get\0QDBusPendingReply<QDBusVariant>\0"
    "name\0GetAll\0QDBusPendingReply<QVariantMap>\0"
    "Set\0QDBusPendingReply<>\0QDBusVariant\0"
    "value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OrgFreedesktopDBusPropertiesInterface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   34,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,   41,    2, 0x0a /* Public */,
       9,    1,   46,    2, 0x0a /* Public */,
      11,    3,   49,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QVariantMap, QMetaType::QStringList,    3,    4,    5,

 // slots: parameters
    0x80000000 | 7, QMetaType::QString, QMetaType::QString,    3,    8,
    0x80000000 | 10, QMetaType::QString,    3,
    0x80000000 | 12, QMetaType::QString, QMetaType::QString, 0x80000000 | 13,    3,    8,   14,

       0        // eod
};

void OrgFreedesktopDBusPropertiesInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OrgFreedesktopDBusPropertiesInterface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->PropertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])),(*reinterpret_cast< const QStringList(*)>(_a[3]))); break;
        case 1: { QDBusPendingReply<QDBusVariant> _r = _t->Get((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<QDBusVariant>*>(_a[0]) = std::move(_r); }  break;
        case 2: { QDBusPendingReply<QVariantMap> _r = _t->GetAll((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<QVariantMap>*>(_a[0]) = std::move(_r); }  break;
        case 3: { QDBusPendingReply<> _r = _t->Set((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QDBusVariant(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusVariant >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (OrgFreedesktopDBusPropertiesInterface::*)(const QString & , const QVariantMap & , const QStringList & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OrgFreedesktopDBusPropertiesInterface::PropertiesChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject OrgFreedesktopDBusPropertiesInterface::staticMetaObject = { {
    &QDBusAbstractInterface::staticMetaObject,
    qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface.data,
    qt_meta_data_OrgFreedesktopDBusPropertiesInterface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OrgFreedesktopDBusPropertiesInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OrgFreedesktopDBusPropertiesInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OrgFreedesktopDBusPropertiesInterface.stringdata0))
        return static_cast<void*>(this);
    return QDBusAbstractInterface::qt_metacast(_clname);
}

int OrgFreedesktopDBusPropertiesInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDBusAbstractInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void OrgFreedesktopDBusPropertiesInterface::PropertiesChanged(const QString & _t1, const QVariantMap & _t2, const QStringList & _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
