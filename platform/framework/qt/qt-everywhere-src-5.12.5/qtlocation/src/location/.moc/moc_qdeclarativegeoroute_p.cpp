/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeoroute_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qdeclarativegeoroute_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeoroute_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeGeoRoute_t {
    QByteArrayData data[18];
    char stringdata0[238];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoRoute_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoRoute_t qt_meta_stringdata_QDeclarativeGeoRoute = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QDeclarativeGeoRoute"
QT_MOC_LITERAL(1, 21, 11), // "pathChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 6), // "equals"
QT_MOC_LITERAL(4, 41, 21), // "QDeclarativeGeoRoute*"
QT_MOC_LITERAL(5, 63, 5), // "other"
QT_MOC_LITERAL(6, 69, 6), // "bounds"
QT_MOC_LITERAL(7, 76, 13), // "QGeoRectangle"
QT_MOC_LITERAL(8, 90, 10), // "travelTime"
QT_MOC_LITERAL(9, 101, 8), // "distance"
QT_MOC_LITERAL(10, 110, 4), // "path"
QT_MOC_LITERAL(11, 115, 8), // "QJSValue"
QT_MOC_LITERAL(12, 124, 8), // "segments"
QT_MOC_LITERAL(13, 133, 45), // "QQmlListProperty<QDeclarative..."
QT_MOC_LITERAL(14, 179, 10), // "routeQuery"
QT_MOC_LITERAL(15, 190, 26), // "QDeclarativeGeoRouteQuery*"
QT_MOC_LITERAL(16, 217, 4), // "legs"
QT_MOC_LITERAL(17, 222, 15) // "QList<QObject*>"

    },
    "QDeclarativeGeoRoute\0pathChanged\0\0"
    "equals\0QDeclarativeGeoRoute*\0other\0"
    "bounds\0QGeoRectangle\0travelTime\0"
    "distance\0path\0QJSValue\0segments\0"
    "QQmlListProperty<QDeclarativeGeoRouteSegment>\0"
    "routeQuery\0QDeclarativeGeoRouteQuery*\0"
    "legs\0QList<QObject*>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoRoute[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       7,   28, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       3,    1,   25,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // methods: parameters
    QMetaType::Bool, 0x80000000 | 4,    5,

 // properties: name, type, flags
       6, 0x80000000 | 7, 0x00095409,
       8, QMetaType::Int, 0x00095401,
       9, QMetaType::QReal, 0x00095401,
      10, 0x80000000 | 11, 0x0049510b,
      12, 0x80000000 | 13, 0x00095409,
      14, 0x80000000 | 15, 0x00895009,
      16, 0x80000000 | 17, 0x00895409,

 // properties: notify_signal_id
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
      11,
      12,

       0        // eod
};

void QDeclarativeGeoRoute::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoRoute *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pathChanged(); break;
        case 1: { bool _r = _t->equals((*reinterpret_cast< QDeclarativeGeoRoute*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoRoute* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoRoute::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRoute::pathChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoRectangle >(); break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoRoute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QGeoRectangle*>(_v) = _t->bounds(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->travelTime(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->distance(); break;
        case 3: *reinterpret_cast< QJSValue*>(_v) = _t->path(); break;
        case 4: *reinterpret_cast< QQmlListProperty<QDeclarativeGeoRouteSegment>*>(_v) = _t->segments(); break;
        case 5: *reinterpret_cast< QDeclarativeGeoRouteQuery**>(_v) = _t->routeQuery(); break;
        case 6: *reinterpret_cast< QList<QObject*>*>(_v) = _t->legs(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoRoute *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 3: _t->setPath(*reinterpret_cast< QJSValue*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoRoute::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoRoute.data,
    qt_meta_data_QDeclarativeGeoRoute,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoRoute::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoRoute::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoRoute.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeGeoRoute::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoRoute::pathChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_QDeclarativeGeoRouteLeg_t {
    QByteArrayData data[3];
    char stringdata0[46];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoRouteLeg_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoRouteLeg_t qt_meta_stringdata_QDeclarativeGeoRouteLeg = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QDeclarativeGeoRouteLeg"
QT_MOC_LITERAL(1, 24, 8), // "legIndex"
QT_MOC_LITERAL(2, 33, 12) // "overallRoute"

    },
    "QDeclarativeGeoRouteLeg\0legIndex\0"
    "overallRoute"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoRouteLeg[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       2,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Int, 0x00095401,
       2, QMetaType::QObjectStar, 0x00095401,

       0        // eod
};

void QDeclarativeGeoRouteLeg::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoRouteLeg *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->legIndex(); break;
        case 1: *reinterpret_cast< QObject**>(_v) = _t->overallRoute(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoRouteLeg::staticMetaObject = { {
    &QDeclarativeGeoRoute::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoRouteLeg.data,
    qt_meta_data_QDeclarativeGeoRouteLeg,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoRouteLeg::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoRouteLeg::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoRouteLeg.stringdata0))
        return static_cast<void*>(this);
    return QDeclarativeGeoRoute::qt_metacast(_clname);
}

int QDeclarativeGeoRouteLeg::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDeclarativeGeoRoute::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
