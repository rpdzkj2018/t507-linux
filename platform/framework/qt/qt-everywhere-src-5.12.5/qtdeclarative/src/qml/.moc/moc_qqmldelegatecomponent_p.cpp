/****************************************************************************
** Meta object code from reading C++ file 'qqmldelegatecomponent_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../types/qqmldelegatecomponent_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmldelegatecomponent_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQmlAbstractDelegateComponent_t {
    QByteArrayData data[3];
    char stringdata0[47];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlAbstractDelegateComponent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlAbstractDelegateComponent_t qt_meta_stringdata_QQmlAbstractDelegateComponent = {
    {
QT_MOC_LITERAL(0, 0, 29), // "QQmlAbstractDelegateComponent"
QT_MOC_LITERAL(1, 30, 15), // "delegateChanged"
QT_MOC_LITERAL(2, 46, 0) // ""

    },
    "QQmlAbstractDelegateComponent\0"
    "delegateChanged\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlAbstractDelegateComponent[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

       0        // eod
};

void QQmlAbstractDelegateComponent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlAbstractDelegateComponent *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->delegateChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlAbstractDelegateComponent::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlAbstractDelegateComponent::delegateChanged)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQmlAbstractDelegateComponent::staticMetaObject = { {
    &QQmlComponent::staticMetaObject,
    qt_meta_stringdata_QQmlAbstractDelegateComponent.data,
    qt_meta_data_QQmlAbstractDelegateComponent,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlAbstractDelegateComponent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlAbstractDelegateComponent::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlAbstractDelegateComponent.stringdata0))
        return static_cast<void*>(this);
    return QQmlComponent::qt_metacast(_clname);
}

int QQmlAbstractDelegateComponent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QQmlAbstractDelegateComponent::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_QQmlDelegateChoice_t {
    QByteArrayData data[15];
    char stringdata0[166];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlDelegateChoice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlDelegateChoice_t qt_meta_stringdata_QQmlDelegateChoice = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QQmlDelegateChoice"
QT_MOC_LITERAL(1, 19, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 35, 8), // "delegate"
QT_MOC_LITERAL(3, 44, 16), // "roleValueChanged"
QT_MOC_LITERAL(4, 61, 0), // ""
QT_MOC_LITERAL(5, 62, 10), // "rowChanged"
QT_MOC_LITERAL(6, 73, 12), // "indexChanged"
QT_MOC_LITERAL(7, 86, 13), // "columnChanged"
QT_MOC_LITERAL(8, 100, 15), // "delegateChanged"
QT_MOC_LITERAL(9, 116, 7), // "changed"
QT_MOC_LITERAL(10, 124, 9), // "roleValue"
QT_MOC_LITERAL(11, 134, 3), // "row"
QT_MOC_LITERAL(12, 138, 5), // "index"
QT_MOC_LITERAL(13, 144, 6), // "column"
QT_MOC_LITERAL(14, 151, 14) // "QQmlComponent*"

    },
    "QQmlDelegateChoice\0DefaultProperty\0"
    "delegate\0roleValueChanged\0\0rowChanged\0"
    "indexChanged\0columnChanged\0delegateChanged\0"
    "changed\0roleValue\0row\0index\0column\0"
    "QQmlComponent*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlDelegateChoice[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       6,   16, // methods
       5,   52, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   46,    4, 0x06 /* Public */,
       5,    0,   47,    4, 0x06 /* Public */,
       6,    0,   48,    4, 0x06 /* Public */,
       7,    0,   49,    4, 0x06 /* Public */,
       8,    0,   50,    4, 0x06 /* Public */,
       9,    0,   51,    4, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      10, QMetaType::QVariant, 0x00495103,
      11, QMetaType::Int, 0x00495103,
      12, QMetaType::Int, 0x00495003,
      13, QMetaType::Int, 0x00495103,
       2, 0x80000000 | 14, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

       0        // eod
};

void QQmlDelegateChoice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlDelegateChoice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->roleValueChanged(); break;
        case 1: _t->rowChanged(); break;
        case 2: _t->indexChanged(); break;
        case 3: _t->columnChanged(); break;
        case 4: _t->delegateChanged(); break;
        case 5: _t->changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::roleValueChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::rowChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::indexChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::columnChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::delegateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateChoice::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChoice::changed)) {
                *result = 5;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQmlDelegateChoice *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->roleValue(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->row(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->row(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->column(); break;
        case 4: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQmlDelegateChoice *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRoleValue(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setRow(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setRow(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setColumn(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQmlDelegateChoice::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQmlDelegateChoice.data,
    qt_meta_data_QQmlDelegateChoice,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlDelegateChoice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlDelegateChoice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlDelegateChoice.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQmlDelegateChoice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQmlDelegateChoice::roleValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQmlDelegateChoice::rowChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQmlDelegateChoice::indexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQmlDelegateChoice::columnChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQmlDelegateChoice::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQmlDelegateChoice::changed()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
struct qt_meta_stringdata_QQmlDelegateChooser_t {
    QByteArrayData data[7];
    char stringdata0[99];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlDelegateChooser_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlDelegateChooser_t qt_meta_stringdata_QQmlDelegateChooser = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QQmlDelegateChooser"
QT_MOC_LITERAL(1, 20, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 36, 7), // "choices"
QT_MOC_LITERAL(3, 44, 11), // "roleChanged"
QT_MOC_LITERAL(4, 56, 0), // ""
QT_MOC_LITERAL(5, 57, 4), // "role"
QT_MOC_LITERAL(6, 62, 36) // "QQmlListProperty<QQmlDelegate..."

    },
    "QQmlDelegateChooser\0DefaultProperty\0"
    "choices\0roleChanged\0\0role\0"
    "QQmlListProperty<QQmlDelegateChoice>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlDelegateChooser[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       1,   16, // methods
       2,   22, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   21,    4, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::QString, 0x00495103,
       2, 0x80000000 | 6, 0x00095409,

 // properties: notify_signal_id
       0,
       0,

       0        // eod
};

void QQmlDelegateChooser::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlDelegateChooser *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->roleChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlDelegateChooser::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateChooser::roleChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QQmlDelegateChoice> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQmlDelegateChooser *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->role(); break;
        case 1: *reinterpret_cast< QQmlListProperty<QQmlDelegateChoice>*>(_v) = _t->choices(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQmlDelegateChooser *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRole(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQmlDelegateChooser::staticMetaObject = { {
    &QQmlAbstractDelegateComponent::staticMetaObject,
    qt_meta_stringdata_QQmlDelegateChooser.data,
    qt_meta_data_QQmlDelegateChooser,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlDelegateChooser::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlDelegateChooser::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlDelegateChooser.stringdata0))
        return static_cast<void*>(this);
    return QQmlAbstractDelegateComponent::qt_metacast(_clname);
}

int QQmlDelegateChooser::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlAbstractDelegateComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQmlDelegateChooser::roleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
