/****************************************************************************
** Meta object code from reading C++ file 'qquickmenubar_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickmenubar_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmenubar_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickMenuBar_t {
    QByteArrayData data[22];
    char stringdata0[278];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuBar_t qt_meta_stringdata_QQuickMenuBar = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickMenuBar"
QT_MOC_LITERAL(1, 14, 15), // "delegateChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 12), // "menusChanged"
QT_MOC_LITERAL(4, 44, 6), // "menuAt"
QT_MOC_LITERAL(5, 51, 11), // "QQuickMenu*"
QT_MOC_LITERAL(6, 63, 5), // "index"
QT_MOC_LITERAL(7, 69, 7), // "addMenu"
QT_MOC_LITERAL(8, 77, 4), // "menu"
QT_MOC_LITERAL(9, 82, 10), // "insertMenu"
QT_MOC_LITERAL(10, 93, 10), // "removeMenu"
QT_MOC_LITERAL(11, 104, 8), // "takeMenu"
QT_MOC_LITERAL(12, 113, 19), // "contentWidthChanged"
QT_MOC_LITERAL(13, 133, 20), // "contentHeightChanged"
QT_MOC_LITERAL(14, 154, 8), // "delegate"
QT_MOC_LITERAL(15, 163, 14), // "QQmlComponent*"
QT_MOC_LITERAL(16, 178, 12), // "contentWidth"
QT_MOC_LITERAL(17, 191, 13), // "contentHeight"
QT_MOC_LITERAL(18, 205, 5), // "menus"
QT_MOC_LITERAL(19, 211, 28), // "QQmlListProperty<QQuickMenu>"
QT_MOC_LITERAL(20, 240, 11), // "contentData"
QT_MOC_LITERAL(21, 252, 25) // "QQmlListProperty<QObject>"

    },
    "QQuickMenuBar\0delegateChanged\0\0"
    "menusChanged\0menuAt\0QQuickMenu*\0index\0"
    "addMenu\0menu\0insertMenu\0removeMenu\0"
    "takeMenu\0contentWidthChanged\0"
    "contentHeightChanged\0delegate\0"
    "QQmlComponent*\0contentWidth\0contentHeight\0"
    "menus\0QQmlListProperty<QQuickMenu>\0"
    "contentData\0QQmlListProperty<QObject>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuBar[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       5,   68, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       4,    1,   51,    2, 0x02 /* Public */,
       7,    1,   54,    2, 0x02 /* Public */,
       9,    2,   57,    2, 0x02 /* Public */,
      10,    1,   62,    2, 0x02 /* Public */,
      11,    1,   65,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 5, QMetaType::Int,    6,
    QMetaType::Void, 0x80000000 | 5,    8,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 5,    6,    8,
    QMetaType::Void, 0x80000000 | 5,    8,
    0x80000000 | 5, QMetaType::Int,    6,

 // properties: name, type, flags
      14, 0x80000000 | 15, 0x0049590b,
      16, QMetaType::QReal, 0x00495907,
      17, QMetaType::QReal, 0x00495907,
      18, 0x80000000 | 19, 0x00495809,
      20, 0x80000000 | 21, 0x00095809,

 // properties: notify_signal_id
       0,
    1879048204,
    1879048205,
       1,
       0,

       0        // eod
};

void QQuickMenuBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickMenuBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->delegateChanged(); break;
        case 1: _t->menusChanged(); break;
        case 2: { QQuickMenu* _r = _t->menuAt((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickMenu**>(_a[0]) = std::move(_r); }  break;
        case 3: _t->addMenu((*reinterpret_cast< QQuickMenu*(*)>(_a[1]))); break;
        case 4: _t->insertMenu((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QQuickMenu*(*)>(_a[2]))); break;
        case 5: _t->removeMenu((*reinterpret_cast< QQuickMenu*(*)>(_a[1]))); break;
        case 6: { QQuickMenu* _r = _t->takeMenu((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickMenu**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickMenuBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBar::delegateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickMenuBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBar::menusChanged)) {
                *result = 1;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickMenuBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 3: *reinterpret_cast< QQmlListProperty<QQuickMenu>*>(_v) = _t->QQuickMenuBar::d_func()->menus(); break;
        case 4: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->QQuickMenuBar::d_func()->contentData(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickMenuBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 1: _t->setContentWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setContentHeight(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickMenuBar *_t = static_cast<QQuickMenuBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 1: _t->resetContentWidth(); break;
        case 2: _t->resetContentHeight(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickMenuBar::staticMetaObject = { {
    &QQuickContainer::staticMetaObject,
    qt_meta_stringdata_QQuickMenuBar.data,
    qt_meta_data_QQuickMenuBar,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickMenuBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuBar.stringdata0))
        return static_cast<void*>(this);
    return QQuickContainer::qt_metacast(_clname);
}

int QQuickMenuBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickContainer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuBar::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickMenuBar::menusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
// If you get a compile error in this function it can be because either
//     a) You are using a NOTIFY signal that does not exist. Fix it.
//     b) You are using a NOTIFY signal that does exist (in a parent class) but has a non-empty parameter list. This is a moc limitation.
Q_DECL_UNUSED static void checkNotifySignalValidity_QQuickMenuBar(QQuickMenuBar *t) {
    t->contentWidthChanged();
    t->contentHeightChanged();
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
