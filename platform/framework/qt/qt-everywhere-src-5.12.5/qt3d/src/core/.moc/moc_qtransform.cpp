/****************************************************************************
** Meta object code from reading C++ file 'qtransform.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../transforms/qtransform.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtransform.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DCore__QTransform_t {
    QByteArrayData data[51];
    char stringdata0[513];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QTransform_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QTransform_t qt_meta_stringdata_Qt3DCore__QTransform = {
    {
QT_MOC_LITERAL(0, 0, 20), // "Qt3DCore::QTransform"
QT_MOC_LITERAL(1, 21, 12), // "scaleChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 5), // "scale"
QT_MOC_LITERAL(4, 41, 14), // "scale3DChanged"
QT_MOC_LITERAL(5, 56, 15), // "rotationChanged"
QT_MOC_LITERAL(6, 72, 8), // "rotation"
QT_MOC_LITERAL(7, 81, 18), // "translationChanged"
QT_MOC_LITERAL(8, 100, 11), // "translation"
QT_MOC_LITERAL(9, 112, 13), // "matrixChanged"
QT_MOC_LITERAL(10, 126, 16), // "rotationXChanged"
QT_MOC_LITERAL(11, 143, 9), // "rotationX"
QT_MOC_LITERAL(12, 153, 16), // "rotationYChanged"
QT_MOC_LITERAL(13, 170, 9), // "rotationY"
QT_MOC_LITERAL(14, 180, 16), // "rotationZChanged"
QT_MOC_LITERAL(15, 197, 9), // "rotationZ"
QT_MOC_LITERAL(16, 207, 8), // "setScale"
QT_MOC_LITERAL(17, 216, 10), // "setScale3D"
QT_MOC_LITERAL(18, 227, 11), // "setRotation"
QT_MOC_LITERAL(19, 239, 14), // "setTranslation"
QT_MOC_LITERAL(20, 254, 9), // "setMatrix"
QT_MOC_LITERAL(21, 264, 6), // "matrix"
QT_MOC_LITERAL(22, 271, 12), // "setRotationX"
QT_MOC_LITERAL(23, 284, 12), // "setRotationY"
QT_MOC_LITERAL(24, 297, 12), // "setRotationZ"
QT_MOC_LITERAL(25, 310, 16), // "fromAxisAndAngle"
QT_MOC_LITERAL(26, 327, 4), // "axis"
QT_MOC_LITERAL(27, 332, 5), // "angle"
QT_MOC_LITERAL(28, 338, 1), // "x"
QT_MOC_LITERAL(29, 340, 1), // "y"
QT_MOC_LITERAL(30, 342, 1), // "z"
QT_MOC_LITERAL(31, 344, 17), // "fromAxesAndAngles"
QT_MOC_LITERAL(32, 362, 5), // "axis1"
QT_MOC_LITERAL(33, 368, 6), // "angle1"
QT_MOC_LITERAL(34, 375, 5), // "axis2"
QT_MOC_LITERAL(35, 381, 6), // "angle2"
QT_MOC_LITERAL(36, 388, 5), // "axis3"
QT_MOC_LITERAL(37, 394, 6), // "angle3"
QT_MOC_LITERAL(38, 401, 8), // "fromAxes"
QT_MOC_LITERAL(39, 410, 5), // "xAxis"
QT_MOC_LITERAL(40, 416, 5), // "yAxis"
QT_MOC_LITERAL(41, 422, 5), // "zAxis"
QT_MOC_LITERAL(42, 428, 15), // "fromEulerAngles"
QT_MOC_LITERAL(43, 444, 11), // "eulerAngles"
QT_MOC_LITERAL(44, 456, 5), // "pitch"
QT_MOC_LITERAL(45, 462, 3), // "yaw"
QT_MOC_LITERAL(46, 466, 4), // "roll"
QT_MOC_LITERAL(47, 471, 12), // "rotateAround"
QT_MOC_LITERAL(48, 484, 5), // "point"
QT_MOC_LITERAL(49, 490, 14), // "rotateFromAxes"
QT_MOC_LITERAL(50, 505, 7) // "scale3D"

    },
    "Qt3DCore::QTransform\0scaleChanged\0\0"
    "scale\0scale3DChanged\0rotationChanged\0"
    "rotation\0translationChanged\0translation\0"
    "matrixChanged\0rotationXChanged\0rotationX\0"
    "rotationYChanged\0rotationY\0rotationZChanged\0"
    "rotationZ\0setScale\0setScale3D\0setRotation\0"
    "setTranslation\0setMatrix\0matrix\0"
    "setRotationX\0setRotationY\0setRotationZ\0"
    "fromAxisAndAngle\0axis\0angle\0x\0y\0z\0"
    "fromAxesAndAngles\0axis1\0angle1\0axis2\0"
    "angle2\0axis3\0angle3\0fromAxes\0xAxis\0"
    "yAxis\0zAxis\0fromEulerAngles\0eulerAngles\0"
    "pitch\0yaw\0roll\0rotateAround\0point\0"
    "rotateFromAxes\0scale3D"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QTransform[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       8,  252, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  139,    2, 0x06 /* Public */,
       4,    1,  142,    2, 0x06 /* Public */,
       5,    1,  145,    2, 0x06 /* Public */,
       7,    1,  148,    2, 0x06 /* Public */,
       9,    0,  151,    2, 0x06 /* Public */,
      10,    1,  152,    2, 0x06 /* Public */,
      12,    1,  155,    2, 0x06 /* Public */,
      14,    1,  158,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    1,  161,    2, 0x0a /* Public */,
      17,    1,  164,    2, 0x0a /* Public */,
      18,    1,  167,    2, 0x0a /* Public */,
      19,    1,  170,    2, 0x0a /* Public */,
      20,    1,  173,    2, 0x0a /* Public */,
      22,    1,  176,    2, 0x0a /* Public */,
      23,    1,  179,    2, 0x0a /* Public */,
      24,    1,  182,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      25,    2,  185,    2, 0x02 /* Public */,
      25,    4,  190,    2, 0x02 /* Public */,
      31,    4,  199,    2, 0x02 /* Public */,
      31,    6,  208,    2, 0x02 /* Public */,
      38,    3,  221,    2, 0x02 /* Public */,
      42,    1,  228,    2, 0x02 /* Public */,
      42,    3,  231,    2, 0x02 /* Public */,
      47,    3,  238,    2, 0x02 /* Public */,
      49,    3,  245,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QQuaternion,    6,
    QMetaType::Void, QMetaType::QVector3D,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Float,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Float,   15,

 // slots: parameters
    QMetaType::Void, QMetaType::Float,    3,
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QQuaternion,    6,
    QMetaType::Void, QMetaType::QVector3D,    8,
    QMetaType::Void, QMetaType::QMatrix4x4,   21,
    QMetaType::Void, QMetaType::Float,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Float,   15,

 // methods: parameters
    QMetaType::QQuaternion, QMetaType::QVector3D, QMetaType::Float,   26,   27,
    QMetaType::QQuaternion, QMetaType::Float, QMetaType::Float, QMetaType::Float, QMetaType::Float,   28,   29,   30,   27,
    QMetaType::QQuaternion, QMetaType::QVector3D, QMetaType::Float, QMetaType::QVector3D, QMetaType::Float,   32,   33,   34,   35,
    QMetaType::QQuaternion, QMetaType::QVector3D, QMetaType::Float, QMetaType::QVector3D, QMetaType::Float, QMetaType::QVector3D, QMetaType::Float,   32,   33,   34,   35,   36,   37,
    QMetaType::QQuaternion, QMetaType::QVector3D, QMetaType::QVector3D, QMetaType::QVector3D,   39,   40,   41,
    QMetaType::QQuaternion, QMetaType::QVector3D,   43,
    QMetaType::QQuaternion, QMetaType::Float, QMetaType::Float, QMetaType::Float,   44,   45,   46,
    QMetaType::QMatrix4x4, QMetaType::QVector3D, QMetaType::Float, QMetaType::QVector3D,   48,   27,   26,
    QMetaType::QMatrix4x4, QMetaType::QVector3D, QMetaType::QVector3D, QMetaType::QVector3D,   39,   40,   41,

 // properties: name, type, flags
      21, QMetaType::QMatrix4x4, 0x00495103,
       3, QMetaType::Float, 0x00495103,
      50, QMetaType::QVector3D, 0x00495103,
       6, QMetaType::QQuaternion, 0x00495103,
       8, QMetaType::QVector3D, 0x00495103,
      11, QMetaType::Float, 0x00495103,
      13, QMetaType::Float, 0x00495103,
      15, QMetaType::Float, 0x00495103,

 // properties: notify_signal_id
       4,
       0,
       1,
       2,
       3,
       5,
       6,
       7,

       0        // eod
};

void Qt3DCore::QTransform::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QTransform *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->scaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 1: _t->scale3DChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 2: _t->rotationChanged((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        case 3: _t->translationChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 4: _t->matrixChanged(); break;
        case 5: _t->rotationXChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->rotationYChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->rotationZChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: _t->setScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->setScale3D((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 10: _t->setRotation((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        case 11: _t->setTranslation((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 12: _t->setMatrix((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 13: _t->setRotationX((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->setRotationY((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: _t->setRotationZ((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 16: { QQuaternion _r = _t->fromAxisAndAngle((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 17: { QQuaternion _r = _t->fromAxisAndAngle((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 18: { QQuaternion _r = _t->fromAxesAndAngles((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< const QVector3D(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 19: { QQuaternion _r = _t->fromAxesAndAngles((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< const QVector3D(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4])),(*reinterpret_cast< const QVector3D(*)>(_a[5])),(*reinterpret_cast< float(*)>(_a[6])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 20: { QQuaternion _r = _t->fromAxes((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< const QVector3D(*)>(_a[2])),(*reinterpret_cast< const QVector3D(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 21: { QQuaternion _r = _t->fromEulerAngles((*reinterpret_cast< const QVector3D(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 22: { QQuaternion _r = _t->fromEulerAngles((*reinterpret_cast< float(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QQuaternion*>(_a[0]) = std::move(_r); }  break;
        case 23: { QMatrix4x4 _r = _t->rotateAround((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< const QVector3D(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QMatrix4x4*>(_a[0]) = std::move(_r); }  break;
        case 24: { QMatrix4x4 _r = _t->rotateFromAxes((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< const QVector3D(*)>(_a[2])),(*reinterpret_cast< const QVector3D(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QMatrix4x4*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QTransform::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::scaleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::scale3DChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(const QQuaternion & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::rotationChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::translationChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QTransform::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::matrixChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::rotationXChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::rotationYChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QTransform::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QTransform::rotationZChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QTransform *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QMatrix4x4*>(_v) = _t->matrix(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->scale(); break;
        case 2: *reinterpret_cast< QVector3D*>(_v) = _t->scale3D(); break;
        case 3: *reinterpret_cast< QQuaternion*>(_v) = _t->rotation(); break;
        case 4: *reinterpret_cast< QVector3D*>(_v) = _t->translation(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->rotationX(); break;
        case 6: *reinterpret_cast< float*>(_v) = _t->rotationY(); break;
        case 7: *reinterpret_cast< float*>(_v) = _t->rotationZ(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QTransform *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setMatrix(*reinterpret_cast< QMatrix4x4*>(_v)); break;
        case 1: _t->setScale(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setScale3D(*reinterpret_cast< QVector3D*>(_v)); break;
        case 3: _t->setRotation(*reinterpret_cast< QQuaternion*>(_v)); break;
        case 4: _t->setTranslation(*reinterpret_cast< QVector3D*>(_v)); break;
        case 5: _t->setRotationX(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setRotationY(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setRotationZ(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DCore::QTransform::staticMetaObject = { {
    &QComponent::staticMetaObject,
    qt_meta_stringdata_Qt3DCore__QTransform.data,
    qt_meta_data_Qt3DCore__QTransform,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DCore::QTransform::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QTransform::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QTransform.stringdata0))
        return static_cast<void*>(this);
    return QComponent::qt_metacast(_clname);
}

int Qt3DCore::QTransform::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::QTransform::scaleChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QTransform::scale3DChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QTransform::rotationChanged(const QQuaternion & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DCore::QTransform::translationChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DCore::QTransform::matrixChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Qt3DCore::QTransform::rotationXChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DCore::QTransform::rotationYChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DCore::QTransform::rotationZChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
