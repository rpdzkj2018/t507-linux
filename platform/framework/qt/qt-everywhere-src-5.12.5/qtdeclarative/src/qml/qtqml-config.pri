QT.qml.enabled_features = qml-network
QT.qml.disabled_features = qml-debug
QT.qml.QT_CONFIG = 
QT.qml.exports = 
QT.qml_private.enabled_features = qml-animation qml-delegate-model qml-devtools qml-list-model qml-locale qml-sequence-object qml-worker-script qml-xml-http-request
QT.qml_private.disabled_features = cxx14_make_unique qml-preview qml-profiler
QT.qml_private.libraries = 
