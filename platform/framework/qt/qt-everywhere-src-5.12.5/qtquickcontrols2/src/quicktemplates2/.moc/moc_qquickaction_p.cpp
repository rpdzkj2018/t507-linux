/****************************************************************************
** Meta object code from reading C++ file 'qquickaction_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickaction_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickaction_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickAction_t {
    QByteArrayData data[20];
    char stringdata0[197];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAction_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAction_t qt_meta_stringdata_QQuickAction = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickAction"
QT_MOC_LITERAL(1, 13, 11), // "textChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 4), // "text"
QT_MOC_LITERAL(4, 31, 11), // "iconChanged"
QT_MOC_LITERAL(5, 43, 10), // "QQuickIcon"
QT_MOC_LITERAL(6, 54, 4), // "icon"
QT_MOC_LITERAL(7, 59, 14), // "enabledChanged"
QT_MOC_LITERAL(8, 74, 7), // "enabled"
QT_MOC_LITERAL(9, 82, 14), // "checkedChanged"
QT_MOC_LITERAL(10, 97, 7), // "checked"
QT_MOC_LITERAL(11, 105, 16), // "checkableChanged"
QT_MOC_LITERAL(12, 122, 9), // "checkable"
QT_MOC_LITERAL(13, 132, 15), // "shortcutChanged"
QT_MOC_LITERAL(14, 148, 8), // "shortcut"
QT_MOC_LITERAL(15, 157, 7), // "toggled"
QT_MOC_LITERAL(16, 165, 6), // "source"
QT_MOC_LITERAL(17, 172, 9), // "triggered"
QT_MOC_LITERAL(18, 182, 6), // "toggle"
QT_MOC_LITERAL(19, 189, 7) // "trigger"

    },
    "QQuickAction\0textChanged\0\0text\0"
    "iconChanged\0QQuickIcon\0icon\0enabledChanged\0"
    "enabled\0checkedChanged\0checked\0"
    "checkableChanged\0checkable\0shortcutChanged\0"
    "shortcut\0toggled\0source\0triggered\0"
    "toggle\0trigger"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAction[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       6,  118, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       4,    1,   87,    2, 0x06 /* Public */,
       7,    1,   90,    2, 0x06 /* Public */,
       9,    1,   93,    2, 0x06 /* Public */,
      11,    1,   96,    2, 0x06 /* Public */,
      13,    1,   99,    2, 0x06 /* Public */,
      15,    1,  102,    2, 0x06 /* Public */,
      15,    0,  105,    2, 0x26 /* Public | MethodCloned */,
      17,    1,  106,    2, 0x06 /* Public */,
      17,    0,  109,    2, 0x26 /* Public | MethodCloned */,

 // slots: name, argc, parameters, tag, flags
      18,    1,  110,    2, 0x0a /* Public */,
      18,    0,  113,    2, 0x2a /* Public | MethodCloned */,
      19,    1,  114,    2, 0x0a /* Public */,
      19,    0,  117,    2, 0x2a /* Public | MethodCloned */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::QKeySequence,   14,
    QMetaType::Void, QMetaType::QObjectStar,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,   16,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QObjectStar,   16,
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495903,
       6, 0x80000000 | 5, 0x0049590b,
       8, QMetaType::Bool, 0x00495907,
      10, QMetaType::Bool, 0x00495903,
      12, QMetaType::Bool, 0x00495903,
      14, QMetaType::QVariant, 0x00495903,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

       0        // eod
};

void QQuickAction::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->iconChanged((*reinterpret_cast< const QQuickIcon(*)>(_a[1]))); break;
        case 2: _t->enabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->checkedChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->checkableChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->shortcutChanged((*reinterpret_cast< const QKeySequence(*)>(_a[1]))); break;
        case 6: _t->toggled((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 7: _t->toggled(); break;
        case 8: _t->triggered((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 9: _t->triggered(); break;
        case 10: _t->toggle((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 11: _t->toggle(); break;
        case 12: _t->trigger((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 13: _t->trigger(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickAction::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::textChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(const QQuickIcon & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::iconChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::enabledChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::checkedChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::checkableChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(const QKeySequence & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::shortcutChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(QObject * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::toggled)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickAction::*)(QObject * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAction::triggered)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< QQuickIcon*>(_v) = _t->icon(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isEnabled(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isChecked(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isCheckable(); break;
        case 5: *reinterpret_cast< QVariant*>(_v) = _t->QQuickAction::d_func()->shortcut(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setIcon(*reinterpret_cast< QQuickIcon*>(_v)); break;
        case 2: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setChecked(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setCheckable(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->QQuickAction::d_func()->setShortcut(*reinterpret_cast< QVariant*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickAction *_t = static_cast<QQuickAction *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 2: _t->resetEnabled(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickAction::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickAction.data,
    qt_meta_data_QQuickAction,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickAction::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAction::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAction.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickAction::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickAction::textChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickAction::iconChanged(const QQuickIcon & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QQuickAction::enabledChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickAction::checkedChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QQuickAction::checkableChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QQuickAction::shortcutChanged(const QKeySequence & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QQuickAction::toggled(QObject * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 8
void QQuickAction::triggered(QObject * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
