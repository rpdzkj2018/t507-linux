/****************************************************************************
** Meta object code from reading C++ file 'qremoteobjectabstractitemmodeladapter_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qremoteobjectabstractitemmodeladapter_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qremoteobjectabstractitemmodeladapter_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QAbstractItemModelSourceAdapter_t {
    QByteArrayData data[58];
    char stringdata0[788];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAbstractItemModelSourceAdapter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAbstractItemModelSourceAdapter_t qt_meta_stringdata_QAbstractItemModelSourceAdapter = {
    {
QT_MOC_LITERAL(0, 0, 31), // "QAbstractItemModelSourceAdapter"
QT_MOC_LITERAL(1, 32, 21), // "availableRolesChanged"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 11), // "dataChanged"
QT_MOC_LITERAL(4, 67, 9), // "IndexList"
QT_MOC_LITERAL(5, 77, 7), // "topLeft"
QT_MOC_LITERAL(6, 85, 11), // "bottomRight"
QT_MOC_LITERAL(7, 97, 12), // "QVector<int>"
QT_MOC_LITERAL(8, 110, 5), // "roles"
QT_MOC_LITERAL(9, 116, 12), // "rowsInserted"
QT_MOC_LITERAL(10, 129, 6), // "parent"
QT_MOC_LITERAL(11, 136, 5), // "start"
QT_MOC_LITERAL(12, 142, 3), // "end"
QT_MOC_LITERAL(13, 146, 11), // "rowsRemoved"
QT_MOC_LITERAL(14, 158, 9), // "rowsMoved"
QT_MOC_LITERAL(15, 168, 12), // "sourceParent"
QT_MOC_LITERAL(16, 181, 9), // "sourceRow"
QT_MOC_LITERAL(17, 191, 5), // "count"
QT_MOC_LITERAL(18, 197, 17), // "destinationParent"
QT_MOC_LITERAL(19, 215, 16), // "destinationChild"
QT_MOC_LITERAL(20, 232, 14), // "currentChanged"
QT_MOC_LITERAL(21, 247, 7), // "current"
QT_MOC_LITERAL(22, 255, 8), // "previous"
QT_MOC_LITERAL(23, 264, 15), // "columnsInserted"
QT_MOC_LITERAL(24, 280, 14), // "availableRoles"
QT_MOC_LITERAL(25, 295, 17), // "setAvailableRoles"
QT_MOC_LITERAL(26, 313, 9), // "roleNames"
QT_MOC_LITERAL(27, 323, 8), // "QIntHash"
QT_MOC_LITERAL(28, 332, 18), // "replicaSizeRequest"
QT_MOC_LITERAL(29, 351, 10), // "parentList"
QT_MOC_LITERAL(30, 362, 17), // "replicaRowRequest"
QT_MOC_LITERAL(31, 380, 11), // "DataEntries"
QT_MOC_LITERAL(32, 392, 20), // "replicaHeaderRequest"
QT_MOC_LITERAL(33, 413, 24), // "QVector<Qt::Orientation>"
QT_MOC_LITERAL(34, 438, 12), // "orientations"
QT_MOC_LITERAL(35, 451, 8), // "sections"
QT_MOC_LITERAL(36, 460, 22), // "replicaSetCurrentIndex"
QT_MOC_LITERAL(37, 483, 5), // "index"
QT_MOC_LITERAL(38, 489, 35), // "QItemSelectionModel::Selectio..."
QT_MOC_LITERAL(39, 525, 7), // "command"
QT_MOC_LITERAL(40, 533, 14), // "replicaSetData"
QT_MOC_LITERAL(41, 548, 5), // "value"
QT_MOC_LITERAL(42, 554, 4), // "role"
QT_MOC_LITERAL(43, 559, 19), // "replicaCacheRequest"
QT_MOC_LITERAL(44, 579, 18), // "MetaAndDataEntries"
QT_MOC_LITERAL(45, 598, 6), // "size_t"
QT_MOC_LITERAL(46, 605, 4), // "size"
QT_MOC_LITERAL(47, 610, 17), // "sourceDataChanged"
QT_MOC_LITERAL(48, 628, 11), // "QModelIndex"
QT_MOC_LITERAL(49, 640, 18), // "sourceRowsInserted"
QT_MOC_LITERAL(50, 659, 21), // "sourceColumnsInserted"
QT_MOC_LITERAL(51, 681, 17), // "sourceRowsRemoved"
QT_MOC_LITERAL(52, 699, 15), // "sourceRowsMoved"
QT_MOC_LITERAL(53, 715, 20), // "sourceCurrentChanged"
QT_MOC_LITERAL(54, 736, 19), // "QAbstractItemModel*"
QT_MOC_LITERAL(55, 756, 6), // "object"
QT_MOC_LITERAL(56, 763, 20), // "QItemSelectionModel*"
QT_MOC_LITERAL(57, 784, 3) // "sel"

    },
    "QAbstractItemModelSourceAdapter\0"
    "availableRolesChanged\0\0dataChanged\0"
    "IndexList\0topLeft\0bottomRight\0"
    "QVector<int>\0roles\0rowsInserted\0parent\0"
    "start\0end\0rowsRemoved\0rowsMoved\0"
    "sourceParent\0sourceRow\0count\0"
    "destinationParent\0destinationChild\0"
    "currentChanged\0current\0previous\0"
    "columnsInserted\0availableRoles\0"
    "setAvailableRoles\0roleNames\0QIntHash\0"
    "replicaSizeRequest\0parentList\0"
    "replicaRowRequest\0DataEntries\0"
    "replicaHeaderRequest\0QVector<Qt::Orientation>\0"
    "orientations\0sections\0replicaSetCurrentIndex\0"
    "index\0QItemSelectionModel::SelectionFlags\0"
    "command\0replicaSetData\0value\0role\0"
    "replicaCacheRequest\0MetaAndDataEntries\0"
    "size_t\0size\0sourceDataChanged\0QModelIndex\0"
    "sourceRowsInserted\0sourceColumnsInserted\0"
    "sourceRowsRemoved\0sourceRowsMoved\0"
    "sourceCurrentChanged\0QAbstractItemModel*\0"
    "object\0QItemSelectionModel*\0sel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAbstractItemModelSourceAdapter[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       2,  274, // properties
       0,    0, // enums/sets
       2,  282, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  129,    2, 0x06 /* Public */,
       3,    3,  130,    2, 0x06 /* Public */,
       9,    3,  137,    2, 0x06 /* Public */,
      13,    3,  144,    2, 0x06 /* Public */,
      14,    5,  151,    2, 0x06 /* Public */,
      20,    2,  162,    2, 0x06 /* Public */,
      23,    3,  167,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      24,    0,  174,    2, 0x0a /* Public */,
      25,    1,  175,    2, 0x0a /* Public */,
      26,    0,  178,    2, 0x0a /* Public */,
      28,    1,  179,    2, 0x0a /* Public */,
      30,    3,  182,    2, 0x0a /* Public */,
      32,    3,  189,    2, 0x0a /* Public */,
      36,    2,  196,    2, 0x0a /* Public */,
      40,    3,  201,    2, 0x0a /* Public */,
      43,    2,  208,    2, 0x0a /* Public */,
      47,    3,  213,    2, 0x0a /* Public */,
      47,    2,  220,    2, 0x2a /* Public | MethodCloned */,
      49,    3,  225,    2, 0x0a /* Public */,
      50,    3,  232,    2, 0x0a /* Public */,
      51,    3,  239,    2, 0x0a /* Public */,
      52,    5,  246,    2, 0x0a /* Public */,
      53,    2,  257,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 4, 0x80000000 | 7,    5,    6,    8,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int, QMetaType::Int,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int, QMetaType::Int,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int, QMetaType::Int, 0x80000000 | 4, QMetaType::Int,   15,   16,   17,   18,   19,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 4,   21,   22,
    QMetaType::Void, 0x80000000 | 4, QMetaType::Int, QMetaType::Int,   10,   11,   12,

 // slots: parameters
    0x80000000 | 7,
    QMetaType::Void, 0x80000000 | 7,   24,
    0x80000000 | 27,
    QMetaType::QSize, 0x80000000 | 4,   29,
    0x80000000 | 31, 0x80000000 | 4, 0x80000000 | 4, 0x80000000 | 7,   11,   12,    8,
    QMetaType::QVariantList, 0x80000000 | 33, 0x80000000 | 7, 0x80000000 | 7,   34,   35,    8,
    QMetaType::Void, 0x80000000 | 4, 0x80000000 | 38,   37,   39,
    QMetaType::Void, 0x80000000 | 4, QMetaType::QVariant, QMetaType::Int,   37,   41,   42,
    0x80000000 | 44, 0x80000000 | 45, 0x80000000 | 7,   46,    8,
    QMetaType::Void, 0x80000000 | 48, 0x80000000 | 48, 0x80000000 | 7,    5,    6,    8,
    QMetaType::Void, 0x80000000 | 48, 0x80000000 | 48,    5,    6,
    QMetaType::Void, 0x80000000 | 48, QMetaType::Int, QMetaType::Int,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 48, QMetaType::Int, QMetaType::Int,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 48, QMetaType::Int, QMetaType::Int,   10,   11,   12,
    QMetaType::Void, 0x80000000 | 48, QMetaType::Int, QMetaType::Int, 0x80000000 | 48, QMetaType::Int,   15,   16,   17,   18,   19,
    QMetaType::Void, 0x80000000 | 48, 0x80000000 | 48,   21,   22,

 // constructors: parameters
    0x80000000 | 2, 0x80000000 | 54, 0x80000000 | 56, 0x80000000 | 7,   55,   57,    8,
    0x80000000 | 2, 0x80000000 | 54, 0x80000000 | 56,   55,   57,

 // properties: name, type, flags
      24, 0x80000000 | 7, 0x0049510b,
      26, 0x80000000 | 27, 0x00095009,

 // properties: notify_signal_id
       0,
       0,

 // constructors: name, argc, parameters, tag, flags
       0,    3,  262,    2, 0x0e /* Public */,
       0,    2,  269,    2, 0x2e /* Public | MethodCloned */,

       0        // eod
};

void QAbstractItemModelSourceAdapter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::CreateInstance) {
        switch (_id) {
        case 0: { QAbstractItemModelSourceAdapter *_r = new QAbstractItemModelSourceAdapter((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1])),(*reinterpret_cast< QItemSelectionModel*(*)>(_a[2])),(*reinterpret_cast< const QVector<int>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        case 1: { QAbstractItemModelSourceAdapter *_r = new QAbstractItemModelSourceAdapter((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1])),(*reinterpret_cast< QItemSelectionModel*(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast<QObject**>(_a[0]) = _r; } break;
        default: break;
        }
    } else if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAbstractItemModelSourceAdapter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->availableRolesChanged(); break;
        case 1: _t->dataChanged((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3]))); break;
        case 2: _t->rowsInserted((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->rowsRemoved((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->rowsMoved((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< IndexList(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 5: _t->currentChanged((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2]))); break;
        case 6: _t->columnsInserted((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 7: { QVector<int> _r = _t->availableRoles();
            if (_a[0]) *reinterpret_cast< QVector<int>*>(_a[0]) = std::move(_r); }  break;
        case 8: _t->setAvailableRoles((*reinterpret_cast< QVector<int>(*)>(_a[1]))); break;
        case 9: { QIntHash _r = _t->roleNames();
            if (_a[0]) *reinterpret_cast< QIntHash*>(_a[0]) = std::move(_r); }  break;
        case 10: { QSize _r = _t->replicaSizeRequest((*reinterpret_cast< IndexList(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QSize*>(_a[0]) = std::move(_r); }  break;
        case 11: { DataEntries _r = _t->replicaRowRequest((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< DataEntries*>(_a[0]) = std::move(_r); }  break;
        case 12: { QVariantList _r = _t->replicaHeaderRequest((*reinterpret_cast< QVector<Qt::Orientation>(*)>(_a[1])),(*reinterpret_cast< QVector<int>(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = std::move(_r); }  break;
        case 13: _t->replicaSetCurrentIndex((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< QItemSelectionModel::SelectionFlags(*)>(_a[2]))); break;
        case 14: _t->replicaSetData((*reinterpret_cast< const IndexList(*)>(_a[1])),(*reinterpret_cast< const QVariant(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 15: { MetaAndDataEntries _r = _t->replicaCacheRequest((*reinterpret_cast< size_t(*)>(_a[1])),(*reinterpret_cast< const QVector<int>(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< MetaAndDataEntries*>(_a[0]) = std::move(_r); }  break;
        case 16: _t->sourceDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2])),(*reinterpret_cast< const QVector<int>(*)>(_a[3]))); break;
        case 17: _t->sourceDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 18: _t->sourceRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 19: _t->sourceColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 20: _t->sourceRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 21: _t->sourceRowsMoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QModelIndex(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 22: _t->sourceCurrentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Qt::Orientation> >(); break;
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelectionModel::SelectionFlags >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::availableRolesChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , IndexList , QVector<int> ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::dataChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , int , int ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::rowsInserted)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , int , int ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::rowsRemoved)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , int , int , IndexList , int ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::rowsMoved)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , IndexList );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::currentChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelSourceAdapter::*)(IndexList , int , int ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelSourceAdapter::columnsInserted)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QIntHash >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QAbstractItemModelSourceAdapter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVector<int>*>(_v) = _t->availableRoles(); break;
        case 1: *reinterpret_cast< QIntHash*>(_v) = _t->roleNames(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QAbstractItemModelSourceAdapter *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAvailableRoles(*reinterpret_cast< QVector<int>*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QAbstractItemModelSourceAdapter::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QAbstractItemModelSourceAdapter.data,
    qt_meta_data_QAbstractItemModelSourceAdapter,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QAbstractItemModelSourceAdapter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAbstractItemModelSourceAdapter::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QAbstractItemModelSourceAdapter.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QAbstractItemModelSourceAdapter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QAbstractItemModelSourceAdapter::availableRolesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QAbstractItemModelSourceAdapter::dataChanged(IndexList _t1, IndexList _t2, QVector<int> _t3)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(const_cast< QAbstractItemModelSourceAdapter *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QAbstractItemModelSourceAdapter::rowsInserted(IndexList _t1, int _t2, int _t3)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(const_cast< QAbstractItemModelSourceAdapter *>(this), &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QAbstractItemModelSourceAdapter::rowsRemoved(IndexList _t1, int _t2, int _t3)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(const_cast< QAbstractItemModelSourceAdapter *>(this), &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QAbstractItemModelSourceAdapter::rowsMoved(IndexList _t1, int _t2, int _t3, IndexList _t4, int _t5)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(const_cast< QAbstractItemModelSourceAdapter *>(this), &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QAbstractItemModelSourceAdapter::currentChanged(IndexList _t1, IndexList _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QAbstractItemModelSourceAdapter::columnsInserted(IndexList _t1, int _t2, int _t3)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(const_cast< QAbstractItemModelSourceAdapter *>(this), &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
