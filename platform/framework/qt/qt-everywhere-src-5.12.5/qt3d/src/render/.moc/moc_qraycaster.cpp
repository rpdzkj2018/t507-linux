/****************************************************************************
** Meta object code from reading C++ file 'qraycaster.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../picking/qraycaster.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qraycaster.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QRayCaster_t {
    QByteArrayData data[12];
    char stringdata0[134];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QRayCaster_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QRayCaster_t qt_meta_stringdata_Qt3DRender__QRayCaster = {
    {
QT_MOC_LITERAL(0, 0, 22), // "Qt3DRender::QRayCaster"
QT_MOC_LITERAL(1, 23, 13), // "originChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 6), // "origin"
QT_MOC_LITERAL(4, 45, 16), // "directionChanged"
QT_MOC_LITERAL(5, 62, 9), // "direction"
QT_MOC_LITERAL(6, 72, 13), // "lengthChanged"
QT_MOC_LITERAL(7, 86, 6), // "length"
QT_MOC_LITERAL(8, 93, 9), // "setOrigin"
QT_MOC_LITERAL(9, 103, 12), // "setDirection"
QT_MOC_LITERAL(10, 116, 9), // "setLength"
QT_MOC_LITERAL(11, 126, 7) // "trigger"

    },
    "Qt3DRender::QRayCaster\0originChanged\0"
    "\0origin\0directionChanged\0direction\0"
    "lengthChanged\0length\0setOrigin\0"
    "setDirection\0setLength\0trigger"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QRayCaster[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       3,   80, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   63,    2, 0x0a /* Public */,
       9,    1,   66,    2, 0x0a /* Public */,
      10,    1,   69,    2, 0x0a /* Public */,
      11,    0,   72,    2, 0x0a /* Public */,
      11,    3,   73,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QVector3D,    5,
    QMetaType::Void, QMetaType::Float,    7,

 // slots: parameters
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QVector3D,    5,
    QMetaType::Void, QMetaType::Float,    7,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QVector3D, QMetaType::QVector3D, QMetaType::Float,    3,    5,    7,

 // properties: name, type, flags
       3, QMetaType::QVector3D, 0x00495103,
       5, QMetaType::QVector3D, 0x00495103,
       7, QMetaType::Float, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void Qt3DRender::QRayCaster::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QRayCaster *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->originChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 1: _t->directionChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 2: _t->lengthChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->setOrigin((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 4: _t->setDirection((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 5: _t->setLength((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->trigger(); break;
        case 7: _t->trigger((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< const QVector3D(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QRayCaster::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRayCaster::originChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QRayCaster::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRayCaster::directionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QRayCaster::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRayCaster::lengthChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QRayCaster *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVector3D*>(_v) = _t->origin(); break;
        case 1: *reinterpret_cast< QVector3D*>(_v) = _t->direction(); break;
        case 2: *reinterpret_cast< float*>(_v) = _t->length(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QRayCaster *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setOrigin(*reinterpret_cast< QVector3D*>(_v)); break;
        case 1: _t->setDirection(*reinterpret_cast< QVector3D*>(_v)); break;
        case 2: _t->setLength(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QRayCaster::staticMetaObject = { {
    &QAbstractRayCaster::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QRayCaster.data,
    qt_meta_data_Qt3DRender__QRayCaster,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QRayCaster::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QRayCaster::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QRayCaster.stringdata0))
        return static_cast<void*>(this);
    return QAbstractRayCaster::qt_metacast(_clname);
}

int Qt3DRender::QRayCaster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractRayCaster::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QRayCaster::originChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QRayCaster::directionChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QRayCaster::lengthChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
