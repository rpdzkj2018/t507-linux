/****************************************************************************
** Meta object code from reading C++ file 'qremoteobjectnode.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qremoteobjectnode.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qremoteobjectnode.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QRemoteObjectAbstractPersistedStore_t {
    QByteArrayData data[1];
    char stringdata0[36];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectAbstractPersistedStore_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectAbstractPersistedStore_t qt_meta_stringdata_QRemoteObjectAbstractPersistedStore = {
    {
QT_MOC_LITERAL(0, 0, 35) // "QRemoteObjectAbstractPersiste..."

    },
    "QRemoteObjectAbstractPersistedStore"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectAbstractPersistedStore[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QRemoteObjectAbstractPersistedStore::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectAbstractPersistedStore::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QRemoteObjectAbstractPersistedStore.data,
    qt_meta_data_QRemoteObjectAbstractPersistedStore,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QRemoteObjectAbstractPersistedStore::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRemoteObjectAbstractPersistedStore::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRemoteObjectAbstractPersistedStore.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QRemoteObjectAbstractPersistedStore::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_QRemoteObjectNode_t {
    QByteArrayData data[28];
    char stringdata0[494];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectNode_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectNode_t qt_meta_stringdata_QRemoteObjectNode = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QRemoteObjectNode"
QT_MOC_LITERAL(1, 18, 17), // "remoteObjectAdded"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 27), // "QRemoteObjectSourceLocation"
QT_MOC_LITERAL(4, 65, 19), // "remoteObjectRemoved"
QT_MOC_LITERAL(5, 85, 5), // "error"
QT_MOC_LITERAL(6, 91, 28), // "QRemoteObjectNode::ErrorCode"
QT_MOC_LITERAL(7, 120, 9), // "errorCode"
QT_MOC_LITERAL(8, 130, 24), // "heartbeatIntervalChanged"
QT_MOC_LITERAL(9, 155, 17), // "heartbeatInterval"
QT_MOC_LITERAL(10, 173, 13), // "connectToNode"
QT_MOC_LITERAL(11, 187, 7), // "address"
QT_MOC_LITERAL(12, 195, 11), // "registryUrl"
QT_MOC_LITERAL(13, 207, 14), // "persistedStore"
QT_MOC_LITERAL(14, 222, 36), // "QRemoteObjectAbstractPersiste..."
QT_MOC_LITERAL(15, 259, 9), // "ErrorCode"
QT_MOC_LITERAL(16, 269, 7), // "NoError"
QT_MOC_LITERAL(17, 277, 19), // "RegistryNotAcquired"
QT_MOC_LITERAL(18, 297, 21), // "RegistryAlreadyHosted"
QT_MOC_LITERAL(19, 319, 14), // "NodeIsNoServer"
QT_MOC_LITERAL(20, 334, 20), // "ServerAlreadyCreated"
QT_MOC_LITERAL(21, 355, 25), // "UnintendedRegistryHosting"
QT_MOC_LITERAL(22, 381, 29), // "OperationNotValidOnClientNode"
QT_MOC_LITERAL(23, 411, 19), // "SourceNotRegistered"
QT_MOC_LITERAL(24, 431, 17), // "MissingObjectName"
QT_MOC_LITERAL(25, 449, 14), // "HostUrlInvalid"
QT_MOC_LITERAL(26, 464, 16), // "ProtocolMismatch"
QT_MOC_LITERAL(27, 481, 12) // "ListenFailed"

    },
    "QRemoteObjectNode\0remoteObjectAdded\0"
    "\0QRemoteObjectSourceLocation\0"
    "remoteObjectRemoved\0error\0"
    "QRemoteObjectNode::ErrorCode\0errorCode\0"
    "heartbeatIntervalChanged\0heartbeatInterval\0"
    "connectToNode\0address\0registryUrl\0"
    "persistedStore\0QRemoteObjectAbstractPersistedStore*\0"
    "ErrorCode\0NoError\0RegistryNotAcquired\0"
    "RegistryAlreadyHosted\0NodeIsNoServer\0"
    "ServerAlreadyCreated\0UnintendedRegistryHosting\0"
    "OperationNotValidOnClientNode\0"
    "SourceNotRegistered\0MissingObjectName\0"
    "HostUrlInvalid\0ProtocolMismatch\0"
    "ListenFailed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectNode[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       3,   54, // properties
       1,   66, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       4,    1,   42,    2, 0x06 /* Public */,
       5,    1,   45,    2, 0x06 /* Public */,
       8,    1,   48,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
      10,    1,   51,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, QMetaType::Int,    9,

 // methods: parameters
    QMetaType::Bool, QMetaType::QUrl,   11,

 // properties: name, type, flags
      12, QMetaType::QUrl, 0x00095103,
      13, 0x80000000 | 14, 0x0009510b,
       9, QMetaType::Int, 0x00495103,

 // properties: notify_signal_id
       0,
       0,
       3,

 // enums: name, alias, flags, count, data
      15,   15, 0x0,   12,   71,

 // enum data: key, value
      16, uint(QRemoteObjectNode::NoError),
      17, uint(QRemoteObjectNode::RegistryNotAcquired),
      18, uint(QRemoteObjectNode::RegistryAlreadyHosted),
      19, uint(QRemoteObjectNode::NodeIsNoServer),
      20, uint(QRemoteObjectNode::ServerAlreadyCreated),
      21, uint(QRemoteObjectNode::UnintendedRegistryHosting),
      22, uint(QRemoteObjectNode::OperationNotValidOnClientNode),
      23, uint(QRemoteObjectNode::SourceNotRegistered),
      24, uint(QRemoteObjectNode::MissingObjectName),
      25, uint(QRemoteObjectNode::HostUrlInvalid),
      26, uint(QRemoteObjectNode::ProtocolMismatch),
      27, uint(QRemoteObjectNode::ListenFailed),

       0        // eod
};

void QRemoteObjectNode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QRemoteObjectNode *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->remoteObjectAdded((*reinterpret_cast< const QRemoteObjectSourceLocation(*)>(_a[1]))); break;
        case 1: _t->remoteObjectRemoved((*reinterpret_cast< const QRemoteObjectSourceLocation(*)>(_a[1]))); break;
        case 2: _t->error((*reinterpret_cast< QRemoteObjectNode::ErrorCode(*)>(_a[1]))); break;
        case 3: _t->heartbeatIntervalChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: { bool _r = _t->connectToNode((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectSourceLocation >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectSourceLocation >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QRemoteObjectNode::*)(const QRemoteObjectSourceLocation & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRemoteObjectNode::remoteObjectAdded)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QRemoteObjectNode::*)(const QRemoteObjectSourceLocation & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRemoteObjectNode::remoteObjectRemoved)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QRemoteObjectNode::*)(QRemoteObjectNode::ErrorCode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRemoteObjectNode::error)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QRemoteObjectNode::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRemoteObjectNode::heartbeatIntervalChanged)) {
                *result = 3;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectAbstractPersistedStore* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QRemoteObjectNode *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QUrl*>(_v) = _t->registryUrl(); break;
        case 1: *reinterpret_cast< QRemoteObjectAbstractPersistedStore**>(_v) = _t->persistedStore(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->heartbeatInterval(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QRemoteObjectNode *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRegistryUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 1: _t->setPersistedStore(*reinterpret_cast< QRemoteObjectAbstractPersistedStore**>(_v)); break;
        case 2: _t->setHeartbeatInterval(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectNode::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QRemoteObjectNode.data,
    qt_meta_data_QRemoteObjectNode,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QRemoteObjectNode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRemoteObjectNode::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRemoteObjectNode.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QRemoteObjectNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QRemoteObjectNode::remoteObjectAdded(const QRemoteObjectSourceLocation & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QRemoteObjectNode::remoteObjectRemoved(const QRemoteObjectSourceLocation & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QRemoteObjectNode::error(QRemoteObjectNode::ErrorCode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QRemoteObjectNode::heartbeatIntervalChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
struct qt_meta_stringdata_QRemoteObjectHostBase_t {
    QByteArrayData data[4];
    char stringdata0[82];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectHostBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectHostBase_t qt_meta_stringdata_QRemoteObjectHostBase = {
    {
QT_MOC_LITERAL(0, 0, 21), // "QRemoteObjectHostBase"
QT_MOC_LITERAL(1, 22, 14), // "AllowedSchemas"
QT_MOC_LITERAL(2, 37, 18), // "BuiltInSchemasOnly"
QT_MOC_LITERAL(3, 56, 25) // "AllowExternalRegistration"

    },
    "QRemoteObjectHostBase\0AllowedSchemas\0"
    "BuiltInSchemasOnly\0AllowExternalRegistration"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectHostBase[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    2,   19,

 // enum data: key, value
       2, uint(QRemoteObjectHostBase::BuiltInSchemasOnly),
       3, uint(QRemoteObjectHostBase::AllowExternalRegistration),

       0        // eod
};

void QRemoteObjectHostBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectHostBase::staticMetaObject = { {
    &QRemoteObjectNode::staticMetaObject,
    qt_meta_stringdata_QRemoteObjectHostBase.data,
    qt_meta_data_QRemoteObjectHostBase,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QRemoteObjectHostBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRemoteObjectHostBase::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRemoteObjectHostBase.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectNode::qt_metacast(_clname);
}

int QRemoteObjectHostBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectNode::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_QRemoteObjectHost_t {
    QByteArrayData data[1];
    char stringdata0[18];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectHost_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectHost_t qt_meta_stringdata_QRemoteObjectHost = {
    {
QT_MOC_LITERAL(0, 0, 17) // "QRemoteObjectHost"

    },
    "QRemoteObjectHost"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectHost[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QRemoteObjectHost::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectHost::staticMetaObject = { {
    &QRemoteObjectHostBase::staticMetaObject,
    qt_meta_stringdata_QRemoteObjectHost.data,
    qt_meta_data_QRemoteObjectHost,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QRemoteObjectHost::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRemoteObjectHost::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRemoteObjectHost.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectHostBase::qt_metacast(_clname);
}

int QRemoteObjectHost::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectHostBase::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_QRemoteObjectRegistryHost_t {
    QByteArrayData data[1];
    char stringdata0[26];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectRegistryHost_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectRegistryHost_t qt_meta_stringdata_QRemoteObjectRegistryHost = {
    {
QT_MOC_LITERAL(0, 0, 25) // "QRemoteObjectRegistryHost"

    },
    "QRemoteObjectRegistryHost"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectRegistryHost[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QRemoteObjectRegistryHost::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectRegistryHost::staticMetaObject = { {
    &QRemoteObjectHostBase::staticMetaObject,
    qt_meta_stringdata_QRemoteObjectRegistryHost.data,
    qt_meta_data_QRemoteObjectRegistryHost,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QRemoteObjectRegistryHost::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRemoteObjectRegistryHost::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRemoteObjectRegistryHost.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectHostBase::qt_metacast(_clname);
}

int QRemoteObjectRegistryHost::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectHostBase::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
