/****************************************************************************
** Meta object code from reading C++ file 'qquickpalette_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickpalette_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickpalette_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickPalette_t {
    QByteArrayData data[20];
    char stringdata0[183];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPalette_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPalette_t qt_meta_stringdata_QQuickPalette = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickPalette"
QT_MOC_LITERAL(1, 14, 13), // "alternateBase"
QT_MOC_LITERAL(2, 28, 4), // "base"
QT_MOC_LITERAL(3, 33, 10), // "brightText"
QT_MOC_LITERAL(4, 44, 6), // "button"
QT_MOC_LITERAL(5, 51, 10), // "buttonText"
QT_MOC_LITERAL(6, 62, 4), // "dark"
QT_MOC_LITERAL(7, 67, 9), // "highlight"
QT_MOC_LITERAL(8, 77, 15), // "highlightedText"
QT_MOC_LITERAL(9, 93, 5), // "light"
QT_MOC_LITERAL(10, 99, 4), // "link"
QT_MOC_LITERAL(11, 104, 11), // "linkVisited"
QT_MOC_LITERAL(12, 116, 3), // "mid"
QT_MOC_LITERAL(13, 120, 8), // "midlight"
QT_MOC_LITERAL(14, 129, 6), // "shadow"
QT_MOC_LITERAL(15, 136, 4), // "text"
QT_MOC_LITERAL(16, 141, 11), // "toolTipBase"
QT_MOC_LITERAL(17, 153, 11), // "toolTipText"
QT_MOC_LITERAL(18, 165, 6), // "window"
QT_MOC_LITERAL(19, 172, 10) // "windowText"

    },
    "QQuickPalette\0alternateBase\0base\0"
    "brightText\0button\0buttonText\0dark\0"
    "highlight\0highlightedText\0light\0link\0"
    "linkVisited\0mid\0midlight\0shadow\0text\0"
    "toolTipBase\0toolTipText\0window\0"
    "windowText"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPalette[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
      19,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::QColor, 0x00095907,
       2, QMetaType::QColor, 0x00095907,
       3, QMetaType::QColor, 0x00095907,
       4, QMetaType::QColor, 0x00095907,
       5, QMetaType::QColor, 0x00095907,
       6, QMetaType::QColor, 0x00095907,
       7, QMetaType::QColor, 0x00095907,
       8, QMetaType::QColor, 0x00095907,
       9, QMetaType::QColor, 0x00095907,
      10, QMetaType::QColor, 0x00095907,
      11, QMetaType::QColor, 0x00095907,
      12, QMetaType::QColor, 0x00095907,
      13, QMetaType::QColor, 0x00095907,
      14, QMetaType::QColor, 0x00095907,
      15, QMetaType::QColor, 0x00095907,
      16, QMetaType::QColor, 0x00095907,
      17, QMetaType::QColor, 0x00095907,
      18, QMetaType::QColor, 0x00095907,
      19, QMetaType::QColor, 0x00095907,

       0        // eod
};

void QQuickPalette::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = reinterpret_cast<QQuickPalette *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->alternateBase(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->base(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->brightText(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->button(); break;
        case 4: *reinterpret_cast< QColor*>(_v) = _t->buttonText(); break;
        case 5: *reinterpret_cast< QColor*>(_v) = _t->dark(); break;
        case 6: *reinterpret_cast< QColor*>(_v) = _t->highlight(); break;
        case 7: *reinterpret_cast< QColor*>(_v) = _t->highlightedText(); break;
        case 8: *reinterpret_cast< QColor*>(_v) = _t->light(); break;
        case 9: *reinterpret_cast< QColor*>(_v) = _t->link(); break;
        case 10: *reinterpret_cast< QColor*>(_v) = _t->linkVisited(); break;
        case 11: *reinterpret_cast< QColor*>(_v) = _t->mid(); break;
        case 12: *reinterpret_cast< QColor*>(_v) = _t->midlight(); break;
        case 13: *reinterpret_cast< QColor*>(_v) = _t->shadow(); break;
        case 14: *reinterpret_cast< QColor*>(_v) = _t->text(); break;
        case 15: *reinterpret_cast< QColor*>(_v) = _t->toolTipBase(); break;
        case 16: *reinterpret_cast< QColor*>(_v) = _t->toolTipText(); break;
        case 17: *reinterpret_cast< QColor*>(_v) = _t->window(); break;
        case 18: *reinterpret_cast< QColor*>(_v) = _t->windowText(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = reinterpret_cast<QQuickPalette *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAlternateBase(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setBase(*reinterpret_cast< QColor*>(_v)); break;
        case 2: _t->setBrightText(*reinterpret_cast< QColor*>(_v)); break;
        case 3: _t->setButton(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setButtonText(*reinterpret_cast< QColor*>(_v)); break;
        case 5: _t->setDark(*reinterpret_cast< QColor*>(_v)); break;
        case 6: _t->setHighlight(*reinterpret_cast< QColor*>(_v)); break;
        case 7: _t->setHighlightedText(*reinterpret_cast< QColor*>(_v)); break;
        case 8: _t->setLight(*reinterpret_cast< QColor*>(_v)); break;
        case 9: _t->setLink(*reinterpret_cast< QColor*>(_v)); break;
        case 10: _t->setLinkVisited(*reinterpret_cast< QColor*>(_v)); break;
        case 11: _t->setMid(*reinterpret_cast< QColor*>(_v)); break;
        case 12: _t->setMidlight(*reinterpret_cast< QColor*>(_v)); break;
        case 13: _t->setShadow(*reinterpret_cast< QColor*>(_v)); break;
        case 14: _t->setText(*reinterpret_cast< QColor*>(_v)); break;
        case 15: _t->setToolTipBase(*reinterpret_cast< QColor*>(_v)); break;
        case 16: _t->setToolTipText(*reinterpret_cast< QColor*>(_v)); break;
        case 17: _t->setWindow(*reinterpret_cast< QColor*>(_v)); break;
        case 18: _t->setWindowText(*reinterpret_cast< QColor*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickPalette *_t = reinterpret_cast<QQuickPalette *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetAlternateBase(); break;
        case 1: _t->resetBase(); break;
        case 2: _t->resetBrightText(); break;
        case 3: _t->resetButton(); break;
        case 4: _t->resetButtonText(); break;
        case 5: _t->resetDark(); break;
        case 6: _t->resetHighlight(); break;
        case 7: _t->resetHighlightedText(); break;
        case 8: _t->resetLight(); break;
        case 9: _t->resetLink(); break;
        case 10: _t->resetLinkVisited(); break;
        case 11: _t->resetMid(); break;
        case 12: _t->resetMidlight(); break;
        case 13: _t->resetShadow(); break;
        case 14: _t->resetText(); break;
        case 15: _t->resetToolTipBase(); break;
        case 16: _t->resetToolTipText(); break;
        case 17: _t->resetWindow(); break;
        case 18: _t->resetWindowText(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQuickPalette::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QQuickPalette.data,
    qt_meta_data_QQuickPalette,
    qt_static_metacall,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
