/****************************************************************************
** Meta object code from reading C++ file 'qmapobjectview_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../labs/qmapobjectview_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmapobjectview_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QMapObjectView_t {
    QByteArrayData data[19];
    char stringdata0[211];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QMapObjectView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QMapObjectView_t qt_meta_stringdata_QMapObjectView = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QMapObjectView"
QT_MOC_LITERAL(1, 15, 12), // "modelChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 5), // "model"
QT_MOC_LITERAL(4, 35, 15), // "delegateChanged"
QT_MOC_LITERAL(5, 51, 14), // "QQmlComponent*"
QT_MOC_LITERAL(6, 66, 8), // "delegate"
QT_MOC_LITERAL(7, 75, 12), // "addMapObject"
QT_MOC_LITERAL(8, 88, 14), // "QGeoMapObject*"
QT_MOC_LITERAL(9, 103, 6), // "object"
QT_MOC_LITERAL(10, 110, 15), // "removeMapObject"
QT_MOC_LITERAL(11, 126, 14), // "destroyingItem"
QT_MOC_LITERAL(12, 141, 8), // "initItem"
QT_MOC_LITERAL(13, 150, 5), // "index"
QT_MOC_LITERAL(14, 156, 11), // "createdItem"
QT_MOC_LITERAL(15, 168, 12), // "modelUpdated"
QT_MOC_LITERAL(16, 181, 13), // "QQmlChangeSet"
QT_MOC_LITERAL(17, 195, 9), // "changeSet"
QT_MOC_LITERAL(18, 205, 5) // "reset"

    },
    "QMapObjectView\0modelChanged\0\0model\0"
    "delegateChanged\0QQmlComponent*\0delegate\0"
    "addMapObject\0QGeoMapObject*\0object\0"
    "removeMapObject\0destroyingItem\0initItem\0"
    "index\0createdItem\0modelUpdated\0"
    "QQmlChangeSet\0changeSet\0reset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QMapObjectView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       2,   84, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    1,   60,    2, 0x0a /* Public */,
      10,    1,   63,    2, 0x0a /* Public */,
      11,    1,   66,    2, 0x09 /* Protected */,
      12,    2,   69,    2, 0x09 /* Protected */,
      14,    2,   74,    2, 0x09 /* Protected */,
      15,    2,   79,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVariant,    3,
    QMetaType::Void, 0x80000000 | 5,    6,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, QMetaType::QObjectStar,    9,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   13,    9,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,   13,    9,
    QMetaType::Void, 0x80000000 | 16, QMetaType::Bool,   17,   18,

 // properties: name, type, flags
       3, QMetaType::QVariant, 0x00495103,
       6, 0x80000000 | 5, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void QMapObjectView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QMapObjectView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelChanged((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 1: _t->delegateChanged((*reinterpret_cast< QQmlComponent*(*)>(_a[1]))); break;
        case 2: _t->addMapObject((*reinterpret_cast< QGeoMapObject*(*)>(_a[1]))); break;
        case 3: _t->removeMapObject((*reinterpret_cast< QGeoMapObject*(*)>(_a[1]))); break;
        case 4: _t->destroyingItem((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 5: _t->initItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 6: _t->createdItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 7: _t->modelUpdated((*reinterpret_cast< const QQmlChangeSet(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoMapObject* >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoMapObject* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QMapObjectView::*)(QVariant );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMapObjectView::modelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QMapObjectView::*)(QQmlComponent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMapObjectView::delegateChanged)) {
                *result = 1;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QMapObjectView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QMapObjectView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QMapObjectView::staticMetaObject = { {
    &QGeoMapObject::staticMetaObject,
    qt_meta_stringdata_QMapObjectView.data,
    qt_meta_data_QMapObjectView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QMapObjectView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QMapObjectView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QMapObjectView.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QGeoMapObject::qt_metacast(_clname);
}

int QMapObjectView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGeoMapObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QMapObjectView::modelChanged(QVariant _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QMapObjectView::delegateChanged(QQmlComponent * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
