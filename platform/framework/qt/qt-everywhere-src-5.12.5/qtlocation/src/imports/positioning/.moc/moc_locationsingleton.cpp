/****************************************************************************
** Meta object code from reading C++ file 'locationsingleton.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../locationsingleton.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'locationsingleton.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_LocationSingleton_t {
    QByteArrayData data[36];
    char stringdata0[360];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_LocationSingleton_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_LocationSingleton_t qt_meta_stringdata_LocationSingleton = {
    {
QT_MOC_LITERAL(0, 0, 17), // "LocationSingleton"
QT_MOC_LITERAL(1, 18, 10), // "coordinate"
QT_MOC_LITERAL(2, 29, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(3, 44, 0), // ""
QT_MOC_LITERAL(4, 45, 8), // "latitude"
QT_MOC_LITERAL(5, 54, 9), // "longitude"
QT_MOC_LITERAL(6, 64, 8), // "altitude"
QT_MOC_LITERAL(7, 73, 5), // "shape"
QT_MOC_LITERAL(8, 79, 9), // "QGeoShape"
QT_MOC_LITERAL(9, 89, 9), // "rectangle"
QT_MOC_LITERAL(10, 99, 13), // "QGeoRectangle"
QT_MOC_LITERAL(11, 113, 6), // "center"
QT_MOC_LITERAL(12, 120, 5), // "width"
QT_MOC_LITERAL(13, 126, 6), // "height"
QT_MOC_LITERAL(14, 133, 7), // "topLeft"
QT_MOC_LITERAL(15, 141, 11), // "bottomRight"
QT_MOC_LITERAL(16, 153, 11), // "coordinates"
QT_MOC_LITERAL(17, 165, 6), // "circle"
QT_MOC_LITERAL(18, 172, 10), // "QGeoCircle"
QT_MOC_LITERAL(19, 183, 6), // "radius"
QT_MOC_LITERAL(20, 190, 4), // "path"
QT_MOC_LITERAL(21, 195, 8), // "QGeoPath"
QT_MOC_LITERAL(22, 204, 8), // "QJSValue"
QT_MOC_LITERAL(23, 213, 5), // "value"
QT_MOC_LITERAL(24, 219, 7), // "polygon"
QT_MOC_LITERAL(25, 227, 11), // "QGeoPolygon"
QT_MOC_LITERAL(26, 239, 9), // "perimeter"
QT_MOC_LITERAL(27, 249, 5), // "holes"
QT_MOC_LITERAL(28, 255, 13), // "shapeToCircle"
QT_MOC_LITERAL(29, 269, 16), // "shapeToRectangle"
QT_MOC_LITERAL(30, 286, 11), // "shapeToPath"
QT_MOC_LITERAL(31, 298, 14), // "shapeToPolygon"
QT_MOC_LITERAL(32, 313, 15), // "mercatorToCoord"
QT_MOC_LITERAL(33, 329, 8), // "mercator"
QT_MOC_LITERAL(34, 338, 15), // "coordToMercator"
QT_MOC_LITERAL(35, 354, 5) // "coord"

    },
    "LocationSingleton\0coordinate\0"
    "QGeoCoordinate\0\0latitude\0longitude\0"
    "altitude\0shape\0QGeoShape\0rectangle\0"
    "QGeoRectangle\0center\0width\0height\0"
    "topLeft\0bottomRight\0coordinates\0circle\0"
    "QGeoCircle\0radius\0path\0QGeoPath\0"
    "QJSValue\0value\0polygon\0QGeoPolygon\0"
    "perimeter\0holes\0shapeToCircle\0"
    "shapeToRectangle\0shapeToPath\0"
    "shapeToPolygon\0mercatorToCoord\0mercator\0"
    "coordToMercator\0coord"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_LocationSingleton[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,  152,    3, 0x02 /* Public */,
       1,    3,  153,    3, 0x02 /* Public */,
       1,    2,  160,    3, 0x22 /* Public | MethodCloned */,
       7,    0,  165,    3, 0x02 /* Public */,
       9,    0,  166,    3, 0x02 /* Public */,
       9,    3,  167,    3, 0x02 /* Public */,
       9,    2,  174,    3, 0x02 /* Public */,
       9,    1,  179,    3, 0x02 /* Public */,
      17,    0,  182,    3, 0x02 /* Public */,
      17,    2,  183,    3, 0x02 /* Public */,
      17,    1,  188,    3, 0x22 /* Public | MethodCloned */,
      20,    0,  191,    3, 0x02 /* Public */,
      20,    2,  192,    3, 0x02 /* Public */,
      20,    1,  197,    3, 0x22 /* Public | MethodCloned */,
      24,    0,  200,    3, 0x02 /* Public */,
      24,    1,  201,    3, 0x02 /* Public */,
      24,    2,  204,    3, 0x02 /* Public */,
      28,    1,  209,    3, 0x02 /* Public */,
      29,    1,  212,    3, 0x02 /* Public */,
      30,    1,  215,    3, 0x02 /* Public */,
      31,    1,  218,    3, 0x02 /* Public */,
      32,    1,  221,    3, 0x82 /* Public | MethodRevisioned */,
      34,    1,  224,    3, 0x82 /* Public | MethodRevisioned */,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      12,
      12,

 // methods: parameters
    0x80000000 | 2,
    0x80000000 | 2, QMetaType::Double, QMetaType::Double, QMetaType::Double,    4,    5,    6,
    0x80000000 | 2, QMetaType::Double, QMetaType::Double,    4,    5,
    0x80000000 | 8,
    0x80000000 | 10,
    0x80000000 | 10, 0x80000000 | 2, QMetaType::Double, QMetaType::Double,   11,   12,   13,
    0x80000000 | 10, 0x80000000 | 2, 0x80000000 | 2,   14,   15,
    0x80000000 | 10, QMetaType::QVariantList,   16,
    0x80000000 | 18,
    0x80000000 | 18, 0x80000000 | 2, QMetaType::QReal,   11,   19,
    0x80000000 | 18, 0x80000000 | 2,   11,
    0x80000000 | 21,
    0x80000000 | 21, 0x80000000 | 22, QMetaType::QReal,   23,   12,
    0x80000000 | 21, 0x80000000 | 22,   23,
    0x80000000 | 25,
    0x80000000 | 25, QMetaType::QVariantList,   23,
    0x80000000 | 25, QMetaType::QVariantList, QMetaType::QVariantList,   26,   27,
    0x80000000 | 18, 0x80000000 | 8,    7,
    0x80000000 | 10, 0x80000000 | 8,    7,
    0x80000000 | 21, 0x80000000 | 8,    7,
    0x80000000 | 25, 0x80000000 | 8,    7,
    0x80000000 | 2, QMetaType::QPointF,   33,
    QMetaType::QPointF, 0x80000000 | 2,   35,

       0        // eod
};

void LocationSingleton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<LocationSingleton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QGeoCoordinate _r = _t->coordinate();
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 1: { QGeoCoordinate _r = _t->coordinate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 2: { QGeoCoordinate _r = _t->coordinate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 3: { QGeoShape _r = _t->shape();
            if (_a[0]) *reinterpret_cast< QGeoShape*>(_a[0]) = std::move(_r); }  break;
        case 4: { QGeoRectangle _r = _t->rectangle();
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = std::move(_r); }  break;
        case 5: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = std::move(_r); }  break;
        case 6: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< const QGeoCoordinate(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = std::move(_r); }  break;
        case 7: { QGeoRectangle _r = _t->rectangle((*reinterpret_cast< const QVariantList(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = std::move(_r); }  break;
        case 8: { QGeoCircle _r = _t->circle();
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = std::move(_r); }  break;
        case 9: { QGeoCircle _r = _t->circle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = std::move(_r); }  break;
        case 10: { QGeoCircle _r = _t->circle((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = std::move(_r); }  break;
        case 11: { QGeoPath _r = _t->path();
            if (_a[0]) *reinterpret_cast< QGeoPath*>(_a[0]) = std::move(_r); }  break;
        case 12: { QGeoPath _r = _t->path((*reinterpret_cast< const QJSValue(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoPath*>(_a[0]) = std::move(_r); }  break;
        case 13: { QGeoPath _r = _t->path((*reinterpret_cast< const QJSValue(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoPath*>(_a[0]) = std::move(_r); }  break;
        case 14: { QGeoPolygon _r = _t->polygon();
            if (_a[0]) *reinterpret_cast< QGeoPolygon*>(_a[0]) = std::move(_r); }  break;
        case 15: { QGeoPolygon _r = _t->polygon((*reinterpret_cast< const QVariantList(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoPolygon*>(_a[0]) = std::move(_r); }  break;
        case 16: { QGeoPolygon _r = _t->polygon((*reinterpret_cast< const QVariantList(*)>(_a[1])),(*reinterpret_cast< const QVariantList(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoPolygon*>(_a[0]) = std::move(_r); }  break;
        case 17: { QGeoCircle _r = _t->shapeToCircle((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCircle*>(_a[0]) = std::move(_r); }  break;
        case 18: { QGeoRectangle _r = _t->shapeToRectangle((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoRectangle*>(_a[0]) = std::move(_r); }  break;
        case 19: { QGeoPath _r = _t->shapeToPath((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoPath*>(_a[0]) = std::move(_r); }  break;
        case 20: { QGeoPolygon _r = _t->shapeToPolygon((*reinterpret_cast< const QGeoShape(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoPolygon*>(_a[0]) = std::move(_r); }  break;
        case 21: { QGeoCoordinate _r = _t->mercatorToCoord((*reinterpret_cast< const QPointF(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 22: { QPointF _r = _t->coordToMercator((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
            }
            break;
        case 22:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject LocationSingleton::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_LocationSingleton.data,
    qt_meta_data_LocationSingleton,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *LocationSingleton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *LocationSingleton::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_LocationSingleton.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int LocationSingleton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
