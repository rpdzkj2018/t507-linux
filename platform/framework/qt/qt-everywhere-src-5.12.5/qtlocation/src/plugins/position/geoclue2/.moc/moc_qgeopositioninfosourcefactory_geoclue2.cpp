/****************************************************************************
** Meta object code from reading C++ file 'qgeopositioninfosourcefactory_geoclue2.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qgeopositioninfosourcefactory_geoclue2.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeopositioninfosourcefactory_geoclue2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2_t {
    QByteArrayData data[1];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2_t qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2 = {
    {
QT_MOC_LITERAL(0, 0, 37) // "QGeoPositionInfoSourceFactory..."

    },
    "QGeoPositionInfoSourceFactoryGeoclue2"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoPositionInfoSourceFactoryGeoclue2[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QGeoPositionInfoSourceFactoryGeoclue2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QGeoPositionInfoSourceFactoryGeoclue2::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2.data,
    qt_meta_data_QGeoPositionInfoSourceFactoryGeoclue2,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGeoPositionInfoSourceFactoryGeoclue2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoPositionInfoSourceFactoryGeoclue2::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoPositionInfoSourceFactoryGeoclue2.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QGeoPositionInfoSourceFactory"))
        return static_cast< QGeoPositionInfoSourceFactory*>(this);
    if (!strcmp(_clname, "org.qt-project.qt.position.sourcefactory/5.0"))
        return static_cast< QGeoPositionInfoSourceFactory*>(this);
    return QObject::qt_metacast(_clname);
}

int QGeoPositionInfoSourceFactoryGeoclue2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x2c,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  'p',  'o',  's', 
    'i',  't',  'i',  'o',  'n',  '.',  's',  'o', 
    'u',  'r',  'c',  'e',  'f',  'a',  'c',  't', 
    'o',  'r',  'y',  '/',  '5',  '.',  '0', 
    // "className"
    0x03,  0x78,  0x25,  'Q',  'G',  'e',  'o',  'P', 
    'o',  's',  'i',  't',  'i',  'o',  'n',  'I', 
    'n',  'f',  'o',  'S',  'o',  'u',  'r',  'c', 
    'e',  'F',  'a',  'c',  't',  'o',  'r',  'y', 
    'G',  'e',  'o',  'c',  'l',  'u',  'e',  '2', 
    // "MetaData"
    0x04,  0xa7,  0x64,  'K',  'e',  'y',  's',  0x81, 
    0x68,  'g',  'e',  'o',  'c',  'l',  'u',  'e', 
    '2',  0x67,  'M',  'o',  'n',  'i',  't',  'o', 
    'r',  0xf4,  0x68,  'P',  'o',  's',  'i',  't', 
    'i',  'o',  'n',  0xf5,  0x68,  'P',  'r',  'i', 
    'o',  'r',  'i',  't',  'y',  0x19,  0x03,  0xe8, 
    0x68,  'P',  'r',  'o',  'v',  'i',  'd',  'e', 
    'r',  0x68,  'g',  'e',  'o',  'c',  'l',  'u', 
    'e',  '2',  0x69,  'S',  'a',  't',  'e',  'l', 
    'l',  'i',  't',  'e',  0xf4,  0x68,  'T',  'e', 
    's',  't',  'a',  'b',  'l',  'e',  0xf4, 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(QGeoPositionInfoSourceFactoryGeoclue2, QGeoPositionInfoSourceFactoryGeoclue2)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
