/****************************************************************************
** Meta object code from reading C++ file 'qquickninepatchimage_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickninepatchimage_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickninepatchimage_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickNinePatchImage_t {
    QByteArrayData data[18];
    char stringdata0[262];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickNinePatchImage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickNinePatchImage_t qt_meta_stringdata_QQuickNinePatchImage = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickNinePatchImage"
QT_MOC_LITERAL(1, 21, 17), // "topPaddingChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(4, 59, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(5, 79, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(6, 100, 15), // "topInsetChanged"
QT_MOC_LITERAL(7, 116, 16), // "leftInsetChanged"
QT_MOC_LITERAL(8, 133, 17), // "rightInsetChanged"
QT_MOC_LITERAL(9, 151, 18), // "bottomInsetChanged"
QT_MOC_LITERAL(10, 170, 10), // "topPadding"
QT_MOC_LITERAL(11, 181, 11), // "leftPadding"
QT_MOC_LITERAL(12, 193, 12), // "rightPadding"
QT_MOC_LITERAL(13, 206, 13), // "bottomPadding"
QT_MOC_LITERAL(14, 220, 8), // "topInset"
QT_MOC_LITERAL(15, 229, 9), // "leftInset"
QT_MOC_LITERAL(16, 239, 10), // "rightInset"
QT_MOC_LITERAL(17, 250, 11) // "bottomInset"

    },
    "QQuickNinePatchImage\0topPaddingChanged\0"
    "\0leftPaddingChanged\0rightPaddingChanged\0"
    "bottomPaddingChanged\0topInsetChanged\0"
    "leftInsetChanged\0rightInsetChanged\0"
    "bottomInsetChanged\0topPadding\0leftPadding\0"
    "rightPadding\0bottomPadding\0topInset\0"
    "leftInset\0rightInset\0bottomInset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickNinePatchImage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       8,   62, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    0,   56,    2, 0x06 /* Public */,
       5,    0,   57,    2, 0x06 /* Public */,
       6,    0,   58,    2, 0x06 /* Public */,
       7,    0,   59,    2, 0x06 /* Public */,
       8,    0,   60,    2, 0x06 /* Public */,
       9,    0,   61,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      10, QMetaType::QReal, 0x00495801,
      11, QMetaType::QReal, 0x00495801,
      12, QMetaType::QReal, 0x00495801,
      13, QMetaType::QReal, 0x00495801,
      14, QMetaType::QReal, 0x00495801,
      15, QMetaType::QReal, 0x00495801,
      16, QMetaType::QReal, 0x00495801,
      17, QMetaType::QReal, 0x00495801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,

       0        // eod
};

void QQuickNinePatchImage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickNinePatchImage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->topPaddingChanged(); break;
        case 1: _t->leftPaddingChanged(); break;
        case 2: _t->rightPaddingChanged(); break;
        case 3: _t->bottomPaddingChanged(); break;
        case 4: _t->topInsetChanged(); break;
        case 5: _t->leftInsetChanged(); break;
        case 6: _t->rightInsetChanged(); break;
        case 7: _t->bottomInsetChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::topPaddingChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::leftPaddingChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::rightPaddingChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::bottomPaddingChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::topInsetChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::leftInsetChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::rightInsetChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickNinePatchImage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickNinePatchImage::bottomInsetChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickNinePatchImage *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->topInset(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->leftInset(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->rightInset(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->bottomInset(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQuickNinePatchImage::staticMetaObject = { {
    &QQuickImage::staticMetaObject,
    qt_meta_stringdata_QQuickNinePatchImage.data,
    qt_meta_data_QQuickNinePatchImage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickNinePatchImage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickNinePatchImage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickNinePatchImage.stringdata0))
        return static_cast<void*>(this);
    return QQuickImage::qt_metacast(_clname);
}

int QQuickNinePatchImage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickImage::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickNinePatchImage::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickNinePatchImage::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickNinePatchImage::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickNinePatchImage::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickNinePatchImage::topInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickNinePatchImage::leftInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickNinePatchImage::rightInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickNinePatchImage::bottomInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
