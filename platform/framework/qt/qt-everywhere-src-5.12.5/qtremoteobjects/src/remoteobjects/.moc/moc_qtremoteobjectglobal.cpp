/****************************************************************************
** Meta object code from reading C++ file 'qtremoteobjectglobal.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qtremoteobjectglobal.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qtremoteobjectglobal.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtRemoteObjects_t {
    QByteArrayData data[17];
    char stringdata0[228];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtRemoteObjects_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtRemoteObjects_t qt_meta_stringdata_QtRemoteObjects = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QtRemoteObjects"
QT_MOC_LITERAL(1, 16, 27), // "QRemoteObjectPacketTypeEnum"
QT_MOC_LITERAL(2, 44, 7), // "Invalid"
QT_MOC_LITERAL(3, 52, 9), // "Handshake"
QT_MOC_LITERAL(4, 62, 10), // "InitPacket"
QT_MOC_LITERAL(5, 73, 17), // "InitDynamicPacket"
QT_MOC_LITERAL(6, 91, 9), // "AddObject"
QT_MOC_LITERAL(7, 101, 12), // "RemoveObject"
QT_MOC_LITERAL(8, 114, 12), // "InvokePacket"
QT_MOC_LITERAL(9, 127, 17), // "InvokeReplyPacket"
QT_MOC_LITERAL(10, 145, 20), // "PropertyChangePacket"
QT_MOC_LITERAL(11, 166, 10), // "ObjectList"
QT_MOC_LITERAL(12, 177, 4), // "Ping"
QT_MOC_LITERAL(13, 182, 4), // "Pong"
QT_MOC_LITERAL(14, 187, 13), // "InitialAction"
QT_MOC_LITERAL(15, 201, 13), // "FetchRootSize"
QT_MOC_LITERAL(16, 215, 12) // "PrefetchData"

    },
    "QtRemoteObjects\0QRemoteObjectPacketTypeEnum\0"
    "Invalid\0Handshake\0InitPacket\0"
    "InitDynamicPacket\0AddObject\0RemoveObject\0"
    "InvokePacket\0InvokeReplyPacket\0"
    "PropertyChangePacket\0ObjectList\0Ping\0"
    "Pong\0InitialAction\0FetchRootSize\0"
    "PrefetchData"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtRemoteObjects[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       2,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,   12,   24,
      14,   14, 0x0,    2,   48,

 // enum data: key, value
       2, uint(QtRemoteObjects::Invalid),
       3, uint(QtRemoteObjects::Handshake),
       4, uint(QtRemoteObjects::InitPacket),
       5, uint(QtRemoteObjects::InitDynamicPacket),
       6, uint(QtRemoteObjects::AddObject),
       7, uint(QtRemoteObjects::RemoveObject),
       8, uint(QtRemoteObjects::InvokePacket),
       9, uint(QtRemoteObjects::InvokeReplyPacket),
      10, uint(QtRemoteObjects::PropertyChangePacket),
      11, uint(QtRemoteObjects::ObjectList),
      12, uint(QtRemoteObjects::Ping),
      13, uint(QtRemoteObjects::Pong),
      15, uint(QtRemoteObjects::FetchRootSize),
      16, uint(QtRemoteObjects::PrefetchData),

       0        // eod
};

QT_INIT_METAOBJECT const QMetaObject QtRemoteObjects::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QtRemoteObjects.data,
    qt_meta_data_QtRemoteObjects,
    nullptr,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
