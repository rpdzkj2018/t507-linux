/****************************************************************************
** Meta object code from reading C++ file 'qquickdialog_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickdialog_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickdialog_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickDialog_t {
    QByteArrayData data[38];
    char stringdata0[566];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickDialog_t qt_meta_stringdata_QQuickDialog = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickDialog"
QT_MOC_LITERAL(1, 13, 8), // "accepted"
QT_MOC_LITERAL(2, 22, 0), // ""
QT_MOC_LITERAL(3, 23, 8), // "rejected"
QT_MOC_LITERAL(4, 32, 12), // "titleChanged"
QT_MOC_LITERAL(5, 45, 13), // "headerChanged"
QT_MOC_LITERAL(6, 59, 13), // "footerChanged"
QT_MOC_LITERAL(7, 73, 22), // "standardButtonsChanged"
QT_MOC_LITERAL(8, 96, 7), // "applied"
QT_MOC_LITERAL(9, 104, 5), // "reset"
QT_MOC_LITERAL(10, 110, 9), // "discarded"
QT_MOC_LITERAL(11, 120, 13), // "helpRequested"
QT_MOC_LITERAL(12, 134, 13), // "resultChanged"
QT_MOC_LITERAL(13, 148, 26), // "implicitHeaderWidthChanged"
QT_MOC_LITERAL(14, 175, 27), // "implicitHeaderHeightChanged"
QT_MOC_LITERAL(15, 203, 26), // "implicitFooterWidthChanged"
QT_MOC_LITERAL(16, 230, 27), // "implicitFooterHeightChanged"
QT_MOC_LITERAL(17, 258, 6), // "accept"
QT_MOC_LITERAL(18, 265, 6), // "reject"
QT_MOC_LITERAL(19, 272, 4), // "done"
QT_MOC_LITERAL(20, 277, 6), // "result"
QT_MOC_LITERAL(21, 284, 14), // "standardButton"
QT_MOC_LITERAL(22, 299, 21), // "QQuickAbstractButton*"
QT_MOC_LITERAL(23, 321, 37), // "QPlatformDialogHelper::Standa..."
QT_MOC_LITERAL(24, 359, 6), // "button"
QT_MOC_LITERAL(25, 366, 5), // "title"
QT_MOC_LITERAL(26, 372, 6), // "header"
QT_MOC_LITERAL(27, 379, 11), // "QQuickItem*"
QT_MOC_LITERAL(28, 391, 6), // "footer"
QT_MOC_LITERAL(29, 398, 15), // "standardButtons"
QT_MOC_LITERAL(30, 414, 38), // "QPlatformDialogHelper::Standa..."
QT_MOC_LITERAL(31, 453, 19), // "implicitHeaderWidth"
QT_MOC_LITERAL(32, 473, 20), // "implicitHeaderHeight"
QT_MOC_LITERAL(33, 494, 19), // "implicitFooterWidth"
QT_MOC_LITERAL(34, 514, 20), // "implicitFooterHeight"
QT_MOC_LITERAL(35, 535, 12), // "StandardCode"
QT_MOC_LITERAL(36, 548, 8), // "Rejected"
QT_MOC_LITERAL(37, 557, 8) // "Accepted"

    },
    "QQuickDialog\0accepted\0\0rejected\0"
    "titleChanged\0headerChanged\0footerChanged\0"
    "standardButtonsChanged\0applied\0reset\0"
    "discarded\0helpRequested\0resultChanged\0"
    "implicitHeaderWidthChanged\0"
    "implicitHeaderHeightChanged\0"
    "implicitFooterWidthChanged\0"
    "implicitFooterHeightChanged\0accept\0"
    "reject\0done\0result\0standardButton\0"
    "QQuickAbstractButton*\0"
    "QPlatformDialogHelper::StandardButton\0"
    "button\0title\0header\0QQuickItem*\0footer\0"
    "standardButtons\0QPlatformDialogHelper::StandardButtons\0"
    "implicitHeaderWidth\0implicitHeaderHeight\0"
    "implicitFooterWidth\0implicitFooterHeight\0"
    "StandardCode\0Rejected\0Accepted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       9,  151, // properties
       1,  196, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  128,    2, 0x06 /* Public */,
       3,    0,  129,    2, 0x06 /* Public */,
       4,    0,  130,    2, 0x06 /* Public */,
       5,    0,  131,    2, 0x06 /* Public */,
       6,    0,  132,    2, 0x06 /* Public */,
       7,    0,  133,    2, 0x06 /* Public */,
       8,    0,  134,    2, 0x86 /* Public | MethodRevisioned */,
       9,    0,  135,    2, 0x86 /* Public | MethodRevisioned */,
      10,    0,  136,    2, 0x86 /* Public | MethodRevisioned */,
      11,    0,  137,    2, 0x86 /* Public | MethodRevisioned */,
      12,    0,  138,    2, 0x86 /* Public | MethodRevisioned */,
      13,    0,  139,    2, 0x06 /* Public */,
      14,    0,  140,    2, 0x06 /* Public */,
      15,    0,  141,    2, 0x06 /* Public */,
      16,    0,  142,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      17,    0,  143,    2, 0x0a /* Public */,
      18,    0,  144,    2, 0x0a /* Public */,
      19,    1,  145,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      21,    1,  148,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       3,
       3,
       3,
       3,
       0,
       0,
       0,
       0,

 // slots: revision
       0,
       0,
       0,

 // methods: revision
       3,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   20,

 // methods: parameters
    0x80000000 | 22, 0x80000000 | 23,   24,

 // properties: name, type, flags
      25, QMetaType::QString, 0x00495903,
      26, 0x80000000 | 27, 0x0049590b,
      28, 0x80000000 | 27, 0x0049590b,
      29, 0x80000000 | 30, 0x0049590b,
      20, QMetaType::Int, 0x00c95903,
      31, QMetaType::QReal, 0x00c95801,
      32, QMetaType::QReal, 0x00c95801,
      33, QMetaType::QReal, 0x00c95801,
      34, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       2,
       3,
       4,
       5,
      10,
      11,
      12,
      13,
      14,

 // properties: revision
       0,
       0,
       0,
       0,
       3,
       5,
       5,
       5,
       5,

 // enums: name, alias, flags, count, data
      35,   35, 0x0,    2,  201,

 // enum data: key, value
      36, uint(QQuickDialog::Rejected),
      37, uint(QQuickDialog::Accepted),

       0        // eod
};

void QQuickDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->accepted(); break;
        case 1: _t->rejected(); break;
        case 2: _t->titleChanged(); break;
        case 3: _t->headerChanged(); break;
        case 4: _t->footerChanged(); break;
        case 5: _t->standardButtonsChanged(); break;
        case 6: _t->applied(); break;
        case 7: _t->reset(); break;
        case 8: _t->discarded(); break;
        case 9: _t->helpRequested(); break;
        case 10: _t->resultChanged(); break;
        case 11: _t->implicitHeaderWidthChanged(); break;
        case 12: _t->implicitHeaderHeightChanged(); break;
        case 13: _t->implicitFooterWidthChanged(); break;
        case 14: _t->implicitFooterHeightChanged(); break;
        case 15: _t->accept(); break;
        case 16: _t->reject(); break;
        case 17: _t->done((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: { QQuickAbstractButton* _r = _t->standardButton((*reinterpret_cast< QPlatformDialogHelper::StandardButton(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickAbstractButton**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QPlatformDialogHelper::StandardButton >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::accepted)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::rejected)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::titleChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::headerChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::footerChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::standardButtonsChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::applied)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::reset)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::discarded)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::helpRequested)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::resultChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::implicitHeaderWidthChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::implicitHeaderHeightChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::implicitFooterWidthChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickDialog::implicitFooterHeightChanged)) {
                *result = 14;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->title(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->header(); break;
        case 2: *reinterpret_cast< QQuickItem**>(_v) = _t->footer(); break;
        case 3: *reinterpret_cast<int*>(_v) = QFlag(_t->standardButtons()); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->result(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->implicitHeaderWidth(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->implicitHeaderHeight(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->implicitFooterWidth(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->implicitFooterHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTitle(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setHeader(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 2: _t->setFooter(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 3: _t->setStandardButtons(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 4: _t->setResult(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickDialog[] = {
        &QPlatformDialogHelper::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QQuickDialog::staticMetaObject = { {
    &QQuickPopup::staticMetaObject,
    qt_meta_stringdata_QQuickDialog.data,
    qt_meta_data_QQuickDialog,
    qt_static_metacall,
    qt_meta_extradata_QQuickDialog,
    nullptr
} };


const QMetaObject *QQuickDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickDialog.stringdata0))
        return static_cast<void*>(this);
    return QQuickPopup::qt_metacast(_clname);
}

int QQuickDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPopup::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickDialog::accepted()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickDialog::rejected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickDialog::titleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickDialog::headerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickDialog::footerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickDialog::standardButtonsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickDialog::applied()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickDialog::reset()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickDialog::discarded()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickDialog::helpRequested()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickDialog::resultChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickDialog::implicitHeaderWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickDialog::implicitHeaderHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickDialog::implicitFooterWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickDialog::implicitFooterHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
