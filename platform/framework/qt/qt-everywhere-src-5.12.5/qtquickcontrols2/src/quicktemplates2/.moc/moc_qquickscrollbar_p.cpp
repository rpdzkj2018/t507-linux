/****************************************************************************
** Meta object code from reading C++ file 'qquickscrollbar_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickscrollbar_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickscrollbar_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickScrollBar_t {
    QByteArrayData data[41];
    char stringdata0[483];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickScrollBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickScrollBar_t qt_meta_stringdata_QQuickScrollBar = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickScrollBar"
QT_MOC_LITERAL(1, 16, 11), // "sizeChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 15), // "positionChanged"
QT_MOC_LITERAL(4, 45, 15), // "stepSizeChanged"
QT_MOC_LITERAL(5, 61, 13), // "activeChanged"
QT_MOC_LITERAL(6, 75, 14), // "pressedChanged"
QT_MOC_LITERAL(7, 90, 18), // "orientationChanged"
QT_MOC_LITERAL(8, 109, 15), // "snapModeChanged"
QT_MOC_LITERAL(9, 125, 18), // "interactiveChanged"
QT_MOC_LITERAL(10, 144, 13), // "policyChanged"
QT_MOC_LITERAL(11, 158, 18), // "minimumSizeChanged"
QT_MOC_LITERAL(12, 177, 17), // "visualSizeChanged"
QT_MOC_LITERAL(13, 195, 21), // "visualPositionChanged"
QT_MOC_LITERAL(14, 217, 8), // "increase"
QT_MOC_LITERAL(15, 226, 8), // "decrease"
QT_MOC_LITERAL(16, 235, 7), // "setSize"
QT_MOC_LITERAL(17, 243, 4), // "size"
QT_MOC_LITERAL(18, 248, 11), // "setPosition"
QT_MOC_LITERAL(19, 260, 8), // "position"
QT_MOC_LITERAL(20, 269, 8), // "stepSize"
QT_MOC_LITERAL(21, 278, 6), // "active"
QT_MOC_LITERAL(22, 285, 7), // "pressed"
QT_MOC_LITERAL(23, 293, 11), // "orientation"
QT_MOC_LITERAL(24, 305, 15), // "Qt::Orientation"
QT_MOC_LITERAL(25, 321, 8), // "snapMode"
QT_MOC_LITERAL(26, 330, 8), // "SnapMode"
QT_MOC_LITERAL(27, 339, 11), // "interactive"
QT_MOC_LITERAL(28, 351, 6), // "policy"
QT_MOC_LITERAL(29, 358, 6), // "Policy"
QT_MOC_LITERAL(30, 365, 10), // "horizontal"
QT_MOC_LITERAL(31, 376, 8), // "vertical"
QT_MOC_LITERAL(32, 385, 11), // "minimumSize"
QT_MOC_LITERAL(33, 397, 10), // "visualSize"
QT_MOC_LITERAL(34, 408, 14), // "visualPosition"
QT_MOC_LITERAL(35, 423, 6), // "NoSnap"
QT_MOC_LITERAL(36, 430, 10), // "SnapAlways"
QT_MOC_LITERAL(37, 441, 13), // "SnapOnRelease"
QT_MOC_LITERAL(38, 455, 8), // "AsNeeded"
QT_MOC_LITERAL(39, 464, 9), // "AlwaysOff"
QT_MOC_LITERAL(40, 474, 8) // "AlwaysOn"

    },
    "QQuickScrollBar\0sizeChanged\0\0"
    "positionChanged\0stepSizeChanged\0"
    "activeChanged\0pressedChanged\0"
    "orientationChanged\0snapModeChanged\0"
    "interactiveChanged\0policyChanged\0"
    "minimumSizeChanged\0visualSizeChanged\0"
    "visualPositionChanged\0increase\0decrease\0"
    "setSize\0size\0setPosition\0position\0"
    "stepSize\0active\0pressed\0orientation\0"
    "Qt::Orientation\0snapMode\0SnapMode\0"
    "interactive\0policy\0Policy\0horizontal\0"
    "vertical\0minimumSize\0visualSize\0"
    "visualPosition\0NoSnap\0SnapAlways\0"
    "SnapOnRelease\0AsNeeded\0AlwaysOff\0"
    "AlwaysOn"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickScrollBar[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
      14,  130, // properties
       2,  200, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  110,    2, 0x06 /* Public */,
       3,    0,  111,    2, 0x06 /* Public */,
       4,    0,  112,    2, 0x06 /* Public */,
       5,    0,  113,    2, 0x06 /* Public */,
       6,    0,  114,    2, 0x06 /* Public */,
       7,    0,  115,    2, 0x06 /* Public */,
       8,    0,  116,    2, 0x86 /* Public | MethodRevisioned */,
       9,    0,  117,    2, 0x86 /* Public | MethodRevisioned */,
      10,    0,  118,    2, 0x86 /* Public | MethodRevisioned */,
      11,    0,  119,    2, 0x86 /* Public | MethodRevisioned */,
      12,    0,  120,    2, 0x86 /* Public | MethodRevisioned */,
      13,    0,  121,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      14,    0,  122,    2, 0x0a /* Public */,
      15,    0,  123,    2, 0x0a /* Public */,
      16,    1,  124,    2, 0x0a /* Public */,
      18,    1,  127,    2, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       2,
       4,
       4,
       4,

 // slots: revision
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   17,
    QMetaType::Void, QMetaType::QReal,   19,

 // properties: name, type, flags
      17, QMetaType::QReal, 0x00495903,
      19, QMetaType::QReal, 0x00495903,
      20, QMetaType::QReal, 0x00495903,
      21, QMetaType::Bool, 0x00495903,
      22, QMetaType::Bool, 0x00495903,
      23, 0x80000000 | 24, 0x0049590b,
      25, 0x80000000 | 26, 0x00c9590b,
      27, QMetaType::Bool, 0x00c95907,
      28, 0x80000000 | 29, 0x00c9590b,
      30, QMetaType::Bool, 0x00c95801,
      31, QMetaType::Bool, 0x00c95801,
      32, QMetaType::QReal, 0x00c95903,
      33, QMetaType::QReal, 0x00c95801,
      34, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       5,
       5,
       9,
      10,
      11,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       2,
       3,
       3,
       4,
       4,
       4,

 // enums: name, alias, flags, count, data
      26,   26, 0x0,    3,  210,
      29,   29, 0x0,    3,  216,

 // enum data: key, value
      35, uint(QQuickScrollBar::NoSnap),
      36, uint(QQuickScrollBar::SnapAlways),
      37, uint(QQuickScrollBar::SnapOnRelease),
      38, uint(QQuickScrollBar::AsNeeded),
      39, uint(QQuickScrollBar::AlwaysOff),
      40, uint(QQuickScrollBar::AlwaysOn),

       0        // eod
};

void QQuickScrollBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickScrollBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sizeChanged(); break;
        case 1: _t->positionChanged(); break;
        case 2: _t->stepSizeChanged(); break;
        case 3: _t->activeChanged(); break;
        case 4: _t->pressedChanged(); break;
        case 5: _t->orientationChanged(); break;
        case 6: _t->snapModeChanged(); break;
        case 7: _t->interactiveChanged(); break;
        case 8: _t->policyChanged(); break;
        case 9: _t->minimumSizeChanged(); break;
        case 10: _t->visualSizeChanged(); break;
        case 11: _t->visualPositionChanged(); break;
        case 12: _t->increase(); break;
        case 13: _t->decrease(); break;
        case 14: _t->setSize((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 15: _t->setPosition((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::sizeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::positionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::stepSizeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::activeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::pressedChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::orientationChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::snapModeChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::interactiveChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::policyChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::minimumSizeChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::visualSizeChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBar::visualPositionChanged)) {
                *result = 11;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickScrollBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->size(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->position(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->stepSize(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isActive(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 5: *reinterpret_cast< Qt::Orientation*>(_v) = _t->orientation(); break;
        case 6: *reinterpret_cast< SnapMode*>(_v) = _t->snapMode(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isInteractive(); break;
        case 8: *reinterpret_cast< Policy*>(_v) = _t->policy(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->isHorizontal(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isVertical(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->minimumSize(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->visualSize(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->visualPosition(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickScrollBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSize(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setPosition(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setStepSize(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setActive(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setPressed(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setOrientation(*reinterpret_cast< Qt::Orientation*>(_v)); break;
        case 6: _t->setSnapMode(*reinterpret_cast< SnapMode*>(_v)); break;
        case 7: _t->setInteractive(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setPolicy(*reinterpret_cast< Policy*>(_v)); break;
        case 11: _t->setMinimumSize(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickScrollBar *_t = static_cast<QQuickScrollBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 7: _t->resetInteractive(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickScrollBar::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickScrollBar.data,
    qt_meta_data_QQuickScrollBar,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickScrollBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickScrollBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickScrollBar.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickScrollBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 14;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickScrollBar::sizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickScrollBar::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickScrollBar::stepSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickScrollBar::activeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickScrollBar::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickScrollBar::orientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickScrollBar::snapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickScrollBar::interactiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickScrollBar::policyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickScrollBar::minimumSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickScrollBar::visualSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickScrollBar::visualPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}
struct qt_meta_stringdata_QQuickScrollBarAttached_t {
    QByteArrayData data[7];
    char stringdata0[96];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickScrollBarAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickScrollBarAttached_t qt_meta_stringdata_QQuickScrollBarAttached = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QQuickScrollBarAttached"
QT_MOC_LITERAL(1, 24, 17), // "horizontalChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 15), // "verticalChanged"
QT_MOC_LITERAL(4, 59, 10), // "horizontal"
QT_MOC_LITERAL(5, 70, 16), // "QQuickScrollBar*"
QT_MOC_LITERAL(6, 87, 8) // "vertical"

    },
    "QQuickScrollBarAttached\0horizontalChanged\0"
    "\0verticalChanged\0horizontal\0"
    "QQuickScrollBar*\0vertical"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickScrollBarAttached[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       2,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, 0x80000000 | 5, 0x0049590b,
       6, 0x80000000 | 5, 0x0049590b,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void QQuickScrollBarAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickScrollBarAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->horizontalChanged(); break;
        case 1: _t->verticalChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickScrollBarAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBarAttached::horizontalChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickScrollBarAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickScrollBarAttached::verticalChanged)) {
                *result = 1;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickScrollBar* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickScrollBarAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickScrollBar**>(_v) = _t->horizontal(); break;
        case 1: *reinterpret_cast< QQuickScrollBar**>(_v) = _t->vertical(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickScrollBarAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setHorizontal(*reinterpret_cast< QQuickScrollBar**>(_v)); break;
        case 1: _t->setVertical(*reinterpret_cast< QQuickScrollBar**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickScrollBarAttached::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickScrollBarAttached.data,
    qt_meta_data_QQuickScrollBarAttached,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickScrollBarAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickScrollBarAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickScrollBarAttached.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickScrollBarAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickScrollBarAttached::horizontalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickScrollBarAttached::verticalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
