/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativecameraimageprocessing_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qdeclarativecameraimageprocessing_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativecameraimageprocessing_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeCameraImageProcessing_t {
    QByteArrayData data[58];
    char stringdata0[1074];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeCameraImageProcessing_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeCameraImageProcessing_t qt_meta_stringdata_QDeclarativeCameraImageProcessing = {
    {
QT_MOC_LITERAL(0, 0, 33), // "QDeclarativeCameraImageProces..."
QT_MOC_LITERAL(1, 34, 23), // "whiteBalanceModeChanged"
QT_MOC_LITERAL(2, 58, 0), // ""
QT_MOC_LITERAL(3, 59, 51), // "QDeclarativeCameraImageProces..."
QT_MOC_LITERAL(4, 111, 25), // "manualWhiteBalanceChanged"
QT_MOC_LITERAL(5, 137, 17), // "brightnessChanged"
QT_MOC_LITERAL(6, 155, 15), // "contrastChanged"
QT_MOC_LITERAL(7, 171, 17), // "saturationChanged"
QT_MOC_LITERAL(8, 189, 22), // "sharpeningLevelChanged"
QT_MOC_LITERAL(9, 212, 21), // "denoisingLevelChanged"
QT_MOC_LITERAL(10, 234, 18), // "colorFilterChanged"
QT_MOC_LITERAL(11, 253, 16), // "availableChanged"
QT_MOC_LITERAL(12, 270, 28), // "supportedColorFiltersChanged"
QT_MOC_LITERAL(13, 299, 33), // "supportedWhiteBalanceModesCha..."
QT_MOC_LITERAL(14, 333, 19), // "setWhiteBalanceMode"
QT_MOC_LITERAL(15, 353, 4), // "mode"
QT_MOC_LITERAL(16, 358, 21), // "setManualWhiteBalance"
QT_MOC_LITERAL(17, 380, 9), // "colorTemp"
QT_MOC_LITERAL(18, 390, 13), // "setBrightness"
QT_MOC_LITERAL(19, 404, 5), // "value"
QT_MOC_LITERAL(20, 410, 11), // "setContrast"
QT_MOC_LITERAL(21, 422, 13), // "setSaturation"
QT_MOC_LITERAL(22, 436, 18), // "setSharpeningLevel"
QT_MOC_LITERAL(23, 455, 17), // "setDenoisingLevel"
QT_MOC_LITERAL(24, 473, 14), // "setColorFilter"
QT_MOC_LITERAL(25, 488, 11), // "ColorFilter"
QT_MOC_LITERAL(26, 500, 11), // "colorFilter"
QT_MOC_LITERAL(27, 512, 16), // "whiteBalanceMode"
QT_MOC_LITERAL(28, 529, 16), // "WhiteBalanceMode"
QT_MOC_LITERAL(29, 546, 18), // "manualWhiteBalance"
QT_MOC_LITERAL(30, 565, 10), // "brightness"
QT_MOC_LITERAL(31, 576, 8), // "contrast"
QT_MOC_LITERAL(32, 585, 10), // "saturation"
QT_MOC_LITERAL(33, 596, 15), // "sharpeningLevel"
QT_MOC_LITERAL(34, 612, 14), // "denoisingLevel"
QT_MOC_LITERAL(35, 627, 9), // "available"
QT_MOC_LITERAL(36, 637, 21), // "supportedColorFilters"
QT_MOC_LITERAL(37, 659, 26), // "supportedWhiteBalanceModes"
QT_MOC_LITERAL(38, 686, 16), // "WhiteBalanceAuto"
QT_MOC_LITERAL(39, 703, 18), // "WhiteBalanceManual"
QT_MOC_LITERAL(40, 722, 20), // "WhiteBalanceSunlight"
QT_MOC_LITERAL(41, 743, 18), // "WhiteBalanceCloudy"
QT_MOC_LITERAL(42, 762, 17), // "WhiteBalanceShade"
QT_MOC_LITERAL(43, 780, 20), // "WhiteBalanceTungsten"
QT_MOC_LITERAL(44, 801, 23), // "WhiteBalanceFluorescent"
QT_MOC_LITERAL(45, 825, 17), // "WhiteBalanceFlash"
QT_MOC_LITERAL(46, 843, 18), // "WhiteBalanceSunset"
QT_MOC_LITERAL(47, 862, 18), // "WhiteBalanceVendor"
QT_MOC_LITERAL(48, 881, 15), // "ColorFilterNone"
QT_MOC_LITERAL(49, 897, 20), // "ColorFilterGrayscale"
QT_MOC_LITERAL(50, 918, 19), // "ColorFilterNegative"
QT_MOC_LITERAL(51, 938, 19), // "ColorFilterSolarize"
QT_MOC_LITERAL(52, 958, 16), // "ColorFilterSepia"
QT_MOC_LITERAL(53, 975, 20), // "ColorFilterPosterize"
QT_MOC_LITERAL(54, 996, 21), // "ColorFilterWhiteboard"
QT_MOC_LITERAL(55, 1018, 21), // "ColorFilterBlackboard"
QT_MOC_LITERAL(56, 1040, 15), // "ColorFilterAqua"
QT_MOC_LITERAL(57, 1056, 17) // "ColorFilterVendor"

    },
    "QDeclarativeCameraImageProcessing\0"
    "whiteBalanceModeChanged\0\0"
    "QDeclarativeCameraImageProcessing::WhiteBalanceMode\0"
    "manualWhiteBalanceChanged\0brightnessChanged\0"
    "contrastChanged\0saturationChanged\0"
    "sharpeningLevelChanged\0denoisingLevelChanged\0"
    "colorFilterChanged\0availableChanged\0"
    "supportedColorFiltersChanged\0"
    "supportedWhiteBalanceModesChanged\0"
    "setWhiteBalanceMode\0mode\0setManualWhiteBalance\0"
    "colorTemp\0setBrightness\0value\0setContrast\0"
    "setSaturation\0setSharpeningLevel\0"
    "setDenoisingLevel\0setColorFilter\0"
    "ColorFilter\0colorFilter\0whiteBalanceMode\0"
    "WhiteBalanceMode\0manualWhiteBalance\0"
    "brightness\0contrast\0saturation\0"
    "sharpeningLevel\0denoisingLevel\0available\0"
    "supportedColorFilters\0supportedWhiteBalanceModes\0"
    "WhiteBalanceAuto\0WhiteBalanceManual\0"
    "WhiteBalanceSunlight\0WhiteBalanceCloudy\0"
    "WhiteBalanceShade\0WhiteBalanceTungsten\0"
    "WhiteBalanceFluorescent\0WhiteBalanceFlash\0"
    "WhiteBalanceSunset\0WhiteBalanceVendor\0"
    "ColorFilterNone\0ColorFilterGrayscale\0"
    "ColorFilterNegative\0ColorFilterSolarize\0"
    "ColorFilterSepia\0ColorFilterPosterize\0"
    "ColorFilterWhiteboard\0ColorFilterBlackboard\0"
    "ColorFilterAqua\0ColorFilterVendor"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeCameraImageProcessing[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
      11,  177, // properties
       2,  232, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  128,    2, 0x06 /* Public */,
       4,    1,  131,    2, 0x06 /* Public */,
       5,    1,  134,    2, 0x86 /* Public | MethodRevisioned */,
       6,    1,  137,    2, 0x06 /* Public */,
       7,    1,  140,    2, 0x06 /* Public */,
       8,    1,  143,    2, 0x06 /* Public */,
       9,    1,  146,    2, 0x06 /* Public */,
      10,    0,  149,    2, 0x06 /* Public */,
      11,    0,  150,    2, 0x06 /* Public */,
      12,    0,  151,    2, 0x06 /* Public */,
      13,    0,  152,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,  153,    2, 0x0a /* Public */,
      16,    1,  156,    2, 0x0a /* Public */,
      18,    1,  159,    2, 0x8a /* Public | MethodRevisioned */,
      20,    1,  162,    2, 0x0a /* Public */,
      21,    1,  165,    2, 0x0a /* Public */,
      22,    1,  168,    2, 0x0a /* Public */,
      23,    1,  171,    2, 0x0a /* Public */,
      24,    1,  174,    2, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // slots: revision
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,   15,
    QMetaType::Void, QMetaType::QReal,   17,
    QMetaType::Void, QMetaType::QReal,   19,
    QMetaType::Void, QMetaType::QReal,   19,
    QMetaType::Void, QMetaType::QReal,   19,
    QMetaType::Void, QMetaType::QReal,   19,
    QMetaType::Void, QMetaType::QReal,   19,
    QMetaType::Void, 0x80000000 | 25,   26,

 // properties: name, type, flags
      27, 0x80000000 | 28, 0x0049510b,
      29, QMetaType::QReal, 0x00495103,
      30, QMetaType::QReal, 0x00c95103,
      31, QMetaType::QReal, 0x00495103,
      32, QMetaType::QReal, 0x00495103,
      33, QMetaType::QReal, 0x00495103,
      34, QMetaType::QReal, 0x00495103,
      26, 0x80000000 | 25, 0x00c9510b,
      35, QMetaType::Bool, 0x00c95001,
      36, QMetaType::QVariantList, 0x00c95001,
      37, QMetaType::QVariantList, 0x00c95001,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,

 // properties: revision
       0,
       0,
       2,
       0,
       0,
       0,
       0,
       1,
       3,
       3,
       3,

 // enums: name, alias, flags, count, data
      28,   28, 0x0,   10,  242,
      25,   25, 0x0,   10,  262,

 // enum data: key, value
      38, uint(QDeclarativeCameraImageProcessing::WhiteBalanceAuto),
      39, uint(QDeclarativeCameraImageProcessing::WhiteBalanceManual),
      40, uint(QDeclarativeCameraImageProcessing::WhiteBalanceSunlight),
      41, uint(QDeclarativeCameraImageProcessing::WhiteBalanceCloudy),
      42, uint(QDeclarativeCameraImageProcessing::WhiteBalanceShade),
      43, uint(QDeclarativeCameraImageProcessing::WhiteBalanceTungsten),
      44, uint(QDeclarativeCameraImageProcessing::WhiteBalanceFluorescent),
      45, uint(QDeclarativeCameraImageProcessing::WhiteBalanceFlash),
      46, uint(QDeclarativeCameraImageProcessing::WhiteBalanceSunset),
      47, uint(QDeclarativeCameraImageProcessing::WhiteBalanceVendor),
      48, uint(QDeclarativeCameraImageProcessing::ColorFilterNone),
      49, uint(QDeclarativeCameraImageProcessing::ColorFilterGrayscale),
      50, uint(QDeclarativeCameraImageProcessing::ColorFilterNegative),
      51, uint(QDeclarativeCameraImageProcessing::ColorFilterSolarize),
      52, uint(QDeclarativeCameraImageProcessing::ColorFilterSepia),
      53, uint(QDeclarativeCameraImageProcessing::ColorFilterPosterize),
      54, uint(QDeclarativeCameraImageProcessing::ColorFilterWhiteboard),
      55, uint(QDeclarativeCameraImageProcessing::ColorFilterBlackboard),
      56, uint(QDeclarativeCameraImageProcessing::ColorFilterAqua),
      57, uint(QDeclarativeCameraImageProcessing::ColorFilterVendor),

       0        // eod
};

void QDeclarativeCameraImageProcessing::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->whiteBalanceModeChanged((*reinterpret_cast< QDeclarativeCameraImageProcessing::WhiteBalanceMode(*)>(_a[1]))); break;
        case 1: _t->manualWhiteBalanceChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->brightnessChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 3: _t->contrastChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 4: _t->saturationChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->sharpeningLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 6: _t->denoisingLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 7: _t->colorFilterChanged(); break;
        case 8: _t->availableChanged(); break;
        case 9: _t->supportedColorFiltersChanged(); break;
        case 10: _t->supportedWhiteBalanceModesChanged(); break;
        case 11: _t->setWhiteBalanceMode((*reinterpret_cast< QDeclarativeCameraImageProcessing::WhiteBalanceMode(*)>(_a[1]))); break;
        case 12: _t->setManualWhiteBalance((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 13: _t->setBrightness((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 14: _t->setContrast((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 15: _t->setSaturation((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 16: _t->setSharpeningLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 17: _t->setDenoisingLevel((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 18: _t->setColorFilter((*reinterpret_cast< ColorFilter(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(QDeclarativeCameraImageProcessing::WhiteBalanceMode ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::whiteBalanceModeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal ) const;
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::manualWhiteBalanceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::brightnessChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::contrastChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::saturationChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::sharpeningLevelChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::denoisingLevelChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::colorFilterChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::availableChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::supportedColorFiltersChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraImageProcessing::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraImageProcessing::supportedWhiteBalanceModesChanged)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< WhiteBalanceMode*>(_v) = _t->whiteBalanceMode(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->manualWhiteBalance(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->brightness(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->contrast(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->saturation(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->sharpeningLevel(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->denoisingLevel(); break;
        case 7: *reinterpret_cast< ColorFilter*>(_v) = _t->colorFilter(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->isAvailable(); break;
        case 9: *reinterpret_cast< QVariantList*>(_v) = _t->supportedColorFilters(); break;
        case 10: *reinterpret_cast< QVariantList*>(_v) = _t->supportedWhiteBalanceModes(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeCameraImageProcessing *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setWhiteBalanceMode(*reinterpret_cast< WhiteBalanceMode*>(_v)); break;
        case 1: _t->setManualWhiteBalance(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setBrightness(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setContrast(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setSaturation(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setSharpeningLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setDenoisingLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setColorFilter(*reinterpret_cast< ColorFilter*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeCameraImageProcessing::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeCameraImageProcessing.data,
    qt_meta_data_QDeclarativeCameraImageProcessing,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeCameraImageProcessing::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeCameraImageProcessing::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeCameraImageProcessing.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeCameraImageProcessing::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeCameraImageProcessing::whiteBalanceModeChanged(QDeclarativeCameraImageProcessing::WhiteBalanceMode _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< QDeclarativeCameraImageProcessing *>(this), &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeCameraImageProcessing::manualWhiteBalanceChanged(qreal _t1)const
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(const_cast< QDeclarativeCameraImageProcessing *>(this), &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeCameraImageProcessing::brightnessChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeCameraImageProcessing::contrastChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QDeclarativeCameraImageProcessing::saturationChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QDeclarativeCameraImageProcessing::sharpeningLevelChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QDeclarativeCameraImageProcessing::denoisingLevelChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QDeclarativeCameraImageProcessing::colorFilterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeCameraImageProcessing::availableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeCameraImageProcessing::supportedColorFiltersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QDeclarativeCameraImageProcessing::supportedWhiteBalanceModesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
