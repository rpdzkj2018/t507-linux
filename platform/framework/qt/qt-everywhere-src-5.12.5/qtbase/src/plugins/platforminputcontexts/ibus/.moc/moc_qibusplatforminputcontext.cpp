/****************************************************************************
** Meta object code from reading C++ file 'qibusplatforminputcontext.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qibusplatforminputcontext.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qibusplatforminputcontext.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QIBusPlatformInputContext_t {
    QByteArrayData data[29];
    char stringdata0[384];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QIBusPlatformInputContext_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QIBusPlatformInputContext_t qt_meta_stringdata_QIBusPlatformInputContext = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QIBusPlatformInputContext"
QT_MOC_LITERAL(1, 26, 10), // "commitText"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 12), // "QDBusVariant"
QT_MOC_LITERAL(4, 51, 4), // "text"
QT_MOC_LITERAL(5, 56, 17), // "updatePreeditText"
QT_MOC_LITERAL(6, 74, 10), // "cursor_pos"
QT_MOC_LITERAL(7, 85, 7), // "visible"
QT_MOC_LITERAL(8, 93, 15), // "forwardKeyEvent"
QT_MOC_LITERAL(9, 109, 6), // "keyval"
QT_MOC_LITERAL(10, 116, 7), // "keycode"
QT_MOC_LITERAL(11, 124, 5), // "state"
QT_MOC_LITERAL(12, 130, 17), // "cursorRectChanged"
QT_MOC_LITERAL(13, 148, 21), // "deleteSurroundingText"
QT_MOC_LITERAL(14, 170, 6), // "offset"
QT_MOC_LITERAL(15, 177, 7), // "n_chars"
QT_MOC_LITERAL(16, 185, 23), // "surroundingTextRequired"
QT_MOC_LITERAL(17, 209, 15), // "hidePreeditText"
QT_MOC_LITERAL(18, 225, 15), // "showPreeditText"
QT_MOC_LITERAL(19, 241, 19), // "filterEventFinished"
QT_MOC_LITERAL(20, 261, 24), // "QDBusPendingCallWatcher*"
QT_MOC_LITERAL(21, 286, 4), // "call"
QT_MOC_LITERAL(22, 291, 13), // "socketChanged"
QT_MOC_LITERAL(23, 305, 3), // "str"
QT_MOC_LITERAL(24, 309, 13), // "busRegistered"
QT_MOC_LITERAL(25, 323, 15), // "busUnregistered"
QT_MOC_LITERAL(26, 339, 12), // "connectToBus"
QT_MOC_LITERAL(27, 352, 19), // "globalEngineChanged"
QT_MOC_LITERAL(28, 372, 11) // "engine_name"

    },
    "QIBusPlatformInputContext\0commitText\0"
    "\0QDBusVariant\0text\0updatePreeditText\0"
    "cursor_pos\0visible\0forwardKeyEvent\0"
    "keyval\0keycode\0state\0cursorRectChanged\0"
    "deleteSurroundingText\0offset\0n_chars\0"
    "surroundingTextRequired\0hidePreeditText\0"
    "showPreeditText\0filterEventFinished\0"
    "QDBusPendingCallWatcher*\0call\0"
    "socketChanged\0str\0busRegistered\0"
    "busUnregistered\0connectToBus\0"
    "globalEngineChanged\0engine_name"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QIBusPlatformInputContext[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x0a /* Public */,
       5,    3,   87,    2, 0x0a /* Public */,
       8,    3,   94,    2, 0x0a /* Public */,
      12,    0,  101,    2, 0x0a /* Public */,
      13,    2,  102,    2, 0x0a /* Public */,
      16,    0,  107,    2, 0x0a /* Public */,
      17,    0,  108,    2, 0x0a /* Public */,
      18,    0,  109,    2, 0x0a /* Public */,
      19,    1,  110,    2, 0x0a /* Public */,
      22,    1,  113,    2, 0x0a /* Public */,
      24,    1,  116,    2, 0x0a /* Public */,
      25,    1,  119,    2, 0x0a /* Public */,
      26,    0,  122,    2, 0x0a /* Public */,
      27,    1,  123,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, QMetaType::UInt, QMetaType::Bool,    4,    6,    7,
    QMetaType::Void, QMetaType::UInt, QMetaType::UInt, QMetaType::UInt,    9,   10,   11,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::UInt,   14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void, QMetaType::QString,   23,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   28,

       0        // eod
};

void QIBusPlatformInputContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QIBusPlatformInputContext *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->commitText((*reinterpret_cast< const QDBusVariant(*)>(_a[1]))); break;
        case 1: _t->updatePreeditText((*reinterpret_cast< const QDBusVariant(*)>(_a[1])),(*reinterpret_cast< uint(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3]))); break;
        case 2: _t->forwardKeyEvent((*reinterpret_cast< uint(*)>(_a[1])),(*reinterpret_cast< uint(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        case 3: _t->cursorRectChanged(); break;
        case 4: _t->deleteSurroundingText((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< uint(*)>(_a[2]))); break;
        case 5: _t->surroundingTextRequired(); break;
        case 6: _t->hidePreeditText(); break;
        case 7: _t->showPreeditText(); break;
        case 8: _t->filterEventFinished((*reinterpret_cast< QDBusPendingCallWatcher*(*)>(_a[1]))); break;
        case 9: _t->socketChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->busRegistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->busUnregistered((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->connectToBus(); break;
        case 13: _t->globalEngineChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusVariant >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusVariant >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusPendingCallWatcher* >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QIBusPlatformInputContext::staticMetaObject = { {
    &QPlatformInputContext::staticMetaObject,
    qt_meta_stringdata_QIBusPlatformInputContext.data,
    qt_meta_data_QIBusPlatformInputContext,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QIBusPlatformInputContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QIBusPlatformInputContext::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QIBusPlatformInputContext.stringdata0))
        return static_cast<void*>(this);
    return QPlatformInputContext::qt_metacast(_clname);
}

int QIBusPlatformInputContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPlatformInputContext::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
