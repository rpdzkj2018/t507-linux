/****************************************************************************
** Meta object code from reading C++ file 'qquicklabel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquicklabel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicklabel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickLabel_t {
    QByteArrayData data[22];
    char stringdata0[340];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickLabel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickLabel_t qt_meta_stringdata_QQuickLabel = {
    {
QT_MOC_LITERAL(0, 0, 11), // "QQuickLabel"
QT_MOC_LITERAL(1, 12, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 34, 10), // "background"
QT_MOC_LITERAL(3, 45, 11), // "fontChanged"
QT_MOC_LITERAL(4, 57, 0), // ""
QT_MOC_LITERAL(5, 58, 17), // "backgroundChanged"
QT_MOC_LITERAL(6, 76, 14), // "paletteChanged"
QT_MOC_LITERAL(7, 91, 30), // "implicitBackgroundWidthChanged"
QT_MOC_LITERAL(8, 122, 31), // "implicitBackgroundHeightChanged"
QT_MOC_LITERAL(9, 154, 15), // "topInsetChanged"
QT_MOC_LITERAL(10, 170, 16), // "leftInsetChanged"
QT_MOC_LITERAL(11, 187, 17), // "rightInsetChanged"
QT_MOC_LITERAL(12, 205, 18), // "bottomInsetChanged"
QT_MOC_LITERAL(13, 224, 4), // "font"
QT_MOC_LITERAL(14, 229, 11), // "QQuickItem*"
QT_MOC_LITERAL(15, 241, 7), // "palette"
QT_MOC_LITERAL(16, 249, 23), // "implicitBackgroundWidth"
QT_MOC_LITERAL(17, 273, 24), // "implicitBackgroundHeight"
QT_MOC_LITERAL(18, 298, 8), // "topInset"
QT_MOC_LITERAL(19, 307, 9), // "leftInset"
QT_MOC_LITERAL(20, 317, 10), // "rightInset"
QT_MOC_LITERAL(21, 328, 11) // "bottomInset"

    },
    "QQuickLabel\0DeferredPropertyNames\0"
    "background\0fontChanged\0\0backgroundChanged\0"
    "paletteChanged\0implicitBackgroundWidthChanged\0"
    "implicitBackgroundHeightChanged\0"
    "topInsetChanged\0leftInsetChanged\0"
    "rightInsetChanged\0bottomInsetChanged\0"
    "font\0QQuickItem*\0palette\0"
    "implicitBackgroundWidth\0"
    "implicitBackgroundHeight\0topInset\0"
    "leftInset\0rightInset\0bottomInset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickLabel[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       9,   16, // methods
       9,   79, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   70,    4, 0x06 /* Public */,
       5,    0,   71,    4, 0x06 /* Public */,
       6,    0,   72,    4, 0x86 /* Public | MethodRevisioned */,
       7,    0,   73,    4, 0x86 /* Public | MethodRevisioned */,
       8,    0,   74,    4, 0x86 /* Public | MethodRevisioned */,
       9,    0,   75,    4, 0x86 /* Public | MethodRevisioned */,
      10,    0,   76,    4, 0x86 /* Public | MethodRevisioned */,
      11,    0,   77,    4, 0x86 /* Public | MethodRevisioned */,
      12,    0,   78,    4, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       3,
       5,
       5,
       5,
       5,
       5,
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      13, QMetaType::QFont, 0x00495103,
       2, 0x80000000 | 14, 0x0049590b,
      15, QMetaType::QPalette, 0x00c95907,
      16, QMetaType::QReal, 0x00c95801,
      17, QMetaType::QReal, 0x00c95801,
      18, QMetaType::QReal, 0x00c95907,
      19, QMetaType::QReal, 0x00c95907,
      20, QMetaType::QReal, 0x00c95907,
      21, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,

 // properties: revision
       0,
       0,
       3,
       5,
       5,
       5,
       5,
       5,
       5,

       0        // eod
};

void QQuickLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fontChanged(); break;
        case 1: _t->backgroundChanged(); break;
        case 2: _t->paletteChanged(); break;
        case 3: _t->implicitBackgroundWidthChanged(); break;
        case 4: _t->implicitBackgroundHeightChanged(); break;
        case 5: _t->topInsetChanged(); break;
        case 6: _t->leftInsetChanged(); break;
        case 7: _t->rightInsetChanged(); break;
        case 8: _t->bottomInsetChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::fontChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::backgroundChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::paletteChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::implicitBackgroundWidthChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::implicitBackgroundHeightChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::topInsetChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::leftInsetChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::rightInsetChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickLabel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickLabel::bottomInsetChanged)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickLabel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->background(); break;
        case 2: *reinterpret_cast< QPalette*>(_v) = _t->palette(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundWidth(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundHeight(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->topInset(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->leftInset(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->rightInset(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->bottomInset(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickLabel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 1: _t->setBackground(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 2: _t->setPalette(*reinterpret_cast< QPalette*>(_v)); break;
        case 5: _t->setTopInset(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setLeftInset(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setRightInset(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setBottomInset(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickLabel *_t = static_cast<QQuickLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 2: _t->resetPalette(); break;
        case 5: _t->resetTopInset(); break;
        case 6: _t->resetLeftInset(); break;
        case 7: _t->resetRightInset(); break;
        case 8: _t->resetBottomInset(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickLabel::staticMetaObject = { {
    &QQuickText::staticMetaObject,
    qt_meta_stringdata_QQuickLabel.data,
    qt_meta_data_QQuickLabel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickLabel.stringdata0))
        return static_cast<void*>(this);
    return QQuickText::qt_metacast(_clname);
}

int QQuickLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickText::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickLabel::fontChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickLabel::backgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickLabel::paletteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickLabel::implicitBackgroundWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickLabel::implicitBackgroundHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickLabel::topInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickLabel::leftInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickLabel::rightInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickLabel::bottomInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
