/****************************************************************************
** Meta object code from reading C++ file 'qvalueaxis.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../axis/valueaxis/qvalueaxis.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qvalueaxis.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtCharts__QValueAxis_t {
    QByteArrayData data[28];
    char stringdata0[346];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtCharts__QValueAxis_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtCharts__QValueAxis_t qt_meta_stringdata_QtCharts__QValueAxis = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QtCharts::QValueAxis"
QT_MOC_LITERAL(1, 21, 10), // "minChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 3), // "min"
QT_MOC_LITERAL(4, 37, 10), // "maxChanged"
QT_MOC_LITERAL(5, 48, 3), // "max"
QT_MOC_LITERAL(6, 52, 12), // "rangeChanged"
QT_MOC_LITERAL(7, 65, 16), // "tickCountChanged"
QT_MOC_LITERAL(8, 82, 9), // "tickCount"
QT_MOC_LITERAL(9, 92, 21), // "minorTickCountChanged"
QT_MOC_LITERAL(10, 114, 18), // "labelFormatChanged"
QT_MOC_LITERAL(11, 133, 6), // "format"
QT_MOC_LITERAL(12, 140, 19), // "tickIntervalChanged"
QT_MOC_LITERAL(13, 160, 8), // "interval"
QT_MOC_LITERAL(14, 169, 17), // "tickAnchorChanged"
QT_MOC_LITERAL(15, 187, 6), // "anchor"
QT_MOC_LITERAL(16, 194, 15), // "tickTypeChanged"
QT_MOC_LITERAL(17, 210, 20), // "QValueAxis::TickType"
QT_MOC_LITERAL(18, 231, 4), // "type"
QT_MOC_LITERAL(19, 236, 16), // "applyNiceNumbers"
QT_MOC_LITERAL(20, 253, 11), // "labelFormat"
QT_MOC_LITERAL(21, 265, 14), // "minorTickCount"
QT_MOC_LITERAL(22, 280, 10), // "tickAnchor"
QT_MOC_LITERAL(23, 291, 12), // "tickInterval"
QT_MOC_LITERAL(24, 304, 8), // "tickType"
QT_MOC_LITERAL(25, 313, 8), // "TickType"
QT_MOC_LITERAL(26, 322, 12), // "TicksDynamic"
QT_MOC_LITERAL(27, 335, 10) // "TicksFixed"

    },
    "QtCharts::QValueAxis\0minChanged\0\0min\0"
    "maxChanged\0max\0rangeChanged\0"
    "tickCountChanged\0tickCount\0"
    "minorTickCountChanged\0labelFormatChanged\0"
    "format\0tickIntervalChanged\0interval\0"
    "tickAnchorChanged\0anchor\0tickTypeChanged\0"
    "QValueAxis::TickType\0type\0applyNiceNumbers\0"
    "labelFormat\0minorTickCount\0tickAnchor\0"
    "tickInterval\0tickType\0TickType\0"
    "TicksDynamic\0TicksFixed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtCharts__QValueAxis[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       8,  104, // properties
       1,  144, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    1,   77,    2, 0x06 /* Public */,
       6,    2,   80,    2, 0x06 /* Public */,
       7,    1,   85,    2, 0x06 /* Public */,
       9,    1,   88,    2, 0x06 /* Public */,
      10,    1,   91,    2, 0x06 /* Public */,
      12,    1,   94,    2, 0x86 /* Public | MethodRevisioned */,
      14,    1,   97,    2, 0x86 /* Public | MethodRevisioned */,
      16,    1,  100,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      19,    0,  103,    2, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,
       1,

 // slots: revision
       0,

 // signals: parameters
    QMetaType::Void, QMetaType::QReal,    3,
    QMetaType::Void, QMetaType::QReal,    5,
    QMetaType::Void, QMetaType::QReal, QMetaType::QReal,    3,    5,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::QString,   11,
    QMetaType::Void, QMetaType::QReal,   13,
    QMetaType::Void, QMetaType::QReal,   15,
    QMetaType::Void, 0x80000000 | 17,   18,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
       8, QMetaType::Int, 0x00495103,
       3, QMetaType::QReal, 0x00495103,
       5, QMetaType::QReal, 0x00495103,
      20, QMetaType::QString, 0x00495103,
      21, QMetaType::Int, 0x00495103,
      22, QMetaType::QReal, 0x00c95103,
      23, QMetaType::QReal, 0x00c95103,
      24, 0x80000000 | 25, 0x00c9510b,

 // properties: notify_signal_id
       3,
       0,
       1,
       5,
       4,
       7,
       6,
       8,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       1,
       1,
       1,

 // enums: name, alias, flags, count, data
      25,   25, 0x0,    2,  149,

 // enum data: key, value
      26, uint(QtCharts::QValueAxis::TicksDynamic),
      27, uint(QtCharts::QValueAxis::TicksFixed),

       0        // eod
};

void QtCharts::QValueAxis::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QValueAxis *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->minChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 1: _t->maxChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->rangeChanged((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2]))); break;
        case 3: _t->tickCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->minorTickCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->labelFormatChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->tickIntervalChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 7: _t->tickAnchorChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 8: _t->tickTypeChanged((*reinterpret_cast< QValueAxis::TickType(*)>(_a[1]))); break;
        case 9: _t->applyNiceNumbers(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QValueAxis::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::minChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::maxChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(qreal , qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::rangeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::tickCountChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::minorTickCountChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::labelFormatChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::tickIntervalChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::tickAnchorChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QValueAxis::*)(QValueAxis::TickType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QValueAxis::tickTypeChanged)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QValueAxis *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->tickCount(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->min(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->max(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->labelFormat(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->minorTickCount(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->tickAnchor(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->tickInterval(); break;
        case 7: *reinterpret_cast< TickType*>(_v) = _t->tickType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QValueAxis *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTickCount(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setMin(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setMax(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setLabelFormat(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setMinorTickCount(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setTickAnchor(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setTickInterval(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setTickType(*reinterpret_cast< TickType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QtCharts::QValueAxis::staticMetaObject = { {
    &QAbstractAxis::staticMetaObject,
    qt_meta_stringdata_QtCharts__QValueAxis.data,
    qt_meta_data_QtCharts__QValueAxis,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtCharts::QValueAxis::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtCharts::QValueAxis::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtCharts__QValueAxis.stringdata0))
        return static_cast<void*>(this);
    return QAbstractAxis::qt_metacast(_clname);
}

int QtCharts::QValueAxis::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractAxis::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QtCharts::QValueAxis::minChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QtCharts::QValueAxis::maxChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QtCharts::QValueAxis::rangeChanged(qreal _t1, qreal _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QtCharts::QValueAxis::tickCountChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QtCharts::QValueAxis::minorTickCountChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QtCharts::QValueAxis::labelFormatChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QtCharts::QValueAxis::tickIntervalChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QtCharts::QValueAxis::tickAnchorChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QtCharts::QValueAxis::tickTypeChanged(QValueAxis::TickType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
