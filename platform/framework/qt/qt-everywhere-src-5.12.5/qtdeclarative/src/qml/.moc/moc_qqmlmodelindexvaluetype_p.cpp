/****************************************************************************
** Meta object code from reading C++ file 'qqmlmodelindexvaluetype_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../types/qqmlmodelindexvaluetype_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmlmodelindexvaluetype_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQmlModelIndexValueType_t {
    QByteArrayData data[11];
    char stringdata0[107];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlModelIndexValueType_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlModelIndexValueType_t qt_meta_stringdata_QQmlModelIndexValueType = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QQmlModelIndexValueType"
QT_MOC_LITERAL(1, 24, 8), // "toString"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 3), // "row"
QT_MOC_LITERAL(4, 38, 6), // "column"
QT_MOC_LITERAL(5, 45, 6), // "parent"
QT_MOC_LITERAL(6, 52, 11), // "QModelIndex"
QT_MOC_LITERAL(7, 64, 5), // "valid"
QT_MOC_LITERAL(8, 70, 5), // "model"
QT_MOC_LITERAL(9, 76, 19), // "QAbstractItemModel*"
QT_MOC_LITERAL(10, 96, 10) // "internalId"

    },
    "QQmlModelIndexValueType\0toString\0\0row\0"
    "column\0parent\0QModelIndex\0valid\0model\0"
    "QAbstractItemModel*\0internalId"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlModelIndexValueType[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       6,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::QString,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00095c01,
       4, QMetaType::Int, 0x00095c01,
       5, 0x80000000 | 6, 0x00095809,
       7, QMetaType::Bool, 0x00095c01,
       8, 0x80000000 | 9, 0x00095c09,
      10, QMetaType::ULongLong, 0x00095c01,

       0        // eod
};

void QQmlModelIndexValueType::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = reinterpret_cast<QQmlModelIndexValueType *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->toString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = reinterpret_cast<QQmlModelIndexValueType *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->row(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->column(); break;
        case 2: *reinterpret_cast< QModelIndex*>(_v) = _t->parent(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isValid(); break;
        case 4: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->model(); break;
        case 5: *reinterpret_cast< quint64*>(_v) = _t->internalId(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQmlModelIndexValueType::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QQmlModelIndexValueType.data,
    qt_meta_data_QQmlModelIndexValueType,
    qt_static_metacall,
    nullptr,
    nullptr
} };

struct qt_meta_stringdata_QQmlPersistentModelIndexValueType_t {
    QByteArrayData data[11];
    char stringdata0[117];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlPersistentModelIndexValueType_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlPersistentModelIndexValueType_t qt_meta_stringdata_QQmlPersistentModelIndexValueType = {
    {
QT_MOC_LITERAL(0, 0, 33), // "QQmlPersistentModelIndexValue..."
QT_MOC_LITERAL(1, 34, 8), // "toString"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 3), // "row"
QT_MOC_LITERAL(4, 48, 6), // "column"
QT_MOC_LITERAL(5, 55, 6), // "parent"
QT_MOC_LITERAL(6, 62, 11), // "QModelIndex"
QT_MOC_LITERAL(7, 74, 5), // "valid"
QT_MOC_LITERAL(8, 80, 5), // "model"
QT_MOC_LITERAL(9, 86, 19), // "QAbstractItemModel*"
QT_MOC_LITERAL(10, 106, 10) // "internalId"

    },
    "QQmlPersistentModelIndexValueType\0"
    "toString\0\0row\0column\0parent\0QModelIndex\0"
    "valid\0model\0QAbstractItemModel*\0"
    "internalId"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlPersistentModelIndexValueType[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       6,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::QString,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00095801,
       4, QMetaType::Int, 0x00095801,
       5, 0x80000000 | 6, 0x00095809,
       7, QMetaType::Bool, 0x00095801,
       8, 0x80000000 | 9, 0x00095809,
      10, QMetaType::ULongLong, 0x00095801,

       0        // eod
};

void QQmlPersistentModelIndexValueType::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = reinterpret_cast<QQmlPersistentModelIndexValueType *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->toString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = reinterpret_cast<QQmlPersistentModelIndexValueType *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->row(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->column(); break;
        case 2: *reinterpret_cast< QModelIndex*>(_v) = _t->parent(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isValid(); break;
        case 4: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->model(); break;
        case 5: *reinterpret_cast< quint64*>(_v) = _t->internalId(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQmlPersistentModelIndexValueType::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QQmlPersistentModelIndexValueType.data,
    qt_meta_data_QQmlPersistentModelIndexValueType,
    qt_static_metacall,
    nullptr,
    nullptr
} };

struct qt_meta_stringdata_QQmlItemSelectionRangeValueType_t {
    QByteArrayData data[27];
    char stringdata0[263];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlItemSelectionRangeValueType_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlItemSelectionRangeValueType_t qt_meta_stringdata_QQmlItemSelectionRangeValueType = {
    {
QT_MOC_LITERAL(0, 0, 31), // "QQmlItemSelectionRangeValueType"
QT_MOC_LITERAL(1, 32, 8), // "toString"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 8), // "contains"
QT_MOC_LITERAL(4, 51, 11), // "QModelIndex"
QT_MOC_LITERAL(5, 63, 5), // "index"
QT_MOC_LITERAL(6, 69, 3), // "row"
QT_MOC_LITERAL(7, 73, 6), // "column"
QT_MOC_LITERAL(8, 80, 11), // "parentIndex"
QT_MOC_LITERAL(9, 92, 10), // "intersects"
QT_MOC_LITERAL(10, 103, 19), // "QItemSelectionRange"
QT_MOC_LITERAL(11, 123, 5), // "other"
QT_MOC_LITERAL(12, 129, 11), // "intersected"
QT_MOC_LITERAL(13, 141, 3), // "top"
QT_MOC_LITERAL(14, 145, 4), // "left"
QT_MOC_LITERAL(15, 150, 6), // "bottom"
QT_MOC_LITERAL(16, 157, 5), // "right"
QT_MOC_LITERAL(17, 163, 5), // "width"
QT_MOC_LITERAL(18, 169, 6), // "height"
QT_MOC_LITERAL(19, 176, 7), // "topLeft"
QT_MOC_LITERAL(20, 184, 21), // "QPersistentModelIndex"
QT_MOC_LITERAL(21, 206, 11), // "bottomRight"
QT_MOC_LITERAL(22, 218, 6), // "parent"
QT_MOC_LITERAL(23, 225, 5), // "valid"
QT_MOC_LITERAL(24, 231, 5), // "empty"
QT_MOC_LITERAL(25, 237, 5), // "model"
QT_MOC_LITERAL(26, 243, 19) // "QAbstractItemModel*"

    },
    "QQmlItemSelectionRangeValueType\0"
    "toString\0\0contains\0QModelIndex\0index\0"
    "row\0column\0parentIndex\0intersects\0"
    "QItemSelectionRange\0other\0intersected\0"
    "top\0left\0bottom\0right\0width\0height\0"
    "topLeft\0QPersistentModelIndex\0bottomRight\0"
    "parent\0valid\0empty\0model\0QAbstractItemModel*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlItemSelectionRangeValueType[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
      12,   56, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x02 /* Public */,
       3,    1,   40,    2, 0x02 /* Public */,
       3,    3,   43,    2, 0x02 /* Public */,
       9,    1,   50,    2, 0x02 /* Public */,
      12,    1,   53,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::QString,
    QMetaType::Bool, 0x80000000 | 4,    5,
    QMetaType::Bool, QMetaType::Int, QMetaType::Int, 0x80000000 | 4,    6,    7,    8,
    QMetaType::Bool, 0x80000000 | 10,   11,
    0x80000000 | 10, 0x80000000 | 10,   11,

 // properties: name, type, flags
      13, QMetaType::Int, 0x00095801,
      14, QMetaType::Int, 0x00095801,
      15, QMetaType::Int, 0x00095801,
      16, QMetaType::Int, 0x00095801,
      17, QMetaType::Int, 0x00095801,
      18, QMetaType::Int, 0x00095801,
      19, 0x80000000 | 20, 0x00095809,
      21, 0x80000000 | 20, 0x00095809,
      22, 0x80000000 | 4, 0x00095809,
      23, QMetaType::Bool, 0x00095801,
      24, QMetaType::Bool, 0x00095801,
      25, 0x80000000 | 26, 0x00095809,

       0        // eod
};

void QQmlItemSelectionRangeValueType::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = reinterpret_cast<QQmlItemSelectionRangeValueType *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QString _r = _t->toString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        case 1: { bool _r = _t->contains((*reinterpret_cast< const QModelIndex(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: { bool _r = _t->contains((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QModelIndex(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 3: { bool _r = _t->intersects((*reinterpret_cast< const QItemSelectionRange(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { QItemSelectionRange _r = _t->intersected((*reinterpret_cast< const QItemSelectionRange(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QItemSelectionRange*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelectionRange >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelectionRange >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 11:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = reinterpret_cast<QQmlItemSelectionRangeValueType *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->top(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->left(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->bottom(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->right(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->width(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->height(); break;
        case 6: *reinterpret_cast< QPersistentModelIndex*>(_v) = _t->topLeft(); break;
        case 7: *reinterpret_cast< QPersistentModelIndex*>(_v) = _t->bottomRight(); break;
        case 8: *reinterpret_cast< QModelIndex*>(_v) = _t->parent(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->isValid(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isEmpty(); break;
        case 11: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->model(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQmlItemSelectionRangeValueType::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QQmlItemSelectionRangeValueType.data,
    qt_meta_data_QQmlItemSelectionRangeValueType,
    qt_static_metacall,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
