/****************************************************************************
** Meta object code from reading C++ file 'qquickabstractbutton_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickabstractbutton_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickabstractbutton_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickAbstractButton_t {
    QByteArrayData data[53];
    char stringdata0[730];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAbstractButton_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAbstractButton_t qt_meta_stringdata_QQuickAbstractButton = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickAbstractButton"
QT_MOC_LITERAL(1, 21, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 43, 32), // "background,contentItem,indicator"
QT_MOC_LITERAL(3, 76, 7), // "pressed"
QT_MOC_LITERAL(4, 84, 0), // ""
QT_MOC_LITERAL(5, 85, 8), // "released"
QT_MOC_LITERAL(6, 94, 8), // "canceled"
QT_MOC_LITERAL(7, 103, 7), // "clicked"
QT_MOC_LITERAL(8, 111, 12), // "pressAndHold"
QT_MOC_LITERAL(9, 124, 13), // "doubleClicked"
QT_MOC_LITERAL(10, 138, 11), // "textChanged"
QT_MOC_LITERAL(11, 150, 11), // "downChanged"
QT_MOC_LITERAL(12, 162, 14), // "pressedChanged"
QT_MOC_LITERAL(13, 177, 14), // "checkedChanged"
QT_MOC_LITERAL(14, 192, 16), // "checkableChanged"
QT_MOC_LITERAL(15, 209, 20), // "autoExclusiveChanged"
QT_MOC_LITERAL(16, 230, 17), // "autoRepeatChanged"
QT_MOC_LITERAL(17, 248, 16), // "indicatorChanged"
QT_MOC_LITERAL(18, 265, 7), // "toggled"
QT_MOC_LITERAL(19, 273, 11), // "iconChanged"
QT_MOC_LITERAL(20, 285, 14), // "displayChanged"
QT_MOC_LITERAL(21, 300, 13), // "actionChanged"
QT_MOC_LITERAL(22, 314, 22), // "autoRepeatDelayChanged"
QT_MOC_LITERAL(23, 337, 25), // "autoRepeatIntervalChanged"
QT_MOC_LITERAL(24, 363, 13), // "pressXChanged"
QT_MOC_LITERAL(25, 377, 13), // "pressYChanged"
QT_MOC_LITERAL(26, 391, 29), // "implicitIndicatorWidthChanged"
QT_MOC_LITERAL(27, 421, 30), // "implicitIndicatorHeightChanged"
QT_MOC_LITERAL(28, 452, 6), // "toggle"
QT_MOC_LITERAL(29, 459, 4), // "text"
QT_MOC_LITERAL(30, 464, 4), // "down"
QT_MOC_LITERAL(31, 469, 7), // "checked"
QT_MOC_LITERAL(32, 477, 9), // "checkable"
QT_MOC_LITERAL(33, 487, 13), // "autoExclusive"
QT_MOC_LITERAL(34, 501, 10), // "autoRepeat"
QT_MOC_LITERAL(35, 512, 9), // "indicator"
QT_MOC_LITERAL(36, 522, 11), // "QQuickItem*"
QT_MOC_LITERAL(37, 534, 4), // "icon"
QT_MOC_LITERAL(38, 539, 10), // "QQuickIcon"
QT_MOC_LITERAL(39, 550, 7), // "display"
QT_MOC_LITERAL(40, 558, 7), // "Display"
QT_MOC_LITERAL(41, 566, 6), // "action"
QT_MOC_LITERAL(42, 573, 13), // "QQuickAction*"
QT_MOC_LITERAL(43, 587, 15), // "autoRepeatDelay"
QT_MOC_LITERAL(44, 603, 18), // "autoRepeatInterval"
QT_MOC_LITERAL(45, 622, 6), // "pressX"
QT_MOC_LITERAL(46, 629, 6), // "pressY"
QT_MOC_LITERAL(47, 636, 22), // "implicitIndicatorWidth"
QT_MOC_LITERAL(48, 659, 23), // "implicitIndicatorHeight"
QT_MOC_LITERAL(49, 683, 8), // "IconOnly"
QT_MOC_LITERAL(50, 692, 8), // "TextOnly"
QT_MOC_LITERAL(51, 701, 14), // "TextBesideIcon"
QT_MOC_LITERAL(52, 716, 13) // "TextUnderIcon"

    },
    "QQuickAbstractButton\0DeferredPropertyNames\0"
    "background,contentItem,indicator\0"
    "pressed\0\0released\0canceled\0clicked\0"
    "pressAndHold\0doubleClicked\0textChanged\0"
    "downChanged\0pressedChanged\0checkedChanged\0"
    "checkableChanged\0autoExclusiveChanged\0"
    "autoRepeatChanged\0indicatorChanged\0"
    "toggled\0iconChanged\0displayChanged\0"
    "actionChanged\0autoRepeatDelayChanged\0"
    "autoRepeatIntervalChanged\0pressXChanged\0"
    "pressYChanged\0implicitIndicatorWidthChanged\0"
    "implicitIndicatorHeightChanged\0toggle\0"
    "text\0down\0checked\0checkable\0autoExclusive\0"
    "autoRepeat\0indicator\0QQuickItem*\0icon\0"
    "QQuickIcon\0display\0Display\0action\0"
    "QQuickAction*\0autoRepeatDelay\0"
    "autoRepeatInterval\0pressX\0pressY\0"
    "implicitIndicatorWidth\0implicitIndicatorHeight\0"
    "IconOnly\0TextOnly\0TextBesideIcon\0"
    "TextUnderIcon"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAbstractButton[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      25,   16, // methods
      17,  191, // properties
       1,  276, // enums/sets
       0,    0, // constructors
       0,       // flags
      24,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  166,    4, 0x06 /* Public */,
       5,    0,  167,    4, 0x06 /* Public */,
       6,    0,  168,    4, 0x06 /* Public */,
       7,    0,  169,    4, 0x06 /* Public */,
       8,    0,  170,    4, 0x06 /* Public */,
       9,    0,  171,    4, 0x06 /* Public */,
      10,    0,  172,    4, 0x06 /* Public */,
      11,    0,  173,    4, 0x06 /* Public */,
      12,    0,  174,    4, 0x06 /* Public */,
      13,    0,  175,    4, 0x06 /* Public */,
      14,    0,  176,    4, 0x06 /* Public */,
      15,    0,  177,    4, 0x06 /* Public */,
      16,    0,  178,    4, 0x06 /* Public */,
      17,    0,  179,    4, 0x06 /* Public */,
      18,    0,  180,    4, 0x86 /* Public | MethodRevisioned */,
      19,    0,  181,    4, 0x86 /* Public | MethodRevisioned */,
      20,    0,  182,    4, 0x86 /* Public | MethodRevisioned */,
      21,    0,  183,    4, 0x86 /* Public | MethodRevisioned */,
      22,    0,  184,    4, 0x86 /* Public | MethodRevisioned */,
      23,    0,  185,    4, 0x86 /* Public | MethodRevisioned */,
      24,    0,  186,    4, 0x86 /* Public | MethodRevisioned */,
      25,    0,  187,    4, 0x86 /* Public | MethodRevisioned */,
      26,    0,  188,    4, 0x86 /* Public | MethodRevisioned */,
      27,    0,  189,    4, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      28,    0,  190,    4, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       3,
       3,
       3,
       4,
       4,
       4,
       4,
       5,
       5,

 // slots: revision
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
      29, QMetaType::QString, 0x00495907,
      30, QMetaType::Bool, 0x00495907,
       3, QMetaType::Bool, 0x00495801,
      31, QMetaType::Bool, 0x00495903,
      32, QMetaType::Bool, 0x00495903,
      33, QMetaType::Bool, 0x00495903,
      34, QMetaType::Bool, 0x00495903,
      35, 0x80000000 | 36, 0x0049590b,
      37, 0x80000000 | 38, 0x00c9590b,
      39, 0x80000000 | 40, 0x00c9590b,
      41, 0x80000000 | 42, 0x00c9590b,
      43, QMetaType::Int, 0x00c95903,
      44, QMetaType::Int, 0x00c95903,
      45, QMetaType::QReal, 0x00c95801,
      46, QMetaType::QReal, 0x00c95801,
      47, QMetaType::QReal, 0x00c95801,
      48, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      13,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       3,
       3,
       4,
       4,
       4,
       4,
       5,
       5,

 // enums: name, alias, flags, count, data
      40,   40, 0x0,    4,  281,

 // enum data: key, value
      49, uint(QQuickAbstractButton::IconOnly),
      50, uint(QQuickAbstractButton::TextOnly),
      51, uint(QQuickAbstractButton::TextBesideIcon),
      52, uint(QQuickAbstractButton::TextUnderIcon),

       0        // eod
};

void QQuickAbstractButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickAbstractButton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressed(); break;
        case 1: _t->released(); break;
        case 2: _t->canceled(); break;
        case 3: _t->clicked(); break;
        case 4: _t->pressAndHold(); break;
        case 5: _t->doubleClicked(); break;
        case 6: _t->textChanged(); break;
        case 7: _t->downChanged(); break;
        case 8: _t->pressedChanged(); break;
        case 9: _t->checkedChanged(); break;
        case 10: _t->checkableChanged(); break;
        case 11: _t->autoExclusiveChanged(); break;
        case 12: _t->autoRepeatChanged(); break;
        case 13: _t->indicatorChanged(); break;
        case 14: _t->toggled(); break;
        case 15: _t->iconChanged(); break;
        case 16: _t->displayChanged(); break;
        case 17: _t->actionChanged(); break;
        case 18: _t->autoRepeatDelayChanged(); break;
        case 19: _t->autoRepeatIntervalChanged(); break;
        case 20: _t->pressXChanged(); break;
        case 21: _t->pressYChanged(); break;
        case 22: _t->implicitIndicatorWidthChanged(); break;
        case 23: _t->implicitIndicatorHeightChanged(); break;
        case 24: _t->toggle(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::pressed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::released)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::canceled)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::clicked)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::pressAndHold)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::doubleClicked)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::textChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::downChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::pressedChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::checkedChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::checkableChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::autoExclusiveChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::autoRepeatChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::indicatorChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::toggled)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::iconChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::displayChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::actionChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::autoRepeatDelayChanged)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::autoRepeatIntervalChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::pressXChanged)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::pressYChanged)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::implicitIndicatorWidthChanged)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractButton::implicitIndicatorHeightChanged)) {
                *result = 23;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickAbstractButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isDown(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isChecked(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isCheckable(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->autoExclusive(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->autoRepeat(); break;
        case 7: *reinterpret_cast< QQuickItem**>(_v) = _t->indicator(); break;
        case 8: *reinterpret_cast< QQuickIcon*>(_v) = _t->icon(); break;
        case 9: *reinterpret_cast< Display*>(_v) = _t->display(); break;
        case 10: *reinterpret_cast< QQuickAction**>(_v) = _t->action(); break;
        case 11: *reinterpret_cast< int*>(_v) = _t->autoRepeatDelay(); break;
        case 12: *reinterpret_cast< int*>(_v) = _t->autoRepeatInterval(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->pressX(); break;
        case 14: *reinterpret_cast< qreal*>(_v) = _t->pressY(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->implicitIndicatorWidth(); break;
        case 16: *reinterpret_cast< qreal*>(_v) = _t->implicitIndicatorHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickAbstractButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setDown(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setChecked(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setCheckable(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setAutoExclusive(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setAutoRepeat(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setIndicator(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 8: _t->setIcon(*reinterpret_cast< QQuickIcon*>(_v)); break;
        case 9: _t->setDisplay(*reinterpret_cast< Display*>(_v)); break;
        case 10: _t->setAction(*reinterpret_cast< QQuickAction**>(_v)); break;
        case 11: _t->setAutoRepeatDelay(*reinterpret_cast< int*>(_v)); break;
        case 12: _t->setAutoRepeatInterval(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickAbstractButton *_t = static_cast<QQuickAbstractButton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetText(); break;
        case 1: _t->resetDown(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickAbstractButton::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickAbstractButton.data,
    qt_meta_data_QQuickAbstractButton,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickAbstractButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAbstractButton::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAbstractButton.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickAbstractButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 17;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 17;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 17;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 17;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 17;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickAbstractButton::pressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickAbstractButton::released()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickAbstractButton::canceled()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickAbstractButton::clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickAbstractButton::pressAndHold()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickAbstractButton::doubleClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickAbstractButton::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickAbstractButton::downChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickAbstractButton::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickAbstractButton::checkedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickAbstractButton::checkableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickAbstractButton::autoExclusiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickAbstractButton::autoRepeatChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickAbstractButton::indicatorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickAbstractButton::toggled()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QQuickAbstractButton::iconChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QQuickAbstractButton::displayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void QQuickAbstractButton::actionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void QQuickAbstractButton::autoRepeatDelayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void QQuickAbstractButton::autoRepeatIntervalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void QQuickAbstractButton::pressXChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void QQuickAbstractButton::pressYChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, nullptr);
}

// SIGNAL 22
void QQuickAbstractButton::implicitIndicatorWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void QQuickAbstractButton::implicitIndicatorHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
