QT.network.enabled_features = networkinterface bearermanagement dnslookup ftp http localserver networkdiskcache networkproxy socks5 udpsocket
QT.network.disabled_features = dtls opensslv11 sctp ssl
QT.network.QT_CONFIG = networkinterface bearermanagement ftp getifaddrs http ipv6ifname localserver networkdiskcache networkproxy socks5 udpsocket
QT.network.exports = 
QT.network_private.enabled_features = linux-netlink
QT.network_private.disabled_features = openssl openssl-linked libproxy securetransport system-proxies
QT.network_private.libraries = network
QMAKE_LIBS_NETWORK = 
