/****************************************************************************
** Meta object code from reading C++ file 'qgamepadmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qgamepadmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgamepadmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGamepadManager_t {
    QByteArrayData data[62];
    char stringdata0[940];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGamepadManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGamepadManager_t qt_meta_stringdata_QGamepadManager = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QGamepadManager"
QT_MOC_LITERAL(1, 16, 24), // "connectedGamepadsChanged"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 16), // "gamepadConnected"
QT_MOC_LITERAL(4, 59, 8), // "deviceId"
QT_MOC_LITERAL(5, 68, 18), // "gamepadNameChanged"
QT_MOC_LITERAL(6, 87, 4), // "name"
QT_MOC_LITERAL(7, 92, 19), // "gamepadDisconnected"
QT_MOC_LITERAL(8, 112, 16), // "gamepadAxisEvent"
QT_MOC_LITERAL(9, 129, 28), // "QGamepadManager::GamepadAxis"
QT_MOC_LITERAL(10, 158, 4), // "axis"
QT_MOC_LITERAL(11, 163, 5), // "value"
QT_MOC_LITERAL(12, 169, 23), // "gamepadButtonPressEvent"
QT_MOC_LITERAL(13, 193, 30), // "QGamepadManager::GamepadButton"
QT_MOC_LITERAL(14, 224, 6), // "button"
QT_MOC_LITERAL(15, 231, 25), // "gamepadButtonReleaseEvent"
QT_MOC_LITERAL(16, 257, 16), // "buttonConfigured"
QT_MOC_LITERAL(17, 274, 14), // "axisConfigured"
QT_MOC_LITERAL(18, 289, 21), // "configurationCanceled"
QT_MOC_LITERAL(19, 311, 21), // "isConfigurationNeeded"
QT_MOC_LITERAL(20, 333, 15), // "configureButton"
QT_MOC_LITERAL(21, 349, 13), // "GamepadButton"
QT_MOC_LITERAL(22, 363, 13), // "configureAxis"
QT_MOC_LITERAL(23, 377, 11), // "GamepadAxis"
QT_MOC_LITERAL(24, 389, 24), // "setCancelConfigureButton"
QT_MOC_LITERAL(25, 414, 18), // "resetConfiguration"
QT_MOC_LITERAL(26, 433, 15), // "setSettingsFile"
QT_MOC_LITERAL(27, 449, 4), // "file"
QT_MOC_LITERAL(28, 454, 26), // "_q_forwardGamepadConnected"
QT_MOC_LITERAL(29, 481, 28), // "_q_forwardGamepadNameChanged"
QT_MOC_LITERAL(30, 510, 29), // "_q_forwardGamepadDisconnected"
QT_MOC_LITERAL(31, 540, 26), // "_q_forwardGamepadAxisEvent"
QT_MOC_LITERAL(32, 567, 33), // "_q_forwardGamepadButtonPressE..."
QT_MOC_LITERAL(33, 601, 35), // "_q_forwardGamepadButtonReleas..."
QT_MOC_LITERAL(34, 637, 17), // "connectedGamepads"
QT_MOC_LITERAL(35, 655, 10), // "QList<int>"
QT_MOC_LITERAL(36, 666, 13), // "ButtonInvalid"
QT_MOC_LITERAL(37, 680, 7), // "ButtonA"
QT_MOC_LITERAL(38, 688, 7), // "ButtonB"
QT_MOC_LITERAL(39, 696, 7), // "ButtonX"
QT_MOC_LITERAL(40, 704, 7), // "ButtonY"
QT_MOC_LITERAL(41, 712, 8), // "ButtonL1"
QT_MOC_LITERAL(42, 721, 8), // "ButtonR1"
QT_MOC_LITERAL(43, 730, 8), // "ButtonL2"
QT_MOC_LITERAL(44, 739, 8), // "ButtonR2"
QT_MOC_LITERAL(45, 748, 12), // "ButtonSelect"
QT_MOC_LITERAL(46, 761, 11), // "ButtonStart"
QT_MOC_LITERAL(47, 773, 8), // "ButtonL3"
QT_MOC_LITERAL(48, 782, 8), // "ButtonR3"
QT_MOC_LITERAL(49, 791, 8), // "ButtonUp"
QT_MOC_LITERAL(50, 800, 10), // "ButtonDown"
QT_MOC_LITERAL(51, 811, 11), // "ButtonRight"
QT_MOC_LITERAL(52, 823, 10), // "ButtonLeft"
QT_MOC_LITERAL(53, 834, 12), // "ButtonCenter"
QT_MOC_LITERAL(54, 847, 11), // "ButtonGuide"
QT_MOC_LITERAL(55, 859, 14), // "GamepadButtons"
QT_MOC_LITERAL(56, 874, 11), // "AxisInvalid"
QT_MOC_LITERAL(57, 886, 9), // "AxisLeftX"
QT_MOC_LITERAL(58, 896, 9), // "AxisLeftY"
QT_MOC_LITERAL(59, 906, 10), // "AxisRightX"
QT_MOC_LITERAL(60, 917, 10), // "AxisRightY"
QT_MOC_LITERAL(61, 928, 11) // "GamepadAxes"

    },
    "QGamepadManager\0connectedGamepadsChanged\0"
    "\0gamepadConnected\0deviceId\0"
    "gamepadNameChanged\0name\0gamepadDisconnected\0"
    "gamepadAxisEvent\0QGamepadManager::GamepadAxis\0"
    "axis\0value\0gamepadButtonPressEvent\0"
    "QGamepadManager::GamepadButton\0button\0"
    "gamepadButtonReleaseEvent\0buttonConfigured\0"
    "axisConfigured\0configurationCanceled\0"
    "isConfigurationNeeded\0configureButton\0"
    "GamepadButton\0configureAxis\0GamepadAxis\0"
    "setCancelConfigureButton\0resetConfiguration\0"
    "setSettingsFile\0file\0_q_forwardGamepadConnected\0"
    "_q_forwardGamepadNameChanged\0"
    "_q_forwardGamepadDisconnected\0"
    "_q_forwardGamepadAxisEvent\0"
    "_q_forwardGamepadButtonPressEvent\0"
    "_q_forwardGamepadButtonReleaseEvent\0"
    "connectedGamepads\0QList<int>\0ButtonInvalid\0"
    "ButtonA\0ButtonB\0ButtonX\0ButtonY\0"
    "ButtonL1\0ButtonR1\0ButtonL2\0ButtonR2\0"
    "ButtonSelect\0ButtonStart\0ButtonL3\0"
    "ButtonR3\0ButtonUp\0ButtonDown\0ButtonRight\0"
    "ButtonLeft\0ButtonCenter\0ButtonGuide\0"
    "GamepadButtons\0AxisInvalid\0AxisLeftX\0"
    "AxisLeftY\0AxisRightX\0AxisRightY\0"
    "GamepadAxes"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGamepadManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       1,  222, // properties
       4,  226, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  124,    2, 0x06 /* Public */,
       3,    1,  125,    2, 0x06 /* Public */,
       5,    2,  128,    2, 0x06 /* Public */,
       7,    1,  133,    2, 0x06 /* Public */,
       8,    3,  136,    2, 0x06 /* Public */,
      12,    3,  143,    2, 0x06 /* Public */,
      15,    2,  150,    2, 0x06 /* Public */,
      16,    2,  155,    2, 0x06 /* Public */,
      17,    2,  160,    2, 0x06 /* Public */,
      18,    1,  165,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,  168,    2, 0x0a /* Public */,
      20,    2,  171,    2, 0x0a /* Public */,
      22,    2,  176,    2, 0x0a /* Public */,
      24,    2,  181,    2, 0x0a /* Public */,
      25,    1,  186,    2, 0x0a /* Public */,
      26,    1,  189,    2, 0x0a /* Public */,
      28,    1,  192,    2, 0x08 /* Private */,
      29,    2,  195,    2, 0x08 /* Private */,
      30,    1,  200,    2, 0x08 /* Private */,
      31,    3,  203,    2, 0x08 /* Private */,
      32,    3,  210,    2, 0x08 /* Private */,
      33,    2,  217,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    4,    6,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9, QMetaType::Double,    4,   10,   11,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13, QMetaType::Double,    4,   14,   11,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13,    4,   14,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13,    4,   14,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9,    4,   10,
    QMetaType::Void, QMetaType::Int,    4,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int,    4,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 21,    4,   14,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 23,    4,   10,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 21,    4,   14,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::QString,   27,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    2,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9, QMetaType::Double,    2,    2,    2,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13, QMetaType::Double,    2,    2,    2,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13,    2,    2,

 // properties: name, type, flags
      34, 0x80000000 | 35, 0x00495009,

 // properties: notify_signal_id
       0,

 // enums: name, alias, flags, count, data
      21,   21, 0x1,   19,  246,
      55,   21, 0x1,   19,  284,
      23,   23, 0x1,    5,  322,
      61,   23, 0x1,    5,  332,

 // enum data: key, value
      36, uint(QGamepadManager::ButtonInvalid),
      37, uint(QGamepadManager::ButtonA),
      38, uint(QGamepadManager::ButtonB),
      39, uint(QGamepadManager::ButtonX),
      40, uint(QGamepadManager::ButtonY),
      41, uint(QGamepadManager::ButtonL1),
      42, uint(QGamepadManager::ButtonR1),
      43, uint(QGamepadManager::ButtonL2),
      44, uint(QGamepadManager::ButtonR2),
      45, uint(QGamepadManager::ButtonSelect),
      46, uint(QGamepadManager::ButtonStart),
      47, uint(QGamepadManager::ButtonL3),
      48, uint(QGamepadManager::ButtonR3),
      49, uint(QGamepadManager::ButtonUp),
      50, uint(QGamepadManager::ButtonDown),
      51, uint(QGamepadManager::ButtonRight),
      52, uint(QGamepadManager::ButtonLeft),
      53, uint(QGamepadManager::ButtonCenter),
      54, uint(QGamepadManager::ButtonGuide),
      36, uint(QGamepadManager::ButtonInvalid),
      37, uint(QGamepadManager::ButtonA),
      38, uint(QGamepadManager::ButtonB),
      39, uint(QGamepadManager::ButtonX),
      40, uint(QGamepadManager::ButtonY),
      41, uint(QGamepadManager::ButtonL1),
      42, uint(QGamepadManager::ButtonR1),
      43, uint(QGamepadManager::ButtonL2),
      44, uint(QGamepadManager::ButtonR2),
      45, uint(QGamepadManager::ButtonSelect),
      46, uint(QGamepadManager::ButtonStart),
      47, uint(QGamepadManager::ButtonL3),
      48, uint(QGamepadManager::ButtonR3),
      49, uint(QGamepadManager::ButtonUp),
      50, uint(QGamepadManager::ButtonDown),
      51, uint(QGamepadManager::ButtonRight),
      52, uint(QGamepadManager::ButtonLeft),
      53, uint(QGamepadManager::ButtonCenter),
      54, uint(QGamepadManager::ButtonGuide),
      56, uint(QGamepadManager::AxisInvalid),
      57, uint(QGamepadManager::AxisLeftX),
      58, uint(QGamepadManager::AxisLeftY),
      59, uint(QGamepadManager::AxisRightX),
      60, uint(QGamepadManager::AxisRightY),
      56, uint(QGamepadManager::AxisInvalid),
      57, uint(QGamepadManager::AxisLeftX),
      58, uint(QGamepadManager::AxisLeftY),
      59, uint(QGamepadManager::AxisRightX),
      60, uint(QGamepadManager::AxisRightY),

       0        // eod
};

void QGamepadManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QGamepadManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->connectedGamepadsChanged(); break;
        case 1: _t->gamepadConnected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->gamepadNameChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 3: _t->gamepadDisconnected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->gamepadAxisEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadAxis(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 5: _t->gamepadButtonPressEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadButton(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 6: _t->gamepadButtonReleaseEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadButton(*)>(_a[2]))); break;
        case 7: _t->buttonConfigured((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadButton(*)>(_a[2]))); break;
        case 8: _t->axisConfigured((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadAxis(*)>(_a[2]))); break;
        case 9: _t->configurationCanceled((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: { bool _r = _t->isConfigurationNeeded((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: { bool _r = _t->configureButton((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< GamepadButton(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 12: { bool _r = _t->configureAxis((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< GamepadAxis(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 13: { bool _r = _t->setCancelConfigureButton((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< GamepadButton(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->resetConfiguration((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->setSettingsFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->d_func()->_q_forwardGamepadConnected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->d_func()->_q_forwardGamepadNameChanged((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 18: _t->d_func()->_q_forwardGamepadDisconnected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->d_func()->_q_forwardGamepadAxisEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadAxis(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 20: _t->d_func()->_q_forwardGamepadButtonPressEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadButton(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3]))); break;
        case 21: _t->d_func()->_q_forwardGamepadButtonReleaseEvent((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QGamepadManager::GamepadButton(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadAxis >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadButton >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadButton >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadButton >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadAxis >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadAxis >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadButton >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGamepadManager::GamepadButton >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QGamepadManager::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::connectedGamepadsChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadConnected)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadNameChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadDisconnected)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , QGamepadManager::GamepadAxis , double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadAxisEvent)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , QGamepadManager::GamepadButton , double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadButtonPressEvent)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , QGamepadManager::GamepadButton );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::gamepadButtonReleaseEvent)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , QGamepadManager::GamepadButton );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::buttonConfigured)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int , QGamepadManager::GamepadAxis );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::axisConfigured)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QGamepadManager::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGamepadManager::configurationCanceled)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<int> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QGamepadManager *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QList<int>*>(_v) = _t->connectedGamepads(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QGamepadManager::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QGamepadManager.data,
    qt_meta_data_QGamepadManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGamepadManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGamepadManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGamepadManager.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QGamepadManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QGamepadManager::connectedGamepadsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QGamepadManager::gamepadConnected(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QGamepadManager::gamepadNameChanged(int _t1, const QString & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QGamepadManager::gamepadDisconnected(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QGamepadManager::gamepadAxisEvent(int _t1, QGamepadManager::GamepadAxis _t2, double _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QGamepadManager::gamepadButtonPressEvent(int _t1, QGamepadManager::GamepadButton _t2, double _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QGamepadManager::gamepadButtonReleaseEvent(int _t1, QGamepadManager::GamepadButton _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QGamepadManager::buttonConfigured(int _t1, QGamepadManager::GamepadButton _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QGamepadManager::axisConfigured(int _t1, QGamepadManager::GamepadAxis _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QGamepadManager::configurationCanceled(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
