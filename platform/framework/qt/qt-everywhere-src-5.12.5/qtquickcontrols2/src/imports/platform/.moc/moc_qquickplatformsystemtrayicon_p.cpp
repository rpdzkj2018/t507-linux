/****************************************************************************
** Meta object code from reading C++ file 'qquickplatformsystemtrayicon_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickplatformsystemtrayicon_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickplatformsystemtrayicon_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickPlatformSystemTrayIcon_t {
    QByteArrayData data[33];
    char stringdata0[428];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPlatformSystemTrayIcon_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPlatformSystemTrayIcon_t qt_meta_stringdata_QQuickPlatformSystemTrayIcon = {
    {
QT_MOC_LITERAL(0, 0, 28), // "QQuickPlatformSystemTrayIcon"
QT_MOC_LITERAL(1, 29, 9), // "activated"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 41), // "QPlatformSystemTrayIcon::Acti..."
QT_MOC_LITERAL(4, 82, 6), // "reason"
QT_MOC_LITERAL(5, 89, 14), // "messageClicked"
QT_MOC_LITERAL(6, 104, 14), // "visibleChanged"
QT_MOC_LITERAL(7, 119, 17), // "iconSourceChanged"
QT_MOC_LITERAL(8, 137, 15), // "iconNameChanged"
QT_MOC_LITERAL(9, 153, 14), // "tooltipChanged"
QT_MOC_LITERAL(10, 168, 11), // "menuChanged"
QT_MOC_LITERAL(11, 180, 15), // "geometryChanged"
QT_MOC_LITERAL(12, 196, 11), // "iconChanged"
QT_MOC_LITERAL(13, 208, 4), // "show"
QT_MOC_LITERAL(14, 213, 4), // "hide"
QT_MOC_LITERAL(15, 218, 11), // "showMessage"
QT_MOC_LITERAL(16, 230, 5), // "title"
QT_MOC_LITERAL(17, 236, 7), // "message"
QT_MOC_LITERAL(18, 244, 36), // "QPlatformSystemTrayIcon::Mess..."
QT_MOC_LITERAL(19, 281, 8), // "iconType"
QT_MOC_LITERAL(20, 290, 5), // "msecs"
QT_MOC_LITERAL(21, 296, 10), // "updateIcon"
QT_MOC_LITERAL(22, 307, 9), // "available"
QT_MOC_LITERAL(23, 317, 16), // "supportsMessages"
QT_MOC_LITERAL(24, 334, 7), // "visible"
QT_MOC_LITERAL(25, 342, 10), // "iconSource"
QT_MOC_LITERAL(26, 353, 8), // "iconName"
QT_MOC_LITERAL(27, 362, 7), // "tooltip"
QT_MOC_LITERAL(28, 370, 4), // "menu"
QT_MOC_LITERAL(29, 375, 19), // "QQuickPlatformMenu*"
QT_MOC_LITERAL(30, 395, 8), // "geometry"
QT_MOC_LITERAL(31, 404, 4), // "icon"
QT_MOC_LITERAL(32, 409, 18) // "QQuickPlatformIcon"

    },
    "QQuickPlatformSystemTrayIcon\0activated\0"
    "\0QPlatformSystemTrayIcon::ActivationReason\0"
    "reason\0messageClicked\0visibleChanged\0"
    "iconSourceChanged\0iconNameChanged\0"
    "tooltipChanged\0menuChanged\0geometryChanged\0"
    "iconChanged\0show\0hide\0showMessage\0"
    "title\0message\0QPlatformSystemTrayIcon::MessageIcon\0"
    "iconType\0msecs\0updateIcon\0available\0"
    "supportsMessages\0visible\0iconSource\0"
    "iconName\0tooltip\0menu\0QQuickPlatformMenu*\0"
    "geometry\0icon\0QQuickPlatformIcon"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPlatformSystemTrayIcon[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       9,  139, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  104,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,
       8,    0,  110,    2, 0x06 /* Public */,
       9,    0,  111,    2, 0x06 /* Public */,
      10,    0,  112,    2, 0x06 /* Public */,
      11,    0,  113,    2, 0x86 /* Public | MethodRevisioned */,
      12,    0,  114,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      13,    0,  115,    2, 0x0a /* Public */,
      14,    0,  116,    2, 0x0a /* Public */,
      15,    4,  117,    2, 0x0a /* Public */,
      15,    3,  126,    2, 0x2a /* Public | MethodCloned */,
      15,    2,  133,    2, 0x2a /* Public | MethodCloned */,
      21,    0,  138,    2, 0x08 /* Private */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, 0x80000000 | 18, QMetaType::Int,   16,   17,   19,   20,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, 0x80000000 | 18,   16,   17,   19,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   16,   17,
    QMetaType::Void,

 // properties: name, type, flags
      22, QMetaType::Bool, 0x00095c01,
      23, QMetaType::Bool, 0x00095c01,
      24, QMetaType::Bool, 0x00495903,
      25, QMetaType::QUrl, 0x00495903,
      26, QMetaType::QString, 0x00495903,
      27, QMetaType::QString, 0x00495903,
      28, 0x80000000 | 29, 0x0049590b,
      30, QMetaType::QRect, 0x00c95801,
      31, 0x80000000 | 32, 0x00c9590b,

 // properties: notify_signal_id
       0,
       0,
       2,
       3,
       4,
       5,
       6,
       7,
       8,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,

 // enums: name, alias, flags, count, data

 // enum data: key, value

       0        // eod
};

void QQuickPlatformSystemTrayIcon::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickPlatformSystemTrayIcon *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activated((*reinterpret_cast< QPlatformSystemTrayIcon::ActivationReason(*)>(_a[1]))); break;
        case 1: _t->messageClicked(); break;
        case 2: _t->visibleChanged(); break;
        case 3: _t->iconSourceChanged(); break;
        case 4: _t->iconNameChanged(); break;
        case 5: _t->tooltipChanged(); break;
        case 6: _t->menuChanged(); break;
        case 7: _t->geometryChanged(); break;
        case 8: _t->iconChanged(); break;
        case 9: _t->show(); break;
        case 10: _t->hide(); break;
        case 11: _t->showMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< QPlatformSystemTrayIcon::MessageIcon(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4]))); break;
        case 12: _t->showMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< QPlatformSystemTrayIcon::MessageIcon(*)>(_a[3]))); break;
        case 13: _t->showMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 14: _t->updateIcon(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QPlatformSystemTrayIcon::ActivationReason >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QPlatformSystemTrayIcon::MessageIcon >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QPlatformSystemTrayIcon::MessageIcon >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)(QPlatformSystemTrayIcon::ActivationReason );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::activated)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::messageClicked)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::visibleChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::iconSourceChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::iconNameChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::tooltipChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::menuChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::geometryChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickPlatformSystemTrayIcon::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPlatformSystemTrayIcon::iconChanged)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickPlatformSystemTrayIcon *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isAvailable(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->supportsMessages(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isVisible(); break;
        case 3: *reinterpret_cast< QUrl*>(_v) = _t->iconSource(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->iconName(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->tooltip(); break;
        case 6: *reinterpret_cast< QQuickPlatformMenu**>(_v) = _t->menu(); break;
        case 7: *reinterpret_cast< QRect*>(_v) = _t->geometry(); break;
        case 8: *reinterpret_cast< QQuickPlatformIcon*>(_v) = _t->icon(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickPlatformSystemTrayIcon *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 2: _t->setVisible(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setIconSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 4: _t->setIconName(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setTooltip(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setMenu(*reinterpret_cast< QQuickPlatformMenu**>(_v)); break;
        case 8: _t->setIcon(*reinterpret_cast< QQuickPlatformIcon*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickPlatformSystemTrayIcon[] = {
        &QPlatformSystemTrayIcon::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QQuickPlatformSystemTrayIcon::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickPlatformSystemTrayIcon.data,
    qt_meta_data_QQuickPlatformSystemTrayIcon,
    qt_static_metacall,
    qt_meta_extradata_QQuickPlatformSystemTrayIcon,
    nullptr
} };


const QMetaObject *QQuickPlatformSystemTrayIcon::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickPlatformSystemTrayIcon::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickPlatformSystemTrayIcon.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickPlatformSystemTrayIcon::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickPlatformSystemTrayIcon::activated(QPlatformSystemTrayIcon::ActivationReason _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickPlatformSystemTrayIcon::messageClicked()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickPlatformSystemTrayIcon::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickPlatformSystemTrayIcon::iconSourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickPlatformSystemTrayIcon::iconNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickPlatformSystemTrayIcon::tooltipChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickPlatformSystemTrayIcon::menuChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickPlatformSystemTrayIcon::geometryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickPlatformSystemTrayIcon::iconChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
