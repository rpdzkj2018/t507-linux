/****************************************************************************
** Meta object code from reading C++ file 'qdbusconnection.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qdbusconnection.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdbusconnection.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDBusConnection_t {
    QByteArrayData data[28];
    char stringdata0[540];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDBusConnection_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDBusConnection_t qt_meta_stringdata_QDBusConnection = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QDBusConnection"
QT_MOC_LITERAL(1, 16, 7), // "BusType"
QT_MOC_LITERAL(2, 24, 10), // "SessionBus"
QT_MOC_LITERAL(3, 35, 9), // "SystemBus"
QT_MOC_LITERAL(4, 45, 13), // "ActivationBus"
QT_MOC_LITERAL(5, 59, 15), // "RegisterOptions"
QT_MOC_LITERAL(6, 75, 14), // "RegisterOption"
QT_MOC_LITERAL(7, 90, 14), // "ExportAdaptors"
QT_MOC_LITERAL(8, 105, 21), // "ExportScriptableSlots"
QT_MOC_LITERAL(9, 127, 23), // "ExportScriptableSignals"
QT_MOC_LITERAL(10, 151, 26), // "ExportScriptableProperties"
QT_MOC_LITERAL(11, 178, 26), // "ExportScriptableInvokables"
QT_MOC_LITERAL(12, 205, 24), // "ExportScriptableContents"
QT_MOC_LITERAL(13, 230, 24), // "ExportNonScriptableSlots"
QT_MOC_LITERAL(14, 255, 26), // "ExportNonScriptableSignals"
QT_MOC_LITERAL(15, 282, 29), // "ExportNonScriptableProperties"
QT_MOC_LITERAL(16, 312, 29), // "ExportNonScriptableInvokables"
QT_MOC_LITERAL(17, 342, 27), // "ExportNonScriptableContents"
QT_MOC_LITERAL(18, 370, 14), // "ExportAllSlots"
QT_MOC_LITERAL(19, 385, 16), // "ExportAllSignals"
QT_MOC_LITERAL(20, 402, 19), // "ExportAllProperties"
QT_MOC_LITERAL(21, 422, 19), // "ExportAllInvokables"
QT_MOC_LITERAL(22, 442, 17), // "ExportAllContents"
QT_MOC_LITERAL(23, 460, 15), // "ExportAllSignal"
QT_MOC_LITERAL(24, 476, 18), // "ExportChildObjects"
QT_MOC_LITERAL(25, 495, 14), // "UnregisterMode"
QT_MOC_LITERAL(26, 510, 14), // "UnregisterNode"
QT_MOC_LITERAL(27, 525, 14) // "UnregisterTree"

    },
    "QDBusConnection\0BusType\0SessionBus\0"
    "SystemBus\0ActivationBus\0RegisterOptions\0"
    "RegisterOption\0ExportAdaptors\0"
    "ExportScriptableSlots\0ExportScriptableSignals\0"
    "ExportScriptableProperties\0"
    "ExportScriptableInvokables\0"
    "ExportScriptableContents\0"
    "ExportNonScriptableSlots\0"
    "ExportNonScriptableSignals\0"
    "ExportNonScriptableProperties\0"
    "ExportNonScriptableInvokables\0"
    "ExportNonScriptableContents\0ExportAllSlots\0"
    "ExportAllSignals\0ExportAllProperties\0"
    "ExportAllInvokables\0ExportAllContents\0"
    "ExportAllSignal\0ExportChildObjects\0"
    "UnregisterMode\0UnregisterNode\0"
    "UnregisterTree"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDBusConnection[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       3,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    3,   29,
       5,    6, 0x1,   18,   35,
      25,   25, 0x0,    2,   71,

 // enum data: key, value
       2, uint(QDBusConnection::SessionBus),
       3, uint(QDBusConnection::SystemBus),
       4, uint(QDBusConnection::ActivationBus),
       7, uint(QDBusConnection::ExportAdaptors),
       8, uint(QDBusConnection::ExportScriptableSlots),
       9, uint(QDBusConnection::ExportScriptableSignals),
      10, uint(QDBusConnection::ExportScriptableProperties),
      11, uint(QDBusConnection::ExportScriptableInvokables),
      12, uint(QDBusConnection::ExportScriptableContents),
      13, uint(QDBusConnection::ExportNonScriptableSlots),
      14, uint(QDBusConnection::ExportNonScriptableSignals),
      15, uint(QDBusConnection::ExportNonScriptableProperties),
      16, uint(QDBusConnection::ExportNonScriptableInvokables),
      17, uint(QDBusConnection::ExportNonScriptableContents),
      18, uint(QDBusConnection::ExportAllSlots),
      19, uint(QDBusConnection::ExportAllSignals),
      20, uint(QDBusConnection::ExportAllProperties),
      21, uint(QDBusConnection::ExportAllInvokables),
      22, uint(QDBusConnection::ExportAllContents),
      23, uint(QDBusConnection::ExportAllSignal),
      24, uint(QDBusConnection::ExportChildObjects),
      26, uint(QDBusConnection::UnregisterNode),
      27, uint(QDBusConnection::UnregisterTree),

       0        // eod
};

QT_INIT_METAOBJECT const QMetaObject QDBusConnection::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QDBusConnection.data,
    qt_meta_data_QDBusConnection,
    nullptr,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
