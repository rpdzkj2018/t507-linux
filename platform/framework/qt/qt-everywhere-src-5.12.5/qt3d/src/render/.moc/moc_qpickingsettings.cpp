/****************************************************************************
** Meta object code from reading C++ file 'qpickingsettings.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../frontend/qpickingsettings.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qpickingsettings.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QPickingSettings_t {
    QByteArrayData data[31];
    char stringdata0[597];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QPickingSettings_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QPickingSettings_t qt_meta_stringdata_Qt3DRender__QPickingSettings = {
    {
QT_MOC_LITERAL(0, 0, 28), // "Qt3DRender::QPickingSettings"
QT_MOC_LITERAL(1, 29, 17), // "pickMethodChanged"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 28), // "QPickingSettings::PickMethod"
QT_MOC_LITERAL(4, 77, 10), // "pickMethod"
QT_MOC_LITERAL(5, 88, 21), // "pickResultModeChanged"
QT_MOC_LITERAL(6, 110, 32), // "QPickingSettings::PickResultMode"
QT_MOC_LITERAL(7, 143, 10), // "pickResult"
QT_MOC_LITERAL(8, 154, 33), // "faceOrientationPickingModeCha..."
QT_MOC_LITERAL(9, 188, 44), // "QPickingSettings::FaceOrienta..."
QT_MOC_LITERAL(10, 233, 26), // "faceOrientationPickingMode"
QT_MOC_LITERAL(11, 260, 26), // "worldSpaceToleranceChanged"
QT_MOC_LITERAL(12, 287, 19), // "worldSpaceTolerance"
QT_MOC_LITERAL(13, 307, 13), // "setPickMethod"
QT_MOC_LITERAL(14, 321, 10), // "PickMethod"
QT_MOC_LITERAL(15, 332, 17), // "setPickResultMode"
QT_MOC_LITERAL(16, 350, 14), // "PickResultMode"
QT_MOC_LITERAL(17, 365, 14), // "pickResultMode"
QT_MOC_LITERAL(18, 380, 29), // "setFaceOrientationPickingMode"
QT_MOC_LITERAL(19, 410, 26), // "FaceOrientationPickingMode"
QT_MOC_LITERAL(20, 437, 22), // "setWorldSpaceTolerance"
QT_MOC_LITERAL(21, 460, 21), // "BoundingVolumePicking"
QT_MOC_LITERAL(22, 482, 15), // "TrianglePicking"
QT_MOC_LITERAL(23, 498, 11), // "LinePicking"
QT_MOC_LITERAL(24, 510, 12), // "PointPicking"
QT_MOC_LITERAL(25, 523, 16), // "PrimitivePicking"
QT_MOC_LITERAL(26, 540, 11), // "NearestPick"
QT_MOC_LITERAL(27, 552, 8), // "AllPicks"
QT_MOC_LITERAL(28, 561, 9), // "FrontFace"
QT_MOC_LITERAL(29, 571, 8), // "BackFace"
QT_MOC_LITERAL(30, 580, 16) // "FrontAndBackFace"

    },
    "Qt3DRender::QPickingSettings\0"
    "pickMethodChanged\0\0QPickingSettings::PickMethod\0"
    "pickMethod\0pickResultModeChanged\0"
    "QPickingSettings::PickResultMode\0"
    "pickResult\0faceOrientationPickingModeChanged\0"
    "QPickingSettings::FaceOrientationPickingMode\0"
    "faceOrientationPickingMode\0"
    "worldSpaceToleranceChanged\0"
    "worldSpaceTolerance\0setPickMethod\0"
    "PickMethod\0setPickResultMode\0"
    "PickResultMode\0pickResultMode\0"
    "setFaceOrientationPickingMode\0"
    "FaceOrientationPickingMode\0"
    "setWorldSpaceTolerance\0BoundingVolumePicking\0"
    "TrianglePicking\0LinePicking\0PointPicking\0"
    "PrimitivePicking\0NearestPick\0AllPicks\0"
    "FrontFace\0BackFace\0FrontAndBackFace"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QPickingSettings[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       4,   78, // properties
       3,   98, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    1,   57,    2, 0x06 /* Public */,
       8,    1,   60,    2, 0x06 /* Public */,
      11,    1,   63,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    1,   66,    2, 0x0a /* Public */,
      15,    1,   69,    2, 0x0a /* Public */,
      18,    1,   72,    2, 0x0a /* Public */,
      20,    1,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::Float,   12,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 14,    4,
    QMetaType::Void, 0x80000000 | 16,   17,
    QMetaType::Void, 0x80000000 | 19,   10,
    QMetaType::Void, QMetaType::Float,   12,

 // properties: name, type, flags
       4, 0x80000000 | 14, 0x0049510b,
      17, 0x80000000 | 16, 0x0049510b,
      10, 0x80000000 | 19, 0x0049510b,
      12, QMetaType::Float, 0x00c95103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

 // properties: revision
       0,
       0,
       0,
      10,

 // enums: name, alias, flags, count, data
      14,   14, 0x0,    5,  113,
      16,   16, 0x0,    2,  123,
      19,   19, 0x0,    3,  127,

 // enum data: key, value
      21, uint(Qt3DRender::QPickingSettings::BoundingVolumePicking),
      22, uint(Qt3DRender::QPickingSettings::TrianglePicking),
      23, uint(Qt3DRender::QPickingSettings::LinePicking),
      24, uint(Qt3DRender::QPickingSettings::PointPicking),
      25, uint(Qt3DRender::QPickingSettings::PrimitivePicking),
      26, uint(Qt3DRender::QPickingSettings::NearestPick),
      27, uint(Qt3DRender::QPickingSettings::AllPicks),
      28, uint(Qt3DRender::QPickingSettings::FrontFace),
      29, uint(Qt3DRender::QPickingSettings::BackFace),
      30, uint(Qt3DRender::QPickingSettings::FrontAndBackFace),

       0        // eod
};

void Qt3DRender::QPickingSettings::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QPickingSettings *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pickMethodChanged((*reinterpret_cast< QPickingSettings::PickMethod(*)>(_a[1]))); break;
        case 1: _t->pickResultModeChanged((*reinterpret_cast< QPickingSettings::PickResultMode(*)>(_a[1]))); break;
        case 2: _t->faceOrientationPickingModeChanged((*reinterpret_cast< QPickingSettings::FaceOrientationPickingMode(*)>(_a[1]))); break;
        case 3: _t->worldSpaceToleranceChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->setPickMethod((*reinterpret_cast< PickMethod(*)>(_a[1]))); break;
        case 5: _t->setPickResultMode((*reinterpret_cast< PickResultMode(*)>(_a[1]))); break;
        case 6: _t->setFaceOrientationPickingMode((*reinterpret_cast< FaceOrientationPickingMode(*)>(_a[1]))); break;
        case 7: _t->setWorldSpaceTolerance((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QPickingSettings::*)(QPickingSettings::PickMethod );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QPickingSettings::pickMethodChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QPickingSettings::*)(QPickingSettings::PickResultMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QPickingSettings::pickResultModeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QPickingSettings::*)(QPickingSettings::FaceOrientationPickingMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QPickingSettings::faceOrientationPickingModeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QPickingSettings::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QPickingSettings::worldSpaceToleranceChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QPickingSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< PickMethod*>(_v) = _t->pickMethod(); break;
        case 1: *reinterpret_cast< PickResultMode*>(_v) = _t->pickResultMode(); break;
        case 2: *reinterpret_cast< FaceOrientationPickingMode*>(_v) = _t->faceOrientationPickingMode(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->worldSpaceTolerance(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QPickingSettings *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPickMethod(*reinterpret_cast< PickMethod*>(_v)); break;
        case 1: _t->setPickResultMode(*reinterpret_cast< PickResultMode*>(_v)); break;
        case 2: _t->setFaceOrientationPickingMode(*reinterpret_cast< FaceOrientationPickingMode*>(_v)); break;
        case 3: _t->setWorldSpaceTolerance(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QPickingSettings::staticMetaObject = { {
    &Qt3DCore::QNode::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QPickingSettings.data,
    qt_meta_data_Qt3DRender__QPickingSettings,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QPickingSettings::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QPickingSettings::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QPickingSettings.stringdata0))
        return static_cast<void*>(this);
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QPickingSettings::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QPickingSettings::pickMethodChanged(QPickingSettings::PickMethod _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QPickingSettings::pickResultModeChanged(QPickingSettings::PickResultMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QPickingSettings::faceOrientationPickingModeChanged(QPickingSettings::FaceOrientationPickingMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QPickingSettings::worldSpaceToleranceChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
