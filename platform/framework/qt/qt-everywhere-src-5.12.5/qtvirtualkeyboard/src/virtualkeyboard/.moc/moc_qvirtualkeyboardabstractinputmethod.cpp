/****************************************************************************
** Meta object code from reading C++ file 'qvirtualkeyboardabstractinputmethod.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qvirtualkeyboardabstractinputmethod.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qvirtualkeyboardabstractinputmethod.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod_t {
    QByteArrayData data[10];
    char stringdata0[176];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod_t qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod = {
    {
QT_MOC_LITERAL(0, 0, 35), // "QVirtualKeyboardAbstractInput..."
QT_MOC_LITERAL(1, 36, 20), // "selectionListChanged"
QT_MOC_LITERAL(2, 57, 0), // ""
QT_MOC_LITERAL(3, 58, 40), // "QVirtualKeyboardSelectionList..."
QT_MOC_LITERAL(4, 99, 4), // "type"
QT_MOC_LITERAL(5, 104, 30), // "selectionListActiveItemChanged"
QT_MOC_LITERAL(6, 135, 5), // "index"
QT_MOC_LITERAL(7, 141, 21), // "selectionListsChanged"
QT_MOC_LITERAL(8, 163, 5), // "reset"
QT_MOC_LITERAL(9, 169, 6) // "update"

    },
    "QVirtualKeyboardAbstractInputMethod\0"
    "selectionListChanged\0\0"
    "QVirtualKeyboardSelectionListModel::Type\0"
    "type\0selectionListActiveItemChanged\0"
    "index\0selectionListsChanged\0reset\0"
    "update"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QVirtualKeyboardAbstractInputMethod[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    2,   42,    2, 0x06 /* Public */,
       7,    0,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   48,    2, 0x0a /* Public */,
       9,    0,   49,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int,    4,    6,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QVirtualKeyboardAbstractInputMethod::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QVirtualKeyboardAbstractInputMethod *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->selectionListChanged((*reinterpret_cast< QVirtualKeyboardSelectionListModel::Type(*)>(_a[1]))); break;
        case 1: _t->selectionListActiveItemChanged((*reinterpret_cast< QVirtualKeyboardSelectionListModel::Type(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->selectionListsChanged(); break;
        case 3: _t->reset(); break;
        case 4: _t->update(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVirtualKeyboardSelectionListModel::Type >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVirtualKeyboardSelectionListModel::Type >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QVirtualKeyboardAbstractInputMethod::*)(QVirtualKeyboardSelectionListModel::Type );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardAbstractInputMethod::selectionListChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardAbstractInputMethod::*)(QVirtualKeyboardSelectionListModel::Type , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardAbstractInputMethod::selectionListActiveItemChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardAbstractInputMethod::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardAbstractInputMethod::selectionListsChanged)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QVirtualKeyboardAbstractInputMethod::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod.data,
    qt_meta_data_QVirtualKeyboardAbstractInputMethod,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QVirtualKeyboardAbstractInputMethod::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QVirtualKeyboardAbstractInputMethod::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QVirtualKeyboardAbstractInputMethod.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QVirtualKeyboardAbstractInputMethod::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void QVirtualKeyboardAbstractInputMethod::selectionListChanged(QVirtualKeyboardSelectionListModel::Type _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QVirtualKeyboardAbstractInputMethod::selectionListActiveItemChanged(QVirtualKeyboardSelectionListModel::Type _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QVirtualKeyboardAbstractInputMethod::selectionListsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
