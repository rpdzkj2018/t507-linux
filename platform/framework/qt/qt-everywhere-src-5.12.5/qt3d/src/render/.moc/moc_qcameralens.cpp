/****************************************************************************
** Meta object code from reading C++ file 'qcameralens.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../frontend/qcameralens.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcameralens.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QCameraLens_t {
    QByteArrayData data[44];
    char stringdata0[601];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QCameraLens_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QCameraLens_t qt_meta_stringdata_Qt3DRender__QCameraLens = {
    {
QT_MOC_LITERAL(0, 0, 23), // "Qt3DRender::QCameraLens"
QT_MOC_LITERAL(1, 24, 21), // "projectionTypeChanged"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 27), // "QCameraLens::ProjectionType"
QT_MOC_LITERAL(4, 75, 14), // "projectionType"
QT_MOC_LITERAL(5, 90, 16), // "nearPlaneChanged"
QT_MOC_LITERAL(6, 107, 9), // "nearPlane"
QT_MOC_LITERAL(7, 117, 15), // "farPlaneChanged"
QT_MOC_LITERAL(8, 133, 8), // "farPlane"
QT_MOC_LITERAL(9, 142, 18), // "fieldOfViewChanged"
QT_MOC_LITERAL(10, 161, 11), // "fieldOfView"
QT_MOC_LITERAL(11, 173, 18), // "aspectRatioChanged"
QT_MOC_LITERAL(12, 192, 11), // "aspectRatio"
QT_MOC_LITERAL(13, 204, 11), // "leftChanged"
QT_MOC_LITERAL(14, 216, 4), // "left"
QT_MOC_LITERAL(15, 221, 12), // "rightChanged"
QT_MOC_LITERAL(16, 234, 5), // "right"
QT_MOC_LITERAL(17, 240, 13), // "bottomChanged"
QT_MOC_LITERAL(18, 254, 6), // "bottom"
QT_MOC_LITERAL(19, 261, 10), // "topChanged"
QT_MOC_LITERAL(20, 272, 3), // "top"
QT_MOC_LITERAL(21, 276, 23), // "projectionMatrixChanged"
QT_MOC_LITERAL(22, 300, 16), // "projectionMatrix"
QT_MOC_LITERAL(23, 317, 15), // "exposureChanged"
QT_MOC_LITERAL(24, 333, 8), // "exposure"
QT_MOC_LITERAL(25, 342, 10), // "viewSphere"
QT_MOC_LITERAL(26, 353, 6), // "center"
QT_MOC_LITERAL(27, 360, 6), // "radius"
QT_MOC_LITERAL(28, 367, 17), // "setProjectionType"
QT_MOC_LITERAL(29, 385, 14), // "ProjectionType"
QT_MOC_LITERAL(30, 400, 12), // "setNearPlane"
QT_MOC_LITERAL(31, 413, 11), // "setFarPlane"
QT_MOC_LITERAL(32, 425, 14), // "setFieldOfView"
QT_MOC_LITERAL(33, 440, 14), // "setAspectRatio"
QT_MOC_LITERAL(34, 455, 7), // "setLeft"
QT_MOC_LITERAL(35, 463, 8), // "setRight"
QT_MOC_LITERAL(36, 472, 9), // "setBottom"
QT_MOC_LITERAL(37, 482, 6), // "setTop"
QT_MOC_LITERAL(38, 489, 19), // "setProjectionMatrix"
QT_MOC_LITERAL(39, 509, 11), // "setExposure"
QT_MOC_LITERAL(40, 521, 22), // "OrthographicProjection"
QT_MOC_LITERAL(41, 544, 21), // "PerspectiveProjection"
QT_MOC_LITERAL(42, 566, 17), // "FrustumProjection"
QT_MOC_LITERAL(43, 584, 16) // "CustomProjection"

    },
    "Qt3DRender::QCameraLens\0projectionTypeChanged\0"
    "\0QCameraLens::ProjectionType\0"
    "projectionType\0nearPlaneChanged\0"
    "nearPlane\0farPlaneChanged\0farPlane\0"
    "fieldOfViewChanged\0fieldOfView\0"
    "aspectRatioChanged\0aspectRatio\0"
    "leftChanged\0left\0rightChanged\0right\0"
    "bottomChanged\0bottom\0topChanged\0top\0"
    "projectionMatrixChanged\0projectionMatrix\0"
    "exposureChanged\0exposure\0viewSphere\0"
    "center\0radius\0setProjectionType\0"
    "ProjectionType\0setNearPlane\0setFarPlane\0"
    "setFieldOfView\0setAspectRatio\0setLeft\0"
    "setRight\0setBottom\0setTop\0setProjectionMatrix\0"
    "setExposure\0OrthographicProjection\0"
    "PerspectiveProjection\0FrustumProjection\0"
    "CustomProjection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QCameraLens[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
      11,  200, // properties
       1,  255, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  129,    2, 0x06 /* Public */,
       5,    1,  132,    2, 0x06 /* Public */,
       7,    1,  135,    2, 0x06 /* Public */,
       9,    1,  138,    2, 0x06 /* Public */,
      11,    1,  141,    2, 0x06 /* Public */,
      13,    1,  144,    2, 0x06 /* Public */,
      15,    1,  147,    2, 0x06 /* Public */,
      17,    1,  150,    2, 0x06 /* Public */,
      19,    1,  153,    2, 0x06 /* Public */,
      21,    1,  156,    2, 0x06 /* Public */,
      23,    1,  159,    2, 0x06 /* Public */,
      25,    2,  162,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      28,    1,  167,    2, 0x0a /* Public */,
      30,    1,  170,    2, 0x0a /* Public */,
      31,    1,  173,    2, 0x0a /* Public */,
      32,    1,  176,    2, 0x0a /* Public */,
      33,    1,  179,    2, 0x0a /* Public */,
      34,    1,  182,    2, 0x0a /* Public */,
      35,    1,  185,    2, 0x0a /* Public */,
      36,    1,  188,    2, 0x0a /* Public */,
      37,    1,  191,    2, 0x0a /* Public */,
      38,    1,  194,    2, 0x0a /* Public */,
      39,    1,  197,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,
    QMetaType::Void, QMetaType::QMatrix4x4,   22,
    QMetaType::Void, QMetaType::Float,   24,
    QMetaType::Void, QMetaType::QVector3D, QMetaType::Float,   26,   27,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 29,    4,
    QMetaType::Void, QMetaType::Float,    6,
    QMetaType::Void, QMetaType::Float,    8,
    QMetaType::Void, QMetaType::Float,   10,
    QMetaType::Void, QMetaType::Float,   12,
    QMetaType::Void, QMetaType::Float,   14,
    QMetaType::Void, QMetaType::Float,   16,
    QMetaType::Void, QMetaType::Float,   18,
    QMetaType::Void, QMetaType::Float,   20,
    QMetaType::Void, QMetaType::QMatrix4x4,   22,
    QMetaType::Void, QMetaType::Float,   24,

 // properties: name, type, flags
       4, 0x80000000 | 29, 0x0049510b,
       6, QMetaType::Float, 0x00495103,
       8, QMetaType::Float, 0x00495103,
      10, QMetaType::Float, 0x00495103,
      12, QMetaType::Float, 0x00495103,
      14, QMetaType::Float, 0x00495103,
      16, QMetaType::Float, 0x00495103,
      18, QMetaType::Float, 0x00495103,
      20, QMetaType::Float, 0x00495103,
      22, QMetaType::QMatrix4x4, 0x00495103,
      24, QMetaType::Float, 0x00c95103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       9,

 // enums: name, alias, flags, count, data
      29,   29, 0x0,    4,  260,

 // enum data: key, value
      40, uint(Qt3DRender::QCameraLens::OrthographicProjection),
      41, uint(Qt3DRender::QCameraLens::PerspectiveProjection),
      42, uint(Qt3DRender::QCameraLens::FrustumProjection),
      43, uint(Qt3DRender::QCameraLens::CustomProjection),

       0        // eod
};

void Qt3DRender::QCameraLens::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->projectionTypeChanged((*reinterpret_cast< QCameraLens::ProjectionType(*)>(_a[1]))); break;
        case 1: _t->nearPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 2: _t->farPlaneChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 3: _t->fieldOfViewChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->aspectRatioChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->leftChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->rightChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->bottomChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 8: _t->topChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 9: _t->projectionMatrixChanged((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 10: _t->exposureChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 11: _t->viewSphere((*reinterpret_cast< const QVector3D(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 12: _t->setProjectionType((*reinterpret_cast< ProjectionType(*)>(_a[1]))); break;
        case 13: _t->setNearPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->setFarPlane((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: _t->setFieldOfView((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 16: _t->setAspectRatio((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 17: _t->setLeft((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 18: _t->setRight((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 19: _t->setBottom((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 20: _t->setTop((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 21: _t->setProjectionMatrix((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 22: _t->setExposure((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QCameraLens::*)(QCameraLens::ProjectionType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::projectionTypeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::nearPlaneChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::farPlaneChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::fieldOfViewChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::aspectRatioChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::leftChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::rightChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::bottomChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::topChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(const QMatrix4x4 & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::projectionMatrixChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::exposureChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QCameraLens::*)(const QVector3D & , float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraLens::viewSphere)) {
                *result = 11;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< ProjectionType*>(_v) = _t->projectionType(); break;
        case 1: *reinterpret_cast< float*>(_v) = _t->nearPlane(); break;
        case 2: *reinterpret_cast< float*>(_v) = _t->farPlane(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->fieldOfView(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->aspectRatio(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->left(); break;
        case 6: *reinterpret_cast< float*>(_v) = _t->right(); break;
        case 7: *reinterpret_cast< float*>(_v) = _t->bottom(); break;
        case 8: *reinterpret_cast< float*>(_v) = _t->top(); break;
        case 9: *reinterpret_cast< QMatrix4x4*>(_v) = _t->projectionMatrix(); break;
        case 10: *reinterpret_cast< float*>(_v) = _t->exposure(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QCameraLens *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setProjectionType(*reinterpret_cast< ProjectionType*>(_v)); break;
        case 1: _t->setNearPlane(*reinterpret_cast< float*>(_v)); break;
        case 2: _t->setFarPlane(*reinterpret_cast< float*>(_v)); break;
        case 3: _t->setFieldOfView(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setAspectRatio(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setLeft(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setRight(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setBottom(*reinterpret_cast< float*>(_v)); break;
        case 8: _t->setTop(*reinterpret_cast< float*>(_v)); break;
        case 9: _t->setProjectionMatrix(*reinterpret_cast< QMatrix4x4*>(_v)); break;
        case 10: _t->setExposure(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QCameraLens::staticMetaObject = { {
    &Qt3DCore::QComponent::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QCameraLens.data,
    qt_meta_data_Qt3DRender__QCameraLens,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QCameraLens::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QCameraLens::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QCameraLens.stringdata0))
        return static_cast<void*>(this);
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QCameraLens::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 23)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 23;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QCameraLens::projectionTypeChanged(QCameraLens::ProjectionType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QCameraLens::nearPlaneChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QCameraLens::farPlaneChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QCameraLens::fieldOfViewChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QCameraLens::aspectRatioChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QCameraLens::leftChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QCameraLens::rightChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QCameraLens::bottomChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DRender::QCameraLens::topChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DRender::QCameraLens::projectionMatrixChanged(const QMatrix4x4 & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DRender::QCameraLens::exposureChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Qt3DRender::QCameraLens::viewSphere(const QVector3D & _t1, float _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
