/****************************************************************************
** Meta object code from reading C++ file 'qquicktabbar_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquicktabbar_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktabbar_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickTabBar_t {
    QByteArrayData data[11];
    char stringdata0[130];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTabBar_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTabBar_t qt_meta_stringdata_QQuickTabBar = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickTabBar"
QT_MOC_LITERAL(1, 13, 15), // "positionChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 19), // "contentWidthChanged"
QT_MOC_LITERAL(4, 50, 20), // "contentHeightChanged"
QT_MOC_LITERAL(5, 71, 8), // "position"
QT_MOC_LITERAL(6, 80, 8), // "Position"
QT_MOC_LITERAL(7, 89, 12), // "contentWidth"
QT_MOC_LITERAL(8, 102, 13), // "contentHeight"
QT_MOC_LITERAL(9, 116, 6), // "Header"
QT_MOC_LITERAL(10, 123, 6) // "Footer"

    },
    "QQuickTabBar\0positionChanged\0\0"
    "contentWidthChanged\0contentHeightChanged\0"
    "position\0Position\0contentWidth\0"
    "contentHeight\0Header\0Footer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTabBar[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       3,   20, // properties
       1,   35, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       5, 0x80000000 | 6, 0x0049590b,
       7, QMetaType::QReal, 0x00c95907,
       8, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       0,
    1879048195,
    1879048196,

 // properties: revision
       0,
       2,
       2,

 // enums: name, alias, flags, count, data
       6,    6, 0x0,    2,   40,

 // enum data: key, value
       9, uint(QQuickTabBar::Header),
      10, uint(QQuickTabBar::Footer),

       0        // eod
};

void QQuickTabBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTabBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->positionChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTabBar::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTabBar::positionChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTabBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Position*>(_v) = _t->position(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickTabBar *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPosition(*reinterpret_cast< Position*>(_v)); break;
        case 1: _t->setContentWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setContentHeight(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTabBar *_t = static_cast<QQuickTabBar *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 1: _t->resetContentWidth(); break;
        case 2: _t->resetContentHeight(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQuickTabBar::staticMetaObject = { {
    &QQuickContainer::staticMetaObject,
    qt_meta_stringdata_QQuickTabBar.data,
    qt_meta_data_QQuickTabBar,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTabBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTabBar::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTabBar.stringdata0))
        return static_cast<void*>(this);
    return QQuickContainer::qt_metacast(_clname);
}

int QQuickTabBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickContainer::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTabBar::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
// If you get a compile error in this function it can be because either
//     a) You are using a NOTIFY signal that does not exist. Fix it.
//     b) You are using a NOTIFY signal that does exist (in a parent class) but has a non-empty parameter list. This is a moc limitation.
Q_DECL_UNUSED static void checkNotifySignalValidity_QQuickTabBar(QQuickTabBar *t) {
    t->contentWidthChanged();
    t->contentHeightChanged();
}
struct qt_meta_stringdata_QQuickTabBarAttached_t {
    QByteArrayData data[10];
    char stringdata0[124];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTabBarAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTabBarAttached_t qt_meta_stringdata_QQuickTabBarAttached = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QQuickTabBarAttached"
QT_MOC_LITERAL(1, 21, 12), // "indexChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13), // "tabBarChanged"
QT_MOC_LITERAL(4, 49, 15), // "positionChanged"
QT_MOC_LITERAL(5, 65, 5), // "index"
QT_MOC_LITERAL(6, 71, 6), // "tabBar"
QT_MOC_LITERAL(7, 78, 13), // "QQuickTabBar*"
QT_MOC_LITERAL(8, 92, 8), // "position"
QT_MOC_LITERAL(9, 101, 22) // "QQuickTabBar::Position"

    },
    "QQuickTabBarAttached\0indexChanged\0\0"
    "tabBarChanged\0positionChanged\0index\0"
    "tabBar\0QQuickTabBar*\0position\0"
    "QQuickTabBar::Position"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTabBarAttached[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       3,   32, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    0,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Int, 0x00495801,
       6, 0x80000000 | 7, 0x00495809,
       8, 0x80000000 | 9, 0x00495809,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void QQuickTabBarAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTabBarAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->indexChanged(); break;
        case 1: _t->tabBarChanged(); break;
        case 2: _t->positionChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTabBarAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTabBarAttached::indexChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickTabBarAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTabBarAttached::tabBarChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickTabBarAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTabBarAttached::positionChanged)) {
                *result = 2;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickTabBar* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTabBarAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->index(); break;
        case 1: *reinterpret_cast< QQuickTabBar**>(_v) = _t->tabBar(); break;
        case 2: *reinterpret_cast< QQuickTabBar::Position*>(_v) = _t->position(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickTabBarAttached[] = {
        &QQuickTabBar::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QQuickTabBarAttached::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickTabBarAttached.data,
    qt_meta_data_QQuickTabBarAttached,
    qt_static_metacall,
    qt_meta_extradata_QQuickTabBarAttached,
    nullptr
} };


const QMetaObject *QQuickTabBarAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTabBarAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTabBarAttached.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickTabBarAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTabBarAttached::indexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickTabBarAttached::tabBarChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickTabBarAttached::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
