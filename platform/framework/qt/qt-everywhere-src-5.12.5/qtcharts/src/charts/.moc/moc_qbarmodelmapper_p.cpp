/****************************************************************************
** Meta object code from reading C++ file 'qbarmodelmapper_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../barchart/qbarmodelmapper_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qbarmodelmapper_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtCharts__QBarModelMapperPrivate_t {
    QByteArrayData data[31];
    char stringdata0[413];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtCharts__QBarModelMapperPrivate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtCharts__QBarModelMapperPrivate_t qt_meta_stringdata_QtCharts__QBarModelMapperPrivate = {
    {
QT_MOC_LITERAL(0, 0, 32), // "QtCharts::QBarModelMapperPrivate"
QT_MOC_LITERAL(1, 33, 12), // "modelUpdated"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 11), // "QModelIndex"
QT_MOC_LITERAL(4, 59, 7), // "topLeft"
QT_MOC_LITERAL(5, 67, 11), // "bottomRight"
QT_MOC_LITERAL(6, 79, 22), // "modelHeaderDataUpdated"
QT_MOC_LITERAL(7, 102, 15), // "Qt::Orientation"
QT_MOC_LITERAL(8, 118, 11), // "orientation"
QT_MOC_LITERAL(9, 130, 5), // "first"
QT_MOC_LITERAL(10, 136, 4), // "last"
QT_MOC_LITERAL(11, 141, 14), // "modelRowsAdded"
QT_MOC_LITERAL(12, 156, 6), // "parent"
QT_MOC_LITERAL(13, 163, 5), // "start"
QT_MOC_LITERAL(14, 169, 3), // "end"
QT_MOC_LITERAL(15, 173, 16), // "modelRowsRemoved"
QT_MOC_LITERAL(16, 190, 17), // "modelColumnsAdded"
QT_MOC_LITERAL(17, 208, 19), // "modelColumnsRemoved"
QT_MOC_LITERAL(18, 228, 20), // "handleModelDestroyed"
QT_MOC_LITERAL(19, 249, 12), // "barSetsAdded"
QT_MOC_LITERAL(20, 262, 15), // "QList<QBarSet*>"
QT_MOC_LITERAL(21, 278, 4), // "sets"
QT_MOC_LITERAL(22, 283, 14), // "barSetsRemoved"
QT_MOC_LITERAL(23, 298, 11), // "valuesAdded"
QT_MOC_LITERAL(24, 310, 5), // "index"
QT_MOC_LITERAL(25, 316, 5), // "count"
QT_MOC_LITERAL(26, 322, 13), // "valuesRemoved"
QT_MOC_LITERAL(27, 336, 15), // "barLabelChanged"
QT_MOC_LITERAL(28, 352, 15), // "barValueChanged"
QT_MOC_LITERAL(29, 368, 21), // "handleSeriesDestroyed"
QT_MOC_LITERAL(30, 390, 22) // "initializeBarFromModel"

    },
    "QtCharts::QBarModelMapperPrivate\0"
    "modelUpdated\0\0QModelIndex\0topLeft\0"
    "bottomRight\0modelHeaderDataUpdated\0"
    "Qt::Orientation\0orientation\0first\0"
    "last\0modelRowsAdded\0parent\0start\0end\0"
    "modelRowsRemoved\0modelColumnsAdded\0"
    "modelColumnsRemoved\0handleModelDestroyed\0"
    "barSetsAdded\0QList<QBarSet*>\0sets\0"
    "barSetsRemoved\0valuesAdded\0index\0count\0"
    "valuesRemoved\0barLabelChanged\0"
    "barValueChanged\0handleSeriesDestroyed\0"
    "initializeBarFromModel"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtCharts__QBarModelMapperPrivate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    2,   89,    2, 0x0a /* Public */,
       6,    3,   94,    2, 0x0a /* Public */,
      11,    3,  101,    2, 0x0a /* Public */,
      15,    3,  108,    2, 0x0a /* Public */,
      16,    3,  115,    2, 0x0a /* Public */,
      17,    3,  122,    2, 0x0a /* Public */,
      18,    0,  129,    2, 0x0a /* Public */,
      19,    1,  130,    2, 0x0a /* Public */,
      22,    1,  133,    2, 0x0a /* Public */,
      23,    2,  136,    2, 0x0a /* Public */,
      26,    2,  141,    2, 0x0a /* Public */,
      27,    0,  146,    2, 0x0a /* Public */,
      28,    1,  147,    2, 0x0a /* Public */,
      29,    0,  150,    2, 0x0a /* Public */,
      30,    0,  151,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int,    8,    9,   10,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void, 0x80000000 | 3, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, 0x80000000 | 20,   21,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   24,   25,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   24,   25,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QtCharts::QBarModelMapperPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QBarModelMapperPrivate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelUpdated((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 1: _t->modelHeaderDataUpdated((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->modelRowsAdded((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->modelRowsRemoved((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->modelColumnsAdded((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 5: _t->modelColumnsRemoved((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 6: _t->handleModelDestroyed(); break;
        case 7: _t->barSetsAdded((*reinterpret_cast< QList<QBarSet*>(*)>(_a[1]))); break;
        case 8: _t->barSetsRemoved((*reinterpret_cast< QList<QBarSet*>(*)>(_a[1]))); break;
        case 9: _t->valuesAdded((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: _t->valuesRemoved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->barLabelChanged(); break;
        case 12: _t->barValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->handleSeriesDestroyed(); break;
        case 14: _t->initializeBarFromModel(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QtCharts::QBarModelMapperPrivate::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QtCharts__QBarModelMapperPrivate.data,
    qt_meta_data_QtCharts__QBarModelMapperPrivate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtCharts::QBarModelMapperPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtCharts::QBarModelMapperPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtCharts__QBarModelMapperPrivate.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QtCharts::QBarModelMapperPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
