/****************************************************************************
** Meta object code from reading C++ file 'qquicktextarea_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquicktextarea_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktextarea_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickTextArea_t {
    QByteArrayData data[42];
    char stringdata0[662];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextArea_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextArea_t qt_meta_stringdata_QQuickTextArea = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickTextArea"
QT_MOC_LITERAL(1, 15, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 37, 10), // "background"
QT_MOC_LITERAL(3, 48, 11), // "fontChanged"
QT_MOC_LITERAL(4, 60, 0), // ""
QT_MOC_LITERAL(5, 61, 21), // "implicitWidthChanged3"
QT_MOC_LITERAL(6, 83, 22), // "implicitHeightChanged3"
QT_MOC_LITERAL(7, 106, 17), // "backgroundChanged"
QT_MOC_LITERAL(8, 124, 22), // "placeholderTextChanged"
QT_MOC_LITERAL(9, 147, 18), // "focusReasonChanged"
QT_MOC_LITERAL(10, 166, 12), // "pressAndHold"
QT_MOC_LITERAL(11, 179, 17), // "QQuickMouseEvent*"
QT_MOC_LITERAL(12, 197, 5), // "event"
QT_MOC_LITERAL(13, 203, 7), // "pressed"
QT_MOC_LITERAL(14, 211, 8), // "released"
QT_MOC_LITERAL(15, 220, 14), // "hoveredChanged"
QT_MOC_LITERAL(16, 235, 19), // "hoverEnabledChanged"
QT_MOC_LITERAL(17, 255, 14), // "paletteChanged"
QT_MOC_LITERAL(18, 270, 27), // "placeholderTextColorChanged"
QT_MOC_LITERAL(19, 298, 30), // "implicitBackgroundWidthChanged"
QT_MOC_LITERAL(20, 329, 31), // "implicitBackgroundHeightChanged"
QT_MOC_LITERAL(21, 361, 15), // "topInsetChanged"
QT_MOC_LITERAL(22, 377, 16), // "leftInsetChanged"
QT_MOC_LITERAL(23, 394, 17), // "rightInsetChanged"
QT_MOC_LITERAL(24, 412, 18), // "bottomInsetChanged"
QT_MOC_LITERAL(25, 431, 4), // "font"
QT_MOC_LITERAL(26, 436, 13), // "implicitWidth"
QT_MOC_LITERAL(27, 450, 14), // "implicitHeight"
QT_MOC_LITERAL(28, 465, 11), // "QQuickItem*"
QT_MOC_LITERAL(29, 477, 15), // "placeholderText"
QT_MOC_LITERAL(30, 493, 11), // "focusReason"
QT_MOC_LITERAL(31, 505, 15), // "Qt::FocusReason"
QT_MOC_LITERAL(32, 521, 7), // "hovered"
QT_MOC_LITERAL(33, 529, 12), // "hoverEnabled"
QT_MOC_LITERAL(34, 542, 7), // "palette"
QT_MOC_LITERAL(35, 550, 20), // "placeholderTextColor"
QT_MOC_LITERAL(36, 571, 23), // "implicitBackgroundWidth"
QT_MOC_LITERAL(37, 595, 24), // "implicitBackgroundHeight"
QT_MOC_LITERAL(38, 620, 8), // "topInset"
QT_MOC_LITERAL(39, 629, 9), // "leftInset"
QT_MOC_LITERAL(40, 639, 10), // "rightInset"
QT_MOC_LITERAL(41, 650, 11) // "bottomInset"

    },
    "QQuickTextArea\0DeferredPropertyNames\0"
    "background\0fontChanged\0\0implicitWidthChanged3\0"
    "implicitHeightChanged3\0backgroundChanged\0"
    "placeholderTextChanged\0focusReasonChanged\0"
    "pressAndHold\0QQuickMouseEvent*\0event\0"
    "pressed\0released\0hoveredChanged\0"
    "hoverEnabledChanged\0paletteChanged\0"
    "placeholderTextColorChanged\0"
    "implicitBackgroundWidthChanged\0"
    "implicitBackgroundHeightChanged\0"
    "topInsetChanged\0leftInsetChanged\0"
    "rightInsetChanged\0bottomInsetChanged\0"
    "font\0implicitWidth\0implicitHeight\0"
    "QQuickItem*\0placeholderText\0focusReason\0"
    "Qt::FocusReason\0hovered\0hoverEnabled\0"
    "palette\0placeholderTextColor\0"
    "implicitBackgroundWidth\0"
    "implicitBackgroundHeight\0topInset\0"
    "leftInset\0rightInset\0bottomInset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextArea[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      19,   16, // methods
      16,  155, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      19,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  130,    4, 0x06 /* Public */,
       5,    0,  131,    4, 0x06 /* Public */,
       6,    0,  132,    4, 0x06 /* Public */,
       7,    0,  133,    4, 0x06 /* Public */,
       8,    0,  134,    4, 0x06 /* Public */,
       9,    0,  135,    4, 0x06 /* Public */,
      10,    1,  136,    4, 0x06 /* Public */,
      13,    1,  139,    4, 0x86 /* Public | MethodRevisioned */,
      14,    1,  142,    4, 0x86 /* Public | MethodRevisioned */,
      15,    0,  145,    4, 0x86 /* Public | MethodRevisioned */,
      16,    0,  146,    4, 0x86 /* Public | MethodRevisioned */,
      17,    0,  147,    4, 0x86 /* Public | MethodRevisioned */,
      18,    0,  148,    4, 0x86 /* Public | MethodRevisioned */,
      19,    0,  149,    4, 0x86 /* Public | MethodRevisioned */,
      20,    0,  150,    4, 0x86 /* Public | MethodRevisioned */,
      21,    0,  151,    4, 0x86 /* Public | MethodRevisioned */,
      22,    0,  152,    4, 0x86 /* Public | MethodRevisioned */,
      23,    0,  153,    4, 0x86 /* Public | MethodRevisioned */,
      24,    0,  154,    4, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,
       1,
       1,
       3,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      25, QMetaType::QFont, 0x00495103,
      26, QMetaType::QReal, 0x00495903,
      27, QMetaType::QReal, 0x00495903,
       2, 0x80000000 | 28, 0x0049590b,
      29, QMetaType::QString, 0x00495903,
      30, 0x80000000 | 31, 0x0049590b,
      32, QMetaType::Bool, 0x00c95801,
      33, QMetaType::Bool, 0x00c95907,
      34, QMetaType::QPalette, 0x00c95907,
      35, QMetaType::QColor, 0x00c95903,
      36, QMetaType::QReal, 0x00c95801,
      37, QMetaType::QReal, 0x00c95801,
      38, QMetaType::QReal, 0x00c95907,
      39, QMetaType::QReal, 0x00c95907,
      40, QMetaType::QReal, 0x00c95907,
      41, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,
       3,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

       0        // eod
};

void QQuickTextArea::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTextArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fontChanged(); break;
        case 1: _t->implicitWidthChanged3(); break;
        case 2: _t->implicitHeightChanged3(); break;
        case 3: _t->backgroundChanged(); break;
        case 4: _t->placeholderTextChanged(); break;
        case 5: _t->focusReasonChanged(); break;
        case 6: _t->pressAndHold((*reinterpret_cast< QQuickMouseEvent*(*)>(_a[1]))); break;
        case 7: _t->pressed((*reinterpret_cast< QQuickMouseEvent*(*)>(_a[1]))); break;
        case 8: _t->released((*reinterpret_cast< QQuickMouseEvent*(*)>(_a[1]))); break;
        case 9: _t->hoveredChanged(); break;
        case 10: _t->hoverEnabledChanged(); break;
        case 11: _t->paletteChanged(); break;
        case 12: _t->placeholderTextColorChanged(); break;
        case 13: _t->implicitBackgroundWidthChanged(); break;
        case 14: _t->implicitBackgroundHeightChanged(); break;
        case 15: _t->topInsetChanged(); break;
        case 16: _t->leftInsetChanged(); break;
        case 17: _t->rightInsetChanged(); break;
        case 18: _t->bottomInsetChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::fontChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::implicitWidthChanged3)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::implicitHeightChanged3)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::backgroundChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::placeholderTextChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::focusReasonChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)(QQuickMouseEvent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::pressAndHold)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)(QQuickMouseEvent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::pressed)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)(QQuickMouseEvent * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::released)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::hoveredChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::hoverEnabledChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::paletteChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::placeholderTextColorChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::implicitBackgroundWidthChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::implicitBackgroundHeightChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::topInsetChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::leftInsetChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::rightInsetChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QQuickTextArea::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextArea::bottomInsetChanged)) {
                *result = 18;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTextArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->implicitWidth(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->implicitHeight(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->background(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->placeholderText(); break;
        case 5: *reinterpret_cast< Qt::FocusReason*>(_v) = _t->focusReason(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isHovered(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isHoverEnabled(); break;
        case 8: *reinterpret_cast< QPalette*>(_v) = _t->palette(); break;
        case 9: *reinterpret_cast< QColor*>(_v) = _t->placeholderTextColor(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundWidth(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundHeight(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->topInset(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->leftInset(); break;
        case 14: *reinterpret_cast< qreal*>(_v) = _t->rightInset(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->bottomInset(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickTextArea *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 1: _t->setImplicitWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setImplicitHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setBackground(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 4: _t->setPlaceholderText(*reinterpret_cast< QString*>(_v)); break;
        case 5: _t->setFocusReason(*reinterpret_cast< Qt::FocusReason*>(_v)); break;
        case 7: _t->setHoverEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setPalette(*reinterpret_cast< QPalette*>(_v)); break;
        case 9: _t->setPlaceholderTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 12: _t->setTopInset(*reinterpret_cast< qreal*>(_v)); break;
        case 13: _t->setLeftInset(*reinterpret_cast< qreal*>(_v)); break;
        case 14: _t->setRightInset(*reinterpret_cast< qreal*>(_v)); break;
        case 15: _t->setBottomInset(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTextArea *_t = static_cast<QQuickTextArea *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 7: _t->resetHoverEnabled(); break;
        case 8: _t->resetPalette(); break;
        case 12: _t->resetTopInset(); break;
        case 13: _t->resetLeftInset(); break;
        case 14: _t->resetRightInset(); break;
        case 15: _t->resetBottomInset(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickTextArea::staticMetaObject = { {
    &QQuickTextEdit::staticMetaObject,
    qt_meta_stringdata_QQuickTextArea.data,
    qt_meta_data_QQuickTextArea,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTextArea::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextArea::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextArea.stringdata0))
        return static_cast<void*>(this);
    return QQuickTextEdit::qt_metacast(_clname);
}

int QQuickTextArea::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 16;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTextArea::fontChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickTextArea::implicitWidthChanged3()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickTextArea::implicitHeightChanged3()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickTextArea::backgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickTextArea::placeholderTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickTextArea::focusReasonChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickTextArea::pressAndHold(QQuickMouseEvent * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QQuickTextArea::pressed(QQuickMouseEvent * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QQuickTextArea::released(QQuickMouseEvent * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QQuickTextArea::hoveredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickTextArea::hoverEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickTextArea::paletteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickTextArea::placeholderTextColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickTextArea::implicitBackgroundWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickTextArea::implicitBackgroundHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QQuickTextArea::topInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QQuickTextArea::leftInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void QQuickTextArea::rightInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void QQuickTextArea::bottomInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}
struct qt_meta_stringdata_QQuickTextAreaAttached_t {
    QByteArrayData data[5];
    char stringdata0[67];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTextAreaAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTextAreaAttached_t qt_meta_stringdata_QQuickTextAreaAttached = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQuickTextAreaAttached"
QT_MOC_LITERAL(1, 23, 16), // "flickableChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 9), // "flickable"
QT_MOC_LITERAL(4, 51, 15) // "QQuickTextArea*"

    },
    "QQuickTextAreaAttached\0flickableChanged\0"
    "\0flickable\0QQuickTextArea*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTextAreaAttached[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       1,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, 0x80000000 | 4, 0x0049590b,

 // properties: notify_signal_id
       0,

       0        // eod
};

void QQuickTextAreaAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTextAreaAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->flickableChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTextAreaAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTextAreaAttached::flickableChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickTextArea* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTextAreaAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickTextArea**>(_v) = _t->flickable(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickTextAreaAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFlickable(*reinterpret_cast< QQuickTextArea**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickTextAreaAttached::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickTextAreaAttached.data,
    qt_meta_data_QQuickTextAreaAttached,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTextAreaAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTextAreaAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTextAreaAttached.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickTextAreaAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTextAreaAttached::flickableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
