/****************************************************************************
** Meta object code from reading C++ file 'qqmltableinstancemodel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../types/qqmltableinstancemodel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmltableinstancemodel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQmlTableInstanceModel_t {
    QByteArrayData data[6];
    char stringdata0[59];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlTableInstanceModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlTableInstanceModel_t qt_meta_stringdata_QQmlTableInstanceModel = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QQmlTableInstanceModel"
QT_MOC_LITERAL(1, 23, 10), // "itemPooled"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 5), // "index"
QT_MOC_LITERAL(4, 41, 6), // "object"
QT_MOC_LITERAL(5, 48, 10) // "itemReused"

    },
    "QQmlTableInstanceModel\0itemPooled\0\0"
    "index\0object\0itemReused"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlTableInstanceModel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   24,    2, 0x06 /* Public */,
       5,    2,   29,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,    3,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,    3,    4,

       0        // eod
};

void QQmlTableInstanceModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlTableInstanceModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->itemPooled((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 1: _t->itemReused((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlTableInstanceModel::*)(int , QObject * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlTableInstanceModel::itemPooled)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQmlTableInstanceModel::*)(int , QObject * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlTableInstanceModel::itemReused)) {
                *result = 1;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QQmlTableInstanceModel::staticMetaObject = { {
    &QQmlInstanceModel::staticMetaObject,
    qt_meta_stringdata_QQmlTableInstanceModel.data,
    qt_meta_data_QQmlTableInstanceModel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlTableInstanceModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlTableInstanceModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlTableInstanceModel.stringdata0))
        return static_cast<void*>(this);
    return QQmlInstanceModel::qt_metacast(_clname);
}

int QQmlTableInstanceModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlInstanceModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void QQmlTableInstanceModel::itemPooled(int _t1, QObject * _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQmlTableInstanceModel::itemReused(int _t1, QObject * _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
