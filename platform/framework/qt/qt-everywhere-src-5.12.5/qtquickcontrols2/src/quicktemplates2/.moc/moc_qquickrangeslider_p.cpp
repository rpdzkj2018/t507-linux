/****************************************************************************
** Meta object code from reading C++ file 'qquickrangeslider_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickrangeslider_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickrangeslider_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickRangeSlider_t {
    QByteArrayData data[31];
    char stringdata0[355];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickRangeSlider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickRangeSlider_t qt_meta_stringdata_QQuickRangeSlider = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QQuickRangeSlider"
QT_MOC_LITERAL(1, 18, 11), // "fromChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 9), // "toChanged"
QT_MOC_LITERAL(4, 41, 15), // "stepSizeChanged"
QT_MOC_LITERAL(5, 57, 15), // "snapModeChanged"
QT_MOC_LITERAL(6, 73, 18), // "orientationChanged"
QT_MOC_LITERAL(7, 92, 11), // "liveChanged"
QT_MOC_LITERAL(8, 104, 25), // "touchDragThresholdChanged"
QT_MOC_LITERAL(9, 130, 9), // "setValues"
QT_MOC_LITERAL(10, 140, 10), // "firstValue"
QT_MOC_LITERAL(11, 151, 11), // "secondValue"
QT_MOC_LITERAL(12, 163, 7), // "valueAt"
QT_MOC_LITERAL(13, 171, 8), // "position"
QT_MOC_LITERAL(14, 180, 4), // "from"
QT_MOC_LITERAL(15, 185, 2), // "to"
QT_MOC_LITERAL(16, 188, 5), // "first"
QT_MOC_LITERAL(17, 194, 22), // "QQuickRangeSliderNode*"
QT_MOC_LITERAL(18, 217, 6), // "second"
QT_MOC_LITERAL(19, 224, 8), // "stepSize"
QT_MOC_LITERAL(20, 233, 8), // "snapMode"
QT_MOC_LITERAL(21, 242, 8), // "SnapMode"
QT_MOC_LITERAL(22, 251, 11), // "orientation"
QT_MOC_LITERAL(23, 263, 15), // "Qt::Orientation"
QT_MOC_LITERAL(24, 279, 4), // "live"
QT_MOC_LITERAL(25, 284, 10), // "horizontal"
QT_MOC_LITERAL(26, 295, 8), // "vertical"
QT_MOC_LITERAL(27, 304, 18), // "touchDragThreshold"
QT_MOC_LITERAL(28, 323, 6), // "NoSnap"
QT_MOC_LITERAL(29, 330, 10), // "SnapAlways"
QT_MOC_LITERAL(30, 341, 13) // "SnapOnRelease"

    },
    "QQuickRangeSlider\0fromChanged\0\0toChanged\0"
    "stepSizeChanged\0snapModeChanged\0"
    "orientationChanged\0liveChanged\0"
    "touchDragThresholdChanged\0setValues\0"
    "firstValue\0secondValue\0valueAt\0position\0"
    "from\0to\0first\0QQuickRangeSliderNode*\0"
    "second\0stepSize\0snapMode\0SnapMode\0"
    "orientation\0Qt::Orientation\0live\0"
    "horizontal\0vertical\0touchDragThreshold\0"
    "NoSnap\0SnapAlways\0SnapOnRelease"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickRangeSlider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
      11,   83, // properties
       1,  138, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   68,    2, 0x06 /* Public */,
       3,    0,   69,    2, 0x06 /* Public */,
       4,    0,   70,    2, 0x06 /* Public */,
       5,    0,   71,    2, 0x06 /* Public */,
       6,    0,   72,    2, 0x06 /* Public */,
       7,    0,   73,    2, 0x86 /* Public | MethodRevisioned */,
       8,    0,   74,    2, 0x86 /* Public | MethodRevisioned */,

 // methods: name, argc, parameters, tag, flags
       9,    2,   75,    2, 0x02 /* Public */,
      12,    1,   80,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       2,
       5,

 // methods: revision
       0,
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::QReal, QMetaType::QReal,   10,   11,
    QMetaType::QReal, QMetaType::QReal,   13,

 // properties: name, type, flags
      14, QMetaType::QReal, 0x00495903,
      15, QMetaType::QReal, 0x00495903,
      16, 0x80000000 | 17, 0x00095c09,
      18, 0x80000000 | 17, 0x00095c09,
      19, QMetaType::QReal, 0x00495903,
      20, 0x80000000 | 21, 0x0049590b,
      22, 0x80000000 | 23, 0x0049590b,
      24, QMetaType::Bool, 0x00c95903,
      25, QMetaType::Bool, 0x00c95801,
      26, QMetaType::Bool, 0x00c95801,
      27, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       0,
       1,
       0,
       0,
       2,
       3,
       4,
       5,
       4,
       4,
       6,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       3,
       3,
       5,

 // enums: name, alias, flags, count, data
      21,   21, 0x0,    3,  143,

 // enum data: key, value
      28, uint(QQuickRangeSlider::NoSnap),
      29, uint(QQuickRangeSlider::SnapAlways),
      30, uint(QQuickRangeSlider::SnapOnRelease),

       0        // eod
};

void QQuickRangeSlider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickRangeSlider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fromChanged(); break;
        case 1: _t->toChanged(); break;
        case 2: _t->stepSizeChanged(); break;
        case 3: _t->snapModeChanged(); break;
        case 4: _t->orientationChanged(); break;
        case 5: _t->liveChanged(); break;
        case 6: _t->touchDragThresholdChanged(); break;
        case 7: _t->setValues((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< qreal(*)>(_a[2]))); break;
        case 8: { qreal _r = _t->valueAt((*reinterpret_cast< qreal(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::fromChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::toChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::stepSizeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::snapModeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::orientationChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::liveChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSlider::touchDragThresholdChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
        case 2:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickRangeSliderNode* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickRangeSlider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->from(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->to(); break;
        case 2: *reinterpret_cast< QQuickRangeSliderNode**>(_v) = _t->first(); break;
        case 3: *reinterpret_cast< QQuickRangeSliderNode**>(_v) = _t->second(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->stepSize(); break;
        case 5: *reinterpret_cast< SnapMode*>(_v) = _t->snapMode(); break;
        case 6: *reinterpret_cast< Qt::Orientation*>(_v) = _t->orientation(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->live(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->isHorizontal(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->isVertical(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->touchDragThreshold(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickRangeSlider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFrom(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setTo(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setStepSize(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setSnapMode(*reinterpret_cast< SnapMode*>(_v)); break;
        case 6: _t->setOrientation(*reinterpret_cast< Qt::Orientation*>(_v)); break;
        case 7: _t->setLive(*reinterpret_cast< bool*>(_v)); break;
        case 10: _t->setTouchDragThreshold(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickRangeSlider *_t = static_cast<QQuickRangeSlider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 10: _t->resetTouchDragThreshold(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickRangeSlider::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickRangeSlider.data,
    qt_meta_data_QQuickRangeSlider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickRangeSlider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickRangeSlider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickRangeSlider.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickRangeSlider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickRangeSlider::fromChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickRangeSlider::toChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickRangeSlider::stepSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickRangeSlider::snapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickRangeSlider::orientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickRangeSlider::liveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickRangeSlider::touchDragThresholdChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
struct qt_meta_stringdata_QQuickRangeSliderNode_t {
    QByteArrayData data[23];
    char stringdata0[325];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickRangeSliderNode_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickRangeSliderNode_t qt_meta_stringdata_QQuickRangeSliderNode = {
    {
QT_MOC_LITERAL(0, 0, 21), // "QQuickRangeSliderNode"
QT_MOC_LITERAL(1, 22, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 44, 6), // "handle"
QT_MOC_LITERAL(3, 51, 12), // "valueChanged"
QT_MOC_LITERAL(4, 64, 0), // ""
QT_MOC_LITERAL(5, 65, 15), // "positionChanged"
QT_MOC_LITERAL(6, 81, 21), // "visualPositionChanged"
QT_MOC_LITERAL(7, 103, 13), // "handleChanged"
QT_MOC_LITERAL(8, 117, 14), // "pressedChanged"
QT_MOC_LITERAL(9, 132, 14), // "hoveredChanged"
QT_MOC_LITERAL(10, 147, 5), // "moved"
QT_MOC_LITERAL(11, 153, 26), // "implicitHandleWidthChanged"
QT_MOC_LITERAL(12, 180, 27), // "implicitHandleHeightChanged"
QT_MOC_LITERAL(13, 208, 8), // "increase"
QT_MOC_LITERAL(14, 217, 8), // "decrease"
QT_MOC_LITERAL(15, 226, 5), // "value"
QT_MOC_LITERAL(16, 232, 8), // "position"
QT_MOC_LITERAL(17, 241, 14), // "visualPosition"
QT_MOC_LITERAL(18, 256, 11), // "QQuickItem*"
QT_MOC_LITERAL(19, 268, 7), // "pressed"
QT_MOC_LITERAL(20, 276, 7), // "hovered"
QT_MOC_LITERAL(21, 284, 19), // "implicitHandleWidth"
QT_MOC_LITERAL(22, 304, 20) // "implicitHandleHeight"

    },
    "QQuickRangeSliderNode\0DeferredPropertyNames\0"
    "handle\0valueChanged\0\0positionChanged\0"
    "visualPositionChanged\0handleChanged\0"
    "pressedChanged\0hoveredChanged\0moved\0"
    "implicitHandleWidthChanged\0"
    "implicitHandleHeightChanged\0increase\0"
    "decrease\0value\0position\0visualPosition\0"
    "QQuickItem*\0pressed\0hovered\0"
    "implicitHandleWidth\0implicitHandleHeight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickRangeSliderNode[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      11,   16, // methods
       8,   93, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   82,    4, 0x06 /* Public */,
       5,    0,   83,    4, 0x06 /* Public */,
       6,    0,   84,    4, 0x06 /* Public */,
       7,    0,   85,    4, 0x06 /* Public */,
       8,    0,   86,    4, 0x06 /* Public */,
       9,    0,   87,    4, 0x86 /* Public | MethodRevisioned */,
      10,    0,   88,    4, 0x06 /* Public */,
      11,    0,   89,    4, 0x06 /* Public */,
      12,    0,   90,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    0,   91,    4, 0x0a /* Public */,
      14,    0,   92,    4, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,

 // slots: revision
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      15, QMetaType::QReal, 0x00495903,
      16, QMetaType::QReal, 0x00495801,
      17, QMetaType::QReal, 0x00495801,
       2, 0x80000000 | 18, 0x0049590b,
      19, QMetaType::Bool, 0x00495903,
      20, QMetaType::Bool, 0x00c95903,
      21, QMetaType::QReal, 0x00c95801,
      22, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       7,
       8,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       1,
       5,
       5,

       0        // eod
};

void QQuickRangeSliderNode::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickRangeSliderNode *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->valueChanged(); break;
        case 1: _t->positionChanged(); break;
        case 2: _t->visualPositionChanged(); break;
        case 3: _t->handleChanged(); break;
        case 4: _t->pressedChanged(); break;
        case 5: _t->hoveredChanged(); break;
        case 6: _t->moved(); break;
        case 7: _t->implicitHandleWidthChanged(); break;
        case 8: _t->implicitHandleHeightChanged(); break;
        case 9: _t->increase(); break;
        case 10: _t->decrease(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::valueChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::positionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::visualPositionChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::handleChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::pressedChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::hoveredChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::moved)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::implicitHandleWidthChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickRangeSliderNode::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickRangeSliderNode::implicitHandleHeightChanged)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickRangeSliderNode *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->value(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->position(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->visualPosition(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->handle(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isHovered(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->implicitHandleWidth(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->implicitHandleHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickRangeSliderNode *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setValue(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setHandle(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 4: _t->setPressed(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setHovered(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickRangeSliderNode::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickRangeSliderNode.data,
    qt_meta_data_QQuickRangeSliderNode,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickRangeSliderNode::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickRangeSliderNode::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickRangeSliderNode.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickRangeSliderNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickRangeSliderNode::valueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickRangeSliderNode::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickRangeSliderNode::visualPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickRangeSliderNode::handleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickRangeSliderNode::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickRangeSliderNode::hoveredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickRangeSliderNode::moved()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickRangeSliderNode::implicitHandleWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickRangeSliderNode::implicitHandleHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
