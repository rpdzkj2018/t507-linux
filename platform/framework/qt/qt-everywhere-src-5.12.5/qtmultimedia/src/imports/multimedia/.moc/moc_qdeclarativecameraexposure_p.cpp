/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativecameraexposure_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qdeclarativecameraexposure_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativecameraexposure_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeCameraExposure_t {
    QByteArrayData data[60];
    char stringdata0[1052];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeCameraExposure_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeCameraExposure_t qt_meta_stringdata_QDeclarativeCameraExposure = {
    {
QT_MOC_LITERAL(0, 0, 26), // "QDeclarativeCameraExposure"
QT_MOC_LITERAL(1, 27, 21), // "isoSensitivityChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 15), // "apertureChanged"
QT_MOC_LITERAL(4, 66, 19), // "shutterSpeedChanged"
QT_MOC_LITERAL(5, 86, 27), // "manualIsoSensitivityChanged"
QT_MOC_LITERAL(6, 114, 21), // "manualApertureChanged"
QT_MOC_LITERAL(7, 136, 25), // "manualShutterSpeedChanged"
QT_MOC_LITERAL(8, 162, 27), // "exposureCompensationChanged"
QT_MOC_LITERAL(9, 190, 19), // "exposureModeChanged"
QT_MOC_LITERAL(10, 210, 12), // "ExposureMode"
QT_MOC_LITERAL(11, 223, 29), // "supportedExposureModesChanged"
QT_MOC_LITERAL(12, 253, 19), // "meteringModeChanged"
QT_MOC_LITERAL(13, 273, 12), // "MeteringMode"
QT_MOC_LITERAL(14, 286, 24), // "spotMeteringPointChanged"
QT_MOC_LITERAL(15, 311, 15), // "setExposureMode"
QT_MOC_LITERAL(16, 327, 23), // "setExposureCompensation"
QT_MOC_LITERAL(17, 351, 2), // "ev"
QT_MOC_LITERAL(18, 354, 17), // "setManualAperture"
QT_MOC_LITERAL(19, 372, 21), // "setManualShutterSpeed"
QT_MOC_LITERAL(20, 394, 23), // "setManualIsoSensitivity"
QT_MOC_LITERAL(21, 418, 3), // "iso"
QT_MOC_LITERAL(22, 422, 15), // "setAutoAperture"
QT_MOC_LITERAL(23, 438, 19), // "setAutoShutterSpeed"
QT_MOC_LITERAL(24, 458, 21), // "setAutoIsoSensitivity"
QT_MOC_LITERAL(25, 480, 20), // "exposureCompensation"
QT_MOC_LITERAL(26, 501, 12), // "shutterSpeed"
QT_MOC_LITERAL(27, 514, 8), // "aperture"
QT_MOC_LITERAL(28, 523, 18), // "manualShutterSpeed"
QT_MOC_LITERAL(29, 542, 14), // "manualAperture"
QT_MOC_LITERAL(30, 557, 9), // "manualIso"
QT_MOC_LITERAL(31, 567, 12), // "exposureMode"
QT_MOC_LITERAL(32, 580, 22), // "supportedExposureModes"
QT_MOC_LITERAL(33, 603, 17), // "spotMeteringPoint"
QT_MOC_LITERAL(34, 621, 12), // "meteringMode"
QT_MOC_LITERAL(35, 634, 12), // "ExposureAuto"
QT_MOC_LITERAL(36, 647, 14), // "ExposureManual"
QT_MOC_LITERAL(37, 662, 16), // "ExposurePortrait"
QT_MOC_LITERAL(38, 679, 13), // "ExposureNight"
QT_MOC_LITERAL(39, 693, 17), // "ExposureBacklight"
QT_MOC_LITERAL(40, 711, 17), // "ExposureSpotlight"
QT_MOC_LITERAL(41, 729, 14), // "ExposureSports"
QT_MOC_LITERAL(42, 744, 12), // "ExposureSnow"
QT_MOC_LITERAL(43, 757, 13), // "ExposureBeach"
QT_MOC_LITERAL(44, 771, 21), // "ExposureLargeAperture"
QT_MOC_LITERAL(45, 793, 21), // "ExposureSmallAperture"
QT_MOC_LITERAL(46, 815, 14), // "ExposureAction"
QT_MOC_LITERAL(47, 830, 17), // "ExposureLandscape"
QT_MOC_LITERAL(48, 848, 21), // "ExposureNightPortrait"
QT_MOC_LITERAL(49, 870, 15), // "ExposureTheatre"
QT_MOC_LITERAL(50, 886, 14), // "ExposureSunset"
QT_MOC_LITERAL(51, 901, 19), // "ExposureSteadyPhoto"
QT_MOC_LITERAL(52, 921, 17), // "ExposureFireworks"
QT_MOC_LITERAL(53, 939, 13), // "ExposureParty"
QT_MOC_LITERAL(54, 953, 19), // "ExposureCandlelight"
QT_MOC_LITERAL(55, 973, 15), // "ExposureBarcode"
QT_MOC_LITERAL(56, 989, 18), // "ExposureModeVendor"
QT_MOC_LITERAL(57, 1008, 14), // "MeteringMatrix"
QT_MOC_LITERAL(58, 1023, 15), // "MeteringAverage"
QT_MOC_LITERAL(59, 1039, 12) // "MeteringSpot"

    },
    "QDeclarativeCameraExposure\0"
    "isoSensitivityChanged\0\0apertureChanged\0"
    "shutterSpeedChanged\0manualIsoSensitivityChanged\0"
    "manualApertureChanged\0manualShutterSpeedChanged\0"
    "exposureCompensationChanged\0"
    "exposureModeChanged\0ExposureMode\0"
    "supportedExposureModesChanged\0"
    "meteringModeChanged\0MeteringMode\0"
    "spotMeteringPointChanged\0setExposureMode\0"
    "setExposureCompensation\0ev\0setManualAperture\0"
    "setManualShutterSpeed\0setManualIsoSensitivity\0"
    "iso\0setAutoAperture\0setAutoShutterSpeed\0"
    "setAutoIsoSensitivity\0exposureCompensation\0"
    "shutterSpeed\0aperture\0manualShutterSpeed\0"
    "manualAperture\0manualIso\0exposureMode\0"
    "supportedExposureModes\0spotMeteringPoint\0"
    "meteringMode\0ExposureAuto\0ExposureManual\0"
    "ExposurePortrait\0ExposureNight\0"
    "ExposureBacklight\0ExposureSpotlight\0"
    "ExposureSports\0ExposureSnow\0ExposureBeach\0"
    "ExposureLargeAperture\0ExposureSmallAperture\0"
    "ExposureAction\0ExposureLandscape\0"
    "ExposureNightPortrait\0ExposureTheatre\0"
    "ExposureSunset\0ExposureSteadyPhoto\0"
    "ExposureFireworks\0ExposureParty\0"
    "ExposureCandlelight\0ExposureBarcode\0"
    "ExposureModeVendor\0MeteringMatrix\0"
    "MeteringAverage\0MeteringSpot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeCameraExposure[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
      11,  158, // properties
       2,  213, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       3,    1,  112,    2, 0x06 /* Public */,
       4,    1,  115,    2, 0x06 /* Public */,
       5,    1,  118,    2, 0x06 /* Public */,
       6,    1,  121,    2, 0x06 /* Public */,
       7,    1,  124,    2, 0x06 /* Public */,
       8,    1,  127,    2, 0x06 /* Public */,
       9,    1,  130,    2, 0x06 /* Public */,
      11,    0,  133,    2, 0x06 /* Public */,
      12,    1,  134,    2, 0x06 /* Public */,
      14,    1,  137,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    1,  140,    2, 0x0a /* Public */,
      16,    1,  143,    2, 0x0a /* Public */,
      18,    1,  146,    2, 0x0a /* Public */,
      19,    1,  149,    2, 0x0a /* Public */,
      20,    1,  152,    2, 0x0a /* Public */,
      22,    0,  155,    2, 0x0a /* Public */,
      23,    0,  156,    2, 0x0a /* Public */,
      24,    0,  157,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,    2,
    QMetaType::Void, QMetaType::QPointF,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 10,    2,
    QMetaType::Void, QMetaType::QReal,   17,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void, QMetaType::Int,   21,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      25, QMetaType::QReal, 0x00495103,
      21, QMetaType::Int, 0x00495001,
      26, QMetaType::QReal, 0x00495001,
      27, QMetaType::QReal, 0x00495001,
      28, QMetaType::QReal, 0x00495103,
      29, QMetaType::QReal, 0x00495103,
      30, QMetaType::QReal, 0x00495003,
      31, 0x80000000 | 10, 0x0049510b,
      32, QMetaType::QVariantList, 0x00c95001,
      33, QMetaType::QPointF, 0x00495103,
      34, 0x80000000 | 13, 0x0049510b,

 // properties: notify_signal_id
       6,
       0,
       2,
       1,
       5,
       4,
       3,
       7,
       8,
      10,
       9,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       0,
       0,

 // enums: name, alias, flags, count, data
      10,   10, 0x0,   22,  223,
      13,   13, 0x0,    3,  267,

 // enum data: key, value
      35, uint(QDeclarativeCameraExposure::ExposureAuto),
      36, uint(QDeclarativeCameraExposure::ExposureManual),
      37, uint(QDeclarativeCameraExposure::ExposurePortrait),
      38, uint(QDeclarativeCameraExposure::ExposureNight),
      39, uint(QDeclarativeCameraExposure::ExposureBacklight),
      40, uint(QDeclarativeCameraExposure::ExposureSpotlight),
      41, uint(QDeclarativeCameraExposure::ExposureSports),
      42, uint(QDeclarativeCameraExposure::ExposureSnow),
      43, uint(QDeclarativeCameraExposure::ExposureBeach),
      44, uint(QDeclarativeCameraExposure::ExposureLargeAperture),
      45, uint(QDeclarativeCameraExposure::ExposureSmallAperture),
      46, uint(QDeclarativeCameraExposure::ExposureAction),
      47, uint(QDeclarativeCameraExposure::ExposureLandscape),
      48, uint(QDeclarativeCameraExposure::ExposureNightPortrait),
      49, uint(QDeclarativeCameraExposure::ExposureTheatre),
      50, uint(QDeclarativeCameraExposure::ExposureSunset),
      51, uint(QDeclarativeCameraExposure::ExposureSteadyPhoto),
      52, uint(QDeclarativeCameraExposure::ExposureFireworks),
      53, uint(QDeclarativeCameraExposure::ExposureParty),
      54, uint(QDeclarativeCameraExposure::ExposureCandlelight),
      55, uint(QDeclarativeCameraExposure::ExposureBarcode),
      56, uint(QDeclarativeCameraExposure::ExposureModeVendor),
      57, uint(QDeclarativeCameraExposure::MeteringMatrix),
      58, uint(QDeclarativeCameraExposure::MeteringAverage),
      59, uint(QDeclarativeCameraExposure::MeteringSpot),

       0        // eod
};

void QDeclarativeCameraExposure::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeCameraExposure *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->isoSensitivityChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->apertureChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->shutterSpeedChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 3: _t->manualIsoSensitivityChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->manualApertureChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->manualShutterSpeedChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 6: _t->exposureCompensationChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 7: _t->exposureModeChanged((*reinterpret_cast< ExposureMode(*)>(_a[1]))); break;
        case 8: _t->supportedExposureModesChanged(); break;
        case 9: _t->meteringModeChanged((*reinterpret_cast< MeteringMode(*)>(_a[1]))); break;
        case 10: _t->spotMeteringPointChanged((*reinterpret_cast< QPointF(*)>(_a[1]))); break;
        case 11: _t->setExposureMode((*reinterpret_cast< ExposureMode(*)>(_a[1]))); break;
        case 12: _t->setExposureCompensation((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 13: _t->setManualAperture((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 14: _t->setManualShutterSpeed((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 15: _t->setManualIsoSensitivity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->setAutoAperture(); break;
        case 17: _t->setAutoShutterSpeed(); break;
        case 18: _t->setAutoIsoSensitivity(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeCameraExposure::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::isoSensitivityChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::apertureChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::shutterSpeedChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::manualIsoSensitivityChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::manualApertureChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::manualShutterSpeedChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::exposureCompensationChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(ExposureMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::exposureModeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::supportedExposureModesChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(MeteringMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::meteringModeChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QDeclarativeCameraExposure::*)(QPointF );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeCameraExposure::spotMeteringPointChanged)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeCameraExposure *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->exposureCompensation(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->isoSensitivity(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->shutterSpeed(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->aperture(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->manualShutterSpeed(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->manualAperture(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->manualIsoSensitivity(); break;
        case 7: *reinterpret_cast< ExposureMode*>(_v) = _t->exposureMode(); break;
        case 8: *reinterpret_cast< QVariantList*>(_v) = _t->supportedExposureModes(); break;
        case 9: *reinterpret_cast< QPointF*>(_v) = _t->spotMeteringPoint(); break;
        case 10: *reinterpret_cast< MeteringMode*>(_v) = _t->meteringMode(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeCameraExposure *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setExposureCompensation(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setManualShutterSpeed(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setManualAperture(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setManualIsoSensitivity(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setExposureMode(*reinterpret_cast< ExposureMode*>(_v)); break;
        case 9: _t->setSpotMeteringPoint(*reinterpret_cast< QPointF*>(_v)); break;
        case 10: _t->setMeteringMode(*reinterpret_cast< MeteringMode*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeCameraExposure::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeCameraExposure.data,
    qt_meta_data_QDeclarativeCameraExposure,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeCameraExposure::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeCameraExposure::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeCameraExposure.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeCameraExposure::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeCameraExposure::isoSensitivityChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeCameraExposure::apertureChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeCameraExposure::shutterSpeedChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeCameraExposure::manualIsoSensitivityChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QDeclarativeCameraExposure::manualApertureChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QDeclarativeCameraExposure::manualShutterSpeedChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QDeclarativeCameraExposure::exposureCompensationChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QDeclarativeCameraExposure::exposureModeChanged(ExposureMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QDeclarativeCameraExposure::supportedExposureModesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeCameraExposure::meteringModeChanged(MeteringMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QDeclarativeCameraExposure::spotMeteringPointChanged(QPointF _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
