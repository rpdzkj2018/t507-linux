/****************************************************************************
** Meta object code from reading C++ file 'qdownloadnetworkworker_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../services/qdownloadnetworkworker_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdownloadnetworkworker_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker_t {
    QByteArrayData data[17];
    char stringdata0[281];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker_t qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker = {
    {
QT_MOC_LITERAL(0, 0, 32), // "Qt3DCore::QDownloadNetworkWorker"
QT_MOC_LITERAL(1, 33, 13), // "submitRequest"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 29), // "Qt3DCore::QDownloadRequestPtr"
QT_MOC_LITERAL(4, 78, 7), // "request"
QT_MOC_LITERAL(5, 86, 13), // "cancelRequest"
QT_MOC_LITERAL(6, 100, 17), // "cancelAllRequests"
QT_MOC_LITERAL(7, 118, 17), // "requestDownloaded"
QT_MOC_LITERAL(8, 136, 17), // "onRequestSubmited"
QT_MOC_LITERAL(9, 154, 18), // "onRequestCancelled"
QT_MOC_LITERAL(10, 173, 22), // "onAllRequestsCancelled"
QT_MOC_LITERAL(11, 196, 17), // "onRequestFinished"
QT_MOC_LITERAL(12, 214, 14), // "QNetworkReply*"
QT_MOC_LITERAL(13, 229, 5), // "reply"
QT_MOC_LITERAL(14, 235, 20), // "onDownloadProgressed"
QT_MOC_LITERAL(15, 256, 13), // "bytesReceived"
QT_MOC_LITERAL(16, 270, 10) // "bytesTotal"

    },
    "Qt3DCore::QDownloadNetworkWorker\0"
    "submitRequest\0\0Qt3DCore::QDownloadRequestPtr\0"
    "request\0cancelRequest\0cancelAllRequests\0"
    "requestDownloaded\0onRequestSubmited\0"
    "onRequestCancelled\0onAllRequestsCancelled\0"
    "onRequestFinished\0QNetworkReply*\0reply\0"
    "onDownloadProgressed\0bytesReceived\0"
    "bytesTotal"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QDownloadNetworkWorker[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,
       5,    1,   62,    2, 0x06 /* Public */,
       6,    0,   65,    2, 0x06 /* Public */,
       7,    1,   66,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   69,    2, 0x08 /* Private */,
       9,    1,   72,    2, 0x08 /* Private */,
      10,    0,   75,    2, 0x08 /* Private */,
      11,    1,   76,    2, 0x08 /* Private */,
      14,    2,   79,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, QMetaType::LongLong, QMetaType::LongLong,   15,   16,

       0        // eod
};

void Qt3DCore::QDownloadNetworkWorker::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDownloadNetworkWorker *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->submitRequest((*reinterpret_cast< const Qt3DCore::QDownloadRequestPtr(*)>(_a[1]))); break;
        case 1: _t->cancelRequest((*reinterpret_cast< const Qt3DCore::QDownloadRequestPtr(*)>(_a[1]))); break;
        case 2: _t->cancelAllRequests(); break;
        case 3: _t->requestDownloaded((*reinterpret_cast< const Qt3DCore::QDownloadRequestPtr(*)>(_a[1]))); break;
        case 4: _t->onRequestSubmited((*reinterpret_cast< const Qt3DCore::QDownloadRequestPtr(*)>(_a[1]))); break;
        case 5: _t->onRequestCancelled((*reinterpret_cast< const Qt3DCore::QDownloadRequestPtr(*)>(_a[1]))); break;
        case 6: _t->onAllRequestsCancelled(); break;
        case 7: _t->onRequestFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 8: _t->onDownloadProgressed((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DCore::QDownloadRequestPtr >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DCore::QDownloadRequestPtr >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DCore::QDownloadRequestPtr >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DCore::QDownloadRequestPtr >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DCore::QDownloadRequestPtr >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDownloadNetworkWorker::*)(const Qt3DCore::QDownloadRequestPtr & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDownloadNetworkWorker::submitRequest)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDownloadNetworkWorker::*)(const Qt3DCore::QDownloadRequestPtr & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDownloadNetworkWorker::cancelRequest)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDownloadNetworkWorker::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDownloadNetworkWorker::cancelAllRequests)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDownloadNetworkWorker::*)(const Qt3DCore::QDownloadRequestPtr & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDownloadNetworkWorker::requestDownloaded)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Qt3DCore::QDownloadNetworkWorker::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker.data,
    qt_meta_data_Qt3DCore__QDownloadNetworkWorker,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DCore::QDownloadNetworkWorker::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QDownloadNetworkWorker::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QDownloadNetworkWorker.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Qt3DCore::QDownloadNetworkWorker::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void Qt3DCore::QDownloadNetworkWorker::submitRequest(const Qt3DCore::QDownloadRequestPtr & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QDownloadNetworkWorker::cancelRequest(const Qt3DCore::QDownloadRequestPtr & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QDownloadNetworkWorker::cancelAllRequests()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Qt3DCore::QDownloadNetworkWorker::requestDownloaded(const Qt3DCore::QDownloadRequestPtr & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
