/****************************************************************************
** Meta object code from reading C++ file 'qskeletonloader.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../transforms/qskeletonloader.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qskeletonloader.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DCore__QSkeletonLoader_t {
    QByteArrayData data[18];
    char stringdata0[230];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QSkeletonLoader_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QSkeletonLoader_t qt_meta_stringdata_Qt3DCore__QSkeletonLoader = {
    {
QT_MOC_LITERAL(0, 0, 25), // "Qt3DCore::QSkeletonLoader"
QT_MOC_LITERAL(1, 26, 13), // "sourceChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 6), // "source"
QT_MOC_LITERAL(4, 48, 13), // "statusChanged"
QT_MOC_LITERAL(5, 62, 6), // "Status"
QT_MOC_LITERAL(6, 69, 6), // "status"
QT_MOC_LITERAL(7, 76, 26), // "createJointsEnabledChanged"
QT_MOC_LITERAL(8, 103, 19), // "createJointsEnabled"
QT_MOC_LITERAL(9, 123, 16), // "rootJointChanged"
QT_MOC_LITERAL(10, 140, 17), // "Qt3DCore::QJoint*"
QT_MOC_LITERAL(11, 158, 9), // "rootJoint"
QT_MOC_LITERAL(12, 168, 9), // "setSource"
QT_MOC_LITERAL(13, 178, 22), // "setCreateJointsEnabled"
QT_MOC_LITERAL(14, 201, 7), // "enabled"
QT_MOC_LITERAL(15, 209, 8), // "NotReady"
QT_MOC_LITERAL(16, 218, 5), // "Ready"
QT_MOC_LITERAL(17, 224, 5) // "Error"

    },
    "Qt3DCore::QSkeletonLoader\0sourceChanged\0"
    "\0source\0statusChanged\0Status\0status\0"
    "createJointsEnabledChanged\0"
    "createJointsEnabled\0rootJointChanged\0"
    "Qt3DCore::QJoint*\0rootJoint\0setSource\0"
    "setCreateJointsEnabled\0enabled\0NotReady\0"
    "Ready\0Error"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QSkeletonLoader[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       4,   62, // properties
       1,   78, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,
       7,    1,   50,    2, 0x06 /* Public */,
       9,    1,   53,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    1,   56,    2, 0x0a /* Public */,
      13,    1,   59,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void, 0x80000000 | 10,   11,

 // slots: parameters
    QMetaType::Void, QMetaType::QUrl,    3,
    QMetaType::Void, QMetaType::Bool,   14,

 // properties: name, type, flags
       3, QMetaType::QUrl, 0x00495103,
       6, 0x80000000 | 5, 0x00495009,
       8, QMetaType::Bool, 0x00495103,
      11, 0x80000000 | 10, 0x00495009,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,

 // enums: name, alias, flags, count, data
       5,    5, 0x0,    3,   83,

 // enum data: key, value
      15, uint(Qt3DCore::QSkeletonLoader::NotReady),
      16, uint(Qt3DCore::QSkeletonLoader::Ready),
      17, uint(Qt3DCore::QSkeletonLoader::Error),

       0        // eod
};

void Qt3DCore::QSkeletonLoader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QSkeletonLoader *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sourceChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 1: _t->statusChanged((*reinterpret_cast< Status(*)>(_a[1]))); break;
        case 2: _t->createJointsEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->rootJointChanged((*reinterpret_cast< Qt3DCore::QJoint*(*)>(_a[1]))); break;
        case 4: _t->setSource((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 5: _t->setCreateJointsEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QSkeletonLoader::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QSkeletonLoader::sourceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QSkeletonLoader::*)(Status );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QSkeletonLoader::statusChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QSkeletonLoader::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QSkeletonLoader::createJointsEnabledChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QSkeletonLoader::*)(Qt3DCore::QJoint * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QSkeletonLoader::rootJointChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QSkeletonLoader *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QUrl*>(_v) = _t->source(); break;
        case 1: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isCreateJointsEnabled(); break;
        case 3: *reinterpret_cast< Qt3DCore::QJoint**>(_v) = _t->rootJoint(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QSkeletonLoader *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 2: _t->setCreateJointsEnabled(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DCore::QSkeletonLoader::staticMetaObject = { {
    &QAbstractSkeleton::staticMetaObject,
    qt_meta_stringdata_Qt3DCore__QSkeletonLoader.data,
    qt_meta_data_Qt3DCore__QSkeletonLoader,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DCore::QSkeletonLoader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QSkeletonLoader::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QSkeletonLoader.stringdata0))
        return static_cast<void*>(this);
    return QAbstractSkeleton::qt_metacast(_clname);
}

int Qt3DCore::QSkeletonLoader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractSkeleton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::QSkeletonLoader::sourceChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QSkeletonLoader::statusChanged(Status _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QSkeletonLoader::createJointsEnabledChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DCore::QSkeletonLoader::rootJointChanged(Qt3DCore::QJoint * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
