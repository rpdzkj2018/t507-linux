/****************************************************************************
** Meta object code from reading C++ file 'iiosensorproxysensorbase.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../iiosensorproxysensorbase.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'iiosensorproxysensorbase.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_IIOSensorProxySensorBase_t {
    QByteArrayData data[8];
    char stringdata0[132];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IIOSensorProxySensorBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IIOSensorProxySensorBase_t qt_meta_stringdata_IIOSensorProxySensorBase = {
    {
QT_MOC_LITERAL(0, 0, 24), // "IIOSensorProxySensorBase"
QT_MOC_LITERAL(1, 25, 17), // "serviceRegistered"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 19), // "serviceUnregistered"
QT_MOC_LITERAL(4, 64, 17), // "propertiesChanged"
QT_MOC_LITERAL(5, 82, 9), // "interface"
QT_MOC_LITERAL(6, 92, 17), // "changedProperties"
QT_MOC_LITERAL(7, 110, 21) // "invalidatedProperties"

    },
    "IIOSensorProxySensorBase\0serviceRegistered\0"
    "\0serviceUnregistered\0propertiesChanged\0"
    "interface\0changedProperties\0"
    "invalidatedProperties"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IIOSensorProxySensorBase[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x08 /* Private */,
       3,    0,   30,    2, 0x08 /* Private */,
       4,    3,   31,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QVariantMap, QMetaType::QStringList,    5,    6,    7,

       0        // eod
};

void IIOSensorProxySensorBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<IIOSensorProxySensorBase *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->serviceRegistered(); break;
        case 1: _t->serviceUnregistered(); break;
        case 2: _t->propertiesChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])),(*reinterpret_cast< const QStringList(*)>(_a[3]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject IIOSensorProxySensorBase::staticMetaObject = { {
    &QSensorBackend::staticMetaObject,
    qt_meta_stringdata_IIOSensorProxySensorBase.data,
    qt_meta_data_IIOSensorProxySensorBase,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *IIOSensorProxySensorBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IIOSensorProxySensorBase::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_IIOSensorProxySensorBase.stringdata0))
        return static_cast<void*>(this);
    return QSensorBackend::qt_metacast(_clname);
}

int IIOSensorProxySensorBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSensorBackend::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
