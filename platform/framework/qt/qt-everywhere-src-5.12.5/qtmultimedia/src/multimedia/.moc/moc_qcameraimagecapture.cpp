/****************************************************************************
** Meta object code from reading C++ file 'qcameraimagecapture.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../camera/qcameraimagecapture.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcameraimagecapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QCameraImageCapture_t {
    QByteArrayData data[42];
    char stringdata0[586];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QCameraImageCapture_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QCameraImageCapture_t qt_meta_stringdata_QCameraImageCapture = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QCameraImageCapture"
QT_MOC_LITERAL(1, 20, 5), // "error"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 2), // "id"
QT_MOC_LITERAL(4, 30, 26), // "QCameraImageCapture::Error"
QT_MOC_LITERAL(5, 57, 11), // "errorString"
QT_MOC_LITERAL(6, 69, 22), // "readyForCaptureChanged"
QT_MOC_LITERAL(7, 92, 5), // "ready"
QT_MOC_LITERAL(8, 98, 19), // "bufferFormatChanged"
QT_MOC_LITERAL(9, 118, 24), // "QVideoFrame::PixelFormat"
QT_MOC_LITERAL(10, 143, 6), // "format"
QT_MOC_LITERAL(11, 150, 25), // "captureDestinationChanged"
QT_MOC_LITERAL(12, 176, 40), // "QCameraImageCapture::CaptureD..."
QT_MOC_LITERAL(13, 217, 11), // "destination"
QT_MOC_LITERAL(14, 229, 12), // "imageExposed"
QT_MOC_LITERAL(15, 242, 13), // "imageCaptured"
QT_MOC_LITERAL(16, 256, 7), // "preview"
QT_MOC_LITERAL(17, 264, 22), // "imageMetadataAvailable"
QT_MOC_LITERAL(18, 287, 3), // "key"
QT_MOC_LITERAL(19, 291, 5), // "value"
QT_MOC_LITERAL(20, 297, 14), // "imageAvailable"
QT_MOC_LITERAL(21, 312, 11), // "QVideoFrame"
QT_MOC_LITERAL(22, 324, 5), // "frame"
QT_MOC_LITERAL(23, 330, 10), // "imageSaved"
QT_MOC_LITERAL(24, 341, 8), // "fileName"
QT_MOC_LITERAL(25, 350, 7), // "capture"
QT_MOC_LITERAL(26, 358, 8), // "location"
QT_MOC_LITERAL(27, 367, 13), // "cancelCapture"
QT_MOC_LITERAL(28, 381, 8), // "_q_error"
QT_MOC_LITERAL(29, 390, 15), // "_q_readyChanged"
QT_MOC_LITERAL(30, 406, 19), // "_q_serviceDestroyed"
QT_MOC_LITERAL(31, 426, 15), // "readyForCapture"
QT_MOC_LITERAL(32, 442, 5), // "Error"
QT_MOC_LITERAL(33, 448, 7), // "NoError"
QT_MOC_LITERAL(34, 456, 13), // "NotReadyError"
QT_MOC_LITERAL(35, 470, 13), // "ResourceError"
QT_MOC_LITERAL(36, 484, 15), // "OutOfSpaceError"
QT_MOC_LITERAL(37, 500, 24), // "NotSupportedFeatureError"
QT_MOC_LITERAL(38, 525, 11), // "FormatError"
QT_MOC_LITERAL(39, 537, 18), // "CaptureDestination"
QT_MOC_LITERAL(40, 556, 13), // "CaptureToFile"
QT_MOC_LITERAL(41, 570, 15) // "CaptureToBuffer"

    },
    "QCameraImageCapture\0error\0\0id\0"
    "QCameraImageCapture::Error\0errorString\0"
    "readyForCaptureChanged\0ready\0"
    "bufferFormatChanged\0QVideoFrame::PixelFormat\0"
    "format\0captureDestinationChanged\0"
    "QCameraImageCapture::CaptureDestinations\0"
    "destination\0imageExposed\0imageCaptured\0"
    "preview\0imageMetadataAvailable\0key\0"
    "value\0imageAvailable\0QVideoFrame\0frame\0"
    "imageSaved\0fileName\0capture\0location\0"
    "cancelCapture\0_q_error\0_q_readyChanged\0"
    "_q_serviceDestroyed\0readyForCapture\0"
    "Error\0NoError\0NotReadyError\0ResourceError\0"
    "OutOfSpaceError\0NotSupportedFeatureError\0"
    "FormatError\0CaptureDestination\0"
    "CaptureToFile\0CaptureToBuffer"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QCameraImageCapture[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       1,  146, // properties
       2,  150, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    3,   89,    2, 0x06 /* Public */,
       6,    1,   96,    2, 0x06 /* Public */,
       8,    1,   99,    2, 0x06 /* Public */,
      11,    1,  102,    2, 0x06 /* Public */,
      14,    1,  105,    2, 0x06 /* Public */,
      15,    2,  108,    2, 0x06 /* Public */,
      17,    3,  113,    2, 0x06 /* Public */,
      20,    2,  120,    2, 0x06 /* Public */,
      23,    2,  125,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      25,    1,  130,    2, 0x0a /* Public */,
      25,    0,  133,    2, 0x2a /* Public | MethodCloned */,
      27,    0,  134,    2, 0x0a /* Public */,
      28,    3,  135,    2, 0x08 /* Private */,
      29,    1,  142,    2, 0x08 /* Private */,
      30,    0,  145,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 4, QMetaType::QString,    3,    1,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, 0x80000000 | 12,   13,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int, QMetaType::QImage,    3,   16,
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::QVariant,    3,   18,   19,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 21,    3,   22,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,    3,   24,

 // slots: parameters
    QMetaType::Int, QMetaType::QString,   26,
    QMetaType::Int,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int, QMetaType::QString,    2,    2,    2,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void,

 // properties: name, type, flags
      31, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       1,

 // enums: name, alias, flags, count, data
      32,   32, 0x0,    6,  160,
      39,   39, 0x0,    2,  172,

 // enum data: key, value
      33, uint(QCameraImageCapture::NoError),
      34, uint(QCameraImageCapture::NotReadyError),
      35, uint(QCameraImageCapture::ResourceError),
      36, uint(QCameraImageCapture::OutOfSpaceError),
      37, uint(QCameraImageCapture::NotSupportedFeatureError),
      38, uint(QCameraImageCapture::FormatError),
      40, uint(QCameraImageCapture::CaptureToFile),
      41, uint(QCameraImageCapture::CaptureToBuffer),

       0        // eod
};

void QCameraImageCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QCameraImageCapture *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->error((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QCameraImageCapture::Error(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 1: _t->readyForCaptureChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->bufferFormatChanged((*reinterpret_cast< QVideoFrame::PixelFormat(*)>(_a[1]))); break;
        case 3: _t->captureDestinationChanged((*reinterpret_cast< QCameraImageCapture::CaptureDestinations(*)>(_a[1]))); break;
        case 4: _t->imageExposed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->imageCaptured((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QImage(*)>(_a[2]))); break;
        case 6: _t->imageMetadataAvailable((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QVariant(*)>(_a[3]))); break;
        case 7: _t->imageAvailable((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QVideoFrame(*)>(_a[2]))); break;
        case 8: _t->imageSaved((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 9: { int _r = _t->capture((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 10: { int _r = _t->capture();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->cancelCapture(); break;
        case 12: _t->d_func()->_q_error((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 13: _t->d_func()->_q_readyChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->d_func()->_q_serviceDestroyed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCameraImageCapture::Error >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVideoFrame::PixelFormat >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCameraImageCapture::CaptureDestinations >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVideoFrame >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QCameraImageCapture::*)(int , QCameraImageCapture::Error , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::error)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::readyForCaptureChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(QVideoFrame::PixelFormat );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::bufferFormatChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(QCameraImageCapture::CaptureDestinations );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::captureDestinationChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::imageExposed)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(int , const QImage & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::imageCaptured)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(int , const QString & , const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::imageMetadataAvailable)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(int , const QVideoFrame & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::imageAvailable)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QCameraImageCapture::*)(int , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraImageCapture::imageSaved)) {
                *result = 8;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QCameraImageCapture *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isReadyForCapture(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QCameraImageCapture::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QCameraImageCapture.data,
    qt_meta_data_QCameraImageCapture,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QCameraImageCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QCameraImageCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QCameraImageCapture.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QMediaBindableInterface"))
        return static_cast< QMediaBindableInterface*>(this);
    if (!strcmp(_clname, "org.qt-project.qt.mediabindable/5.0"))
        return static_cast< QMediaBindableInterface*>(this);
    return QObject::qt_metacast(_clname);
}

int QCameraImageCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QCameraImageCapture::error(int _t1, QCameraImageCapture::Error _t2, const QString & _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QCameraImageCapture::readyForCaptureChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QCameraImageCapture::bufferFormatChanged(QVideoFrame::PixelFormat _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QCameraImageCapture::captureDestinationChanged(QCameraImageCapture::CaptureDestinations _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QCameraImageCapture::imageExposed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QCameraImageCapture::imageCaptured(int _t1, const QImage & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QCameraImageCapture::imageMetadataAvailable(int _t1, const QString & _t2, const QVariant & _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QCameraImageCapture::imageAvailable(int _t1, const QVideoFrame & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QCameraImageCapture::imageSaved(int _t1, const QString & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
