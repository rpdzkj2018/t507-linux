/****************************************************************************
** Meta object code from reading C++ file 'qcameraexposure.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../camera/qcameraexposure.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcameraexposure.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QCameraExposure_t {
    QByteArrayData data[75];
    char stringdata0[1233];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QCameraExposure_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QCameraExposure_t qt_meta_stringdata_QCameraExposure = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QCameraExposure"
QT_MOC_LITERAL(1, 16, 10), // "flashReady"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 15), // "apertureChanged"
QT_MOC_LITERAL(4, 44, 20), // "apertureRangeChanged"
QT_MOC_LITERAL(5, 65, 19), // "shutterSpeedChanged"
QT_MOC_LITERAL(6, 85, 5), // "speed"
QT_MOC_LITERAL(7, 91, 24), // "shutterSpeedRangeChanged"
QT_MOC_LITERAL(8, 116, 21), // "isoSensitivityChanged"
QT_MOC_LITERAL(9, 138, 27), // "exposureCompensationChanged"
QT_MOC_LITERAL(10, 166, 12), // "setFlashMode"
QT_MOC_LITERAL(11, 179, 10), // "FlashModes"
QT_MOC_LITERAL(12, 190, 4), // "mode"
QT_MOC_LITERAL(13, 195, 15), // "setExposureMode"
QT_MOC_LITERAL(14, 211, 12), // "ExposureMode"
QT_MOC_LITERAL(15, 224, 15), // "setMeteringMode"
QT_MOC_LITERAL(16, 240, 12), // "MeteringMode"
QT_MOC_LITERAL(17, 253, 23), // "setExposureCompensation"
QT_MOC_LITERAL(18, 277, 2), // "ev"
QT_MOC_LITERAL(19, 280, 23), // "setManualIsoSensitivity"
QT_MOC_LITERAL(20, 304, 3), // "iso"
QT_MOC_LITERAL(21, 308, 21), // "setAutoIsoSensitivity"
QT_MOC_LITERAL(22, 330, 17), // "setManualAperture"
QT_MOC_LITERAL(23, 348, 8), // "aperture"
QT_MOC_LITERAL(24, 357, 15), // "setAutoAperture"
QT_MOC_LITERAL(25, 373, 21), // "setManualShutterSpeed"
QT_MOC_LITERAL(26, 395, 7), // "seconds"
QT_MOC_LITERAL(27, 403, 19), // "setAutoShutterSpeed"
QT_MOC_LITERAL(28, 423, 27), // "_q_exposureParameterChanged"
QT_MOC_LITERAL(29, 451, 32), // "_q_exposureParameterRangeChanged"
QT_MOC_LITERAL(30, 484, 12), // "shutterSpeed"
QT_MOC_LITERAL(31, 497, 14), // "isoSensitivity"
QT_MOC_LITERAL(32, 512, 20), // "exposureCompensation"
QT_MOC_LITERAL(33, 533, 9), // "flashMode"
QT_MOC_LITERAL(34, 543, 27), // "QCameraExposure::FlashModes"
QT_MOC_LITERAL(35, 571, 12), // "exposureMode"
QT_MOC_LITERAL(36, 584, 29), // "QCameraExposure::ExposureMode"
QT_MOC_LITERAL(37, 614, 12), // "meteringMode"
QT_MOC_LITERAL(38, 627, 29), // "QCameraExposure::MeteringMode"
QT_MOC_LITERAL(39, 657, 9), // "FlashMode"
QT_MOC_LITERAL(40, 667, 9), // "FlashAuto"
QT_MOC_LITERAL(41, 677, 8), // "FlashOff"
QT_MOC_LITERAL(42, 686, 7), // "FlashOn"
QT_MOC_LITERAL(43, 694, 20), // "FlashRedEyeReduction"
QT_MOC_LITERAL(44, 715, 9), // "FlashFill"
QT_MOC_LITERAL(45, 725, 10), // "FlashTorch"
QT_MOC_LITERAL(46, 736, 15), // "FlashVideoLight"
QT_MOC_LITERAL(47, 752, 25), // "FlashSlowSyncFrontCurtain"
QT_MOC_LITERAL(48, 778, 24), // "FlashSlowSyncRearCurtain"
QT_MOC_LITERAL(49, 803, 11), // "FlashManual"
QT_MOC_LITERAL(50, 815, 12), // "ExposureAuto"
QT_MOC_LITERAL(51, 828, 14), // "ExposureManual"
QT_MOC_LITERAL(52, 843, 16), // "ExposurePortrait"
QT_MOC_LITERAL(53, 860, 13), // "ExposureNight"
QT_MOC_LITERAL(54, 874, 17), // "ExposureBacklight"
QT_MOC_LITERAL(55, 892, 17), // "ExposureSpotlight"
QT_MOC_LITERAL(56, 910, 14), // "ExposureSports"
QT_MOC_LITERAL(57, 925, 12), // "ExposureSnow"
QT_MOC_LITERAL(58, 938, 13), // "ExposureBeach"
QT_MOC_LITERAL(59, 952, 21), // "ExposureLargeAperture"
QT_MOC_LITERAL(60, 974, 21), // "ExposureSmallAperture"
QT_MOC_LITERAL(61, 996, 14), // "ExposureAction"
QT_MOC_LITERAL(62, 1011, 17), // "ExposureLandscape"
QT_MOC_LITERAL(63, 1029, 21), // "ExposureNightPortrait"
QT_MOC_LITERAL(64, 1051, 15), // "ExposureTheatre"
QT_MOC_LITERAL(65, 1067, 14), // "ExposureSunset"
QT_MOC_LITERAL(66, 1082, 19), // "ExposureSteadyPhoto"
QT_MOC_LITERAL(67, 1102, 17), // "ExposureFireworks"
QT_MOC_LITERAL(68, 1120, 13), // "ExposureParty"
QT_MOC_LITERAL(69, 1134, 19), // "ExposureCandlelight"
QT_MOC_LITERAL(70, 1154, 15), // "ExposureBarcode"
QT_MOC_LITERAL(71, 1170, 18), // "ExposureModeVendor"
QT_MOC_LITERAL(72, 1189, 14), // "MeteringMatrix"
QT_MOC_LITERAL(73, 1204, 15), // "MeteringAverage"
QT_MOC_LITERAL(74, 1220, 12) // "MeteringSpot"

    },
    "QCameraExposure\0flashReady\0\0apertureChanged\0"
    "apertureRangeChanged\0shutterSpeedChanged\0"
    "speed\0shutterSpeedRangeChanged\0"
    "isoSensitivityChanged\0exposureCompensationChanged\0"
    "setFlashMode\0FlashModes\0mode\0"
    "setExposureMode\0ExposureMode\0"
    "setMeteringMode\0MeteringMode\0"
    "setExposureCompensation\0ev\0"
    "setManualIsoSensitivity\0iso\0"
    "setAutoIsoSensitivity\0setManualAperture\0"
    "aperture\0setAutoAperture\0setManualShutterSpeed\0"
    "seconds\0setAutoShutterSpeed\0"
    "_q_exposureParameterChanged\0"
    "_q_exposureParameterRangeChanged\0"
    "shutterSpeed\0isoSensitivity\0"
    "exposureCompensation\0flashMode\0"
    "QCameraExposure::FlashModes\0exposureMode\0"
    "QCameraExposure::ExposureMode\0"
    "meteringMode\0QCameraExposure::MeteringMode\0"
    "FlashMode\0FlashAuto\0FlashOff\0FlashOn\0"
    "FlashRedEyeReduction\0FlashFill\0"
    "FlashTorch\0FlashVideoLight\0"
    "FlashSlowSyncFrontCurtain\0"
    "FlashSlowSyncRearCurtain\0FlashManual\0"
    "ExposureAuto\0ExposureManual\0"
    "ExposurePortrait\0ExposureNight\0"
    "ExposureBacklight\0ExposureSpotlight\0"
    "ExposureSports\0ExposureSnow\0ExposureBeach\0"
    "ExposureLargeAperture\0ExposureSmallAperture\0"
    "ExposureAction\0ExposureLandscape\0"
    "ExposureNightPortrait\0ExposureTheatre\0"
    "ExposureSunset\0ExposureSteadyPhoto\0"
    "ExposureFireworks\0ExposureParty\0"
    "ExposureCandlelight\0ExposureBarcode\0"
    "ExposureModeVendor\0MeteringMatrix\0"
    "MeteringAverage\0MeteringSpot"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QCameraExposure[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       8,  156, // properties
       3,  188, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  109,    2, 0x06 /* Public */,
       3,    1,  112,    2, 0x06 /* Public */,
       4,    0,  115,    2, 0x06 /* Public */,
       5,    1,  116,    2, 0x06 /* Public */,
       7,    0,  119,    2, 0x06 /* Public */,
       8,    1,  120,    2, 0x06 /* Public */,
       9,    1,  123,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,  126,    2, 0x0a /* Public */,
      13,    1,  129,    2, 0x0a /* Public */,
      15,    1,  132,    2, 0x0a /* Public */,
      17,    1,  135,    2, 0x0a /* Public */,
      19,    1,  138,    2, 0x0a /* Public */,
      21,    0,  141,    2, 0x0a /* Public */,
      22,    1,  142,    2, 0x0a /* Public */,
      24,    0,  145,    2, 0x0a /* Public */,
      25,    1,  146,    2, 0x0a /* Public */,
      27,    0,  149,    2, 0x0a /* Public */,
      28,    1,  150,    2, 0x08 /* Private */,
      29,    1,  153,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::QReal,    2,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QReal,    2,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, 0x80000000 | 14,   12,
    QMetaType::Void, 0x80000000 | 16,   12,
    QMetaType::Void, QMetaType::QReal,   18,
    QMetaType::Void, QMetaType::Int,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   23,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QReal,   26,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,

 // properties: name, type, flags
      23, QMetaType::QReal, 0x00495001,
      30, QMetaType::QReal, 0x00495001,
      31, QMetaType::Int, 0x00495001,
      32, QMetaType::QReal, 0x00495103,
       1, QMetaType::Bool, 0x00495001,
      33, 0x80000000 | 34, 0x0009510b,
      35, 0x80000000 | 36, 0x0009510b,
      37, 0x80000000 | 38, 0x0009510b,

 // properties: notify_signal_id
       1,
       3,
       5,
       6,
       0,
       0,
       0,
       0,

 // enums: name, alias, flags, count, data
      39,   39, 0x0,   10,  203,
      14,   14, 0x0,   22,  223,
      16,   16, 0x0,    3,  267,

 // enum data: key, value
      40, uint(QCameraExposure::FlashAuto),
      41, uint(QCameraExposure::FlashOff),
      42, uint(QCameraExposure::FlashOn),
      43, uint(QCameraExposure::FlashRedEyeReduction),
      44, uint(QCameraExposure::FlashFill),
      45, uint(QCameraExposure::FlashTorch),
      46, uint(QCameraExposure::FlashVideoLight),
      47, uint(QCameraExposure::FlashSlowSyncFrontCurtain),
      48, uint(QCameraExposure::FlashSlowSyncRearCurtain),
      49, uint(QCameraExposure::FlashManual),
      50, uint(QCameraExposure::ExposureAuto),
      51, uint(QCameraExposure::ExposureManual),
      52, uint(QCameraExposure::ExposurePortrait),
      53, uint(QCameraExposure::ExposureNight),
      54, uint(QCameraExposure::ExposureBacklight),
      55, uint(QCameraExposure::ExposureSpotlight),
      56, uint(QCameraExposure::ExposureSports),
      57, uint(QCameraExposure::ExposureSnow),
      58, uint(QCameraExposure::ExposureBeach),
      59, uint(QCameraExposure::ExposureLargeAperture),
      60, uint(QCameraExposure::ExposureSmallAperture),
      61, uint(QCameraExposure::ExposureAction),
      62, uint(QCameraExposure::ExposureLandscape),
      63, uint(QCameraExposure::ExposureNightPortrait),
      64, uint(QCameraExposure::ExposureTheatre),
      65, uint(QCameraExposure::ExposureSunset),
      66, uint(QCameraExposure::ExposureSteadyPhoto),
      67, uint(QCameraExposure::ExposureFireworks),
      68, uint(QCameraExposure::ExposureParty),
      69, uint(QCameraExposure::ExposureCandlelight),
      70, uint(QCameraExposure::ExposureBarcode),
      71, uint(QCameraExposure::ExposureModeVendor),
      72, uint(QCameraExposure::MeteringMatrix),
      73, uint(QCameraExposure::MeteringAverage),
      74, uint(QCameraExposure::MeteringSpot),

       0        // eod
};

void QCameraExposure::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QCameraExposure *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->flashReady((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->apertureChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->apertureRangeChanged(); break;
        case 3: _t->shutterSpeedChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 4: _t->shutterSpeedRangeChanged(); break;
        case 5: _t->isoSensitivityChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->exposureCompensationChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 7: _t->setFlashMode((*reinterpret_cast< FlashModes(*)>(_a[1]))); break;
        case 8: _t->setExposureMode((*reinterpret_cast< ExposureMode(*)>(_a[1]))); break;
        case 9: _t->setMeteringMode((*reinterpret_cast< MeteringMode(*)>(_a[1]))); break;
        case 10: _t->setExposureCompensation((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 11: _t->setManualIsoSensitivity((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->setAutoIsoSensitivity(); break;
        case 13: _t->setManualAperture((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 14: _t->setAutoAperture(); break;
        case 15: _t->setManualShutterSpeed((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 16: _t->setAutoShutterSpeed(); break;
        case 17: _t->d_func()->_q_exposureParameterChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->d_func()->_q_exposureParameterRangeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QCameraExposure::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::flashReady)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::apertureChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::apertureRangeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::shutterSpeedChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::shutterSpeedRangeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::isoSensitivityChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QCameraExposure::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCameraExposure::exposureCompensationChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCameraExposure::ExposureMode >(); break;
        case 5:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCameraExposure::FlashModes >(); break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QCameraExposure::MeteringMode >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QCameraExposure *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->aperture(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->shutterSpeed(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->isoSensitivity(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->exposureCompensation(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isFlashReady(); break;
        case 5: *reinterpret_cast< QCameraExposure::FlashModes*>(_v) = _t->flashMode(); break;
        case 6: *reinterpret_cast< QCameraExposure::ExposureMode*>(_v) = _t->exposureMode(); break;
        case 7: *reinterpret_cast< QCameraExposure::MeteringMode*>(_v) = _t->meteringMode(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QCameraExposure *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 3: _t->setExposureCompensation(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setFlashMode(*reinterpret_cast< QCameraExposure::FlashModes*>(_v)); break;
        case 6: _t->setExposureMode(*reinterpret_cast< QCameraExposure::ExposureMode*>(_v)); break;
        case 7: _t->setMeteringMode(*reinterpret_cast< QCameraExposure::MeteringMode*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QCameraExposure::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QCameraExposure.data,
    qt_meta_data_QCameraExposure,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QCameraExposure::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QCameraExposure::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QCameraExposure.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QCameraExposure::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QCameraExposure::flashReady(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QCameraExposure::apertureChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QCameraExposure::apertureRangeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QCameraExposure::shutterSpeedChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QCameraExposure::shutterSpeedRangeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QCameraExposure::isoSensitivityChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QCameraExposure::exposureCompensationChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
