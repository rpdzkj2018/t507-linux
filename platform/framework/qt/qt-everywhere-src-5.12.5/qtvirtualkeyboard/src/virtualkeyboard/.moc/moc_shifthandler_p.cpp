/****************************************************************************
** Meta object code from reading C++ file 'shifthandler_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../shifthandler_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'shifthandler_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler_t {
    QByteArrayData data[21];
    char stringdata0[392];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler_t qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler = {
    {
QT_MOC_LITERAL(0, 0, 31), // "QtVirtualKeyboard::ShiftHandler"
QT_MOC_LITERAL(1, 32, 31), // "sentenceEndingCharactersChanged"
QT_MOC_LITERAL(2, 64, 0), // ""
QT_MOC_LITERAL(3, 65, 25), // "toggleShiftEnabledChanged"
QT_MOC_LITERAL(4, 91, 32), // "autoCapitalizationEnabledChanged"
QT_MOC_LITERAL(5, 124, 18), // "shiftActiveChanged"
QT_MOC_LITERAL(6, 143, 21), // "capsLockActiveChanged"
QT_MOC_LITERAL(7, 165, 16), // "uppercaseChanged"
QT_MOC_LITERAL(8, 182, 5), // "reset"
QT_MOC_LITERAL(9, 188, 14), // "autoCapitalize"
QT_MOC_LITERAL(10, 203, 7), // "restart"
QT_MOC_LITERAL(11, 211, 13), // "localeChanged"
QT_MOC_LITERAL(12, 225, 25), // "inputMethodVisibleChanged"
QT_MOC_LITERAL(13, 251, 11), // "toggleShift"
QT_MOC_LITERAL(14, 263, 21), // "clearToggleShiftTimer"
QT_MOC_LITERAL(15, 285, 24), // "sentenceEndingCharacters"
QT_MOC_LITERAL(16, 310, 25), // "autoCapitalizationEnabled"
QT_MOC_LITERAL(17, 336, 18), // "toggleShiftEnabled"
QT_MOC_LITERAL(18, 355, 11), // "shiftActive"
QT_MOC_LITERAL(19, 367, 14), // "capsLockActive"
QT_MOC_LITERAL(20, 382, 9) // "uppercase"

    },
    "QtVirtualKeyboard::ShiftHandler\0"
    "sentenceEndingCharactersChanged\0\0"
    "toggleShiftEnabledChanged\0"
    "autoCapitalizationEnabledChanged\0"
    "shiftActiveChanged\0capsLockActiveChanged\0"
    "uppercaseChanged\0reset\0autoCapitalize\0"
    "restart\0localeChanged\0inputMethodVisibleChanged\0"
    "toggleShift\0clearToggleShiftTimer\0"
    "sentenceEndingCharacters\0"
    "autoCapitalizationEnabled\0toggleShiftEnabled\0"
    "shiftActive\0capsLockActive\0uppercase"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtVirtualKeyboard__ShiftHandler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       6,   92, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    0,   80,    2, 0x06 /* Public */,
       4,    0,   81,    2, 0x06 /* Public */,
       5,    0,   82,    2, 0x06 /* Public */,
       6,    0,   83,    2, 0x06 /* Public */,
       7,    0,   84,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   85,    2, 0x08 /* Private */,
       9,    0,   86,    2, 0x08 /* Private */,
      10,    0,   87,    2, 0x08 /* Private */,
      11,    0,   88,    2, 0x08 /* Private */,
      12,    0,   89,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      13,    0,   90,    2, 0x02 /* Public */,
      14,    0,   91,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      15, QMetaType::QString, 0x00495103,
      16, QMetaType::Bool, 0x00495001,
      17, QMetaType::Bool, 0x00495001,
      18, QMetaType::Bool, 0x00495103,
      19, QMetaType::Bool, 0x00495103,
      20, QMetaType::Bool, 0x00495001,

 // properties: notify_signal_id
       0,
       2,
       1,
       3,
       4,
       5,

       0        // eod
};

void QtVirtualKeyboard::ShiftHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ShiftHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sentenceEndingCharactersChanged(); break;
        case 1: _t->toggleShiftEnabledChanged(); break;
        case 2: _t->autoCapitalizationEnabledChanged(); break;
        case 3: _t->shiftActiveChanged(); break;
        case 4: _t->capsLockActiveChanged(); break;
        case 5: _t->uppercaseChanged(); break;
        case 6: _t->reset(); break;
        case 7: _t->autoCapitalize(); break;
        case 8: _t->restart(); break;
        case 9: _t->localeChanged(); break;
        case 10: _t->inputMethodVisibleChanged(); break;
        case 11: _t->toggleShift(); break;
        case 12: _t->clearToggleShiftTimer(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::sentenceEndingCharactersChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::toggleShiftEnabledChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::autoCapitalizationEnabledChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::shiftActiveChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::capsLockActiveChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (ShiftHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ShiftHandler::uppercaseChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<ShiftHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->sentenceEndingCharacters(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isAutoCapitalizationEnabled(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isToggleShiftEnabled(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isShiftActive(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isCapsLockActive(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isUppercase(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<ShiftHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSentenceEndingCharacters(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setShiftActive(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setCapsLockActive(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QtVirtualKeyboard::ShiftHandler::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler.data,
    qt_meta_data_QtVirtualKeyboard__ShiftHandler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtVirtualKeyboard::ShiftHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtVirtualKeyboard::ShiftHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtVirtualKeyboard__ShiftHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QtVirtualKeyboard::ShiftHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QtVirtualKeyboard::ShiftHandler::sentenceEndingCharactersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QtVirtualKeyboard::ShiftHandler::toggleShiftEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QtVirtualKeyboard::ShiftHandler::autoCapitalizationEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QtVirtualKeyboard::ShiftHandler::shiftActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QtVirtualKeyboard::ShiftHandler::capsLockActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QtVirtualKeyboard::ShiftHandler::uppercaseChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
