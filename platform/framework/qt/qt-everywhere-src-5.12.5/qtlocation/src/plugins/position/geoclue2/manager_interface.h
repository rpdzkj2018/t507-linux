/*
 * This file was generated by qdbusxml2cpp version 0.8
 * Command line was: qdbusxml2cpp -N -i geocluetypes.h -p manager_interface.h: org.freedesktop.GeoClue2.Manager.xml
 *
 * qdbusxml2cpp is Copyright (C) 2019 The Qt Company Ltd.
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef MANAGER_INTERFACE_H
#define MANAGER_INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>
#include "geocluetypes.h"

/*
 * Proxy class for interface org.freedesktop.GeoClue2.Manager
 */
class OrgFreedesktopGeoClue2ManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "org.freedesktop.GeoClue2.Manager"; }

public:
    OrgFreedesktopGeoClue2ManagerInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    ~OrgFreedesktopGeoClue2ManagerInterface();

    Q_PROPERTY(uint AvailableAccuracyLevel READ availableAccuracyLevel)
    inline uint availableAccuracyLevel() const
    { return qvariant_cast< uint >(property("AvailableAccuracyLevel")); }

    Q_PROPERTY(bool InUse READ inUse)
    inline bool inUse() const
    { return qvariant_cast< bool >(property("InUse")); }

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> AddAgent(const QString &id)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(id);
        return asyncCallWithArgumentList(QStringLiteral("AddAgent"), argumentList);
    }

    inline QDBusPendingReply<QDBusObjectPath> GetClient()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("GetClient"), argumentList);
    }

Q_SIGNALS: // SIGNALS
};

#endif
