/****************************************************************************
** Meta object code from reading C++ file 'qquickpopup_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickpopup_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickpopup_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickPopup_t {
    QByteArrayData data[148];
    char stringdata0[2062];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPopup_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPopup_t qt_meta_stringdata_QQuickPopup = {
    {
QT_MOC_LITERAL(0, 0, 11), // "QQuickPopup"
QT_MOC_LITERAL(1, 12, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 34, 22), // "background,contentItem"
QT_MOC_LITERAL(3, 57, 15), // "DefaultProperty"
QT_MOC_LITERAL(4, 73, 11), // "contentData"
QT_MOC_LITERAL(5, 85, 6), // "opened"
QT_MOC_LITERAL(6, 92, 0), // ""
QT_MOC_LITERAL(7, 93, 6), // "closed"
QT_MOC_LITERAL(8, 100, 11), // "aboutToShow"
QT_MOC_LITERAL(9, 112, 11), // "aboutToHide"
QT_MOC_LITERAL(10, 124, 8), // "xChanged"
QT_MOC_LITERAL(11, 133, 8), // "yChanged"
QT_MOC_LITERAL(12, 142, 8), // "zChanged"
QT_MOC_LITERAL(13, 151, 12), // "widthChanged"
QT_MOC_LITERAL(14, 164, 13), // "heightChanged"
QT_MOC_LITERAL(15, 178, 20), // "implicitWidthChanged"
QT_MOC_LITERAL(16, 199, 21), // "implicitHeightChanged"
QT_MOC_LITERAL(17, 221, 19), // "contentWidthChanged"
QT_MOC_LITERAL(18, 241, 20), // "contentHeightChanged"
QT_MOC_LITERAL(19, 262, 21), // "availableWidthChanged"
QT_MOC_LITERAL(20, 284, 22), // "availableHeightChanged"
QT_MOC_LITERAL(21, 307, 14), // "marginsChanged"
QT_MOC_LITERAL(22, 322, 16), // "topMarginChanged"
QT_MOC_LITERAL(23, 339, 17), // "leftMarginChanged"
QT_MOC_LITERAL(24, 357, 18), // "rightMarginChanged"
QT_MOC_LITERAL(25, 376, 19), // "bottomMarginChanged"
QT_MOC_LITERAL(26, 396, 14), // "paddingChanged"
QT_MOC_LITERAL(27, 411, 17), // "topPaddingChanged"
QT_MOC_LITERAL(28, 429, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(29, 448, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(30, 468, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(31, 489, 11), // "fontChanged"
QT_MOC_LITERAL(32, 501, 13), // "localeChanged"
QT_MOC_LITERAL(33, 515, 13), // "parentChanged"
QT_MOC_LITERAL(34, 529, 17), // "backgroundChanged"
QT_MOC_LITERAL(35, 547, 18), // "contentItemChanged"
QT_MOC_LITERAL(36, 566, 22), // "contentChildrenChanged"
QT_MOC_LITERAL(37, 589, 11), // "clipChanged"
QT_MOC_LITERAL(38, 601, 12), // "focusChanged"
QT_MOC_LITERAL(39, 614, 18), // "activeFocusChanged"
QT_MOC_LITERAL(40, 633, 12), // "modalChanged"
QT_MOC_LITERAL(41, 646, 10), // "dimChanged"
QT_MOC_LITERAL(42, 657, 14), // "visibleChanged"
QT_MOC_LITERAL(43, 672, 14), // "opacityChanged"
QT_MOC_LITERAL(44, 687, 12), // "scaleChanged"
QT_MOC_LITERAL(45, 700, 18), // "closePolicyChanged"
QT_MOC_LITERAL(46, 719, 12), // "enterChanged"
QT_MOC_LITERAL(47, 732, 11), // "exitChanged"
QT_MOC_LITERAL(48, 744, 13), // "windowChanged"
QT_MOC_LITERAL(49, 758, 13), // "QQuickWindow*"
QT_MOC_LITERAL(50, 772, 6), // "window"
QT_MOC_LITERAL(51, 779, 14), // "spacingChanged"
QT_MOC_LITERAL(52, 794, 13), // "openedChanged"
QT_MOC_LITERAL(53, 808, 15), // "mirroredChanged"
QT_MOC_LITERAL(54, 824, 14), // "enabledChanged"
QT_MOC_LITERAL(55, 839, 14), // "paletteChanged"
QT_MOC_LITERAL(56, 854, 24), // "horizontalPaddingChanged"
QT_MOC_LITERAL(57, 879, 22), // "verticalPaddingChanged"
QT_MOC_LITERAL(58, 902, 27), // "implicitContentWidthChanged"
QT_MOC_LITERAL(59, 930, 28), // "implicitContentHeightChanged"
QT_MOC_LITERAL(60, 959, 30), // "implicitBackgroundWidthChanged"
QT_MOC_LITERAL(61, 990, 31), // "implicitBackgroundHeightChanged"
QT_MOC_LITERAL(62, 1022, 15), // "topInsetChanged"
QT_MOC_LITERAL(63, 1038, 16), // "leftInsetChanged"
QT_MOC_LITERAL(64, 1055, 17), // "rightInsetChanged"
QT_MOC_LITERAL(65, 1073, 18), // "bottomInsetChanged"
QT_MOC_LITERAL(66, 1092, 4), // "open"
QT_MOC_LITERAL(67, 1097, 5), // "close"
QT_MOC_LITERAL(68, 1103, 16), // "forceActiveFocus"
QT_MOC_LITERAL(69, 1120, 15), // "Qt::FocusReason"
QT_MOC_LITERAL(70, 1136, 6), // "reason"
QT_MOC_LITERAL(71, 1143, 1), // "x"
QT_MOC_LITERAL(72, 1145, 1), // "y"
QT_MOC_LITERAL(73, 1147, 1), // "z"
QT_MOC_LITERAL(74, 1149, 5), // "width"
QT_MOC_LITERAL(75, 1155, 6), // "height"
QT_MOC_LITERAL(76, 1162, 13), // "implicitWidth"
QT_MOC_LITERAL(77, 1176, 14), // "implicitHeight"
QT_MOC_LITERAL(78, 1191, 12), // "contentWidth"
QT_MOC_LITERAL(79, 1204, 13), // "contentHeight"
QT_MOC_LITERAL(80, 1218, 14), // "availableWidth"
QT_MOC_LITERAL(81, 1233, 15), // "availableHeight"
QT_MOC_LITERAL(82, 1249, 7), // "margins"
QT_MOC_LITERAL(83, 1257, 9), // "topMargin"
QT_MOC_LITERAL(84, 1267, 10), // "leftMargin"
QT_MOC_LITERAL(85, 1278, 11), // "rightMargin"
QT_MOC_LITERAL(86, 1290, 12), // "bottomMargin"
QT_MOC_LITERAL(87, 1303, 7), // "padding"
QT_MOC_LITERAL(88, 1311, 10), // "topPadding"
QT_MOC_LITERAL(89, 1322, 11), // "leftPadding"
QT_MOC_LITERAL(90, 1334, 12), // "rightPadding"
QT_MOC_LITERAL(91, 1347, 13), // "bottomPadding"
QT_MOC_LITERAL(92, 1361, 6), // "locale"
QT_MOC_LITERAL(93, 1368, 4), // "font"
QT_MOC_LITERAL(94, 1373, 6), // "parent"
QT_MOC_LITERAL(95, 1380, 11), // "QQuickItem*"
QT_MOC_LITERAL(96, 1392, 10), // "background"
QT_MOC_LITERAL(97, 1403, 11), // "contentItem"
QT_MOC_LITERAL(98, 1415, 25), // "QQmlListProperty<QObject>"
QT_MOC_LITERAL(99, 1441, 15), // "contentChildren"
QT_MOC_LITERAL(100, 1457, 28), // "QQmlListProperty<QQuickItem>"
QT_MOC_LITERAL(101, 1486, 4), // "clip"
QT_MOC_LITERAL(102, 1491, 5), // "focus"
QT_MOC_LITERAL(103, 1497, 11), // "activeFocus"
QT_MOC_LITERAL(104, 1509, 5), // "modal"
QT_MOC_LITERAL(105, 1515, 3), // "dim"
QT_MOC_LITERAL(106, 1519, 7), // "visible"
QT_MOC_LITERAL(107, 1527, 7), // "opacity"
QT_MOC_LITERAL(108, 1535, 5), // "scale"
QT_MOC_LITERAL(109, 1541, 11), // "closePolicy"
QT_MOC_LITERAL(110, 1553, 11), // "ClosePolicy"
QT_MOC_LITERAL(111, 1565, 15), // "transformOrigin"
QT_MOC_LITERAL(112, 1581, 15), // "TransformOrigin"
QT_MOC_LITERAL(113, 1597, 5), // "enter"
QT_MOC_LITERAL(114, 1603, 17), // "QQuickTransition*"
QT_MOC_LITERAL(115, 1621, 4), // "exit"
QT_MOC_LITERAL(116, 1626, 7), // "spacing"
QT_MOC_LITERAL(117, 1634, 8), // "mirrored"
QT_MOC_LITERAL(118, 1643, 7), // "enabled"
QT_MOC_LITERAL(119, 1651, 7), // "palette"
QT_MOC_LITERAL(120, 1659, 17), // "horizontalPadding"
QT_MOC_LITERAL(121, 1677, 15), // "verticalPadding"
QT_MOC_LITERAL(122, 1693, 7), // "anchors"
QT_MOC_LITERAL(123, 1701, 19), // "QQuickPopupAnchors*"
QT_MOC_LITERAL(124, 1721, 20), // "implicitContentWidth"
QT_MOC_LITERAL(125, 1742, 21), // "implicitContentHeight"
QT_MOC_LITERAL(126, 1764, 23), // "implicitBackgroundWidth"
QT_MOC_LITERAL(127, 1788, 24), // "implicitBackgroundHeight"
QT_MOC_LITERAL(128, 1813, 8), // "topInset"
QT_MOC_LITERAL(129, 1822, 9), // "leftInset"
QT_MOC_LITERAL(130, 1832, 10), // "rightInset"
QT_MOC_LITERAL(131, 1843, 11), // "bottomInset"
QT_MOC_LITERAL(132, 1855, 15), // "ClosePolicyFlag"
QT_MOC_LITERAL(133, 1871, 11), // "NoAutoClose"
QT_MOC_LITERAL(134, 1883, 19), // "CloseOnPressOutside"
QT_MOC_LITERAL(135, 1903, 25), // "CloseOnPressOutsideParent"
QT_MOC_LITERAL(136, 1929, 21), // "CloseOnReleaseOutside"
QT_MOC_LITERAL(137, 1951, 27), // "CloseOnReleaseOutsideParent"
QT_MOC_LITERAL(138, 1979, 13), // "CloseOnEscape"
QT_MOC_LITERAL(139, 1993, 7), // "TopLeft"
QT_MOC_LITERAL(140, 2001, 3), // "Top"
QT_MOC_LITERAL(141, 2005, 8), // "TopRight"
QT_MOC_LITERAL(142, 2014, 4), // "Left"
QT_MOC_LITERAL(143, 2019, 6), // "Center"
QT_MOC_LITERAL(144, 2026, 5), // "Right"
QT_MOC_LITERAL(145, 2032, 10), // "BottomLeft"
QT_MOC_LITERAL(146, 2043, 6), // "Bottom"
QT_MOC_LITERAL(147, 2050, 11) // "BottomRight"

    },
    "QQuickPopup\0DeferredPropertyNames\0"
    "background,contentItem\0DefaultProperty\0"
    "contentData\0opened\0\0closed\0aboutToShow\0"
    "aboutToHide\0xChanged\0yChanged\0zChanged\0"
    "widthChanged\0heightChanged\0"
    "implicitWidthChanged\0implicitHeightChanged\0"
    "contentWidthChanged\0contentHeightChanged\0"
    "availableWidthChanged\0availableHeightChanged\0"
    "marginsChanged\0topMarginChanged\0"
    "leftMarginChanged\0rightMarginChanged\0"
    "bottomMarginChanged\0paddingChanged\0"
    "topPaddingChanged\0leftPaddingChanged\0"
    "rightPaddingChanged\0bottomPaddingChanged\0"
    "fontChanged\0localeChanged\0parentChanged\0"
    "backgroundChanged\0contentItemChanged\0"
    "contentChildrenChanged\0clipChanged\0"
    "focusChanged\0activeFocusChanged\0"
    "modalChanged\0dimChanged\0visibleChanged\0"
    "opacityChanged\0scaleChanged\0"
    "closePolicyChanged\0enterChanged\0"
    "exitChanged\0windowChanged\0QQuickWindow*\0"
    "window\0spacingChanged\0openedChanged\0"
    "mirroredChanged\0enabledChanged\0"
    "paletteChanged\0horizontalPaddingChanged\0"
    "verticalPaddingChanged\0"
    "implicitContentWidthChanged\0"
    "implicitContentHeightChanged\0"
    "implicitBackgroundWidthChanged\0"
    "implicitBackgroundHeightChanged\0"
    "topInsetChanged\0leftInsetChanged\0"
    "rightInsetChanged\0bottomInsetChanged\0"
    "open\0close\0forceActiveFocus\0Qt::FocusReason\0"
    "reason\0x\0y\0z\0width\0height\0implicitWidth\0"
    "implicitHeight\0contentWidth\0contentHeight\0"
    "availableWidth\0availableHeight\0margins\0"
    "topMargin\0leftMargin\0rightMargin\0"
    "bottomMargin\0padding\0topPadding\0"
    "leftPadding\0rightPadding\0bottomPadding\0"
    "locale\0font\0parent\0QQuickItem*\0"
    "background\0contentItem\0QQmlListProperty<QObject>\0"
    "contentChildren\0QQmlListProperty<QQuickItem>\0"
    "clip\0focus\0activeFocus\0modal\0dim\0"
    "visible\0opacity\0scale\0closePolicy\0"
    "ClosePolicy\0transformOrigin\0TransformOrigin\0"
    "enter\0QQuickTransition*\0exit\0spacing\0"
    "mirrored\0enabled\0palette\0horizontalPadding\0"
    "verticalPadding\0anchors\0QQuickPopupAnchors*\0"
    "implicitContentWidth\0implicitContentHeight\0"
    "implicitBackgroundWidth\0"
    "implicitBackgroundHeight\0topInset\0"
    "leftInset\0rightInset\0bottomInset\0"
    "ClosePolicyFlag\0NoAutoClose\0"
    "CloseOnPressOutside\0CloseOnPressOutsideParent\0"
    "CloseOnReleaseOutside\0CloseOnReleaseOutsideParent\0"
    "CloseOnEscape\0TopLeft\0Top\0TopRight\0"
    "Left\0Center\0Right\0BottomLeft\0Bottom\0"
    "BottomRight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPopup[] = {

 // content:
       8,       // revision
       0,       // classname
       2,   14, // classinfo
      62,   18, // methods
      56,  456, // properties
       2,  736, // enums/sets
       0,    0, // constructors
       0,       // flags
      58,       // signalCount

 // classinfo: key, value
       1,    2,
       3,    4,

 // signals: name, argc, parameters, tag, flags
       5,    0,  390,    6, 0x06 /* Public */,
       7,    0,  391,    6, 0x06 /* Public */,
       8,    0,  392,    6, 0x06 /* Public */,
       9,    0,  393,    6, 0x06 /* Public */,
      10,    0,  394,    6, 0x06 /* Public */,
      11,    0,  395,    6, 0x06 /* Public */,
      12,    0,  396,    6, 0x06 /* Public */,
      13,    0,  397,    6, 0x06 /* Public */,
      14,    0,  398,    6, 0x06 /* Public */,
      15,    0,  399,    6, 0x06 /* Public */,
      16,    0,  400,    6, 0x06 /* Public */,
      17,    0,  401,    6, 0x06 /* Public */,
      18,    0,  402,    6, 0x06 /* Public */,
      19,    0,  403,    6, 0x06 /* Public */,
      20,    0,  404,    6, 0x06 /* Public */,
      21,    0,  405,    6, 0x06 /* Public */,
      22,    0,  406,    6, 0x06 /* Public */,
      23,    0,  407,    6, 0x06 /* Public */,
      24,    0,  408,    6, 0x06 /* Public */,
      25,    0,  409,    6, 0x06 /* Public */,
      26,    0,  410,    6, 0x06 /* Public */,
      27,    0,  411,    6, 0x06 /* Public */,
      28,    0,  412,    6, 0x06 /* Public */,
      29,    0,  413,    6, 0x06 /* Public */,
      30,    0,  414,    6, 0x06 /* Public */,
      31,    0,  415,    6, 0x06 /* Public */,
      32,    0,  416,    6, 0x06 /* Public */,
      33,    0,  417,    6, 0x06 /* Public */,
      34,    0,  418,    6, 0x06 /* Public */,
      35,    0,  419,    6, 0x06 /* Public */,
      36,    0,  420,    6, 0x06 /* Public */,
      37,    0,  421,    6, 0x06 /* Public */,
      38,    0,  422,    6, 0x06 /* Public */,
      39,    0,  423,    6, 0x06 /* Public */,
      40,    0,  424,    6, 0x06 /* Public */,
      41,    0,  425,    6, 0x06 /* Public */,
      42,    0,  426,    6, 0x06 /* Public */,
      43,    0,  427,    6, 0x06 /* Public */,
      44,    0,  428,    6, 0x06 /* Public */,
      45,    0,  429,    6, 0x06 /* Public */,
      46,    0,  430,    6, 0x06 /* Public */,
      47,    0,  431,    6, 0x06 /* Public */,
      48,    1,  432,    6, 0x06 /* Public */,
      51,    0,  435,    6, 0x86 /* Public | MethodRevisioned */,
      52,    0,  436,    6, 0x86 /* Public | MethodRevisioned */,
      53,    0,  437,    6, 0x86 /* Public | MethodRevisioned */,
      54,    0,  438,    6, 0x86 /* Public | MethodRevisioned */,
      55,    0,  439,    6, 0x86 /* Public | MethodRevisioned */,
      56,    0,  440,    6, 0x86 /* Public | MethodRevisioned */,
      57,    0,  441,    6, 0x86 /* Public | MethodRevisioned */,
      58,    0,  442,    6, 0x86 /* Public | MethodRevisioned */,
      59,    0,  443,    6, 0x86 /* Public | MethodRevisioned */,
      60,    0,  444,    6, 0x86 /* Public | MethodRevisioned */,
      61,    0,  445,    6, 0x86 /* Public | MethodRevisioned */,
      62,    0,  446,    6, 0x86 /* Public | MethodRevisioned */,
      63,    0,  447,    6, 0x86 /* Public | MethodRevisioned */,
      64,    0,  448,    6, 0x86 /* Public | MethodRevisioned */,
      65,    0,  449,    6, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      66,    0,  450,    6, 0x0a /* Public */,
      67,    0,  451,    6, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      68,    1,  452,    6, 0x02 /* Public */,
      68,    0,  455,    6, 0x22 /* Public | MethodCloned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       3,
       3,
       3,
       3,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

 // slots: revision
       0,
       0,

 // methods: revision
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 49,   50,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, 0x80000000 | 69,   70,
    QMetaType::Void,

 // properties: name, type, flags
      71, QMetaType::QReal, 0x00495903,
      72, QMetaType::QReal, 0x00495903,
      73, QMetaType::QReal, 0x00495903,
      74, QMetaType::QReal, 0x00495907,
      75, QMetaType::QReal, 0x00495907,
      76, QMetaType::QReal, 0x00495903,
      77, QMetaType::QReal, 0x00495903,
      78, QMetaType::QReal, 0x00495903,
      79, QMetaType::QReal, 0x00495903,
      80, QMetaType::QReal, 0x00495801,
      81, QMetaType::QReal, 0x00495801,
      82, QMetaType::QReal, 0x00495907,
      83, QMetaType::QReal, 0x00495907,
      84, QMetaType::QReal, 0x00495907,
      85, QMetaType::QReal, 0x00495907,
      86, QMetaType::QReal, 0x00495907,
      87, QMetaType::QReal, 0x00495907,
      88, QMetaType::QReal, 0x00495907,
      89, QMetaType::QReal, 0x00495907,
      90, QMetaType::QReal, 0x00495907,
      91, QMetaType::QReal, 0x00495907,
      92, QMetaType::QLocale, 0x00495907,
      93, QMetaType::QFont, 0x00495907,
      94, 0x80000000 | 95, 0x0049580f,
      96, 0x80000000 | 95, 0x0049590b,
      97, 0x80000000 | 95, 0x0049590b,
       4, 0x80000000 | 98, 0x00095809,
      99, 0x80000000 | 100, 0x00495809,
     101, QMetaType::Bool, 0x00495903,
     102, QMetaType::Bool, 0x00495903,
     103, QMetaType::Bool, 0x00495801,
     104, QMetaType::Bool, 0x00495903,
     105, QMetaType::Bool, 0x00495907,
     106, QMetaType::Bool, 0x00495903,
     107, QMetaType::QReal, 0x00495903,
     108, QMetaType::QReal, 0x00495903,
     109, 0x80000000 | 110, 0x0049590f,
     111, 0x80000000 | 112, 0x0009590b,
     113, 0x80000000 | 114, 0x0049590b,
     115, 0x80000000 | 114, 0x0049590b,
     116, QMetaType::QReal, 0x00c95907,
       5, QMetaType::Bool, 0x00c95801,
     117, QMetaType::Bool, 0x00c95801,
     118, QMetaType::Bool, 0x00c95903,
     119, QMetaType::QPalette, 0x00c95907,
     120, QMetaType::QReal, 0x00495907,
     121, QMetaType::QReal, 0x00495907,
     122, 0x80000000 | 123, 0x00894c09,
     124, QMetaType::QReal, 0x00c95801,
     125, QMetaType::QReal, 0x00c95801,
     126, QMetaType::QReal, 0x00c95801,
     127, QMetaType::QReal, 0x00c95801,
     128, QMetaType::QReal, 0x00c95907,
     129, QMetaType::QReal, 0x00c95907,
     130, QMetaType::QReal, 0x00c95907,
     131, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      26,
      25,
      27,
      28,
      29,
       0,
      30,
      31,
      32,
      33,
      34,
      35,
      36,
      37,
      38,
      39,
       0,
      40,
      41,
      43,
      44,
      45,
      46,
      47,
      48,
      49,
       0,
      50,
      51,
      52,
      53,
      54,
      55,
      56,
      57,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       3,
       3,
       3,
       3,
       0,
       0,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

 // enums: name, alias, flags, count, data
     110,  132, 0x1,    6,  746,
     112,  112, 0x0,    9,  758,

 // enum data: key, value
     133, uint(QQuickPopup::NoAutoClose),
     134, uint(QQuickPopup::CloseOnPressOutside),
     135, uint(QQuickPopup::CloseOnPressOutsideParent),
     136, uint(QQuickPopup::CloseOnReleaseOutside),
     137, uint(QQuickPopup::CloseOnReleaseOutsideParent),
     138, uint(QQuickPopup::CloseOnEscape),
     139, uint(QQuickPopup::TopLeft),
     140, uint(QQuickPopup::Top),
     141, uint(QQuickPopup::TopRight),
     142, uint(QQuickPopup::Left),
     143, uint(QQuickPopup::Center),
     144, uint(QQuickPopup::Right),
     145, uint(QQuickPopup::BottomLeft),
     146, uint(QQuickPopup::Bottom),
     147, uint(QQuickPopup::BottomRight),

       0        // eod
};

void QQuickPopup::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickPopup *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->opened(); break;
        case 1: _t->closed(); break;
        case 2: _t->aboutToShow(); break;
        case 3: _t->aboutToHide(); break;
        case 4: _t->xChanged(); break;
        case 5: _t->yChanged(); break;
        case 6: _t->zChanged(); break;
        case 7: _t->widthChanged(); break;
        case 8: _t->heightChanged(); break;
        case 9: _t->implicitWidthChanged(); break;
        case 10: _t->implicitHeightChanged(); break;
        case 11: _t->contentWidthChanged(); break;
        case 12: _t->contentHeightChanged(); break;
        case 13: _t->availableWidthChanged(); break;
        case 14: _t->availableHeightChanged(); break;
        case 15: _t->marginsChanged(); break;
        case 16: _t->topMarginChanged(); break;
        case 17: _t->leftMarginChanged(); break;
        case 18: _t->rightMarginChanged(); break;
        case 19: _t->bottomMarginChanged(); break;
        case 20: _t->paddingChanged(); break;
        case 21: _t->topPaddingChanged(); break;
        case 22: _t->leftPaddingChanged(); break;
        case 23: _t->rightPaddingChanged(); break;
        case 24: _t->bottomPaddingChanged(); break;
        case 25: _t->fontChanged(); break;
        case 26: _t->localeChanged(); break;
        case 27: _t->parentChanged(); break;
        case 28: _t->backgroundChanged(); break;
        case 29: _t->contentItemChanged(); break;
        case 30: _t->contentChildrenChanged(); break;
        case 31: _t->clipChanged(); break;
        case 32: _t->focusChanged(); break;
        case 33: _t->activeFocusChanged(); break;
        case 34: _t->modalChanged(); break;
        case 35: _t->dimChanged(); break;
        case 36: _t->visibleChanged(); break;
        case 37: _t->opacityChanged(); break;
        case 38: _t->scaleChanged(); break;
        case 39: _t->closePolicyChanged(); break;
        case 40: _t->enterChanged(); break;
        case 41: _t->exitChanged(); break;
        case 42: _t->windowChanged((*reinterpret_cast< QQuickWindow*(*)>(_a[1]))); break;
        case 43: _t->spacingChanged(); break;
        case 44: _t->openedChanged(); break;
        case 45: _t->mirroredChanged(); break;
        case 46: _t->enabledChanged(); break;
        case 47: _t->paletteChanged(); break;
        case 48: _t->horizontalPaddingChanged(); break;
        case 49: _t->verticalPaddingChanged(); break;
        case 50: _t->implicitContentWidthChanged(); break;
        case 51: _t->implicitContentHeightChanged(); break;
        case 52: _t->implicitBackgroundWidthChanged(); break;
        case 53: _t->implicitBackgroundHeightChanged(); break;
        case 54: _t->topInsetChanged(); break;
        case 55: _t->leftInsetChanged(); break;
        case 56: _t->rightInsetChanged(); break;
        case 57: _t->bottomInsetChanged(); break;
        case 58: _t->open(); break;
        case 59: _t->close(); break;
        case 60: _t->forceActiveFocus((*reinterpret_cast< Qt::FocusReason(*)>(_a[1]))); break;
        case 61: _t->forceActiveFocus(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::opened)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::closed)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::aboutToShow)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::aboutToHide)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::xChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::yChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::zChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::widthChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::heightChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitWidthChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitHeightChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::contentWidthChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::contentHeightChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::availableWidthChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::availableHeightChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::marginsChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::topMarginChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::leftMarginChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::rightMarginChanged)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::bottomMarginChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::paddingChanged)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::topPaddingChanged)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::leftPaddingChanged)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::rightPaddingChanged)) {
                *result = 23;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::bottomPaddingChanged)) {
                *result = 24;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::fontChanged)) {
                *result = 25;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::localeChanged)) {
                *result = 26;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::parentChanged)) {
                *result = 27;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::backgroundChanged)) {
                *result = 28;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::contentItemChanged)) {
                *result = 29;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::contentChildrenChanged)) {
                *result = 30;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::clipChanged)) {
                *result = 31;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::focusChanged)) {
                *result = 32;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::activeFocusChanged)) {
                *result = 33;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::modalChanged)) {
                *result = 34;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::dimChanged)) {
                *result = 35;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::visibleChanged)) {
                *result = 36;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::opacityChanged)) {
                *result = 37;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::scaleChanged)) {
                *result = 38;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::closePolicyChanged)) {
                *result = 39;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::enterChanged)) {
                *result = 40;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::exitChanged)) {
                *result = 41;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)(QQuickWindow * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::windowChanged)) {
                *result = 42;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::spacingChanged)) {
                *result = 43;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::openedChanged)) {
                *result = 44;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::mirroredChanged)) {
                *result = 45;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::enabledChanged)) {
                *result = 46;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::paletteChanged)) {
                *result = 47;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::horizontalPaddingChanged)) {
                *result = 48;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::verticalPaddingChanged)) {
                *result = 49;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitContentWidthChanged)) {
                *result = 50;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitContentHeightChanged)) {
                *result = 51;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitBackgroundWidthChanged)) {
                *result = 52;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::implicitBackgroundHeightChanged)) {
                *result = 53;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::topInsetChanged)) {
                *result = 54;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::leftInsetChanged)) {
                *result = 55;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::rightInsetChanged)) {
                *result = 56;
                return;
            }
        }
        {
            using _t = void (QQuickPopup::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPopup::bottomInsetChanged)) {
                *result = 57;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 26:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        case 27:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QQuickItem> >(); break;
        case 25:
        case 24:
        case 23:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickPopup *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->x(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->y(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->z(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->width(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->height(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->implicitWidth(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->implicitHeight(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->availableWidth(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->availableHeight(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->margins(); break;
        case 12: *reinterpret_cast< qreal*>(_v) = _t->topMargin(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->leftMargin(); break;
        case 14: *reinterpret_cast< qreal*>(_v) = _t->rightMargin(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->bottomMargin(); break;
        case 16: *reinterpret_cast< qreal*>(_v) = _t->padding(); break;
        case 17: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 18: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 19: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 20: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        case 21: *reinterpret_cast< QLocale*>(_v) = _t->locale(); break;
        case 22: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 23: *reinterpret_cast< QQuickItem**>(_v) = _t->parentItem(); break;
        case 24: *reinterpret_cast< QQuickItem**>(_v) = _t->background(); break;
        case 25: *reinterpret_cast< QQuickItem**>(_v) = _t->contentItem(); break;
        case 26: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->QQuickPopup::d_func()->contentData(); break;
        case 27: *reinterpret_cast< QQmlListProperty<QQuickItem>*>(_v) = _t->QQuickPopup::d_func()->contentChildren(); break;
        case 28: *reinterpret_cast< bool*>(_v) = _t->clip(); break;
        case 29: *reinterpret_cast< bool*>(_v) = _t->hasFocus(); break;
        case 30: *reinterpret_cast< bool*>(_v) = _t->hasActiveFocus(); break;
        case 31: *reinterpret_cast< bool*>(_v) = _t->isModal(); break;
        case 32: *reinterpret_cast< bool*>(_v) = _t->dim(); break;
        case 33: *reinterpret_cast< bool*>(_v) = _t->isVisible(); break;
        case 34: *reinterpret_cast< qreal*>(_v) = _t->opacity(); break;
        case 35: *reinterpret_cast< qreal*>(_v) = _t->scale(); break;
        case 36: *reinterpret_cast<int*>(_v) = QFlag(_t->closePolicy()); break;
        case 37: *reinterpret_cast< TransformOrigin*>(_v) = _t->transformOrigin(); break;
        case 38: *reinterpret_cast< QQuickTransition**>(_v) = _t->enter(); break;
        case 39: *reinterpret_cast< QQuickTransition**>(_v) = _t->exit(); break;
        case 40: *reinterpret_cast< qreal*>(_v) = _t->spacing(); break;
        case 41: *reinterpret_cast< bool*>(_v) = _t->isOpened(); break;
        case 42: *reinterpret_cast< bool*>(_v) = _t->isMirrored(); break;
        case 43: *reinterpret_cast< bool*>(_v) = _t->isEnabled(); break;
        case 44: *reinterpret_cast< QPalette*>(_v) = _t->palette(); break;
        case 45: *reinterpret_cast< qreal*>(_v) = _t->horizontalPadding(); break;
        case 46: *reinterpret_cast< qreal*>(_v) = _t->verticalPadding(); break;
        case 47: *reinterpret_cast< QQuickPopupAnchors**>(_v) = _t->QQuickPopup::d_func()->getAnchors(); break;
        case 48: *reinterpret_cast< qreal*>(_v) = _t->implicitContentWidth(); break;
        case 49: *reinterpret_cast< qreal*>(_v) = _t->implicitContentHeight(); break;
        case 50: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundWidth(); break;
        case 51: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundHeight(); break;
        case 52: *reinterpret_cast< qreal*>(_v) = _t->topInset(); break;
        case 53: *reinterpret_cast< qreal*>(_v) = _t->leftInset(); break;
        case 54: *reinterpret_cast< qreal*>(_v) = _t->rightInset(); break;
        case 55: *reinterpret_cast< qreal*>(_v) = _t->bottomInset(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickPopup *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setX(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setY(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setZ(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setImplicitWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setImplicitHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setContentWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setContentHeight(*reinterpret_cast< qreal*>(_v)); break;
        case 11: _t->setMargins(*reinterpret_cast< qreal*>(_v)); break;
        case 12: _t->setTopMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 13: _t->setLeftMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 14: _t->setRightMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 15: _t->setBottomMargin(*reinterpret_cast< qreal*>(_v)); break;
        case 16: _t->setPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 17: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 18: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 19: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 20: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 21: _t->setLocale(*reinterpret_cast< QLocale*>(_v)); break;
        case 22: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 23: _t->setParentItem(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 24: _t->setBackground(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 25: _t->setContentItem(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 28: _t->setClip(*reinterpret_cast< bool*>(_v)); break;
        case 29: _t->setFocus(*reinterpret_cast< bool*>(_v)); break;
        case 31: _t->setModal(*reinterpret_cast< bool*>(_v)); break;
        case 32: _t->setDim(*reinterpret_cast< bool*>(_v)); break;
        case 33: _t->setVisible(*reinterpret_cast< bool*>(_v)); break;
        case 34: _t->setOpacity(*reinterpret_cast< qreal*>(_v)); break;
        case 35: _t->setScale(*reinterpret_cast< qreal*>(_v)); break;
        case 36: _t->setClosePolicy(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 37: _t->setTransformOrigin(*reinterpret_cast< TransformOrigin*>(_v)); break;
        case 38: _t->setEnter(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 39: _t->setExit(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 40: _t->setSpacing(*reinterpret_cast< qreal*>(_v)); break;
        case 43: _t->setEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 44: _t->setPalette(*reinterpret_cast< QPalette*>(_v)); break;
        case 45: _t->setHorizontalPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 46: _t->setVerticalPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 52: _t->setTopInset(*reinterpret_cast< qreal*>(_v)); break;
        case 53: _t->setLeftInset(*reinterpret_cast< qreal*>(_v)); break;
        case 54: _t->setRightInset(*reinterpret_cast< qreal*>(_v)); break;
        case 55: _t->setBottomInset(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickPopup *_t = static_cast<QQuickPopup *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 3: _t->resetWidth(); break;
        case 4: _t->resetHeight(); break;
        case 11: _t->resetMargins(); break;
        case 12: _t->resetTopMargin(); break;
        case 13: _t->resetLeftMargin(); break;
        case 14: _t->resetRightMargin(); break;
        case 15: _t->resetBottomMargin(); break;
        case 16: _t->resetPadding(); break;
        case 17: _t->resetTopPadding(); break;
        case 18: _t->resetLeftPadding(); break;
        case 19: _t->resetRightPadding(); break;
        case 20: _t->resetBottomPadding(); break;
        case 21: _t->resetLocale(); break;
        case 22: _t->resetFont(); break;
        case 23: _t->resetParentItem(); break;
        case 32: _t->resetDim(); break;
        case 36: _t->resetClosePolicy(); break;
        case 40: _t->resetSpacing(); break;
        case 44: _t->resetPalette(); break;
        case 45: _t->resetHorizontalPadding(); break;
        case 46: _t->resetVerticalPadding(); break;
        case 52: _t->resetTopInset(); break;
        case 53: _t->resetLeftInset(); break;
        case 54: _t->resetRightInset(); break;
        case 55: _t->resetBottomInset(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickPopup::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickPopup.data,
    qt_meta_data_QQuickPopup,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickPopup::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickPopup::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickPopup.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickPopup::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 62)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 62;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 62)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 62;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 56;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 56;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 56;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 56;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 56;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 56;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickPopup::opened()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickPopup::closed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickPopup::aboutToShow()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickPopup::aboutToHide()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickPopup::xChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickPopup::yChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickPopup::zChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickPopup::widthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickPopup::heightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickPopup::implicitWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickPopup::implicitHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickPopup::contentWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickPopup::contentHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickPopup::availableWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickPopup::availableHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QQuickPopup::marginsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QQuickPopup::topMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void QQuickPopup::leftMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void QQuickPopup::rightMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void QQuickPopup::bottomMarginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void QQuickPopup::paddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void QQuickPopup::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, nullptr);
}

// SIGNAL 22
void QQuickPopup::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void QQuickPopup::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, nullptr);
}

// SIGNAL 24
void QQuickPopup::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, nullptr);
}

// SIGNAL 25
void QQuickPopup::fontChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, nullptr);
}

// SIGNAL 26
void QQuickPopup::localeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, nullptr);
}

// SIGNAL 27
void QQuickPopup::parentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, nullptr);
}

// SIGNAL 28
void QQuickPopup::backgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, nullptr);
}

// SIGNAL 29
void QQuickPopup::contentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, nullptr);
}

// SIGNAL 30
void QQuickPopup::contentChildrenChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, nullptr);
}

// SIGNAL 31
void QQuickPopup::clipChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 31, nullptr);
}

// SIGNAL 32
void QQuickPopup::focusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 32, nullptr);
}

// SIGNAL 33
void QQuickPopup::activeFocusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 33, nullptr);
}

// SIGNAL 34
void QQuickPopup::modalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 34, nullptr);
}

// SIGNAL 35
void QQuickPopup::dimChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 35, nullptr);
}

// SIGNAL 36
void QQuickPopup::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 36, nullptr);
}

// SIGNAL 37
void QQuickPopup::opacityChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 37, nullptr);
}

// SIGNAL 38
void QQuickPopup::scaleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 38, nullptr);
}

// SIGNAL 39
void QQuickPopup::closePolicyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 39, nullptr);
}

// SIGNAL 40
void QQuickPopup::enterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 40, nullptr);
}

// SIGNAL 41
void QQuickPopup::exitChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 41, nullptr);
}

// SIGNAL 42
void QQuickPopup::windowChanged(QQuickWindow * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 42, _a);
}

// SIGNAL 43
void QQuickPopup::spacingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 43, nullptr);
}

// SIGNAL 44
void QQuickPopup::openedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 44, nullptr);
}

// SIGNAL 45
void QQuickPopup::mirroredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 45, nullptr);
}

// SIGNAL 46
void QQuickPopup::enabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 46, nullptr);
}

// SIGNAL 47
void QQuickPopup::paletteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 47, nullptr);
}

// SIGNAL 48
void QQuickPopup::horizontalPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 48, nullptr);
}

// SIGNAL 49
void QQuickPopup::verticalPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 49, nullptr);
}

// SIGNAL 50
void QQuickPopup::implicitContentWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 50, nullptr);
}

// SIGNAL 51
void QQuickPopup::implicitContentHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 51, nullptr);
}

// SIGNAL 52
void QQuickPopup::implicitBackgroundWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 52, nullptr);
}

// SIGNAL 53
void QQuickPopup::implicitBackgroundHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 53, nullptr);
}

// SIGNAL 54
void QQuickPopup::topInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 54, nullptr);
}

// SIGNAL 55
void QQuickPopup::leftInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 55, nullptr);
}

// SIGNAL 56
void QQuickPopup::rightInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 56, nullptr);
}

// SIGNAL 57
void QQuickPopup::bottomInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 57, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
