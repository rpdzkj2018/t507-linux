/****************************************************************************
** Meta object code from reading C++ file 'qquicktreemodeladaptor_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../Private/qquicktreemodeladaptor_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktreemodeladaptor_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickTreeModelAdaptor1_t {
    QByteArrayData data[48];
    char stringdata0[675];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTreeModelAdaptor1_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTreeModelAdaptor1_t qt_meta_stringdata_QQuickTreeModelAdaptor1 = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QQuickTreeModelAdaptor1"
QT_MOC_LITERAL(1, 24, 12), // "modelChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 19), // "QAbstractItemModel*"
QT_MOC_LITERAL(4, 58, 5), // "model"
QT_MOC_LITERAL(5, 64, 16), // "rootIndexChanged"
QT_MOC_LITERAL(6, 81, 8), // "expanded"
QT_MOC_LITERAL(7, 90, 11), // "QModelIndex"
QT_MOC_LITERAL(8, 102, 5), // "index"
QT_MOC_LITERAL(9, 108, 9), // "collapsed"
QT_MOC_LITERAL(10, 118, 6), // "expand"
QT_MOC_LITERAL(11, 125, 8), // "collapse"
QT_MOC_LITERAL(12, 134, 8), // "setModel"
QT_MOC_LITERAL(13, 143, 21), // "modelHasBeenDestroyed"
QT_MOC_LITERAL(14, 165, 17), // "modelHasBeenReset"
QT_MOC_LITERAL(15, 183, 16), // "modelDataChanged"
QT_MOC_LITERAL(16, 200, 7), // "topLeft"
QT_MOC_LITERAL(17, 208, 11), // "bottomRigth"
QT_MOC_LITERAL(18, 220, 12), // "QVector<int>"
QT_MOC_LITERAL(19, 233, 5), // "roles"
QT_MOC_LITERAL(20, 239, 27), // "modelLayoutAboutToBeChanged"
QT_MOC_LITERAL(21, 267, 28), // "QList<QPersistentModelIndex>"
QT_MOC_LITERAL(22, 296, 7), // "parents"
QT_MOC_LITERAL(23, 304, 36), // "QAbstractItemModel::LayoutCha..."
QT_MOC_LITERAL(24, 341, 4), // "hint"
QT_MOC_LITERAL(25, 346, 18), // "modelLayoutChanged"
QT_MOC_LITERAL(26, 365, 26), // "modelRowsAboutToBeInserted"
QT_MOC_LITERAL(27, 392, 6), // "parent"
QT_MOC_LITERAL(28, 399, 5), // "start"
QT_MOC_LITERAL(29, 405, 3), // "end"
QT_MOC_LITERAL(30, 409, 23), // "modelRowsAboutToBeMoved"
QT_MOC_LITERAL(31, 433, 12), // "sourceParent"
QT_MOC_LITERAL(32, 446, 11), // "sourceStart"
QT_MOC_LITERAL(33, 458, 9), // "sourceEnd"
QT_MOC_LITERAL(34, 468, 17), // "destinationParent"
QT_MOC_LITERAL(35, 486, 14), // "destinationRow"
QT_MOC_LITERAL(36, 501, 25), // "modelRowsAboutToBeRemoved"
QT_MOC_LITERAL(37, 527, 17), // "modelRowsInserted"
QT_MOC_LITERAL(38, 545, 14), // "modelRowsMoved"
QT_MOC_LITERAL(39, 560, 16), // "modelRowsRemoved"
QT_MOC_LITERAL(40, 577, 18), // "mapRowToModelIndex"
QT_MOC_LITERAL(41, 596, 3), // "row"
QT_MOC_LITERAL(42, 600, 20), // "selectionForRowRange"
QT_MOC_LITERAL(43, 621, 14), // "QItemSelection"
QT_MOC_LITERAL(44, 636, 9), // "fromIndex"
QT_MOC_LITERAL(45, 646, 7), // "toIndex"
QT_MOC_LITERAL(46, 654, 10), // "isExpanded"
QT_MOC_LITERAL(47, 665, 9) // "rootIndex"

    },
    "QQuickTreeModelAdaptor1\0modelChanged\0"
    "\0QAbstractItemModel*\0model\0rootIndexChanged\0"
    "expanded\0QModelIndex\0index\0collapsed\0"
    "expand\0collapse\0setModel\0modelHasBeenDestroyed\0"
    "modelHasBeenReset\0modelDataChanged\0"
    "topLeft\0bottomRigth\0QVector<int>\0roles\0"
    "modelLayoutAboutToBeChanged\0"
    "QList<QPersistentModelIndex>\0parents\0"
    "QAbstractItemModel::LayoutChangeHint\0"
    "hint\0modelLayoutChanged\0"
    "modelRowsAboutToBeInserted\0parent\0"
    "start\0end\0modelRowsAboutToBeMoved\0"
    "sourceParent\0sourceStart\0sourceEnd\0"
    "destinationParent\0destinationRow\0"
    "modelRowsAboutToBeRemoved\0modelRowsInserted\0"
    "modelRowsMoved\0modelRowsRemoved\0"
    "mapRowToModelIndex\0row\0selectionForRowRange\0"
    "QItemSelection\0fromIndex\0toIndex\0"
    "isExpanded\0rootIndex"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTreeModelAdaptor1[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       2,  218, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  119,    2, 0x06 /* Public */,
       5,    0,  122,    2, 0x06 /* Public */,
       6,    1,  123,    2, 0x06 /* Public */,
       9,    1,  126,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      10,    1,  129,    2, 0x0a /* Public */,
      11,    1,  132,    2, 0x0a /* Public */,
      12,    1,  135,    2, 0x0a /* Public */,
      13,    0,  138,    2, 0x08 /* Private */,
      14,    0,  139,    2, 0x08 /* Private */,
      15,    3,  140,    2, 0x08 /* Private */,
      20,    2,  147,    2, 0x08 /* Private */,
      25,    2,  152,    2, 0x08 /* Private */,
      26,    3,  157,    2, 0x08 /* Private */,
      30,    5,  164,    2, 0x08 /* Private */,
      36,    3,  175,    2, 0x08 /* Private */,
      37,    3,  182,    2, 0x08 /* Private */,
      38,    5,  189,    2, 0x08 /* Private */,
      39,    3,  200,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      40,    1,  207,    2, 0x02 /* Public */,
      42,    2,  210,    2, 0x02 /* Public */,
      46,    1,  215,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, 0x80000000 | 7,    8,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void, 0x80000000 | 7,    2,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 7, 0x80000000 | 7, 0x80000000 | 18,   16,   17,   19,
    QMetaType::Void, 0x80000000 | 21, 0x80000000 | 23,   22,   24,
    QMetaType::Void, 0x80000000 | 21, 0x80000000 | 23,   22,   24,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int,   27,   28,   29,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int, 0x80000000 | 7, QMetaType::Int,   31,   32,   33,   34,   35,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int,   27,   28,   29,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int,   27,   28,   29,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int, 0x80000000 | 7, QMetaType::Int,   31,   32,   33,   34,   35,
    QMetaType::Void, 0x80000000 | 7, QMetaType::Int, QMetaType::Int,   27,   28,   29,

 // methods: parameters
    0x80000000 | 7, QMetaType::Int,   41,
    0x80000000 | 43, 0x80000000 | 7, 0x80000000 | 7,   44,   45,
    QMetaType::Bool, 0x80000000 | 7,    2,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
      47, 0x80000000 | 7, 0x0049510f,

 // properties: notify_signal_id
       0,
       1,

       0        // eod
};

void QQuickTreeModelAdaptor1::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTreeModelAdaptor1 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelChanged((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 1: _t->rootIndexChanged(); break;
        case 2: _t->expanded((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 3: _t->collapsed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 4: _t->expand((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 5: _t->collapse((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 6: _t->setModel((*reinterpret_cast< QAbstractItemModel*(*)>(_a[1]))); break;
        case 7: _t->modelHasBeenDestroyed(); break;
        case 8: _t->modelHasBeenReset(); break;
        case 9: _t->modelDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2])),(*reinterpret_cast< const QVector<int>(*)>(_a[3]))); break;
        case 10: _t->modelLayoutAboutToBeChanged((*reinterpret_cast< const QList<QPersistentModelIndex>(*)>(_a[1])),(*reinterpret_cast< QAbstractItemModel::LayoutChangeHint(*)>(_a[2]))); break;
        case 11: _t->modelLayoutChanged((*reinterpret_cast< const QList<QPersistentModelIndex>(*)>(_a[1])),(*reinterpret_cast< QAbstractItemModel::LayoutChangeHint(*)>(_a[2]))); break;
        case 12: _t->modelRowsAboutToBeInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 13: _t->modelRowsAboutToBeMoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QModelIndex(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 14: _t->modelRowsAboutToBeRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 15: _t->modelRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 16: _t->modelRowsMoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QModelIndex(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 17: _t->modelRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 18: { QModelIndex _r = _t->mapRowToModelIndex((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QModelIndex*>(_a[0]) = std::move(_r); }  break;
        case 19: { QItemSelection _r = _t->selectionForRowRange((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QItemSelection*>(_a[0]) = std::move(_r); }  break;
        case 20: { bool _r = _t->isExpanded((*reinterpret_cast< const QModelIndex(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTreeModelAdaptor1::*)(QAbstractItemModel * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTreeModelAdaptor1::modelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickTreeModelAdaptor1::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTreeModelAdaptor1::rootIndexChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickTreeModelAdaptor1::*)(const QModelIndex & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTreeModelAdaptor1::expanded)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickTreeModelAdaptor1::*)(const QModelIndex & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTreeModelAdaptor1::collapsed)) {
                *result = 3;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QAbstractItemModel* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTreeModelAdaptor1 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QAbstractItemModel**>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< QModelIndex*>(_v) = _t->rootIndex(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickTreeModelAdaptor1 *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QAbstractItemModel**>(_v)); break;
        case 1: _t->setRootIndex(*reinterpret_cast< QModelIndex*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTreeModelAdaptor1 *_t = static_cast<QQuickTreeModelAdaptor1 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 1: _t->resetRootIndex(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickTreeModelAdaptor1::staticMetaObject = { {
    &QAbstractListModel::staticMetaObject,
    qt_meta_stringdata_QQuickTreeModelAdaptor1.data,
    qt_meta_data_QQuickTreeModelAdaptor1,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTreeModelAdaptor1::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTreeModelAdaptor1::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTreeModelAdaptor1.stringdata0))
        return static_cast<void*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int QQuickTreeModelAdaptor1::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTreeModelAdaptor1::modelChanged(QAbstractItemModel * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickTreeModelAdaptor1::rootIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickTreeModelAdaptor1::expanded(const QModelIndex & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickTreeModelAdaptor1::collapsed(const QModelIndex & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
