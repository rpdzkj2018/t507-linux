/****************************************************************************
** Meta object code from reading C++ file 'qqmldelegatemodel_p_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../types/qqmldelegatemodel_p_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qqmldelegatemodel_p_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQmlDelegateModelItem_t {
    QByteArrayData data[9];
    char stringdata0[89];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlDelegateModelItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlDelegateModelItem_t qt_meta_stringdata_QQmlDelegateModelItem = {
    {
QT_MOC_LITERAL(0, 0, 21), // "QQmlDelegateModelItem"
QT_MOC_LITERAL(1, 22, 17), // "modelIndexChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 10), // "rowChanged"
QT_MOC_LITERAL(4, 52, 13), // "columnChanged"
QT_MOC_LITERAL(5, 66, 5), // "index"
QT_MOC_LITERAL(6, 72, 3), // "row"
QT_MOC_LITERAL(7, 76, 6), // "column"
QT_MOC_LITERAL(8, 83, 5) // "model"

    },
    "QQmlDelegateModelItem\0modelIndexChanged\0"
    "\0rowChanged\0columnChanged\0index\0row\0"
    "column\0model"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlDelegateModelItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       4,   35, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   32,    2, 0x06 /* Public */,
       3,    0,   33,    2, 0x86 /* Public | MethodRevisioned */,
       4,    0,   34,    2, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
      12,
      12,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Int, 0x00495001,
       6, QMetaType::Int, 0x00c95001,
       7, QMetaType::Int, 0x00c95001,
       8, QMetaType::QObjectStar, 0x00095401,

 // properties: notify_signal_id
       0,
       1,
       2,
       0,

 // properties: revision
       0,
      12,
      12,
       0,

       0        // eod
};

void QQmlDelegateModelItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlDelegateModelItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelIndexChanged(); break;
        case 1: _t->rowChanged(); break;
        case 2: _t->columnChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlDelegateModelItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateModelItem::modelIndexChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateModelItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateModelItem::rowChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQmlDelegateModelItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlDelegateModelItem::columnChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQmlDelegateModelItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->modelIndex(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->modelRow(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->modelColumn(); break;
        case 3: *reinterpret_cast< QObject**>(_v) = _t->modelObject(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQmlDelegateModelItem::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQmlDelegateModelItem.data,
    qt_meta_data_QQmlDelegateModelItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlDelegateModelItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlDelegateModelItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlDelegateModelItem.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQmlDelegateModelItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQmlDelegateModelItem::modelIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQmlDelegateModelItem::rowChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQmlDelegateModelItem::columnChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
struct qt_meta_stringdata_QQmlPartsModel_t {
    QByteArrayData data[4];
    char stringdata0[49];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlPartsModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlPartsModel_t qt_meta_stringdata_QQmlPartsModel = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQmlPartsModel"
QT_MOC_LITERAL(1, 15, 18), // "filterGroupChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13) // "filterOnGroup"

    },
    "QQmlPartsModel\0filterGroupChanged\0\0"
    "filterOnGroup"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlPartsModel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       1,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495007,

 // properties: notify_signal_id
       0,

       0        // eod
};

void QQmlPartsModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQmlPartsModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->filterGroupChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQmlPartsModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQmlPartsModel::filterGroupChanged)) {
                *result = 0;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQmlPartsModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->filterGroup(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQmlPartsModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFilterGroup(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQmlPartsModel *_t = static_cast<QQmlPartsModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetFilterGroup(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQmlPartsModel::staticMetaObject = { {
    &QQmlInstanceModel::staticMetaObject,
    qt_meta_stringdata_QQmlPartsModel.data,
    qt_meta_data_QQmlPartsModel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlPartsModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlPartsModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlPartsModel.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlDelegateModelGroupEmitter"))
        return static_cast< QQmlDelegateModelGroupEmitter*>(this);
    return QQmlInstanceModel::qt_metacast(_clname);
}

int QQmlPartsModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQmlInstanceModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQmlPartsModel::filterGroupChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_QQmlDelegateModelParts_t {
    QByteArrayData data[1];
    char stringdata0[23];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQmlDelegateModelParts_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQmlDelegateModelParts_t qt_meta_stringdata_QQmlDelegateModelParts = {
    {
QT_MOC_LITERAL(0, 0, 22) // "QQmlDelegateModelParts"

    },
    "QQmlDelegateModelParts"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQmlDelegateModelParts[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QQmlDelegateModelParts::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQmlDelegateModelParts::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQmlDelegateModelParts.data,
    qt_meta_data_QQmlDelegateModelParts,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQmlDelegateModelParts::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQmlDelegateModelParts::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQmlDelegateModelParts.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQmlDelegateModelParts::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
