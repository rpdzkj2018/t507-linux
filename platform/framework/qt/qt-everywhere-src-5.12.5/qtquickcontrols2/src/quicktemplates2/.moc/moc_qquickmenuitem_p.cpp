/****************************************************************************
** Meta object code from reading C++ file 'qquickmenuitem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickmenuitem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmenuitem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickMenuItem_t {
    QByteArrayData data[15];
    char stringdata0[201];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuItem_t qt_meta_stringdata_QQuickMenuItem = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickMenuItem"
QT_MOC_LITERAL(1, 15, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 37, 38), // "arrow,background,contentItem,..."
QT_MOC_LITERAL(3, 76, 9), // "triggered"
QT_MOC_LITERAL(4, 86, 0), // ""
QT_MOC_LITERAL(5, 87, 18), // "highlightedChanged"
QT_MOC_LITERAL(6, 106, 12), // "arrowChanged"
QT_MOC_LITERAL(7, 119, 11), // "menuChanged"
QT_MOC_LITERAL(8, 131, 14), // "subMenuChanged"
QT_MOC_LITERAL(9, 146, 11), // "highlighted"
QT_MOC_LITERAL(10, 158, 5), // "arrow"
QT_MOC_LITERAL(11, 164, 11), // "QQuickItem*"
QT_MOC_LITERAL(12, 176, 4), // "menu"
QT_MOC_LITERAL(13, 181, 11), // "QQuickMenu*"
QT_MOC_LITERAL(14, 193, 7) // "subMenu"

    },
    "QQuickMenuItem\0DeferredPropertyNames\0"
    "arrow,background,contentItem,indicator\0"
    "triggered\0\0highlightedChanged\0"
    "arrowChanged\0menuChanged\0subMenuChanged\0"
    "highlighted\0arrow\0QQuickItem*\0menu\0"
    "QQuickMenu*\0subMenu"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuItem[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       5,   16, // methods
       4,   51, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   46,    4, 0x06 /* Public */,
       5,    0,   47,    4, 0x06 /* Public */,
       6,    0,   48,    4, 0x86 /* Public | MethodRevisioned */,
       7,    0,   49,    4, 0x86 /* Public | MethodRevisioned */,
       8,    0,   50,    4, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       3,
       3,
       3,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       9, QMetaType::Bool, 0x00495903,
      10, 0x80000000 | 11, 0x00c9590b,
      12, 0x80000000 | 13, 0x00c95809,
      14, 0x80000000 | 13, 0x00c95809,

 // properties: notify_signal_id
       1,
       2,
       3,
       4,

 // properties: revision
       0,
       3,
       3,
       3,

       0        // eod
};

void QQuickMenuItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickMenuItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->triggered(); break;
        case 1: _t->highlightedChanged(); break;
        case 2: _t->arrowChanged(); break;
        case 3: _t->menuChanged(); break;
        case 4: _t->subMenuChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickMenuItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuItem::triggered)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickMenuItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuItem::highlightedChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickMenuItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuItem::arrowChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickMenuItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuItem::menuChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickMenuItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuItem::subMenuChanged)) {
                *result = 4;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickMenuItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isHighlighted(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->arrow(); break;
        case 2: *reinterpret_cast< QQuickMenu**>(_v) = _t->menu(); break;
        case 3: *reinterpret_cast< QQuickMenu**>(_v) = _t->subMenu(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickMenuItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setHighlighted(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setArrow(*reinterpret_cast< QQuickItem**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickMenuItem::staticMetaObject = { {
    &QQuickAbstractButton::staticMetaObject,
    qt_meta_stringdata_QQuickMenuItem.data,
    qt_meta_data_QQuickMenuItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickMenuItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuItem.stringdata0))
        return static_cast<void*>(this);
    return QQuickAbstractButton::qt_metacast(_clname);
}

int QQuickMenuItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuItem::triggered()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickMenuItem::highlightedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickMenuItem::arrowChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickMenuItem::menuChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickMenuItem::subMenuChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
