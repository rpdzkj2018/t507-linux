/****************************************************************************
** Meta object code from reading C++ file 'qconnectionfactories_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qconnectionfactories_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qconnectionfactories_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_IoDeviceBase_t {
    QByteArrayData data[4];
    char stringdata0[37];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_IoDeviceBase_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_IoDeviceBase_t qt_meta_stringdata_IoDeviceBase = {
    {
QT_MOC_LITERAL(0, 0, 12), // "IoDeviceBase"
QT_MOC_LITERAL(1, 13, 9), // "readyRead"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 12) // "disconnected"

    },
    "IoDeviceBase\0readyRead\0\0disconnected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_IoDeviceBase[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void IoDeviceBase::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<IoDeviceBase *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->readyRead(); break;
        case 1: _t->disconnected(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (IoDeviceBase::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&IoDeviceBase::readyRead)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (IoDeviceBase::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&IoDeviceBase::disconnected)) {
                *result = 1;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject IoDeviceBase::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_IoDeviceBase.data,
    qt_meta_data_IoDeviceBase,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *IoDeviceBase::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *IoDeviceBase::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_IoDeviceBase.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int IoDeviceBase::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void IoDeviceBase::readyRead()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void IoDeviceBase::disconnected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
struct qt_meta_stringdata_ServerIoDevice_t {
    QByteArrayData data[1];
    char stringdata0[15];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ServerIoDevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ServerIoDevice_t qt_meta_stringdata_ServerIoDevice = {
    {
QT_MOC_LITERAL(0, 0, 14) // "ServerIoDevice"

    },
    "ServerIoDevice"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ServerIoDevice[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void ServerIoDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ServerIoDevice::staticMetaObject = { {
    &IoDeviceBase::staticMetaObject,
    qt_meta_stringdata_ServerIoDevice.data,
    qt_meta_data_ServerIoDevice,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ServerIoDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ServerIoDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ServerIoDevice.stringdata0))
        return static_cast<void*>(this);
    return IoDeviceBase::qt_metacast(_clname);
}

int ServerIoDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IoDeviceBase::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_QConnectionAbstractServer_t {
    QByteArrayData data[3];
    char stringdata0[41];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QConnectionAbstractServer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QConnectionAbstractServer_t qt_meta_stringdata_QConnectionAbstractServer = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QConnectionAbstractServer"
QT_MOC_LITERAL(1, 26, 13), // "newConnection"
QT_MOC_LITERAL(2, 40, 0) // ""

    },
    "QConnectionAbstractServer\0newConnection\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QConnectionAbstractServer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

       0        // eod
};

void QConnectionAbstractServer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QConnectionAbstractServer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->newConnection(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QConnectionAbstractServer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QConnectionAbstractServer::newConnection)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QConnectionAbstractServer::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QConnectionAbstractServer.data,
    qt_meta_data_QConnectionAbstractServer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QConnectionAbstractServer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QConnectionAbstractServer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QConnectionAbstractServer.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QConnectionAbstractServer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QConnectionAbstractServer::newConnection()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_ClientIoDevice_t {
    QByteArrayData data[4];
    char stringdata0[48];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ClientIoDevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ClientIoDevice_t qt_meta_stringdata_ClientIoDevice = {
    {
QT_MOC_LITERAL(0, 0, 14), // "ClientIoDevice"
QT_MOC_LITERAL(1, 15, 15), // "shouldReconnect"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 15) // "ClientIoDevice*"

    },
    "ClientIoDevice\0shouldReconnect\0\0"
    "ClientIoDevice*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ClientIoDevice[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

       0        // eod
};

void ClientIoDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ClientIoDevice *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->shouldReconnect((*reinterpret_cast< ClientIoDevice*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< ClientIoDevice* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (ClientIoDevice::*)(ClientIoDevice * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClientIoDevice::shouldReconnect)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ClientIoDevice::staticMetaObject = { {
    &IoDeviceBase::staticMetaObject,
    qt_meta_stringdata_ClientIoDevice.data,
    qt_meta_data_ClientIoDevice,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ClientIoDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClientIoDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ClientIoDevice.stringdata0))
        return static_cast<void*>(this);
    return IoDeviceBase::qt_metacast(_clname);
}

int ClientIoDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IoDeviceBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void ClientIoDevice::shouldReconnect(ClientIoDevice * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
struct qt_meta_stringdata_ExternalIoDevice_t {
    QByteArrayData data[1];
    char stringdata0[17];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ExternalIoDevice_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ExternalIoDevice_t qt_meta_stringdata_ExternalIoDevice = {
    {
QT_MOC_LITERAL(0, 0, 16) // "ExternalIoDevice"

    },
    "ExternalIoDevice"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ExternalIoDevice[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void ExternalIoDevice::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject ExternalIoDevice::staticMetaObject = { {
    &IoDeviceBase::staticMetaObject,
    qt_meta_stringdata_ExternalIoDevice.data,
    qt_meta_data_ExternalIoDevice,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ExternalIoDevice::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ExternalIoDevice::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ExternalIoDevice.stringdata0))
        return static_cast<void*>(this);
    return IoDeviceBase::qt_metacast(_clname);
}

int ExternalIoDevice::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = IoDeviceBase::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
