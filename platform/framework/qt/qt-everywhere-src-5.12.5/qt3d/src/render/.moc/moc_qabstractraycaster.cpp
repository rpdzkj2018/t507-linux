/****************************************************************************
** Meta object code from reading C++ file 'qabstractraycaster.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../picking/qabstractraycaster.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstractraycaster.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QAbstractRayCaster_t {
    QByteArrayData data[22];
    char stringdata0[390];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QAbstractRayCaster_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QAbstractRayCaster_t qt_meta_stringdata_Qt3DRender__QAbstractRayCaster = {
    {
QT_MOC_LITERAL(0, 0, 30), // "Qt3DRender::QAbstractRayCaster"
QT_MOC_LITERAL(1, 31, 14), // "runModeChanged"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 39), // "Qt3DRender::QAbstractRayCaste..."
QT_MOC_LITERAL(4, 87, 7), // "runMode"
QT_MOC_LITERAL(5, 95, 11), // "hitsChanged"
QT_MOC_LITERAL(6, 107, 36), // "Qt3DRender::QAbstractRayCaste..."
QT_MOC_LITERAL(7, 144, 4), // "hits"
QT_MOC_LITERAL(8, 149, 17), // "filterModeChanged"
QT_MOC_LITERAL(9, 167, 42), // "Qt3DRender::QAbstractRayCaste..."
QT_MOC_LITERAL(10, 210, 10), // "filterMode"
QT_MOC_LITERAL(11, 221, 10), // "setRunMode"
QT_MOC_LITERAL(12, 232, 7), // "RunMode"
QT_MOC_LITERAL(13, 240, 13), // "setFilterMode"
QT_MOC_LITERAL(14, 254, 10), // "FilterMode"
QT_MOC_LITERAL(15, 265, 4), // "Hits"
QT_MOC_LITERAL(16, 270, 10), // "Continuous"
QT_MOC_LITERAL(17, 281, 10), // "SingleShot"
QT_MOC_LITERAL(18, 292, 23), // "AcceptAnyMatchingLayers"
QT_MOC_LITERAL(19, 316, 23), // "AcceptAllMatchingLayers"
QT_MOC_LITERAL(20, 340, 24), // "DiscardAnyMatchingLayers"
QT_MOC_LITERAL(21, 365, 24) // "DiscardAllMatchingLayers"

    },
    "Qt3DRender::QAbstractRayCaster\0"
    "runModeChanged\0\0Qt3DRender::QAbstractRayCaster::RunMode\0"
    "runMode\0hitsChanged\0"
    "Qt3DRender::QAbstractRayCaster::Hits\0"
    "hits\0filterModeChanged\0"
    "Qt3DRender::QAbstractRayCaster::FilterMode\0"
    "filterMode\0setRunMode\0RunMode\0"
    "setFilterMode\0FilterMode\0Hits\0Continuous\0"
    "SingleShot\0AcceptAnyMatchingLayers\0"
    "AcceptAllMatchingLayers\0"
    "DiscardAnyMatchingLayers\0"
    "DiscardAllMatchingLayers"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QAbstractRayCaster[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       3,   54, // properties
       2,   66, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       5,    1,   42,    2, 0x06 /* Public */,
       8,    1,   45,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    1,   48,    2, 0x0a /* Public */,
      13,    1,   51,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 6,    7,
    QMetaType::Void, 0x80000000 | 9,   10,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 12,    4,
    QMetaType::Void, 0x80000000 | 14,   10,

 // properties: name, type, flags
       4, 0x80000000 | 12, 0x0049510b,
      10, 0x80000000 | 14, 0x0049510b,
       7, 0x80000000 | 15, 0x00495009,

 // properties: notify_signal_id
       0,
       2,
       1,

 // enums: name, alias, flags, count, data
      12,   12, 0x0,    2,   76,
      14,   14, 0x0,    4,   80,

 // enum data: key, value
      16, uint(Qt3DRender::QAbstractRayCaster::Continuous),
      17, uint(Qt3DRender::QAbstractRayCaster::SingleShot),
      18, uint(Qt3DRender::QAbstractRayCaster::AcceptAnyMatchingLayers),
      19, uint(Qt3DRender::QAbstractRayCaster::AcceptAllMatchingLayers),
      20, uint(Qt3DRender::QAbstractRayCaster::DiscardAnyMatchingLayers),
      21, uint(Qt3DRender::QAbstractRayCaster::DiscardAllMatchingLayers),

       0        // eod
};

void Qt3DRender::QAbstractRayCaster::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAbstractRayCaster *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->runModeChanged((*reinterpret_cast< Qt3DRender::QAbstractRayCaster::RunMode(*)>(_a[1]))); break;
        case 1: _t->hitsChanged((*reinterpret_cast< const Qt3DRender::QAbstractRayCaster::Hits(*)>(_a[1]))); break;
        case 2: _t->filterModeChanged((*reinterpret_cast< Qt3DRender::QAbstractRayCaster::FilterMode(*)>(_a[1]))); break;
        case 3: _t->setRunMode((*reinterpret_cast< RunMode(*)>(_a[1]))); break;
        case 4: _t->setFilterMode((*reinterpret_cast< FilterMode(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DRender::QAbstractRayCaster::Hits >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAbstractRayCaster::*)(Qt3DRender::QAbstractRayCaster::RunMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractRayCaster::runModeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QAbstractRayCaster::*)(const Qt3DRender::QAbstractRayCaster::Hits & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractRayCaster::hitsChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QAbstractRayCaster::*)(Qt3DRender::QAbstractRayCaster::FilterMode );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractRayCaster::filterModeChanged)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QAbstractRayCaster *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< RunMode*>(_v) = _t->runMode(); break;
        case 1: *reinterpret_cast< FilterMode*>(_v) = _t->filterMode(); break;
        case 2: *reinterpret_cast< Hits*>(_v) = _t->hits(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QAbstractRayCaster *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setRunMode(*reinterpret_cast< RunMode*>(_v)); break;
        case 1: _t->setFilterMode(*reinterpret_cast< FilterMode*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QAbstractRayCaster::staticMetaObject = { {
    &Qt3DCore::QComponent::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QAbstractRayCaster.data,
    qt_meta_data_Qt3DRender__QAbstractRayCaster,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QAbstractRayCaster::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QAbstractRayCaster::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QAbstractRayCaster.stringdata0))
        return static_cast<void*>(this);
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QAbstractRayCaster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QAbstractRayCaster::runModeChanged(Qt3DRender::QAbstractRayCaster::RunMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QAbstractRayCaster::hitsChanged(const Qt3DRender::QAbstractRayCaster::Hits & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QAbstractRayCaster::filterModeChanged(Qt3DRender::QAbstractRayCaster::FilterMode _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
