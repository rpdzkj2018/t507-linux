/****************************************************************************
** Meta object code from reading C++ file 'qquickslider_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickslider_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickslider_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickSlider_t {
    QByteArrayData data[44];
    char stringdata0[569];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickSlider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickSlider_t qt_meta_stringdata_QQuickSlider = {
    {
QT_MOC_LITERAL(0, 0, 12), // "QQuickSlider"
QT_MOC_LITERAL(1, 13, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 35, 17), // "background,handle"
QT_MOC_LITERAL(3, 53, 11), // "fromChanged"
QT_MOC_LITERAL(4, 65, 0), // ""
QT_MOC_LITERAL(5, 66, 9), // "toChanged"
QT_MOC_LITERAL(6, 76, 12), // "valueChanged"
QT_MOC_LITERAL(7, 89, 15), // "positionChanged"
QT_MOC_LITERAL(8, 105, 21), // "visualPositionChanged"
QT_MOC_LITERAL(9, 127, 15), // "stepSizeChanged"
QT_MOC_LITERAL(10, 143, 15), // "snapModeChanged"
QT_MOC_LITERAL(11, 159, 14), // "pressedChanged"
QT_MOC_LITERAL(12, 174, 18), // "orientationChanged"
QT_MOC_LITERAL(13, 193, 13), // "handleChanged"
QT_MOC_LITERAL(14, 207, 5), // "moved"
QT_MOC_LITERAL(15, 213, 11), // "liveChanged"
QT_MOC_LITERAL(16, 225, 25), // "touchDragThresholdChanged"
QT_MOC_LITERAL(17, 251, 26), // "implicitHandleWidthChanged"
QT_MOC_LITERAL(18, 278, 27), // "implicitHandleHeightChanged"
QT_MOC_LITERAL(19, 306, 8), // "increase"
QT_MOC_LITERAL(20, 315, 8), // "decrease"
QT_MOC_LITERAL(21, 324, 7), // "valueAt"
QT_MOC_LITERAL(22, 332, 8), // "position"
QT_MOC_LITERAL(23, 341, 4), // "from"
QT_MOC_LITERAL(24, 346, 2), // "to"
QT_MOC_LITERAL(25, 349, 5), // "value"
QT_MOC_LITERAL(26, 355, 14), // "visualPosition"
QT_MOC_LITERAL(27, 370, 8), // "stepSize"
QT_MOC_LITERAL(28, 379, 8), // "snapMode"
QT_MOC_LITERAL(29, 388, 8), // "SnapMode"
QT_MOC_LITERAL(30, 397, 7), // "pressed"
QT_MOC_LITERAL(31, 405, 11), // "orientation"
QT_MOC_LITERAL(32, 417, 15), // "Qt::Orientation"
QT_MOC_LITERAL(33, 433, 6), // "handle"
QT_MOC_LITERAL(34, 440, 11), // "QQuickItem*"
QT_MOC_LITERAL(35, 452, 4), // "live"
QT_MOC_LITERAL(36, 457, 10), // "horizontal"
QT_MOC_LITERAL(37, 468, 8), // "vertical"
QT_MOC_LITERAL(38, 477, 18), // "touchDragThreshold"
QT_MOC_LITERAL(39, 496, 19), // "implicitHandleWidth"
QT_MOC_LITERAL(40, 516, 20), // "implicitHandleHeight"
QT_MOC_LITERAL(41, 537, 6), // "NoSnap"
QT_MOC_LITERAL(42, 544, 10), // "SnapAlways"
QT_MOC_LITERAL(43, 555, 13) // "SnapOnRelease"

    },
    "QQuickSlider\0DeferredPropertyNames\0"
    "background,handle\0fromChanged\0\0toChanged\0"
    "valueChanged\0positionChanged\0"
    "visualPositionChanged\0stepSizeChanged\0"
    "snapModeChanged\0pressedChanged\0"
    "orientationChanged\0handleChanged\0moved\0"
    "liveChanged\0touchDragThresholdChanged\0"
    "implicitHandleWidthChanged\0"
    "implicitHandleHeightChanged\0increase\0"
    "decrease\0valueAt\0position\0from\0to\0"
    "value\0visualPosition\0stepSize\0snapMode\0"
    "SnapMode\0pressed\0orientation\0"
    "Qt::Orientation\0handle\0QQuickItem*\0"
    "live\0horizontal\0vertical\0touchDragThreshold\0"
    "implicitHandleWidth\0implicitHandleHeight\0"
    "NoSnap\0SnapAlways\0SnapOnRelease"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickSlider[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      18,   16, // methods
      16,  144, // properties
       1,  224, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  124,    4, 0x06 /* Public */,
       5,    0,  125,    4, 0x06 /* Public */,
       6,    0,  126,    4, 0x06 /* Public */,
       7,    0,  127,    4, 0x06 /* Public */,
       8,    0,  128,    4, 0x06 /* Public */,
       9,    0,  129,    4, 0x06 /* Public */,
      10,    0,  130,    4, 0x06 /* Public */,
      11,    0,  131,    4, 0x06 /* Public */,
      12,    0,  132,    4, 0x06 /* Public */,
      13,    0,  133,    4, 0x06 /* Public */,
      14,    0,  134,    4, 0x86 /* Public | MethodRevisioned */,
      15,    0,  135,    4, 0x86 /* Public | MethodRevisioned */,
      16,    0,  136,    4, 0x86 /* Public | MethodRevisioned */,
      17,    0,  137,    4, 0x86 /* Public | MethodRevisioned */,
      18,    0,  138,    4, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      19,    0,  139,    4, 0x0a /* Public */,
      20,    0,  140,    4, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      21,    1,  141,    4, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       5,
       5,
       5,

 // slots: revision
       0,
       0,

 // methods: revision
       1,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QReal, QMetaType::QReal,   22,

 // properties: name, type, flags
      23, QMetaType::QReal, 0x00495903,
      24, QMetaType::QReal, 0x00495903,
      25, QMetaType::QReal, 0x00495903,
      22, QMetaType::QReal, 0x00495801,
      26, QMetaType::QReal, 0x00495801,
      27, QMetaType::QReal, 0x00495903,
      28, 0x80000000 | 29, 0x0049590b,
      30, QMetaType::Bool, 0x00495903,
      31, 0x80000000 | 32, 0x0049590b,
      33, 0x80000000 | 34, 0x0049590b,
      35, QMetaType::Bool, 0x00c95903,
      36, QMetaType::Bool, 0x00c95801,
      37, QMetaType::Bool, 0x00c95801,
      38, QMetaType::QReal, 0x00c95907,
      39, QMetaType::QReal, 0x00c95801,
      40, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      11,
       8,
       8,
      12,
      13,
      14,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       3,
       3,
       5,
       5,
       5,

 // enums: name, alias, flags, count, data
      29,   29, 0x0,    3,  229,

 // enum data: key, value
      41, uint(QQuickSlider::NoSnap),
      42, uint(QQuickSlider::SnapAlways),
      43, uint(QQuickSlider::SnapOnRelease),

       0        // eod
};

void QQuickSlider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickSlider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fromChanged(); break;
        case 1: _t->toChanged(); break;
        case 2: _t->valueChanged(); break;
        case 3: _t->positionChanged(); break;
        case 4: _t->visualPositionChanged(); break;
        case 5: _t->stepSizeChanged(); break;
        case 6: _t->snapModeChanged(); break;
        case 7: _t->pressedChanged(); break;
        case 8: _t->orientationChanged(); break;
        case 9: _t->handleChanged(); break;
        case 10: _t->moved(); break;
        case 11: _t->liveChanged(); break;
        case 12: _t->touchDragThresholdChanged(); break;
        case 13: _t->implicitHandleWidthChanged(); break;
        case 14: _t->implicitHandleHeightChanged(); break;
        case 15: _t->increase(); break;
        case 16: _t->decrease(); break;
        case 17: { qreal _r = _t->valueAt((*reinterpret_cast< qreal(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< qreal*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::fromChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::toChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::valueChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::positionChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::visualPositionChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::stepSizeChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::snapModeChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::pressedChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::orientationChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::handleChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::moved)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::liveChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::touchDragThresholdChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::implicitHandleWidthChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickSlider::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSlider::implicitHandleHeightChanged)) {
                *result = 14;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickSlider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< qreal*>(_v) = _t->from(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->to(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->value(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->position(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->visualPosition(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->stepSize(); break;
        case 6: *reinterpret_cast< SnapMode*>(_v) = _t->snapMode(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 8: *reinterpret_cast< Qt::Orientation*>(_v) = _t->orientation(); break;
        case 9: *reinterpret_cast< QQuickItem**>(_v) = _t->handle(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->live(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->isHorizontal(); break;
        case 12: *reinterpret_cast< bool*>(_v) = _t->isVertical(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->touchDragThreshold(); break;
        case 14: *reinterpret_cast< qreal*>(_v) = _t->implicitHandleWidth(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->implicitHandleHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickSlider *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFrom(*reinterpret_cast< qreal*>(_v)); break;
        case 1: _t->setTo(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setValue(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setStepSize(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setSnapMode(*reinterpret_cast< SnapMode*>(_v)); break;
        case 7: _t->setPressed(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setOrientation(*reinterpret_cast< Qt::Orientation*>(_v)); break;
        case 9: _t->setHandle(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 10: _t->setLive(*reinterpret_cast< bool*>(_v)); break;
        case 13: _t->setTouchDragThreshold(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickSlider *_t = static_cast<QQuickSlider *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 13: _t->resetTouchDragThreshold(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickSlider::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickSlider.data,
    qt_meta_data_QQuickSlider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickSlider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickSlider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickSlider.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickSlider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 16;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 16;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickSlider::fromChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickSlider::toChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickSlider::valueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickSlider::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickSlider::visualPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickSlider::stepSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickSlider::snapModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickSlider::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickSlider::orientationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickSlider::handleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickSlider::moved()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickSlider::liveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickSlider::touchDragThresholdChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickSlider::implicitHandleWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickSlider::implicitHandleHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
