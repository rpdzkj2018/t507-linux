/****************************************************************************
** Meta object code from reading C++ file 'qbluetoothdevicediscoveryagent.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qbluetoothdevicediscoveryagent.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qbluetoothdevicediscoveryagent.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent_t {
    QByteArrayData data[33];
    char stringdata0[532];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent_t qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent = {
    {
QT_MOC_LITERAL(0, 0, 30), // "QBluetoothDeviceDiscoveryAgent"
QT_MOC_LITERAL(1, 31, 16), // "deviceDiscovered"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 20), // "QBluetoothDeviceInfo"
QT_MOC_LITERAL(4, 70, 4), // "info"
QT_MOC_LITERAL(5, 75, 13), // "deviceUpdated"
QT_MOC_LITERAL(6, 89, 28), // "QBluetoothDeviceInfo::Fields"
QT_MOC_LITERAL(7, 118, 13), // "updatedFields"
QT_MOC_LITERAL(8, 132, 8), // "finished"
QT_MOC_LITERAL(9, 141, 5), // "error"
QT_MOC_LITERAL(10, 147, 37), // "QBluetoothDeviceDiscoveryAgen..."
QT_MOC_LITERAL(11, 185, 8), // "canceled"
QT_MOC_LITERAL(12, 194, 5), // "start"
QT_MOC_LITERAL(13, 200, 16), // "DiscoveryMethods"
QT_MOC_LITERAL(14, 217, 6), // "method"
QT_MOC_LITERAL(15, 224, 4), // "stop"
QT_MOC_LITERAL(16, 229, 11), // "inquiryType"
QT_MOC_LITERAL(17, 241, 43), // "QBluetoothDeviceDiscoveryAgen..."
QT_MOC_LITERAL(18, 285, 5), // "Error"
QT_MOC_LITERAL(19, 291, 7), // "NoError"
QT_MOC_LITERAL(20, 299, 16), // "InputOutputError"
QT_MOC_LITERAL(21, 316, 15), // "PoweredOffError"
QT_MOC_LITERAL(22, 332, 28), // "InvalidBluetoothAdapterError"
QT_MOC_LITERAL(23, 361, 24), // "UnsupportedPlatformError"
QT_MOC_LITERAL(24, 386, 26), // "UnsupportedDiscoveryMethod"
QT_MOC_LITERAL(25, 413, 12), // "UnknownError"
QT_MOC_LITERAL(26, 426, 11), // "InquiryType"
QT_MOC_LITERAL(27, 438, 23), // "GeneralUnlimitedInquiry"
QT_MOC_LITERAL(28, 462, 14), // "LimitedInquiry"
QT_MOC_LITERAL(29, 477, 15), // "DiscoveryMethod"
QT_MOC_LITERAL(30, 493, 8), // "NoMethod"
QT_MOC_LITERAL(31, 502, 13), // "ClassicMethod"
QT_MOC_LITERAL(32, 516, 15) // "LowEnergyMethod"

    },
    "QBluetoothDeviceDiscoveryAgent\0"
    "deviceDiscovered\0\0QBluetoothDeviceInfo\0"
    "info\0deviceUpdated\0QBluetoothDeviceInfo::Fields\0"
    "updatedFields\0finished\0error\0"
    "QBluetoothDeviceDiscoveryAgent::Error\0"
    "canceled\0start\0DiscoveryMethods\0method\0"
    "stop\0inquiryType\0"
    "QBluetoothDeviceDiscoveryAgent::InquiryType\0"
    "Error\0NoError\0InputOutputError\0"
    "PoweredOffError\0InvalidBluetoothAdapterError\0"
    "UnsupportedPlatformError\0"
    "UnsupportedDiscoveryMethod\0UnknownError\0"
    "InquiryType\0GeneralUnlimitedInquiry\0"
    "LimitedInquiry\0DiscoveryMethod\0NoMethod\0"
    "ClassicMethod\0LowEnergyMethod"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QBluetoothDeviceDiscoveryAgent[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       1,   72, // properties
       3,   75, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    2,   57,    2, 0x06 /* Public */,
       8,    0,   62,    2, 0x06 /* Public */,
       9,    1,   63,    2, 0x06 /* Public */,
      11,    0,   66,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,   67,    2, 0x0a /* Public */,
      12,    1,   68,    2, 0x0a /* Public */,
      15,    0,   71,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 6,    4,    7,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 10,    9,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,

 // properties: name, type, flags
      16, 0x80000000 | 17, 0x0009510b,

 // enums: name, alias, flags, count, data
      18,   18, 0x0,    7,   90,
      26,   26, 0x0,    2,  104,
      13,   29, 0x1,    3,  108,

 // enum data: key, value
      19, uint(QBluetoothDeviceDiscoveryAgent::NoError),
      20, uint(QBluetoothDeviceDiscoveryAgent::InputOutputError),
      21, uint(QBluetoothDeviceDiscoveryAgent::PoweredOffError),
      22, uint(QBluetoothDeviceDiscoveryAgent::InvalidBluetoothAdapterError),
      23, uint(QBluetoothDeviceDiscoveryAgent::UnsupportedPlatformError),
      24, uint(QBluetoothDeviceDiscoveryAgent::UnsupportedDiscoveryMethod),
      25, uint(QBluetoothDeviceDiscoveryAgent::UnknownError),
      27, uint(QBluetoothDeviceDiscoveryAgent::GeneralUnlimitedInquiry),
      28, uint(QBluetoothDeviceDiscoveryAgent::LimitedInquiry),
      30, uint(QBluetoothDeviceDiscoveryAgent::NoMethod),
      31, uint(QBluetoothDeviceDiscoveryAgent::ClassicMethod),
      32, uint(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod),

       0        // eod
};

void QBluetoothDeviceDiscoveryAgent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QBluetoothDeviceDiscoveryAgent *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->deviceDiscovered((*reinterpret_cast< const QBluetoothDeviceInfo(*)>(_a[1]))); break;
        case 1: _t->deviceUpdated((*reinterpret_cast< const QBluetoothDeviceInfo(*)>(_a[1])),(*reinterpret_cast< QBluetoothDeviceInfo::Fields(*)>(_a[2]))); break;
        case 2: _t->finished(); break;
        case 3: _t->error((*reinterpret_cast< QBluetoothDeviceDiscoveryAgent::Error(*)>(_a[1]))); break;
        case 4: _t->canceled(); break;
        case 5: _t->start(); break;
        case 6: _t->start((*reinterpret_cast< DiscoveryMethods(*)>(_a[1]))); break;
        case 7: _t->stop(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QBluetoothDeviceInfo >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QBluetoothDeviceInfo >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QBluetoothDeviceDiscoveryAgent::*)(const QBluetoothDeviceInfo & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothDeviceDiscoveryAgent::deviceDiscovered)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QBluetoothDeviceDiscoveryAgent::*)(const QBluetoothDeviceInfo & , QBluetoothDeviceInfo::Fields );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothDeviceDiscoveryAgent::deviceUpdated)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QBluetoothDeviceDiscoveryAgent::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothDeviceDiscoveryAgent::finished)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QBluetoothDeviceDiscoveryAgent::*)(QBluetoothDeviceDiscoveryAgent::Error );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothDeviceDiscoveryAgent::error)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QBluetoothDeviceDiscoveryAgent::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothDeviceDiscoveryAgent::canceled)) {
                *result = 4;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QBluetoothDeviceDiscoveryAgent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QBluetoothDeviceDiscoveryAgent::InquiryType*>(_v) = _t->inquiryType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QBluetoothDeviceDiscoveryAgent *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setInquiryType(*reinterpret_cast< QBluetoothDeviceDiscoveryAgent::InquiryType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QBluetoothDeviceDiscoveryAgent::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent.data,
    qt_meta_data_QBluetoothDeviceDiscoveryAgent,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QBluetoothDeviceDiscoveryAgent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QBluetoothDeviceDiscoveryAgent::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QBluetoothDeviceDiscoveryAgent.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QBluetoothDeviceDiscoveryAgent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QBluetoothDeviceDiscoveryAgent::deviceDiscovered(const QBluetoothDeviceInfo & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QBluetoothDeviceDiscoveryAgent::deviceUpdated(const QBluetoothDeviceInfo & _t1, QBluetoothDeviceInfo::Fields _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QBluetoothDeviceDiscoveryAgent::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QBluetoothDeviceDiscoveryAgent::error(QBluetoothDeviceDiscoveryAgent::Error _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QBluetoothDeviceDiscoveryAgent::canceled()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
