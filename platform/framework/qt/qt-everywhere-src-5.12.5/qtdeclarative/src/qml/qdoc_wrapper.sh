#!/bin/sh
QT_VERSION=5.12.5
export QT_VERSION
QT_VER=5.12
export QT_VER
QT_VERSION_TAG=5125
export QT_VERSION_TAG
QT_INSTALL_DOCS=/home/fourth/rp-dev/T507/linux/test/lichee/platform/framework/qt/qt-everywhere-src-5.12.5/qtbase/doc
export QT_INSTALL_DOCS
BUILDDIR=/home/fourth/rp-dev/T507/linux/test/lichee/platform/framework/qt/qt-everywhere-src-5.12.5/qtdeclarative/src/qml
export BUILDDIR
exec /home/fourth/rp-dev/T507/linux/test/lichee/platform/framework/qt/qt-everywhere-src-5.12.5/Qt_5.12.5/bin/qdoc "$@"
