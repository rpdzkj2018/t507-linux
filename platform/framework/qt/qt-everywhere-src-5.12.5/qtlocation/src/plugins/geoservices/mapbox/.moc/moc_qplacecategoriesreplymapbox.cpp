/****************************************************************************
** Meta object code from reading C++ file 'qplacecategoriesreplymapbox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qplacecategoriesreplymapbox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qplacecategoriesreplymapbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QPlaceCategoriesReplyMapbox_t {
    QByteArrayData data[7];
    char stringdata0[86];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QPlaceCategoriesReplyMapbox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QPlaceCategoriesReplyMapbox_t qt_meta_stringdata_QPlaceCategoriesReplyMapbox = {
    {
QT_MOC_LITERAL(0, 0, 27), // "QPlaceCategoriesReplyMapbox"
QT_MOC_LITERAL(1, 28, 6), // "finish"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 8), // "setError"
QT_MOC_LITERAL(4, 45, 18), // "QPlaceReply::Error"
QT_MOC_LITERAL(5, 64, 9), // "errorCode"
QT_MOC_LITERAL(6, 74, 11) // "errorString"

    },
    "QPlaceCategoriesReplyMapbox\0finish\0\0"
    "setError\0QPlaceReply::Error\0errorCode\0"
    "errorString"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QPlaceCategoriesReplyMapbox[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x0a /* Public */,
       3,    2,   25,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, QMetaType::QString,    5,    6,

       0        // eod
};

void QPlaceCategoriesReplyMapbox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QPlaceCategoriesReplyMapbox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finish(); break;
        case 1: _t->setError((*reinterpret_cast< QPlaceReply::Error(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QPlaceReply::Error >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QPlaceCategoriesReplyMapbox::staticMetaObject = { {
    &QPlaceReply::staticMetaObject,
    qt_meta_stringdata_QPlaceCategoriesReplyMapbox.data,
    qt_meta_data_QPlaceCategoriesReplyMapbox,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QPlaceCategoriesReplyMapbox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QPlaceCategoriesReplyMapbox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QPlaceCategoriesReplyMapbox.stringdata0))
        return static_cast<void*>(this);
    return QPlaceReply::qt_metacast(_clname);
}

int QPlaceCategoriesReplyMapbox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QPlaceReply::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
