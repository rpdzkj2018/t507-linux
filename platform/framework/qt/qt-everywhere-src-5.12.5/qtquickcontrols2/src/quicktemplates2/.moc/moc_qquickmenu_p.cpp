/****************************************************************************
** Meta object code from reading C++ file 'qquickmenu_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickmenu_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmenu_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickMenu_t {
    QByteArrayData data[48];
    char stringdata0[497];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenu_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenu_t qt_meta_stringdata_QQuickMenu = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QQuickMenu"
QT_MOC_LITERAL(1, 11, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 27, 11), // "contentData"
QT_MOC_LITERAL(3, 39, 12), // "titleChanged"
QT_MOC_LITERAL(4, 52, 0), // ""
QT_MOC_LITERAL(5, 53, 5), // "title"
QT_MOC_LITERAL(6, 59, 12), // "countChanged"
QT_MOC_LITERAL(7, 72, 14), // "cascadeChanged"
QT_MOC_LITERAL(8, 87, 7), // "cascade"
QT_MOC_LITERAL(9, 95, 14), // "overlapChanged"
QT_MOC_LITERAL(10, 110, 15), // "delegateChanged"
QT_MOC_LITERAL(11, 126, 19), // "currentIndexChanged"
QT_MOC_LITERAL(12, 146, 6), // "itemAt"
QT_MOC_LITERAL(13, 153, 11), // "QQuickItem*"
QT_MOC_LITERAL(14, 165, 5), // "index"
QT_MOC_LITERAL(15, 171, 7), // "addItem"
QT_MOC_LITERAL(16, 179, 4), // "item"
QT_MOC_LITERAL(17, 184, 10), // "insertItem"
QT_MOC_LITERAL(18, 195, 8), // "moveItem"
QT_MOC_LITERAL(19, 204, 4), // "from"
QT_MOC_LITERAL(20, 209, 2), // "to"
QT_MOC_LITERAL(21, 212, 10), // "removeItem"
QT_MOC_LITERAL(22, 223, 8), // "takeItem"
QT_MOC_LITERAL(23, 232, 6), // "menuAt"
QT_MOC_LITERAL(24, 239, 11), // "QQuickMenu*"
QT_MOC_LITERAL(25, 251, 7), // "addMenu"
QT_MOC_LITERAL(26, 259, 4), // "menu"
QT_MOC_LITERAL(27, 264, 10), // "insertMenu"
QT_MOC_LITERAL(28, 275, 10), // "removeMenu"
QT_MOC_LITERAL(29, 286, 8), // "takeMenu"
QT_MOC_LITERAL(30, 295, 8), // "actionAt"
QT_MOC_LITERAL(31, 304, 13), // "QQuickAction*"
QT_MOC_LITERAL(32, 318, 9), // "addAction"
QT_MOC_LITERAL(33, 328, 6), // "action"
QT_MOC_LITERAL(34, 335, 12), // "insertAction"
QT_MOC_LITERAL(35, 348, 12), // "removeAction"
QT_MOC_LITERAL(36, 361, 10), // "takeAction"
QT_MOC_LITERAL(37, 372, 5), // "popup"
QT_MOC_LITERAL(38, 378, 15), // "QQmlV4Function*"
QT_MOC_LITERAL(39, 394, 4), // "args"
QT_MOC_LITERAL(40, 399, 7), // "dismiss"
QT_MOC_LITERAL(41, 407, 12), // "contentModel"
QT_MOC_LITERAL(42, 420, 25), // "QQmlListProperty<QObject>"
QT_MOC_LITERAL(43, 446, 5), // "count"
QT_MOC_LITERAL(44, 452, 7), // "overlap"
QT_MOC_LITERAL(45, 460, 8), // "delegate"
QT_MOC_LITERAL(46, 469, 14), // "QQmlComponent*"
QT_MOC_LITERAL(47, 484, 12) // "currentIndex"

    },
    "QQuickMenu\0DefaultProperty\0contentData\0"
    "titleChanged\0\0title\0countChanged\0"
    "cascadeChanged\0cascade\0overlapChanged\0"
    "delegateChanged\0currentIndexChanged\0"
    "itemAt\0QQuickItem*\0index\0addItem\0item\0"
    "insertItem\0moveItem\0from\0to\0removeItem\0"
    "takeItem\0menuAt\0QQuickMenu*\0addMenu\0"
    "menu\0insertMenu\0removeMenu\0takeMenu\0"
    "actionAt\0QQuickAction*\0addAction\0"
    "action\0insertAction\0removeAction\0"
    "takeAction\0popup\0QQmlV4Function*\0args\0"
    "dismiss\0contentModel\0QQmlListProperty<QObject>\0"
    "count\0overlap\0delegate\0QQmlComponent*\0"
    "currentIndex"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenu[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      24,   16, // methods
       8,  230, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    1,  160,    4, 0x06 /* Public */,
       6,    0,  163,    4, 0x86 /* Public | MethodRevisioned */,
       7,    1,  164,    4, 0x86 /* Public | MethodRevisioned */,
       9,    0,  167,    4, 0x86 /* Public | MethodRevisioned */,
      10,    0,  168,    4, 0x86 /* Public | MethodRevisioned */,
      11,    0,  169,    4, 0x86 /* Public | MethodRevisioned */,

 // methods: name, argc, parameters, tag, flags
      12,    1,  170,    4, 0x02 /* Public */,
      15,    1,  173,    4, 0x02 /* Public */,
      17,    2,  176,    4, 0x02 /* Public */,
      18,    2,  181,    4, 0x02 /* Public */,
      21,    1,  186,    4, 0x02 /* Public */,
      22,    1,  189,    4, 0x82 /* Public | MethodRevisioned */,
      23,    1,  192,    4, 0x82 /* Public | MethodRevisioned */,
      25,    1,  195,    4, 0x82 /* Public | MethodRevisioned */,
      27,    2,  198,    4, 0x82 /* Public | MethodRevisioned */,
      28,    1,  203,    4, 0x82 /* Public | MethodRevisioned */,
      29,    1,  206,    4, 0x82 /* Public | MethodRevisioned */,
      30,    1,  209,    4, 0x82 /* Public | MethodRevisioned */,
      32,    1,  212,    4, 0x82 /* Public | MethodRevisioned */,
      34,    2,  215,    4, 0x82 /* Public | MethodRevisioned */,
      35,    1,  220,    4, 0x82 /* Public | MethodRevisioned */,
      36,    1,  223,    4, 0x82 /* Public | MethodRevisioned */,
      37,    1,  226,    4, 0x82 /* Public | MethodRevisioned */,
      40,    0,  229,    4, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       3,
       3,
       3,
       3,
       3,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,
       3,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 13, QMetaType::Int,   14,
    QMetaType::Void, 0x80000000 | 13,   16,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 13,   14,   16,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   19,   20,
    QMetaType::Void, QMetaType::QVariant,   16,
    0x80000000 | 13, QMetaType::Int,   14,
    0x80000000 | 24, QMetaType::Int,   14,
    QMetaType::Void, 0x80000000 | 24,   26,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 24,   14,   26,
    QMetaType::Void, 0x80000000 | 24,   26,
    0x80000000 | 24, QMetaType::Int,   14,
    0x80000000 | 31, QMetaType::Int,   14,
    QMetaType::Void, 0x80000000 | 31,   33,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 31,   14,   33,
    QMetaType::Void, 0x80000000 | 31,   33,
    0x80000000 | 31, QMetaType::Int,   14,
    QMetaType::Void, 0x80000000 | 38,   39,
    QMetaType::Void,

 // properties: name, type, flags
      41, QMetaType::QVariant, 0x00095c01,
       2, 0x80000000 | 42, 0x00095809,
       5, QMetaType::QString, 0x00495903,
      43, QMetaType::Int, 0x00c95801,
       8, QMetaType::Bool, 0x00c95907,
      44, QMetaType::QReal, 0x00c95903,
      45, 0x80000000 | 46, 0x00c9590b,
      47, QMetaType::Int, 0x00c95903,

 // properties: notify_signal_id
       0,
       0,
       0,
       1,
       2,
       3,
       4,
       5,

 // properties: revision
       0,
       0,
       0,
       3,
       3,
       3,
       3,
       3,

       0        // eod
};

void QQuickMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickMenu *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->titleChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->countChanged(); break;
        case 2: _t->cascadeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->overlapChanged(); break;
        case 4: _t->delegateChanged(); break;
        case 5: _t->currentIndexChanged(); break;
        case 6: { QQuickItem* _r = _t->itemAt((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 7: _t->addItem((*reinterpret_cast< QQuickItem*(*)>(_a[1]))); break;
        case 8: _t->insertItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QQuickItem*(*)>(_a[2]))); break;
        case 9: _t->moveItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 10: _t->removeItem((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 11: { QQuickItem* _r = _t->takeItem((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 12: { QQuickMenu* _r = _t->menuAt((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickMenu**>(_a[0]) = std::move(_r); }  break;
        case 13: _t->addMenu((*reinterpret_cast< QQuickMenu*(*)>(_a[1]))); break;
        case 14: _t->insertMenu((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QQuickMenu*(*)>(_a[2]))); break;
        case 15: _t->removeMenu((*reinterpret_cast< QQuickMenu*(*)>(_a[1]))); break;
        case 16: { QQuickMenu* _r = _t->takeMenu((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickMenu**>(_a[0]) = std::move(_r); }  break;
        case 17: { QQuickAction* _r = _t->actionAt((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickAction**>(_a[0]) = std::move(_r); }  break;
        case 18: _t->addAction((*reinterpret_cast< QQuickAction*(*)>(_a[1]))); break;
        case 19: _t->insertAction((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QQuickAction*(*)>(_a[2]))); break;
        case 20: _t->removeAction((*reinterpret_cast< QQuickAction*(*)>(_a[1]))); break;
        case 21: { QQuickAction* _r = _t->takeAction((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickAction**>(_a[0]) = std::move(_r); }  break;
        case 22: _t->popup((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 23: _t->dismiss(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickMenu* >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickMenu* >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickMenu* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickMenu::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::titleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickMenu::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::countChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickMenu::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::cascadeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickMenu::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::overlapChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickMenu::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::delegateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickMenu::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenu::currentIndexChanged)) {
                *result = 5;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickMenu *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->contentModel(); break;
        case 1: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->contentData(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->title(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->cascade(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->overlap(); break;
        case 6: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->currentIndex(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickMenu *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 2: _t->setTitle(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setCascade(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setOverlap(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 7: _t->setCurrentIndex(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickMenu *_t = static_cast<QQuickMenu *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 4: _t->resetCascade(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickMenu::staticMetaObject = { {
    &QQuickPopup::staticMetaObject,
    qt_meta_stringdata_QQuickMenu.data,
    qt_meta_data_QQuickMenu,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenu.stringdata0))
        return static_cast<void*>(this);
    return QQuickPopup::qt_metacast(_clname);
}

int QQuickMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPopup::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenu::titleChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QQuickMenu::countChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickMenu::cascadeChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QQuickMenu::overlapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickMenu::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickMenu::currentIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
