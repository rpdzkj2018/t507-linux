/****************************************************************************
** Meta object code from reading C++ file 'qalsaaudiooutput.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qalsaaudiooutput.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qalsaaudiooutput.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QAlsaAudioOutput_t {
    QByteArrayData data[5];
    char stringdata0[51];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAlsaAudioOutput_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAlsaAudioOutput_t qt_meta_stringdata_QAlsaAudioOutput = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QAlsaAudioOutput"
QT_MOC_LITERAL(1, 17, 11), // "processMore"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 8), // "userFeed"
QT_MOC_LITERAL(4, 39, 11) // "deviceReady"

    },
    "QAlsaAudioOutput\0processMore\0\0userFeed\0"
    "deviceReady"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAlsaAudioOutput[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   30,    2, 0x08 /* Private */,
       4,    0,   31,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Bool,

       0        // eod
};

void QAlsaAudioOutput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAlsaAudioOutput *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->processMore(); break;
        case 1: _t->userFeed(); break;
        case 2: { bool _r = _t->deviceReady();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAlsaAudioOutput::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAlsaAudioOutput::processMore)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QAlsaAudioOutput::staticMetaObject = { {
    &QAbstractAudioOutput::staticMetaObject,
    qt_meta_stringdata_QAlsaAudioOutput.data,
    qt_meta_data_QAlsaAudioOutput,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QAlsaAudioOutput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAlsaAudioOutput::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QAlsaAudioOutput.stringdata0))
        return static_cast<void*>(this);
    return QAbstractAudioOutput::qt_metacast(_clname);
}

int QAlsaAudioOutput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractAudioOutput::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void QAlsaAudioOutput::processMore()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
struct qt_meta_stringdata_AlsaOutputPrivate_t {
    QByteArrayData data[1];
    char stringdata0[18];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AlsaOutputPrivate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AlsaOutputPrivate_t qt_meta_stringdata_AlsaOutputPrivate = {
    {
QT_MOC_LITERAL(0, 0, 17) // "AlsaOutputPrivate"

    },
    "AlsaOutputPrivate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AlsaOutputPrivate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void AlsaOutputPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject AlsaOutputPrivate::staticMetaObject = { {
    &QIODevice::staticMetaObject,
    qt_meta_stringdata_AlsaOutputPrivate.data,
    qt_meta_data_AlsaOutputPrivate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *AlsaOutputPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AlsaOutputPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AlsaOutputPrivate.stringdata0))
        return static_cast<void*>(this);
    return QIODevice::qt_metacast(_clname);
}

int AlsaOutputPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QIODevice::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
