/****************************************************************************
** Meta object code from reading C++ file 'qgeomapobject_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qgeomapobject_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeomapobject_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoMapObject_t {
    QByteArrayData data[17];
    char stringdata0[166];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoMapObject_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoMapObject_t qt_meta_stringdata_QGeoMapObject = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QGeoMapObject"
QT_MOC_LITERAL(1, 14, 14), // "visibleChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 8), // "selected"
QT_MOC_LITERAL(4, 39, 9), // "completed"
QT_MOC_LITERAL(5, 49, 7), // "visible"
QT_MOC_LITERAL(6, 57, 4), // "type"
QT_MOC_LITERAL(7, 62, 4), // "Type"
QT_MOC_LITERAL(8, 67, 11), // "InvalidType"
QT_MOC_LITERAL(9, 79, 8), // "ViewType"
QT_MOC_LITERAL(10, 88, 9), // "RouteType"
QT_MOC_LITERAL(11, 98, 13), // "RectangleType"
QT_MOC_LITERAL(12, 112, 10), // "CircleType"
QT_MOC_LITERAL(13, 123, 12), // "PolylineType"
QT_MOC_LITERAL(14, 136, 11), // "PolygonType"
QT_MOC_LITERAL(15, 148, 8), // "IconType"
QT_MOC_LITERAL(16, 157, 8) // "UserType"

    },
    "QGeoMapObject\0visibleChanged\0\0selected\0"
    "completed\0visible\0type\0Type\0InvalidType\0"
    "ViewType\0RouteType\0RectangleType\0"
    "CircleType\0PolylineType\0PolygonType\0"
    "IconType\0UserType"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoMapObject[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       2,   32, // properties
       1,   40, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    0,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Bool, 0x00495103,
       6, 0x80000000 | 7, 0x00095409,

 // properties: notify_signal_id
       0,
       0,

 // enums: name, alias, flags, count, data
       7,    7, 0x0,    9,   45,

 // enum data: key, value
       8, uint(QGeoMapObject::InvalidType),
       9, uint(QGeoMapObject::ViewType),
      10, uint(QGeoMapObject::RouteType),
      11, uint(QGeoMapObject::RectangleType),
      12, uint(QGeoMapObject::CircleType),
      13, uint(QGeoMapObject::PolylineType),
      14, uint(QGeoMapObject::PolygonType),
      15, uint(QGeoMapObject::IconType),
      16, uint(QGeoMapObject::UserType),

       0        // eod
};

void QGeoMapObject::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QGeoMapObject *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->visibleChanged(); break;
        case 1: _t->selected(); break;
        case 2: _t->completed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QGeoMapObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeoMapObject::visibleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QGeoMapObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeoMapObject::selected)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QGeoMapObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeoMapObject::completed)) {
                *result = 2;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QGeoMapObject *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->visible(); break;
        case 1: *reinterpret_cast< Type*>(_v) = _t->type(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QGeoMapObject *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setVisible(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QGeoMapObject::staticMetaObject = { {
    &QParameterizableObject::staticMetaObject,
    qt_meta_stringdata_QGeoMapObject.data,
    qt_meta_data_QGeoMapObject,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGeoMapObject::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoMapObject::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoMapObject.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QParameterizableObject::qt_metacast(_clname);
}

int QGeoMapObject::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QParameterizableObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QGeoMapObject::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QGeoMapObject::selected()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QGeoMapObject::completed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
