host_build {
    QT_CPU_FEATURES.x86_64 = mmx sse sse2
} else {
    QT_CPU_FEATURES.arm64 = neon crc32
}
QT.global_private.enabled_features = alloca_h alloca dbus gui network posix_fallocate reduce_exports release_tools sql testlib widgets xml
QT.global_private.disabled_features = private_tests sse2 alloca_malloc_h android-style-assets avx2 dbus-linked gc_binaries libudev reduce_relocations stack-protector-strong system-zlib
PKG_CONFIG_EXECUTABLE = /home/fourth/rp-dev/T507/linux/test/lichee/out/t507/demo2.0/longan/buildroot/host/bin//pkg-config
QT_COORD_TYPE = double
CONFIG += cross_compile compile_examples enable_new_dtags largefile neon precompile_header nostrip
QT_BUILD_PARTS += libs
QT_HOST_CFLAGS_DBUS += 
EXTRA_RPATHS += /usr/lib
