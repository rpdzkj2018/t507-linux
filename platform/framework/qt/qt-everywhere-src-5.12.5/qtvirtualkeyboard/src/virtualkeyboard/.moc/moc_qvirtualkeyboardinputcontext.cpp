/****************************************************************************
** Meta object code from reading C++ file 'qvirtualkeyboardinputcontext.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qvirtualkeyboardinputcontext.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qvirtualkeyboardinputcontext.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QVirtualKeyboardInputContext_t {
    QByteArrayData data[54];
    char stringdata0[917];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QVirtualKeyboardInputContext_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QVirtualKeyboardInputContext_t qt_meta_stringdata_QVirtualKeyboardInputContext = {
    {
QT_MOC_LITERAL(0, 0, 28), // "QVirtualKeyboardInputContext"
QT_MOC_LITERAL(1, 29, 18), // "preeditTextChanged"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 23), // "inputMethodHintsChanged"
QT_MOC_LITERAL(4, 73, 22), // "surroundingTextChanged"
QT_MOC_LITERAL(5, 96, 19), // "selectedTextChanged"
QT_MOC_LITERAL(6, 116, 21), // "anchorPositionChanged"
QT_MOC_LITERAL(7, 138, 21), // "cursorPositionChanged"
QT_MOC_LITERAL(8, 160, 22), // "anchorRectangleChanged"
QT_MOC_LITERAL(9, 183, 22), // "cursorRectangleChanged"
QT_MOC_LITERAL(10, 206, 18), // "shiftActiveChanged"
QT_MOC_LITERAL(11, 225, 21), // "capsLockActiveChanged"
QT_MOC_LITERAL(12, 247, 16), // "uppercaseChanged"
QT_MOC_LITERAL(13, 264, 16), // "animatingChanged"
QT_MOC_LITERAL(14, 281, 13), // "localeChanged"
QT_MOC_LITERAL(15, 295, 16), // "inputItemChanged"
QT_MOC_LITERAL(16, 312, 30), // "selectionControlVisibleChanged"
QT_MOC_LITERAL(17, 343, 35), // "anchorRectIntersectsClipRectC..."
QT_MOC_LITERAL(18, 379, 35), // "cursorRectIntersectsClipRectC..."
QT_MOC_LITERAL(19, 415, 12), // "sendKeyClick"
QT_MOC_LITERAL(20, 428, 3), // "key"
QT_MOC_LITERAL(21, 432, 4), // "text"
QT_MOC_LITERAL(22, 437, 9), // "modifiers"
QT_MOC_LITERAL(23, 447, 6), // "commit"
QT_MOC_LITERAL(24, 454, 11), // "replaceFrom"
QT_MOC_LITERAL(25, 466, 13), // "replaceLength"
QT_MOC_LITERAL(26, 480, 5), // "clear"
QT_MOC_LITERAL(27, 486, 25), // "setSelectionOnFocusObject"
QT_MOC_LITERAL(28, 512, 9), // "anchorPos"
QT_MOC_LITERAL(29, 522, 9), // "cursorPos"
QT_MOC_LITERAL(30, 532, 5), // "shift"
QT_MOC_LITERAL(31, 538, 11), // "shiftActive"
QT_MOC_LITERAL(32, 550, 8), // "capsLock"
QT_MOC_LITERAL(33, 559, 14), // "capsLockActive"
QT_MOC_LITERAL(34, 574, 9), // "uppercase"
QT_MOC_LITERAL(35, 584, 14), // "anchorPosition"
QT_MOC_LITERAL(36, 599, 14), // "cursorPosition"
QT_MOC_LITERAL(37, 614, 16), // "inputMethodHints"
QT_MOC_LITERAL(38, 631, 20), // "Qt::InputMethodHints"
QT_MOC_LITERAL(39, 652, 11), // "preeditText"
QT_MOC_LITERAL(40, 664, 15), // "surroundingText"
QT_MOC_LITERAL(41, 680, 12), // "selectedText"
QT_MOC_LITERAL(42, 693, 15), // "anchorRectangle"
QT_MOC_LITERAL(43, 709, 15), // "cursorRectangle"
QT_MOC_LITERAL(44, 725, 9), // "animating"
QT_MOC_LITERAL(45, 735, 6), // "locale"
QT_MOC_LITERAL(46, 742, 9), // "inputItem"
QT_MOC_LITERAL(47, 752, 11), // "inputEngine"
QT_MOC_LITERAL(48, 764, 28), // "QVirtualKeyboardInputEngine*"
QT_MOC_LITERAL(49, 793, 23), // "selectionControlVisible"
QT_MOC_LITERAL(50, 817, 28), // "anchorRectIntersectsClipRect"
QT_MOC_LITERAL(51, 846, 28), // "cursorRectIntersectsClipRect"
QT_MOC_LITERAL(52, 875, 4), // "priv"
QT_MOC_LITERAL(53, 880, 36) // "QVirtualKeyboardInputContextP..."

    },
    "QVirtualKeyboardInputContext\0"
    "preeditTextChanged\0\0inputMethodHintsChanged\0"
    "surroundingTextChanged\0selectedTextChanged\0"
    "anchorPositionChanged\0cursorPositionChanged\0"
    "anchorRectangleChanged\0cursorRectangleChanged\0"
    "shiftActiveChanged\0capsLockActiveChanged\0"
    "uppercaseChanged\0animatingChanged\0"
    "localeChanged\0inputItemChanged\0"
    "selectionControlVisibleChanged\0"
    "anchorRectIntersectsClipRectChanged\0"
    "cursorRectIntersectsClipRectChanged\0"
    "sendKeyClick\0key\0text\0modifiers\0commit\0"
    "replaceFrom\0replaceLength\0clear\0"
    "setSelectionOnFocusObject\0anchorPos\0"
    "cursorPos\0shift\0shiftActive\0capsLock\0"
    "capsLockActive\0uppercase\0anchorPosition\0"
    "cursorPosition\0inputMethodHints\0"
    "Qt::InputMethodHints\0preeditText\0"
    "surroundingText\0selectedText\0"
    "anchorRectangle\0cursorRectangle\0"
    "animating\0locale\0inputItem\0inputEngine\0"
    "QVirtualKeyboardInputEngine*\0"
    "selectionControlVisible\0"
    "anchorRectIntersectsClipRect\0"
    "cursorRectIntersectsClipRect\0priv\0"
    "QVirtualKeyboardInputContextPrivate*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QVirtualKeyboardInputContext[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
      21,  190, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      17,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x06 /* Public */,
       3,    0,  140,    2, 0x06 /* Public */,
       4,    0,  141,    2, 0x06 /* Public */,
       5,    0,  142,    2, 0x06 /* Public */,
       6,    0,  143,    2, 0x06 /* Public */,
       7,    0,  144,    2, 0x06 /* Public */,
       8,    0,  145,    2, 0x06 /* Public */,
       9,    0,  146,    2, 0x06 /* Public */,
      10,    0,  147,    2, 0x06 /* Public */,
      11,    0,  148,    2, 0x06 /* Public */,
      12,    0,  149,    2, 0x06 /* Public */,
      13,    0,  150,    2, 0x06 /* Public */,
      14,    0,  151,    2, 0x06 /* Public */,
      15,    0,  152,    2, 0x06 /* Public */,
      16,    0,  153,    2, 0x06 /* Public */,
      17,    0,  154,    2, 0x06 /* Public */,
      18,    0,  155,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
      19,    3,  156,    2, 0x02 /* Public */,
      19,    2,  163,    2, 0x22 /* Public | MethodCloned */,
      23,    0,  168,    2, 0x02 /* Public */,
      23,    3,  169,    2, 0x02 /* Public */,
      23,    2,  176,    2, 0x22 /* Public | MethodCloned */,
      23,    1,  181,    2, 0x22 /* Public | MethodCloned */,
      26,    0,  184,    2, 0x02 /* Public */,
      27,    2,  185,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::QString, QMetaType::Int,   20,   21,   22,
    QMetaType::Void, QMetaType::Int, QMetaType::QString,   20,   21,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int, QMetaType::Int,   21,   24,   25,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   21,   24,
    QMetaType::Void, QMetaType::QString,   21,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QPointF, QMetaType::QPointF,   28,   29,

 // properties: name, type, flags
      30, QMetaType::Bool, 0x00495001,
      31, QMetaType::Bool, 0x00c95001,
      32, QMetaType::Bool, 0x00495001,
      33, QMetaType::Bool, 0x00c95001,
      34, QMetaType::Bool, 0x00495001,
      35, QMetaType::Int, 0x00495001,
      36, QMetaType::Int, 0x00495001,
      37, 0x80000000 | 38, 0x00495009,
      39, QMetaType::QString, 0x00495103,
      40, QMetaType::QString, 0x00495001,
      41, QMetaType::QString, 0x00495001,
      42, QMetaType::QRectF, 0x00495001,
      43, QMetaType::QRectF, 0x00495001,
      44, QMetaType::Bool, 0x00495103,
      45, QMetaType::QString, 0x00495001,
      46, QMetaType::QObjectStar, 0x00495001,
      47, 0x80000000 | 48, 0x00095409,
      49, QMetaType::Bool, 0x00495001,
      50, QMetaType::Bool, 0x00495001,
      51, QMetaType::Bool, 0x00495001,
      52, 0x80000000 | 53, 0x00095409,

 // properties: notify_signal_id
       8,
       8,
       9,
       9,
      10,
       4,
       5,
       1,
       0,
       2,
       3,
       6,
       7,
      11,
      12,
      13,
       0,
      14,
      15,
      16,
       0,

 // properties: revision
       0,
       4,
       0,
       4,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

       0        // eod
};

void QVirtualKeyboardInputContext::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QVirtualKeyboardInputContext *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->preeditTextChanged(); break;
        case 1: _t->inputMethodHintsChanged(); break;
        case 2: _t->surroundingTextChanged(); break;
        case 3: _t->selectedTextChanged(); break;
        case 4: _t->anchorPositionChanged(); break;
        case 5: _t->cursorPositionChanged(); break;
        case 6: _t->anchorRectangleChanged(); break;
        case 7: _t->cursorRectangleChanged(); break;
        case 8: _t->shiftActiveChanged(); break;
        case 9: _t->capsLockActiveChanged(); break;
        case 10: _t->uppercaseChanged(); break;
        case 11: _t->animatingChanged(); break;
        case 12: _t->localeChanged(); break;
        case 13: _t->inputItemChanged(); break;
        case 14: _t->selectionControlVisibleChanged(); break;
        case 15: _t->anchorRectIntersectsClipRectChanged(); break;
        case 16: _t->cursorRectIntersectsClipRectChanged(); break;
        case 17: _t->sendKeyClick((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 18: _t->sendKeyClick((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 19: _t->commit(); break;
        case 20: _t->commit((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 21: _t->commit((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 22: _t->commit((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: _t->clear(); break;
        case 24: _t->setSelectionOnFocusObject((*reinterpret_cast< const QPointF(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::preeditTextChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::inputMethodHintsChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::surroundingTextChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::selectedTextChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::anchorPositionChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::cursorPositionChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::anchorRectangleChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::cursorRectangleChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::shiftActiveChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::capsLockActiveChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::uppercaseChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::animatingChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::localeChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::inputItemChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::selectionControlVisibleChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::anchorRectIntersectsClipRectChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContext::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContext::cursorRectIntersectsClipRectChanged)) {
                *result = 16;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QVirtualKeyboardInputContext *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isShiftActive(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isShiftActive(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isCapsLockActive(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isCapsLockActive(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isUppercase(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->anchorPosition(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->cursorPosition(); break;
        case 7: *reinterpret_cast< Qt::InputMethodHints*>(_v) = _t->inputMethodHints(); break;
        case 8: *reinterpret_cast< QString*>(_v) = _t->preeditText(); break;
        case 9: *reinterpret_cast< QString*>(_v) = _t->surroundingText(); break;
        case 10: *reinterpret_cast< QString*>(_v) = _t->selectedText(); break;
        case 11: *reinterpret_cast< QRectF*>(_v) = _t->anchorRectangle(); break;
        case 12: *reinterpret_cast< QRectF*>(_v) = _t->cursorRectangle(); break;
        case 13: *reinterpret_cast< bool*>(_v) = _t->isAnimating(); break;
        case 14: *reinterpret_cast< QString*>(_v) = _t->locale(); break;
        case 15: *reinterpret_cast< QObject**>(_v) = _t->inputItem(); break;
        case 16: *reinterpret_cast< QVirtualKeyboardInputEngine**>(_v) = _t->inputEngine(); break;
        case 17: *reinterpret_cast< bool*>(_v) = _t->isSelectionControlVisible(); break;
        case 18: *reinterpret_cast< bool*>(_v) = _t->anchorRectIntersectsClipRect(); break;
        case 19: *reinterpret_cast< bool*>(_v) = _t->cursorRectIntersectsClipRect(); break;
        case 20: *reinterpret_cast< QVirtualKeyboardInputContextPrivate**>(_v) = _t->priv(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QVirtualKeyboardInputContext *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 8: _t->setPreeditText(*reinterpret_cast< QString*>(_v)); break;
        case 13: _t->setAnimating(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QVirtualKeyboardInputContext::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QVirtualKeyboardInputContext.data,
    qt_meta_data_QVirtualKeyboardInputContext,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QVirtualKeyboardInputContext::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QVirtualKeyboardInputContext::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QVirtualKeyboardInputContext.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QVirtualKeyboardInputContext::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 21;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 21;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QVirtualKeyboardInputContext::preeditTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QVirtualKeyboardInputContext::inputMethodHintsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QVirtualKeyboardInputContext::surroundingTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QVirtualKeyboardInputContext::selectedTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QVirtualKeyboardInputContext::anchorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QVirtualKeyboardInputContext::cursorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QVirtualKeyboardInputContext::anchorRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QVirtualKeyboardInputContext::cursorRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QVirtualKeyboardInputContext::shiftActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QVirtualKeyboardInputContext::capsLockActiveChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QVirtualKeyboardInputContext::uppercaseChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QVirtualKeyboardInputContext::animatingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QVirtualKeyboardInputContext::localeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QVirtualKeyboardInputContext::inputItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QVirtualKeyboardInputContext::selectionControlVisibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QVirtualKeyboardInputContext::anchorRectIntersectsClipRectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QVirtualKeyboardInputContext::cursorRectIntersectsClipRectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
