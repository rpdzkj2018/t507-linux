/****************************************************************************
** Meta object code from reading C++ file 'qquickspinbox_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickspinbox_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickspinbox_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickSpinBox_t {
    QByteArrayData data[35];
    char stringdata0[449];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickSpinBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickSpinBox_t qt_meta_stringdata_QQuickSpinBox = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickSpinBox"
QT_MOC_LITERAL(1, 14, 11), // "fromChanged"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 9), // "toChanged"
QT_MOC_LITERAL(4, 37, 12), // "valueChanged"
QT_MOC_LITERAL(5, 50, 15), // "stepSizeChanged"
QT_MOC_LITERAL(6, 66, 15), // "editableChanged"
QT_MOC_LITERAL(7, 82, 16), // "validatorChanged"
QT_MOC_LITERAL(8, 99, 20), // "textFromValueChanged"
QT_MOC_LITERAL(9, 120, 20), // "valueFromTextChanged"
QT_MOC_LITERAL(10, 141, 13), // "valueModified"
QT_MOC_LITERAL(11, 155, 23), // "inputMethodHintsChanged"
QT_MOC_LITERAL(12, 179, 27), // "inputMethodComposingChanged"
QT_MOC_LITERAL(13, 207, 11), // "wrapChanged"
QT_MOC_LITERAL(14, 219, 18), // "displayTextChanged"
QT_MOC_LITERAL(15, 238, 8), // "increase"
QT_MOC_LITERAL(16, 247, 8), // "decrease"
QT_MOC_LITERAL(17, 256, 4), // "from"
QT_MOC_LITERAL(18, 261, 2), // "to"
QT_MOC_LITERAL(19, 264, 5), // "value"
QT_MOC_LITERAL(20, 270, 8), // "stepSize"
QT_MOC_LITERAL(21, 279, 8), // "editable"
QT_MOC_LITERAL(22, 288, 9), // "validator"
QT_MOC_LITERAL(23, 298, 11), // "QValidator*"
QT_MOC_LITERAL(24, 310, 13), // "textFromValue"
QT_MOC_LITERAL(25, 324, 8), // "QJSValue"
QT_MOC_LITERAL(26, 333, 13), // "valueFromText"
QT_MOC_LITERAL(27, 347, 2), // "up"
QT_MOC_LITERAL(28, 350, 17), // "QQuickSpinButton*"
QT_MOC_LITERAL(29, 368, 4), // "down"
QT_MOC_LITERAL(30, 373, 16), // "inputMethodHints"
QT_MOC_LITERAL(31, 390, 20), // "Qt::InputMethodHints"
QT_MOC_LITERAL(32, 411, 20), // "inputMethodComposing"
QT_MOC_LITERAL(33, 432, 4), // "wrap"
QT_MOC_LITERAL(34, 437, 11) // "displayText"

    },
    "QQuickSpinBox\0fromChanged\0\0toChanged\0"
    "valueChanged\0stepSizeChanged\0"
    "editableChanged\0validatorChanged\0"
    "textFromValueChanged\0valueFromTextChanged\0"
    "valueModified\0inputMethodHintsChanged\0"
    "inputMethodComposingChanged\0wrapChanged\0"
    "displayTextChanged\0increase\0decrease\0"
    "from\0to\0value\0stepSize\0editable\0"
    "validator\0QValidator*\0textFromValue\0"
    "QJSValue\0valueFromText\0up\0QQuickSpinButton*\0"
    "down\0inputMethodHints\0Qt::InputMethodHints\0"
    "inputMethodComposing\0wrap\0displayText"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickSpinBox[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
      14,  119, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      13,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06 /* Public */,
       3,    0,  105,    2, 0x06 /* Public */,
       4,    0,  106,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,
       8,    0,  110,    2, 0x06 /* Public */,
       9,    0,  111,    2, 0x06 /* Public */,
      10,    0,  112,    2, 0x86 /* Public | MethodRevisioned */,
      11,    0,  113,    2, 0x86 /* Public | MethodRevisioned */,
      12,    0,  114,    2, 0x86 /* Public | MethodRevisioned */,
      13,    0,  115,    2, 0x86 /* Public | MethodRevisioned */,
      14,    0,  116,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      15,    0,  117,    2, 0x0a /* Public */,
      16,    0,  118,    2, 0x0a /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       2,
       3,
       4,

 // slots: revision
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      17, QMetaType::Int, 0x00495903,
      18, QMetaType::Int, 0x00495903,
      19, QMetaType::Int, 0x00495903,
      20, QMetaType::Int, 0x00495903,
      21, QMetaType::Bool, 0x00495903,
      22, 0x80000000 | 23, 0x0049590b,
      24, 0x80000000 | 25, 0x0049590b,
      26, 0x80000000 | 25, 0x0049590b,
      27, 0x80000000 | 28, 0x00095c09,
      29, 0x80000000 | 28, 0x00095c09,
      30, 0x80000000 | 31, 0x00c9590b,
      32, QMetaType::Bool, 0x00c95801,
      33, QMetaType::Bool, 0x00c95903,
      34, QMetaType::QString, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       0,
       0,
       9,
      10,
      11,
      12,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       2,
       2,
       3,
       4,

       0        // eod
};

void QQuickSpinBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickSpinBox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fromChanged(); break;
        case 1: _t->toChanged(); break;
        case 2: _t->valueChanged(); break;
        case 3: _t->stepSizeChanged(); break;
        case 4: _t->editableChanged(); break;
        case 5: _t->validatorChanged(); break;
        case 6: _t->textFromValueChanged(); break;
        case 7: _t->valueFromTextChanged(); break;
        case 8: _t->valueModified(); break;
        case 9: _t->inputMethodHintsChanged(); break;
        case 10: _t->inputMethodComposingChanged(); break;
        case 11: _t->wrapChanged(); break;
        case 12: _t->displayTextChanged(); break;
        case 13: _t->increase(); break;
        case 14: _t->decrease(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::fromChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::toChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::valueChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::stepSizeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::editableChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::validatorChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::textFromValueChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::valueFromTextChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::valueModified)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::inputMethodHintsChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::inputMethodComposingChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::wrapChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickSpinBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinBox::displayTextChanged)) {
                *result = 12;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 9:
        case 8:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickSpinButton* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickSpinBox *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->from(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->to(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->value(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->stepSize(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->isEditable(); break;
        case 5: *reinterpret_cast< QValidator**>(_v) = _t->validator(); break;
        case 6: *reinterpret_cast< QJSValue*>(_v) = _t->textFromValue(); break;
        case 7: *reinterpret_cast< QJSValue*>(_v) = _t->valueFromText(); break;
        case 8: *reinterpret_cast< QQuickSpinButton**>(_v) = _t->up(); break;
        case 9: *reinterpret_cast< QQuickSpinButton**>(_v) = _t->down(); break;
        case 10: *reinterpret_cast< Qt::InputMethodHints*>(_v) = _t->inputMethodHints(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->isInputMethodComposing(); break;
        case 12: *reinterpret_cast< bool*>(_v) = _t->wrap(); break;
        case 13: *reinterpret_cast< QString*>(_v) = _t->displayText(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickSpinBox *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFrom(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setTo(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setValue(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setStepSize(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setEditable(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setValidator(*reinterpret_cast< QValidator**>(_v)); break;
        case 6: _t->setTextFromValue(*reinterpret_cast< QJSValue*>(_v)); break;
        case 7: _t->setValueFromText(*reinterpret_cast< QJSValue*>(_v)); break;
        case 10: _t->setInputMethodHints(*reinterpret_cast< Qt::InputMethodHints*>(_v)); break;
        case 12: _t->setWrap(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickSpinBox::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickSpinBox.data,
    qt_meta_data_QQuickSpinBox,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickSpinBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickSpinBox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickSpinBox.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickSpinBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 14;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickSpinBox::fromChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickSpinBox::toChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickSpinBox::valueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickSpinBox::stepSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickSpinBox::editableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickSpinBox::validatorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickSpinBox::textFromValueChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickSpinBox::valueFromTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickSpinBox::valueModified()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickSpinBox::inputMethodHintsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickSpinBox::inputMethodComposingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickSpinBox::wrapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickSpinBox::displayTextChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}
struct qt_meta_stringdata_QQuickSpinButton_t {
    QByteArrayData data[14];
    char stringdata0[233];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickSpinButton_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickSpinButton_t qt_meta_stringdata_QQuickSpinButton = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QQuickSpinButton"
QT_MOC_LITERAL(1, 17, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 39, 9), // "indicator"
QT_MOC_LITERAL(3, 49, 14), // "pressedChanged"
QT_MOC_LITERAL(4, 64, 0), // ""
QT_MOC_LITERAL(5, 65, 16), // "indicatorChanged"
QT_MOC_LITERAL(6, 82, 14), // "hoveredChanged"
QT_MOC_LITERAL(7, 97, 29), // "implicitIndicatorWidthChanged"
QT_MOC_LITERAL(8, 127, 30), // "implicitIndicatorHeightChanged"
QT_MOC_LITERAL(9, 158, 7), // "pressed"
QT_MOC_LITERAL(10, 166, 11), // "QQuickItem*"
QT_MOC_LITERAL(11, 178, 7), // "hovered"
QT_MOC_LITERAL(12, 186, 22), // "implicitIndicatorWidth"
QT_MOC_LITERAL(13, 209, 23) // "implicitIndicatorHeight"

    },
    "QQuickSpinButton\0DeferredPropertyNames\0"
    "indicator\0pressedChanged\0\0indicatorChanged\0"
    "hoveredChanged\0implicitIndicatorWidthChanged\0"
    "implicitIndicatorHeightChanged\0pressed\0"
    "QQuickItem*\0hovered\0implicitIndicatorWidth\0"
    "implicitIndicatorHeight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickSpinButton[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       5,   16, // methods
       5,   51, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   46,    4, 0x06 /* Public */,
       5,    0,   47,    4, 0x06 /* Public */,
       6,    0,   48,    4, 0x86 /* Public | MethodRevisioned */,
       7,    0,   49,    4, 0x86 /* Public | MethodRevisioned */,
       8,    0,   50,    4, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       1,
       5,
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       9, QMetaType::Bool, 0x00495903,
       2, 0x80000000 | 10, 0x0049590b,
      11, QMetaType::Bool, 0x00c95903,
      12, QMetaType::QReal, 0x00c95801,
      13, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,

 // properties: revision
       0,
       0,
       1,
       5,
       5,

       0        // eod
};

void QQuickSpinButton::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickSpinButton *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pressedChanged(); break;
        case 1: _t->indicatorChanged(); break;
        case 2: _t->hoveredChanged(); break;
        case 3: _t->implicitIndicatorWidthChanged(); break;
        case 4: _t->implicitIndicatorHeightChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickSpinButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinButton::pressedChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickSpinButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinButton::indicatorChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickSpinButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinButton::hoveredChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickSpinButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinButton::implicitIndicatorWidthChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickSpinButton::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickSpinButton::implicitIndicatorHeightChanged)) {
                *result = 4;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickSpinButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isPressed(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->indicator(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isHovered(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->implicitIndicatorWidth(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->implicitIndicatorHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickSpinButton *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPressed(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setIndicator(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 2: _t->setHovered(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickSpinButton::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickSpinButton.data,
    qt_meta_data_QQuickSpinButton,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickSpinButton::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickSpinButton::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickSpinButton.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickSpinButton::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickSpinButton::pressedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickSpinButton::indicatorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickSpinButton::hoveredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickSpinButton::implicitIndicatorWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickSpinButton::implicitIndicatorHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
