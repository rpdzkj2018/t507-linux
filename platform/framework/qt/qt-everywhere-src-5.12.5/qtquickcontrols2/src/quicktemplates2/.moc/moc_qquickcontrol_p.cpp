/****************************************************************************
** Meta object code from reading C++ file 'qquickcontrol_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickcontrol_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickcontrol_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickControl_t {
    QByteArrayData data[69];
    char stringdata0[1125];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickControl_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickControl_t qt_meta_stringdata_QQuickControl = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickControl"
QT_MOC_LITERAL(1, 14, 21), // "DeferredPropertyNames"
QT_MOC_LITERAL(2, 36, 22), // "background,contentItem"
QT_MOC_LITERAL(3, 59, 11), // "fontChanged"
QT_MOC_LITERAL(4, 71, 0), // ""
QT_MOC_LITERAL(5, 72, 21), // "availableWidthChanged"
QT_MOC_LITERAL(6, 94, 22), // "availableHeightChanged"
QT_MOC_LITERAL(7, 117, 14), // "paddingChanged"
QT_MOC_LITERAL(8, 132, 17), // "topPaddingChanged"
QT_MOC_LITERAL(9, 150, 18), // "leftPaddingChanged"
QT_MOC_LITERAL(10, 169, 19), // "rightPaddingChanged"
QT_MOC_LITERAL(11, 189, 20), // "bottomPaddingChanged"
QT_MOC_LITERAL(12, 210, 14), // "spacingChanged"
QT_MOC_LITERAL(13, 225, 13), // "localeChanged"
QT_MOC_LITERAL(14, 239, 15), // "mirroredChanged"
QT_MOC_LITERAL(15, 255, 18), // "focusPolicyChanged"
QT_MOC_LITERAL(16, 274, 18), // "focusReasonChanged"
QT_MOC_LITERAL(17, 293, 18), // "visualFocusChanged"
QT_MOC_LITERAL(18, 312, 14), // "hoveredChanged"
QT_MOC_LITERAL(19, 327, 19), // "hoverEnabledChanged"
QT_MOC_LITERAL(20, 347, 19), // "wheelEnabledChanged"
QT_MOC_LITERAL(21, 367, 17), // "backgroundChanged"
QT_MOC_LITERAL(22, 385, 18), // "contentItemChanged"
QT_MOC_LITERAL(23, 404, 21), // "baselineOffsetChanged"
QT_MOC_LITERAL(24, 426, 14), // "paletteChanged"
QT_MOC_LITERAL(25, 441, 24), // "horizontalPaddingChanged"
QT_MOC_LITERAL(26, 466, 22), // "verticalPaddingChanged"
QT_MOC_LITERAL(27, 489, 27), // "implicitContentWidthChanged"
QT_MOC_LITERAL(28, 517, 28), // "implicitContentHeightChanged"
QT_MOC_LITERAL(29, 546, 30), // "implicitBackgroundWidthChanged"
QT_MOC_LITERAL(30, 577, 31), // "implicitBackgroundHeightChanged"
QT_MOC_LITERAL(31, 609, 15), // "topInsetChanged"
QT_MOC_LITERAL(32, 625, 16), // "leftInsetChanged"
QT_MOC_LITERAL(33, 642, 17), // "rightInsetChanged"
QT_MOC_LITERAL(34, 660, 18), // "bottomInsetChanged"
QT_MOC_LITERAL(35, 679, 4), // "font"
QT_MOC_LITERAL(36, 684, 14), // "availableWidth"
QT_MOC_LITERAL(37, 699, 15), // "availableHeight"
QT_MOC_LITERAL(38, 715, 7), // "padding"
QT_MOC_LITERAL(39, 723, 10), // "topPadding"
QT_MOC_LITERAL(40, 734, 11), // "leftPadding"
QT_MOC_LITERAL(41, 746, 12), // "rightPadding"
QT_MOC_LITERAL(42, 759, 13), // "bottomPadding"
QT_MOC_LITERAL(43, 773, 7), // "spacing"
QT_MOC_LITERAL(44, 781, 6), // "locale"
QT_MOC_LITERAL(45, 788, 8), // "mirrored"
QT_MOC_LITERAL(46, 797, 11), // "focusPolicy"
QT_MOC_LITERAL(47, 809, 15), // "Qt::FocusPolicy"
QT_MOC_LITERAL(48, 825, 11), // "focusReason"
QT_MOC_LITERAL(49, 837, 15), // "Qt::FocusReason"
QT_MOC_LITERAL(50, 853, 11), // "visualFocus"
QT_MOC_LITERAL(51, 865, 7), // "hovered"
QT_MOC_LITERAL(52, 873, 12), // "hoverEnabled"
QT_MOC_LITERAL(53, 886, 12), // "wheelEnabled"
QT_MOC_LITERAL(54, 899, 10), // "background"
QT_MOC_LITERAL(55, 910, 11), // "QQuickItem*"
QT_MOC_LITERAL(56, 922, 11), // "contentItem"
QT_MOC_LITERAL(57, 934, 14), // "baselineOffset"
QT_MOC_LITERAL(58, 949, 7), // "palette"
QT_MOC_LITERAL(59, 957, 17), // "horizontalPadding"
QT_MOC_LITERAL(60, 975, 15), // "verticalPadding"
QT_MOC_LITERAL(61, 991, 20), // "implicitContentWidth"
QT_MOC_LITERAL(62, 1012, 21), // "implicitContentHeight"
QT_MOC_LITERAL(63, 1034, 23), // "implicitBackgroundWidth"
QT_MOC_LITERAL(64, 1058, 24), // "implicitBackgroundHeight"
QT_MOC_LITERAL(65, 1083, 8), // "topInset"
QT_MOC_LITERAL(66, 1092, 9), // "leftInset"
QT_MOC_LITERAL(67, 1102, 10), // "rightInset"
QT_MOC_LITERAL(68, 1113, 11) // "bottomInset"

    },
    "QQuickControl\0DeferredPropertyNames\0"
    "background,contentItem\0fontChanged\0\0"
    "availableWidthChanged\0availableHeightChanged\0"
    "paddingChanged\0topPaddingChanged\0"
    "leftPaddingChanged\0rightPaddingChanged\0"
    "bottomPaddingChanged\0spacingChanged\0"
    "localeChanged\0mirroredChanged\0"
    "focusPolicyChanged\0focusReasonChanged\0"
    "visualFocusChanged\0hoveredChanged\0"
    "hoverEnabledChanged\0wheelEnabledChanged\0"
    "backgroundChanged\0contentItemChanged\0"
    "baselineOffsetChanged\0paletteChanged\0"
    "horizontalPaddingChanged\0"
    "verticalPaddingChanged\0"
    "implicitContentWidthChanged\0"
    "implicitContentHeightChanged\0"
    "implicitBackgroundWidthChanged\0"
    "implicitBackgroundHeightChanged\0"
    "topInsetChanged\0leftInsetChanged\0"
    "rightInsetChanged\0bottomInsetChanged\0"
    "font\0availableWidth\0availableHeight\0"
    "padding\0topPadding\0leftPadding\0"
    "rightPadding\0bottomPadding\0spacing\0"
    "locale\0mirrored\0focusPolicy\0Qt::FocusPolicy\0"
    "focusReason\0Qt::FocusReason\0visualFocus\0"
    "hovered\0hoverEnabled\0wheelEnabled\0"
    "background\0QQuickItem*\0contentItem\0"
    "baselineOffset\0palette\0horizontalPadding\0"
    "verticalPadding\0implicitContentWidth\0"
    "implicitContentHeight\0implicitBackgroundWidth\0"
    "implicitBackgroundHeight\0topInset\0"
    "leftInset\0rightInset\0bottomInset"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickControl[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      31,   16, // methods
      31,  233, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      31,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  202,    4, 0x06 /* Public */,
       5,    0,  203,    4, 0x06 /* Public */,
       6,    0,  204,    4, 0x06 /* Public */,
       7,    0,  205,    4, 0x06 /* Public */,
       8,    0,  206,    4, 0x06 /* Public */,
       9,    0,  207,    4, 0x06 /* Public */,
      10,    0,  208,    4, 0x06 /* Public */,
      11,    0,  209,    4, 0x06 /* Public */,
      12,    0,  210,    4, 0x06 /* Public */,
      13,    0,  211,    4, 0x06 /* Public */,
      14,    0,  212,    4, 0x06 /* Public */,
      15,    0,  213,    4, 0x06 /* Public */,
      16,    0,  214,    4, 0x06 /* Public */,
      17,    0,  215,    4, 0x06 /* Public */,
      18,    0,  216,    4, 0x06 /* Public */,
      19,    0,  217,    4, 0x06 /* Public */,
      20,    0,  218,    4, 0x06 /* Public */,
      21,    0,  219,    4, 0x06 /* Public */,
      22,    0,  220,    4, 0x06 /* Public */,
      23,    0,  221,    4, 0x06 /* Public */,
      24,    0,  222,    4, 0x86 /* Public | MethodRevisioned */,
      25,    0,  223,    4, 0x86 /* Public | MethodRevisioned */,
      26,    0,  224,    4, 0x86 /* Public | MethodRevisioned */,
      27,    0,  225,    4, 0x86 /* Public | MethodRevisioned */,
      28,    0,  226,    4, 0x86 /* Public | MethodRevisioned */,
      29,    0,  227,    4, 0x86 /* Public | MethodRevisioned */,
      30,    0,  228,    4, 0x86 /* Public | MethodRevisioned */,
      31,    0,  229,    4, 0x86 /* Public | MethodRevisioned */,
      32,    0,  230,    4, 0x86 /* Public | MethodRevisioned */,
      33,    0,  231,    4, 0x86 /* Public | MethodRevisioned */,
      34,    0,  232,    4, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      35, QMetaType::QFont, 0x00495907,
      36, QMetaType::QReal, 0x00495801,
      37, QMetaType::QReal, 0x00495801,
      38, QMetaType::QReal, 0x00495907,
      39, QMetaType::QReal, 0x00495907,
      40, QMetaType::QReal, 0x00495907,
      41, QMetaType::QReal, 0x00495907,
      42, QMetaType::QReal, 0x00495907,
      43, QMetaType::QReal, 0x00495907,
      44, QMetaType::QLocale, 0x00495907,
      45, QMetaType::Bool, 0x00495801,
      46, 0x80000000 | 47, 0x0049590b,
      48, 0x80000000 | 49, 0x0049590b,
      50, QMetaType::Bool, 0x00495801,
      51, QMetaType::Bool, 0x00495801,
      52, QMetaType::Bool, 0x00495907,
      53, QMetaType::Bool, 0x00495903,
      54, 0x80000000 | 55, 0x0049590b,
      56, 0x80000000 | 55, 0x0049590b,
      57, QMetaType::QReal, 0x00495907,
      58, QMetaType::QPalette, 0x00c95907,
      59, QMetaType::QReal, 0x00c95907,
      60, QMetaType::QReal, 0x00c95907,
      61, QMetaType::QReal, 0x00c95801,
      62, QMetaType::QReal, 0x00c95801,
      63, QMetaType::QReal, 0x00c95801,
      64, QMetaType::QReal, 0x00c95801,
      65, QMetaType::QReal, 0x00c95907,
      66, QMetaType::QReal, 0x00c95907,
      67, QMetaType::QReal, 0x00c95907,
      68, QMetaType::QReal, 0x00c95907,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27,
      28,
      29,
      30,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,
       5,

       0        // eod
};

void QQuickControl::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->fontChanged(); break;
        case 1: _t->availableWidthChanged(); break;
        case 2: _t->availableHeightChanged(); break;
        case 3: _t->paddingChanged(); break;
        case 4: _t->topPaddingChanged(); break;
        case 5: _t->leftPaddingChanged(); break;
        case 6: _t->rightPaddingChanged(); break;
        case 7: _t->bottomPaddingChanged(); break;
        case 8: _t->spacingChanged(); break;
        case 9: _t->localeChanged(); break;
        case 10: _t->mirroredChanged(); break;
        case 11: _t->focusPolicyChanged(); break;
        case 12: _t->focusReasonChanged(); break;
        case 13: _t->visualFocusChanged(); break;
        case 14: _t->hoveredChanged(); break;
        case 15: _t->hoverEnabledChanged(); break;
        case 16: _t->wheelEnabledChanged(); break;
        case 17: _t->backgroundChanged(); break;
        case 18: _t->contentItemChanged(); break;
        case 19: _t->baselineOffsetChanged(); break;
        case 20: _t->paletteChanged(); break;
        case 21: _t->horizontalPaddingChanged(); break;
        case 22: _t->verticalPaddingChanged(); break;
        case 23: _t->implicitContentWidthChanged(); break;
        case 24: _t->implicitContentHeightChanged(); break;
        case 25: _t->implicitBackgroundWidthChanged(); break;
        case 26: _t->implicitBackgroundHeightChanged(); break;
        case 27: _t->topInsetChanged(); break;
        case 28: _t->leftInsetChanged(); break;
        case 29: _t->rightInsetChanged(); break;
        case 30: _t->bottomInsetChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::fontChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::availableWidthChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::availableHeightChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::paddingChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::topPaddingChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::leftPaddingChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::rightPaddingChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::bottomPaddingChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::spacingChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::localeChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::mirroredChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::focusPolicyChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::focusReasonChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::visualFocusChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::hoveredChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::hoverEnabledChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::wheelEnabledChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::backgroundChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::contentItemChanged)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::baselineOffsetChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::paletteChanged)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::horizontalPaddingChanged)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::verticalPaddingChanged)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::implicitContentWidthChanged)) {
                *result = 23;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::implicitContentHeightChanged)) {
                *result = 24;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::implicitBackgroundWidthChanged)) {
                *result = 25;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::implicitBackgroundHeightChanged)) {
                *result = 26;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::topInsetChanged)) {
                *result = 27;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::leftInsetChanged)) {
                *result = 28;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::rightInsetChanged)) {
                *result = 29;
                return;
            }
        }
        {
            using _t = void (QQuickControl::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickControl::bottomInsetChanged)) {
                *result = 30;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 18:
        case 17:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickControl *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->availableWidth(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->availableHeight(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->padding(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->spacing(); break;
        case 9: *reinterpret_cast< QLocale*>(_v) = _t->locale(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isMirrored(); break;
        case 11: *reinterpret_cast< Qt::FocusPolicy*>(_v) = _t->focusPolicy(); break;
        case 12: *reinterpret_cast< Qt::FocusReason*>(_v) = _t->focusReason(); break;
        case 13: *reinterpret_cast< bool*>(_v) = _t->hasVisualFocus(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->isHovered(); break;
        case 15: *reinterpret_cast< bool*>(_v) = _t->isHoverEnabled(); break;
        case 16: *reinterpret_cast< bool*>(_v) = _t->isWheelEnabled(); break;
        case 17: *reinterpret_cast< QQuickItem**>(_v) = _t->background(); break;
        case 18: *reinterpret_cast< QQuickItem**>(_v) = _t->contentItem(); break;
        case 19: *reinterpret_cast< qreal*>(_v) = _t->baselineOffset(); break;
        case 20: *reinterpret_cast< QPalette*>(_v) = _t->palette(); break;
        case 21: *reinterpret_cast< qreal*>(_v) = _t->horizontalPadding(); break;
        case 22: *reinterpret_cast< qreal*>(_v) = _t->verticalPadding(); break;
        case 23: *reinterpret_cast< qreal*>(_v) = _t->implicitContentWidth(); break;
        case 24: *reinterpret_cast< qreal*>(_v) = _t->implicitContentHeight(); break;
        case 25: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundWidth(); break;
        case 26: *reinterpret_cast< qreal*>(_v) = _t->implicitBackgroundHeight(); break;
        case 27: *reinterpret_cast< qreal*>(_v) = _t->topInset(); break;
        case 28: *reinterpret_cast< qreal*>(_v) = _t->leftInset(); break;
        case 29: *reinterpret_cast< qreal*>(_v) = _t->rightInset(); break;
        case 30: *reinterpret_cast< qreal*>(_v) = _t->bottomInset(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickControl *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 3: _t->setPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setSpacing(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setLocale(*reinterpret_cast< QLocale*>(_v)); break;
        case 11: _t->setFocusPolicy(*reinterpret_cast< Qt::FocusPolicy*>(_v)); break;
        case 12: _t->setFocusReason(*reinterpret_cast< Qt::FocusReason*>(_v)); break;
        case 15: _t->setHoverEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 16: _t->setWheelEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 17: _t->setBackground(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 18: _t->setContentItem(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 19: _t->setBaselineOffset(*reinterpret_cast< qreal*>(_v)); break;
        case 20: _t->setPalette(*reinterpret_cast< QPalette*>(_v)); break;
        case 21: _t->setHorizontalPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 22: _t->setVerticalPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 27: _t->setTopInset(*reinterpret_cast< qreal*>(_v)); break;
        case 28: _t->setLeftInset(*reinterpret_cast< qreal*>(_v)); break;
        case 29: _t->setRightInset(*reinterpret_cast< qreal*>(_v)); break;
        case 30: _t->setBottomInset(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickControl *_t = static_cast<QQuickControl *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetFont(); break;
        case 3: _t->resetPadding(); break;
        case 4: _t->resetTopPadding(); break;
        case 5: _t->resetLeftPadding(); break;
        case 6: _t->resetRightPadding(); break;
        case 7: _t->resetBottomPadding(); break;
        case 8: _t->resetSpacing(); break;
        case 9: _t->resetLocale(); break;
        case 15: _t->resetHoverEnabled(); break;
        case 19: _t->resetBaselineOffset(); break;
        case 20: _t->resetPalette(); break;
        case 21: _t->resetHorizontalPadding(); break;
        case 22: _t->resetVerticalPadding(); break;
        case 27: _t->resetTopInset(); break;
        case 28: _t->resetLeftInset(); break;
        case 29: _t->resetRightInset(); break;
        case 30: _t->resetBottomInset(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickControl::staticMetaObject = { {
    &QQuickItem::staticMetaObject,
    qt_meta_stringdata_QQuickControl.data,
    qt_meta_data_QQuickControl,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickControl::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickControl::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickControl.stringdata0))
        return static_cast<void*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int QQuickControl::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 31)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 31)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 31;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 31;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 31;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickControl::fontChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickControl::availableWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickControl::availableHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickControl::paddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickControl::topPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickControl::leftPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickControl::rightPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickControl::bottomPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickControl::spacingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickControl::localeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QQuickControl::mirroredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QQuickControl::focusPolicyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QQuickControl::focusReasonChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QQuickControl::visualFocusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QQuickControl::hoveredChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QQuickControl::hoverEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QQuickControl::wheelEnabledChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void QQuickControl::backgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void QQuickControl::contentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void QQuickControl::baselineOffsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void QQuickControl::paletteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void QQuickControl::horizontalPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 21, nullptr);
}

// SIGNAL 22
void QQuickControl::verticalPaddingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void QQuickControl::implicitContentWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, nullptr);
}

// SIGNAL 24
void QQuickControl::implicitContentHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, nullptr);
}

// SIGNAL 25
void QQuickControl::implicitBackgroundWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, nullptr);
}

// SIGNAL 26
void QQuickControl::implicitBackgroundHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 26, nullptr);
}

// SIGNAL 27
void QQuickControl::topInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 27, nullptr);
}

// SIGNAL 28
void QQuickControl::leftInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 28, nullptr);
}

// SIGNAL 29
void QQuickControl::rightInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 29, nullptr);
}

// SIGNAL 30
void QQuickControl::bottomInsetChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 30, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
