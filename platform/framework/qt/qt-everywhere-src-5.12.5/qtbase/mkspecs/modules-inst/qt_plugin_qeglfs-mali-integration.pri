QT_PLUGIN.qeglfs-mali-integration.TYPE = egldeviceintegrations
QT_PLUGIN.qeglfs-mali-integration.EXTENDS =
QT_PLUGIN.qeglfs-mali-integration.DEPENDS = core gui core_private gui_private eglfsdeviceintegration_private
QT_PLUGIN.qeglfs-mali-integration.CLASS_NAME = QEglFSMaliIntegrationPlugin
QT_PLUGINS += qeglfs-mali-integration
