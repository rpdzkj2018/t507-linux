/****************************************************************************
** Meta object code from reading C++ file 'qquickstackview_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickstackview_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickstackview_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickStackView_t {
    QByteArrayData data[52];
    char stringdata0[562];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickStackView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickStackView_t qt_meta_stringdata_QQuickStackView = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickStackView"
QT_MOC_LITERAL(1, 16, 11), // "busyChanged"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 12), // "depthChanged"
QT_MOC_LITERAL(4, 42, 18), // "currentItemChanged"
QT_MOC_LITERAL(5, 61, 15), // "popEnterChanged"
QT_MOC_LITERAL(6, 77, 14), // "popExitChanged"
QT_MOC_LITERAL(7, 92, 16), // "pushEnterChanged"
QT_MOC_LITERAL(8, 109, 15), // "pushExitChanged"
QT_MOC_LITERAL(9, 125, 19), // "replaceEnterChanged"
QT_MOC_LITERAL(10, 145, 18), // "replaceExitChanged"
QT_MOC_LITERAL(11, 164, 12), // "emptyChanged"
QT_MOC_LITERAL(12, 177, 5), // "clear"
QT_MOC_LITERAL(13, 183, 9), // "Operation"
QT_MOC_LITERAL(14, 193, 9), // "operation"
QT_MOC_LITERAL(15, 203, 3), // "get"
QT_MOC_LITERAL(16, 207, 11), // "QQuickItem*"
QT_MOC_LITERAL(17, 219, 5), // "index"
QT_MOC_LITERAL(18, 225, 12), // "LoadBehavior"
QT_MOC_LITERAL(19, 238, 8), // "behavior"
QT_MOC_LITERAL(20, 247, 4), // "find"
QT_MOC_LITERAL(21, 252, 8), // "QJSValue"
QT_MOC_LITERAL(22, 261, 8), // "callback"
QT_MOC_LITERAL(23, 270, 4), // "push"
QT_MOC_LITERAL(24, 275, 15), // "QQmlV4Function*"
QT_MOC_LITERAL(25, 291, 4), // "args"
QT_MOC_LITERAL(26, 296, 3), // "pop"
QT_MOC_LITERAL(27, 300, 7), // "replace"
QT_MOC_LITERAL(28, 308, 4), // "busy"
QT_MOC_LITERAL(29, 313, 5), // "depth"
QT_MOC_LITERAL(30, 319, 11), // "currentItem"
QT_MOC_LITERAL(31, 331, 11), // "initialItem"
QT_MOC_LITERAL(32, 343, 8), // "popEnter"
QT_MOC_LITERAL(33, 352, 17), // "QQuickTransition*"
QT_MOC_LITERAL(34, 370, 7), // "popExit"
QT_MOC_LITERAL(35, 378, 9), // "pushEnter"
QT_MOC_LITERAL(36, 388, 8), // "pushExit"
QT_MOC_LITERAL(37, 397, 12), // "replaceEnter"
QT_MOC_LITERAL(38, 410, 11), // "replaceExit"
QT_MOC_LITERAL(39, 422, 5), // "empty"
QT_MOC_LITERAL(40, 428, 6), // "Status"
QT_MOC_LITERAL(41, 435, 8), // "Inactive"
QT_MOC_LITERAL(42, 444, 12), // "Deactivating"
QT_MOC_LITERAL(43, 457, 10), // "Activating"
QT_MOC_LITERAL(44, 468, 6), // "Active"
QT_MOC_LITERAL(45, 475, 8), // "DontLoad"
QT_MOC_LITERAL(46, 484, 9), // "ForceLoad"
QT_MOC_LITERAL(47, 494, 10), // "Transition"
QT_MOC_LITERAL(48, 505, 9), // "Immediate"
QT_MOC_LITERAL(49, 515, 14), // "PushTransition"
QT_MOC_LITERAL(50, 530, 17), // "ReplaceTransition"
QT_MOC_LITERAL(51, 548, 13) // "PopTransition"

    },
    "QQuickStackView\0busyChanged\0\0depthChanged\0"
    "currentItemChanged\0popEnterChanged\0"
    "popExitChanged\0pushEnterChanged\0"
    "pushExitChanged\0replaceEnterChanged\0"
    "replaceExitChanged\0emptyChanged\0clear\0"
    "Operation\0operation\0get\0QQuickItem*\0"
    "index\0LoadBehavior\0behavior\0find\0"
    "QJSValue\0callback\0push\0QQmlV4Function*\0"
    "args\0pop\0replace\0busy\0depth\0currentItem\0"
    "initialItem\0popEnter\0QQuickTransition*\0"
    "popExit\0pushEnter\0pushExit\0replaceEnter\0"
    "replaceExit\0empty\0Status\0Inactive\0"
    "Deactivating\0Activating\0Active\0DontLoad\0"
    "ForceLoad\0Transition\0Immediate\0"
    "PushTransition\0ReplaceTransition\0"
    "PopTransition"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickStackView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
      11,  167, // properties
       3,  222, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  128,    2, 0x06 /* Public */,
       3,    0,  129,    2, 0x06 /* Public */,
       4,    0,  130,    2, 0x06 /* Public */,
       5,    0,  131,    2, 0x06 /* Public */,
       6,    0,  132,    2, 0x06 /* Public */,
       7,    0,  133,    2, 0x06 /* Public */,
       8,    0,  134,    2, 0x06 /* Public */,
       9,    0,  135,    2, 0x06 /* Public */,
      10,    0,  136,    2, 0x06 /* Public */,
      11,    0,  137,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      12,    1,  138,    2, 0x0a /* Public */,
      12,    0,  141,    2, 0x2a /* Public | MethodCloned */,

 // methods: name, argc, parameters, tag, flags
      15,    2,  142,    2, 0x02 /* Public */,
      15,    1,  147,    2, 0x22 /* Public | MethodCloned */,
      20,    2,  150,    2, 0x02 /* Public */,
      20,    1,  155,    2, 0x22 /* Public | MethodCloned */,
      23,    1,  158,    2, 0x02 /* Public */,
      26,    1,  161,    2, 0x02 /* Public */,
      27,    1,  164,    2, 0x02 /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,

 // slots: revision
       0,
       0,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 16, QMetaType::Int, 0x80000000 | 18,   17,   19,
    0x80000000 | 16, QMetaType::Int,   17,
    0x80000000 | 16, 0x80000000 | 21, 0x80000000 | 18,   22,   19,
    0x80000000 | 16, 0x80000000 | 21,   22,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,

 // properties: name, type, flags
      28, QMetaType::Bool, 0x00495801,
      29, QMetaType::Int, 0x00495801,
      30, 0x80000000 | 16, 0x00495809,
      31, 0x80000000 | 21, 0x0009590b,
      32, 0x80000000 | 33, 0x0049590b,
      34, 0x80000000 | 33, 0x0049590b,
      35, 0x80000000 | 33, 0x0049590b,
      36, 0x80000000 | 33, 0x0049590b,
      37, 0x80000000 | 33, 0x0049590b,
      38, 0x80000000 | 33, 0x0049590b,
      39, QMetaType::Bool, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       0,
       3,
       4,
       5,
       6,
       7,
       8,
       9,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       3,

 // enums: name, alias, flags, count, data
      40,   40, 0x0,    4,  237,
      18,   18, 0x0,    2,  245,
      13,   13, 0x0,    5,  249,

 // enum data: key, value
      41, uint(QQuickStackView::Inactive),
      42, uint(QQuickStackView::Deactivating),
      43, uint(QQuickStackView::Activating),
      44, uint(QQuickStackView::Active),
      45, uint(QQuickStackView::DontLoad),
      46, uint(QQuickStackView::ForceLoad),
      47, uint(QQuickStackView::Transition),
      48, uint(QQuickStackView::Immediate),
      49, uint(QQuickStackView::PushTransition),
      50, uint(QQuickStackView::ReplaceTransition),
      51, uint(QQuickStackView::PopTransition),

       0        // eod
};

void QQuickStackView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickStackView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->busyChanged(); break;
        case 1: _t->depthChanged(); break;
        case 2: _t->currentItemChanged(); break;
        case 3: _t->popEnterChanged(); break;
        case 4: _t->popExitChanged(); break;
        case 5: _t->pushEnterChanged(); break;
        case 6: _t->pushExitChanged(); break;
        case 7: _t->replaceEnterChanged(); break;
        case 8: _t->replaceExitChanged(); break;
        case 9: _t->emptyChanged(); break;
        case 10: _t->clear((*reinterpret_cast< Operation(*)>(_a[1]))); break;
        case 11: _t->clear(); break;
        case 12: { QQuickItem* _r = _t->get((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< LoadBehavior(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 13: { QQuickItem* _r = _t->get((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 14: { QQuickItem* _r = _t->find((*reinterpret_cast< const QJSValue(*)>(_a[1])),(*reinterpret_cast< LoadBehavior(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 15: { QQuickItem* _r = _t->find((*reinterpret_cast< const QJSValue(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QQuickItem**>(_a[0]) = std::move(_r); }  break;
        case 16: _t->push((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 17: _t->pop((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        case 18: _t->replace((*reinterpret_cast< QQmlV4Function*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::busyChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::depthChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::currentItemChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::popEnterChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::popExitChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::pushEnterChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::pushExitChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::replaceEnterChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::replaceExitChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QQuickStackView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackView::emptyChanged)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 2:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickStackView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isBusy(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->depth(); break;
        case 2: *reinterpret_cast< QQuickItem**>(_v) = _t->currentItem(); break;
        case 3: *reinterpret_cast< QJSValue*>(_v) = _t->initialItem(); break;
        case 4: *reinterpret_cast< QQuickTransition**>(_v) = _t->popEnter(); break;
        case 5: *reinterpret_cast< QQuickTransition**>(_v) = _t->popExit(); break;
        case 6: *reinterpret_cast< QQuickTransition**>(_v) = _t->pushEnter(); break;
        case 7: *reinterpret_cast< QQuickTransition**>(_v) = _t->pushExit(); break;
        case 8: *reinterpret_cast< QQuickTransition**>(_v) = _t->replaceEnter(); break;
        case 9: *reinterpret_cast< QQuickTransition**>(_v) = _t->replaceExit(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isEmpty(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickStackView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 3: _t->setInitialItem(*reinterpret_cast< QJSValue*>(_v)); break;
        case 4: _t->setPopEnter(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 5: _t->setPopExit(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 6: _t->setPushEnter(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 7: _t->setPushExit(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 8: _t->setReplaceEnter(*reinterpret_cast< QQuickTransition**>(_v)); break;
        case 9: _t->setReplaceExit(*reinterpret_cast< QQuickTransition**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickStackView::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickStackView.data,
    qt_meta_data_QQuickStackView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickStackView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickStackView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickStackView.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickStackView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickStackView::busyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickStackView::depthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickStackView::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickStackView::popEnterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickStackView::popExitChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickStackView::pushEnterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickStackView::pushExitChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickStackView::replaceEnterChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickStackView::replaceExitChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QQuickStackView::emptyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
struct qt_meta_stringdata_QQuickStackViewAttached_t {
    QByteArrayData data[17];
    char stringdata0[200];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickStackViewAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickStackViewAttached_t qt_meta_stringdata_QQuickStackViewAttached = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QQuickStackViewAttached"
QT_MOC_LITERAL(1, 24, 12), // "indexChanged"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 11), // "viewChanged"
QT_MOC_LITERAL(4, 50, 13), // "statusChanged"
QT_MOC_LITERAL(5, 64, 9), // "activated"
QT_MOC_LITERAL(6, 74, 10), // "activating"
QT_MOC_LITERAL(7, 85, 11), // "deactivated"
QT_MOC_LITERAL(8, 97, 12), // "deactivating"
QT_MOC_LITERAL(9, 110, 7), // "removed"
QT_MOC_LITERAL(10, 118, 14), // "visibleChanged"
QT_MOC_LITERAL(11, 133, 5), // "index"
QT_MOC_LITERAL(12, 139, 4), // "view"
QT_MOC_LITERAL(13, 144, 16), // "QQuickStackView*"
QT_MOC_LITERAL(14, 161, 6), // "status"
QT_MOC_LITERAL(15, 168, 23), // "QQuickStackView::Status"
QT_MOC_LITERAL(16, 192, 7) // "visible"

    },
    "QQuickStackViewAttached\0indexChanged\0"
    "\0viewChanged\0statusChanged\0activated\0"
    "activating\0deactivated\0deactivating\0"
    "removed\0visibleChanged\0index\0view\0"
    "QQuickStackView*\0status\0QQuickStackView::Status\0"
    "visible"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickStackViewAttached[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       4,   68, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    0,   60,    2, 0x06 /* Public */,
       4,    0,   61,    2, 0x06 /* Public */,
       5,    0,   62,    2, 0x06 /* Public */,
       6,    0,   63,    2, 0x06 /* Public */,
       7,    0,   64,    2, 0x06 /* Public */,
       8,    0,   65,    2, 0x06 /* Public */,
       9,    0,   66,    2, 0x06 /* Public */,
      10,    0,   67,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      11, QMetaType::Int, 0x00495801,
      12, 0x80000000 | 13, 0x00495809,
      14, 0x80000000 | 15, 0x00495809,
      16, QMetaType::Bool, 0x00495907,

 // properties: notify_signal_id
       0,
       1,
       2,
       8,

       0        // eod
};

void QQuickStackViewAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickStackViewAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->indexChanged(); break;
        case 1: _t->viewChanged(); break;
        case 2: _t->statusChanged(); break;
        case 3: _t->activated(); break;
        case 4: _t->activating(); break;
        case 5: _t->deactivated(); break;
        case 6: _t->deactivating(); break;
        case 7: _t->removed(); break;
        case 8: _t->visibleChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::indexChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::viewChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::statusChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::activated)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::activating)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::deactivated)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::deactivating)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::removed)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QQuickStackViewAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickStackViewAttached::visibleChanged)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickStackView* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickStackViewAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->index(); break;
        case 1: *reinterpret_cast< QQuickStackView**>(_v) = _t->view(); break;
        case 2: *reinterpret_cast< QQuickStackView::Status*>(_v) = _t->status(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isVisible(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickStackViewAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 3: _t->setVisible(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickStackViewAttached *_t = static_cast<QQuickStackViewAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 3: _t->resetVisible(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QQuickStackViewAttached[] = {
        &QQuickStackView::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QQuickStackViewAttached::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickStackViewAttached.data,
    qt_meta_data_QQuickStackViewAttached,
    qt_static_metacall,
    qt_meta_extradata_QQuickStackViewAttached,
    nullptr
} };


const QMetaObject *QQuickStackViewAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickStackViewAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickStackViewAttached.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickStackViewAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickStackViewAttached::indexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickStackViewAttached::viewChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickStackViewAttached::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickStackViewAttached::activated()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickStackViewAttached::activating()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickStackViewAttached::deactivated()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickStackViewAttached::deactivating()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickStackViewAttached::removed()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QQuickStackViewAttached::visibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
