/****************************************************************************
** Meta object code from reading C++ file 'qmapcircleobject_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../labs/qmapcircleobject_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmapcircleobject_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QMapCircleObject_t {
    QByteArrayData data[11];
    char stringdata0[132];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QMapCircleObject_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QMapCircleObject_t qt_meta_stringdata_QMapCircleObject = {
    {
QT_MOC_LITERAL(0, 0, 16), // "QMapCircleObject"
QT_MOC_LITERAL(1, 17, 13), // "centerChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 13), // "radiusChanged"
QT_MOC_LITERAL(4, 46, 12), // "colorChanged"
QT_MOC_LITERAL(5, 59, 6), // "center"
QT_MOC_LITERAL(6, 66, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(7, 81, 6), // "radius"
QT_MOC_LITERAL(8, 88, 5), // "color"
QT_MOC_LITERAL(9, 94, 6), // "border"
QT_MOC_LITERAL(10, 101, 30) // "QDeclarativeMapLineProperties*"

    },
    "QMapCircleObject\0centerChanged\0\0"
    "radiusChanged\0colorChanged\0center\0"
    "QGeoCoordinate\0radius\0color\0border\0"
    "QDeclarativeMapLineProperties*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QMapCircleObject[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       4,   32, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   29,    2, 0x06 /* Public */,
       3,    0,   30,    2, 0x06 /* Public */,
       4,    0,   31,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, 0x80000000 | 6, 0x0049510b,
       7, QMetaType::QReal, 0x00495103,
       8, QMetaType::QColor, 0x00495103,
       9, 0x80000000 | 10, 0x00095409,

 // properties: notify_signal_id
       0,
       1,
       2,
       0,

       0        // eod
};

void QMapCircleObject::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QMapCircleObject *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->centerChanged(); break;
        case 1: _t->radiusChanged(); break;
        case 2: _t->colorChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QMapCircleObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMapCircleObject::centerChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QMapCircleObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMapCircleObject::radiusChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QMapCircleObject::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMapCircleObject::colorChanged)) {
                *result = 2;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeMapLineProperties* >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QMapCircleObject *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->center(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->radius(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 3: *reinterpret_cast< QDeclarativeMapLineProperties**>(_v) = _t->border(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QMapCircleObject *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setCenter(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 1: _t->setRadius(*reinterpret_cast< qreal*>(_v)); break;
        case 2: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QMapCircleObject::staticMetaObject = { {
    &QGeoMapObject::staticMetaObject,
    qt_meta_stringdata_QMapCircleObject.data,
    qt_meta_data_QMapCircleObject,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QMapCircleObject::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QMapCircleObject::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QMapCircleObject.stringdata0))
        return static_cast<void*>(this);
    return QGeoMapObject::qt_metacast(_clname);
}

int QMapCircleObject::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGeoMapObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QMapCircleObject::centerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QMapCircleObject::radiusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QMapCircleObject::colorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
