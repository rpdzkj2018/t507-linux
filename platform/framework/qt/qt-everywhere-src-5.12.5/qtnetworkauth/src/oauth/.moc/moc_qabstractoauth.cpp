/****************************************************************************
** Meta object code from reading C++ file 'qabstractoauth.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qabstractoauth.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qabstractoauth.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QAbstractOAuth_t {
    QByteArrayData data[51];
    char stringdata0[703];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAbstractOAuth_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAbstractOAuth_t qt_meta_stringdata_QAbstractOAuth = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QAbstractOAuth"
QT_MOC_LITERAL(1, 15, 23), // "clientIdentifierChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 16), // "clientIdentifier"
QT_MOC_LITERAL(4, 57, 12), // "tokenChanged"
QT_MOC_LITERAL(5, 70, 5), // "token"
QT_MOC_LITERAL(6, 76, 13), // "statusChanged"
QT_MOC_LITERAL(7, 90, 6), // "Status"
QT_MOC_LITERAL(8, 97, 6), // "status"
QT_MOC_LITERAL(9, 104, 23), // "authorizationUrlChanged"
QT_MOC_LITERAL(10, 128, 3), // "url"
QT_MOC_LITERAL(11, 132, 18), // "extraTokensChanged"
QT_MOC_LITERAL(12, 151, 6), // "tokens"
QT_MOC_LITERAL(13, 158, 18), // "contentTypeChanged"
QT_MOC_LITERAL(14, 177, 11), // "ContentType"
QT_MOC_LITERAL(15, 189, 11), // "contentType"
QT_MOC_LITERAL(16, 201, 13), // "requestFailed"
QT_MOC_LITERAL(17, 215, 5), // "Error"
QT_MOC_LITERAL(18, 221, 5), // "error"
QT_MOC_LITERAL(19, 227, 20), // "authorizeWithBrowser"
QT_MOC_LITERAL(20, 248, 7), // "granted"
QT_MOC_LITERAL(21, 256, 8), // "finished"
QT_MOC_LITERAL(22, 265, 14), // "QNetworkReply*"
QT_MOC_LITERAL(23, 280, 5), // "reply"
QT_MOC_LITERAL(24, 286, 17), // "replyDataReceived"
QT_MOC_LITERAL(25, 304, 4), // "data"
QT_MOC_LITERAL(26, 309, 5), // "grant"
QT_MOC_LITERAL(27, 315, 4), // "head"
QT_MOC_LITERAL(28, 320, 10), // "parameters"
QT_MOC_LITERAL(29, 331, 3), // "get"
QT_MOC_LITERAL(30, 335, 4), // "post"
QT_MOC_LITERAL(31, 340, 3), // "put"
QT_MOC_LITERAL(32, 344, 14), // "deleteResource"
QT_MOC_LITERAL(33, 359, 11), // "extraTokens"
QT_MOC_LITERAL(34, 371, 16), // "authorizationUrl"
QT_MOC_LITERAL(35, 388, 27), // "QAbstractOAuth::ContentType"
QT_MOC_LITERAL(36, 416, 16), // "NotAuthenticated"
QT_MOC_LITERAL(37, 433, 28), // "TemporaryCredentialsReceived"
QT_MOC_LITERAL(38, 462, 7), // "Granted"
QT_MOC_LITERAL(39, 470, 15), // "RefreshingToken"
QT_MOC_LITERAL(40, 486, 5), // "Stage"
QT_MOC_LITERAL(41, 492, 30), // "RequestingTemporaryCredentials"
QT_MOC_LITERAL(42, 523, 23), // "RequestingAuthorization"
QT_MOC_LITERAL(43, 547, 21), // "RequestingAccessToken"
QT_MOC_LITERAL(44, 569, 21), // "RefreshingAccessToken"
QT_MOC_LITERAL(45, 591, 7), // "NoError"
QT_MOC_LITERAL(46, 599, 12), // "NetworkError"
QT_MOC_LITERAL(47, 612, 11), // "ServerError"
QT_MOC_LITERAL(48, 624, 23), // "OAuthTokenNotFoundError"
QT_MOC_LITERAL(49, 648, 29), // "OAuthTokenSecretNotFoundError"
QT_MOC_LITERAL(50, 678, 24) // "OAuthCallbackNotVerified"

    },
    "QAbstractOAuth\0clientIdentifierChanged\0"
    "\0clientIdentifier\0tokenChanged\0token\0"
    "statusChanged\0Status\0status\0"
    "authorizationUrlChanged\0url\0"
    "extraTokensChanged\0tokens\0contentTypeChanged\0"
    "ContentType\0contentType\0requestFailed\0"
    "Error\0error\0authorizeWithBrowser\0"
    "granted\0finished\0QNetworkReply*\0reply\0"
    "replyDataReceived\0data\0grant\0head\0"
    "parameters\0get\0post\0put\0deleteResource\0"
    "extraTokens\0authorizationUrl\0"
    "QAbstractOAuth::ContentType\0"
    "NotAuthenticated\0TemporaryCredentialsReceived\0"
    "Granted\0RefreshingToken\0Stage\0"
    "RequestingTemporaryCredentials\0"
    "RequestingAuthorization\0RequestingAccessToken\0"
    "RefreshingAccessToken\0NoError\0"
    "NetworkError\0ServerError\0"
    "OAuthTokenNotFoundError\0"
    "OAuthTokenSecretNotFoundError\0"
    "OAuthCallbackNotVerified"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAbstractOAuth[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       6,  196, // properties
       3,  220, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  124,    2, 0x06 /* Public */,
       4,    1,  127,    2, 0x06 /* Public */,
       6,    1,  130,    2, 0x06 /* Public */,
       9,    1,  133,    2, 0x06 /* Public */,
      11,    1,  136,    2, 0x06 /* Public */,
      13,    1,  139,    2, 0x06 /* Public */,
      16,    1,  142,    2, 0x06 /* Public */,
      19,    1,  145,    2, 0x06 /* Public */,
      20,    0,  148,    2, 0x06 /* Public */,
      21,    1,  149,    2, 0x06 /* Public */,
      24,    1,  152,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      26,    0,  155,    2, 0x0a /* Public */,

 // methods: name, argc, parameters, tag, flags
      27,    2,  156,    2, 0x02 /* Public */,
      27,    1,  161,    2, 0x22 /* Public | MethodCloned */,
      29,    2,  164,    2, 0x02 /* Public */,
      29,    1,  169,    2, 0x22 /* Public | MethodCloned */,
      30,    2,  172,    2, 0x02 /* Public */,
      30,    1,  177,    2, 0x22 /* Public | MethodCloned */,
      31,    2,  180,    2, 0x02 /* Public */,
      31,    1,  185,    2, 0x22 /* Public | MethodCloned */,
      32,    2,  188,    2, 0x02 /* Public */,
      32,    1,  193,    2, 0x22 /* Public | MethodCloned */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void, QMetaType::QVariantMap,   12,
    QMetaType::Void, 0x80000000 | 14,   15,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 22,   23,
    QMetaType::Void, QMetaType::QByteArray,   25,

 // slots: parameters
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 22, QMetaType::QUrl, QMetaType::QVariantMap,   10,   28,
    0x80000000 | 22, QMetaType::QUrl,   10,
    0x80000000 | 22, QMetaType::QUrl, QMetaType::QVariantMap,   10,   28,
    0x80000000 | 22, QMetaType::QUrl,   10,
    0x80000000 | 22, QMetaType::QUrl, QMetaType::QVariantMap,   10,   28,
    0x80000000 | 22, QMetaType::QUrl,   10,
    0x80000000 | 22, QMetaType::QUrl, QMetaType::QVariantMap,   10,   28,
    0x80000000 | 22, QMetaType::QUrl,   10,
    0x80000000 | 22, QMetaType::QUrl, QMetaType::QVariantMap,   10,   28,
    0x80000000 | 22, QMetaType::QUrl,   10,

 // properties: name, type, flags
       3, QMetaType::QString, 0x00495103,
       5, QMetaType::QString, 0x00495103,
       8, 0x80000000 | 7, 0x00495009,
      33, QMetaType::QVariantMap, 0x00495001,
      34, QMetaType::QUrl, 0x00495103,
      15, 0x80000000 | 35, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       4,
       3,
       5,

 // enums: name, alias, flags, count, data
       7,    7, 0x2,    4,  235,
      40,   40, 0x2,    4,  243,
      17,   17, 0x2,    6,  251,

 // enum data: key, value
      36, uint(QAbstractOAuth::Status::NotAuthenticated),
      37, uint(QAbstractOAuth::Status::TemporaryCredentialsReceived),
      38, uint(QAbstractOAuth::Status::Granted),
      39, uint(QAbstractOAuth::Status::RefreshingToken),
      41, uint(QAbstractOAuth::Stage::RequestingTemporaryCredentials),
      42, uint(QAbstractOAuth::Stage::RequestingAuthorization),
      43, uint(QAbstractOAuth::Stage::RequestingAccessToken),
      44, uint(QAbstractOAuth::Stage::RefreshingAccessToken),
      45, uint(QAbstractOAuth::Error::NoError),
      46, uint(QAbstractOAuth::Error::NetworkError),
      47, uint(QAbstractOAuth::Error::ServerError),
      48, uint(QAbstractOAuth::Error::OAuthTokenNotFoundError),
      49, uint(QAbstractOAuth::Error::OAuthTokenSecretNotFoundError),
      50, uint(QAbstractOAuth::Error::OAuthCallbackNotVerified),

       0        // eod
};

void QAbstractOAuth::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAbstractOAuth *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->clientIdentifierChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->tokenChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->statusChanged((*reinterpret_cast< Status(*)>(_a[1]))); break;
        case 3: _t->authorizationUrlChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 4: _t->extraTokensChanged((*reinterpret_cast< const QVariantMap(*)>(_a[1]))); break;
        case 5: _t->contentTypeChanged((*reinterpret_cast< ContentType(*)>(_a[1]))); break;
        case 6: _t->requestFailed((*reinterpret_cast< const Error(*)>(_a[1]))); break;
        case 7: _t->authorizeWithBrowser((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 8: _t->granted(); break;
        case 9: _t->finished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 10: _t->replyDataReceived((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 11: _t->grant(); break;
        case 12: { QNetworkReply* _r = _t->head((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 13: { QNetworkReply* _r = _t->head((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 14: { QNetworkReply* _r = _t->get((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 15: { QNetworkReply* _r = _t->get((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 16: { QNetworkReply* _r = _t->post((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 17: { QNetworkReply* _r = _t->post((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 18: { QNetworkReply* _r = _t->put((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 19: { QNetworkReply* _r = _t->put((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 20: { QNetworkReply* _r = _t->deleteResource((*reinterpret_cast< const QUrl(*)>(_a[1])),(*reinterpret_cast< const QVariantMap(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        case 21: { QNetworkReply* _r = _t->deleteResource((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QNetworkReply**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAbstractOAuth::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::clientIdentifierChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::tokenChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(Status );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::statusChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::authorizationUrlChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const QVariantMap & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::extraTokensChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(ContentType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::contentTypeChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const Error );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::requestFailed)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::authorizeWithBrowser)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::granted)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(QNetworkReply * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::finished)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QAbstractOAuth::*)(const QByteArray & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractOAuth::replyDataReceived)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QAbstractOAuth *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->clientIdentifier(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->token(); break;
        case 2: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 3: *reinterpret_cast< QVariantMap*>(_v) = _t->extraTokens(); break;
        case 4: *reinterpret_cast< QUrl*>(_v) = _t->authorizationUrl(); break;
        case 5: *reinterpret_cast< QAbstractOAuth::ContentType*>(_v) = _t->contentType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QAbstractOAuth *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setClientIdentifier(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setToken(*reinterpret_cast< QString*>(_v)); break;
        case 4: _t->setAuthorizationUrl(*reinterpret_cast< QUrl*>(_v)); break;
        case 5: _t->setContentType(*reinterpret_cast< QAbstractOAuth::ContentType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QAbstractOAuth::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QAbstractOAuth.data,
    qt_meta_data_QAbstractOAuth,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QAbstractOAuth::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAbstractOAuth::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QAbstractOAuth.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QAbstractOAuth::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 22;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QAbstractOAuth::clientIdentifierChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QAbstractOAuth::tokenChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QAbstractOAuth::statusChanged(Status _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QAbstractOAuth::authorizationUrlChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QAbstractOAuth::extraTokensChanged(const QVariantMap & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QAbstractOAuth::contentTypeChanged(ContentType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QAbstractOAuth::requestFailed(const Error _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QAbstractOAuth::authorizeWithBrowser(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QAbstractOAuth::granted()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QAbstractOAuth::finished(QNetworkReply * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QAbstractOAuth::replyDataReceived(const QByteArray & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
