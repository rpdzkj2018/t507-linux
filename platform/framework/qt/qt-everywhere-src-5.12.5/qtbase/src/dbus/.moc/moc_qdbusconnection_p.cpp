/****************************************************************************
** Meta object code from reading C++ file 'qdbusconnection_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qdbusconnection_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdbusconnection_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDBusConnectionPrivate_t {
    QByteArrayData data[46];
    char stringdata0[656];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDBusConnectionPrivate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDBusConnectionPrivate_t qt_meta_stringdata_QDBusConnectionPrivate = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QDBusConnectionPrivate"
QT_MOC_LITERAL(1, 23, 21), // "dispatchStatusChanged"
QT_MOC_LITERAL(2, 45, 0), // ""
QT_MOC_LITERAL(3, 46, 16), // "spyHooksFinished"
QT_MOC_LITERAL(4, 63, 12), // "QDBusMessage"
QT_MOC_LITERAL(5, 76, 3), // "msg"
QT_MOC_LITERAL(6, 80, 19), // "messageNeedsSending"
QT_MOC_LITERAL(7, 100, 24), // "QDBusPendingCallPrivate*"
QT_MOC_LITERAL(8, 125, 5), // "pcall"
QT_MOC_LITERAL(9, 131, 7), // "timeout"
QT_MOC_LITERAL(10, 139, 21), // "signalNeedsConnecting"
QT_MOC_LITERAL(11, 161, 3), // "key"
QT_MOC_LITERAL(12, 165, 34), // "QDBusConnectionPrivate::Signa..."
QT_MOC_LITERAL(13, 200, 4), // "hook"
QT_MOC_LITERAL(14, 205, 24), // "signalNeedsDisconnecting"
QT_MOC_LITERAL(15, 230, 19), // "serviceOwnerChanged"
QT_MOC_LITERAL(16, 250, 4), // "name"
QT_MOC_LITERAL(17, 255, 8), // "oldOwner"
QT_MOC_LITERAL(18, 264, 8), // "newOwner"
QT_MOC_LITERAL(19, 273, 22), // "callWithCallbackFailed"
QT_MOC_LITERAL(20, 296, 10), // "QDBusError"
QT_MOC_LITERAL(21, 307, 5), // "error"
QT_MOC_LITERAL(22, 313, 7), // "message"
QT_MOC_LITERAL(23, 321, 19), // "newServerConnection"
QT_MOC_LITERAL(24, 341, 23), // "QDBusConnectionPrivate*"
QT_MOC_LITERAL(25, 365, 13), // "newConnection"
QT_MOC_LITERAL(26, 379, 18), // "setDispatchEnabled"
QT_MOC_LITERAL(27, 398, 6), // "enable"
QT_MOC_LITERAL(28, 405, 10), // "doDispatch"
QT_MOC_LITERAL(29, 416, 10), // "socketRead"
QT_MOC_LITERAL(30, 427, 11), // "socketWrite"
QT_MOC_LITERAL(31, 439, 15), // "objectDestroyed"
QT_MOC_LITERAL(32, 455, 1), // "o"
QT_MOC_LITERAL(33, 457, 11), // "relaySignal"
QT_MOC_LITERAL(34, 469, 3), // "obj"
QT_MOC_LITERAL(35, 473, 18), // "const QMetaObject*"
QT_MOC_LITERAL(36, 492, 8), // "signalId"
QT_MOC_LITERAL(37, 501, 4), // "args"
QT_MOC_LITERAL(38, 506, 13), // "addSignalHook"
QT_MOC_LITERAL(39, 520, 10), // "SignalHook"
QT_MOC_LITERAL(40, 531, 16), // "removeSignalHook"
QT_MOC_LITERAL(41, 548, 25), // "serviceOwnerChangedNoLock"
QT_MOC_LITERAL(42, 574, 21), // "registerServiceNoLock"
QT_MOC_LITERAL(43, 596, 11), // "serviceName"
QT_MOC_LITERAL(44, 608, 23), // "unregisterServiceNoLock"
QT_MOC_LITERAL(45, 632, 23) // "handleDBusDisconnection"

    },
    "QDBusConnectionPrivate\0dispatchStatusChanged\0"
    "\0spyHooksFinished\0QDBusMessage\0msg\0"
    "messageNeedsSending\0QDBusPendingCallPrivate*\0"
    "pcall\0timeout\0signalNeedsConnecting\0"
    "key\0QDBusConnectionPrivate::SignalHook\0"
    "hook\0signalNeedsDisconnecting\0"
    "serviceOwnerChanged\0name\0oldOwner\0"
    "newOwner\0callWithCallbackFailed\0"
    "QDBusError\0error\0message\0newServerConnection\0"
    "QDBusConnectionPrivate*\0newConnection\0"
    "setDispatchEnabled\0enable\0doDispatch\0"
    "socketRead\0socketWrite\0objectDestroyed\0"
    "o\0relaySignal\0obj\0const QMetaObject*\0"
    "signalId\0args\0addSignalHook\0SignalHook\0"
    "removeSignalHook\0serviceOwnerChangedNoLock\0"
    "registerServiceNoLock\0serviceName\0"
    "unregisterServiceNoLock\0handleDBusDisconnection"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDBusConnectionPrivate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  119,    2, 0x06 /* Public */,
       3,    1,  120,    2, 0x06 /* Public */,
       6,    3,  123,    2, 0x06 /* Public */,
       6,    2,  130,    2, 0x26 /* Public | MethodCloned */,
      10,    2,  135,    2, 0x06 /* Public */,
      14,    2,  140,    2, 0x06 /* Public */,
      15,    3,  145,    2, 0x06 /* Public */,
      19,    2,  152,    2, 0x06 /* Public */,
      23,    1,  157,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      26,    1,  160,    2, 0x0a /* Public */,
      28,    0,  163,    2, 0x0a /* Public */,
      29,    1,  164,    2, 0x0a /* Public */,
      30,    1,  167,    2, 0x0a /* Public */,
      31,    1,  170,    2, 0x0a /* Public */,
      33,    4,  173,    2, 0x0a /* Public */,
      38,    2,  182,    2, 0x0a /* Public */,
      40,    2,  187,    2, 0x0a /* Public */,
      41,    3,  192,    2, 0x08 /* Private */,
      42,    1,  199,    2, 0x08 /* Private */,
      44,    1,  202,    2, 0x08 /* Private */,
      45,    0,  205,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 7, QMetaType::VoidStar, QMetaType::Int,    8,    5,    9,
    QMetaType::Void, 0x80000000 | 7, QMetaType::VoidStar,    8,    5,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 12,   11,   13,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 12,   11,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,   16,   17,   18,
    QMetaType::Void, 0x80000000 | 20, 0x80000000 | 4,   21,   22,
    QMetaType::Void, 0x80000000 | 24,   25,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   27,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::QObjectStar,   32,
    QMetaType::Void, QMetaType::QObjectStar, 0x80000000 | 35, QMetaType::Int, QMetaType::QVariantList,   34,    2,   36,   37,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 39,   11,   13,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 39,   11,   13,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::QString,   16,   17,   18,
    QMetaType::Void, QMetaType::QString,   43,
    QMetaType::Void, QMetaType::QString,   43,
    QMetaType::Void,

       0        // eod
};

void QDBusConnectionPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDBusConnectionPrivate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dispatchStatusChanged(); break;
        case 1: _t->spyHooksFinished((*reinterpret_cast< const QDBusMessage(*)>(_a[1]))); break;
        case 2: _t->messageNeedsSending((*reinterpret_cast< QDBusPendingCallPrivate*(*)>(_a[1])),(*reinterpret_cast< void*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->messageNeedsSending((*reinterpret_cast< QDBusPendingCallPrivate*(*)>(_a[1])),(*reinterpret_cast< void*(*)>(_a[2]))); break;
        case 4: { bool _r = _t->signalNeedsConnecting((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QDBusConnectionPrivate::SignalHook(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->signalNeedsDisconnecting((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QDBusConnectionPrivate::SignalHook(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: _t->serviceOwnerChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 7: _t->callWithCallbackFailed((*reinterpret_cast< const QDBusError(*)>(_a[1])),(*reinterpret_cast< const QDBusMessage(*)>(_a[2]))); break;
        case 8: _t->newServerConnection((*reinterpret_cast< QDBusConnectionPrivate*(*)>(_a[1]))); break;
        case 9: _t->setDispatchEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->doDispatch(); break;
        case 11: _t->socketRead((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->socketWrite((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->objectDestroyed((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 14: _t->relaySignal((*reinterpret_cast< QObject*(*)>(_a[1])),(*reinterpret_cast< const QMetaObject*(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QVariantList(*)>(_a[4]))); break;
        case 15: { bool _r = _t->addSignalHook((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const SignalHook(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 16: { bool _r = _t->removeSignalHook((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const SignalHook(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 17: _t->serviceOwnerChangedNoLock((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 18: _t->registerServiceNoLock((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 19: _t->unregisterServiceNoLock((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 20: _t->handleDBusDisconnection(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusMessage >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusError >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusMessage >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusConnectionPrivate* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDBusConnectionPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::dispatchStatusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDBusConnectionPrivate::*)(const QDBusMessage & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::spyHooksFinished)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDBusConnectionPrivate::*)(QDBusPendingCallPrivate * , void * , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::messageNeedsSending)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = bool (QDBusConnectionPrivate::*)(const QString & , const QDBusConnectionPrivate::SignalHook & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::signalNeedsConnecting)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = bool (QDBusConnectionPrivate::*)(const QString & , const QDBusConnectionPrivate::SignalHook & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::signalNeedsDisconnecting)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDBusConnectionPrivate::*)(const QString & , const QString & , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::serviceOwnerChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDBusConnectionPrivate::*)(const QDBusError & , const QDBusMessage & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::callWithCallbackFailed)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDBusConnectionPrivate::*)(QDBusConnectionPrivate * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDBusConnectionPrivate::newServerConnection)) {
                *result = 8;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QDBusConnectionPrivate::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDBusConnectionPrivate.data,
    qt_meta_data_QDBusConnectionPrivate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDBusConnectionPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDBusConnectionPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDBusConnectionPrivate.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDBusConnectionPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}

// SIGNAL 0
void QDBusConnectionPrivate::dispatchStatusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDBusConnectionPrivate::spyHooksFinished(const QDBusMessage & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDBusConnectionPrivate::messageNeedsSending(QDBusPendingCallPrivate * _t1, void * _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 4
bool QDBusConnectionPrivate::signalNeedsConnecting(const QString & _t1, const QDBusConnectionPrivate::SignalHook & _t2)
{
    bool _t0{};
    void *_a[] = { const_cast<void*>(reinterpret_cast<const void*>(&_t0)), const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
    return _t0;
}

// SIGNAL 5
bool QDBusConnectionPrivate::signalNeedsDisconnecting(const QString & _t1, const QDBusConnectionPrivate::SignalHook & _t2)
{
    bool _t0{};
    void *_a[] = { const_cast<void*>(reinterpret_cast<const void*>(&_t0)), const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
    return _t0;
}

// SIGNAL 6
void QDBusConnectionPrivate::serviceOwnerChanged(const QString & _t1, const QString & _t2, const QString & _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QDBusConnectionPrivate::callWithCallbackFailed(const QDBusError & _t1, const QDBusMessage & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QDBusConnectionPrivate::newServerConnection(QDBusConnectionPrivate * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
struct qt_meta_stringdata_QDBusConnectionDispatchEnabler_t {
    QByteArrayData data[3];
    char stringdata0[40];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDBusConnectionDispatchEnabler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDBusConnectionDispatchEnabler_t qt_meta_stringdata_QDBusConnectionDispatchEnabler = {
    {
QT_MOC_LITERAL(0, 0, 30), // "QDBusConnectionDispatchEnabler"
QT_MOC_LITERAL(1, 31, 7), // "execute"
QT_MOC_LITERAL(2, 39, 0) // ""

    },
    "QDBusConnectionDispatchEnabler\0execute\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDBusConnectionDispatchEnabler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void QDBusConnectionDispatchEnabler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDBusConnectionDispatchEnabler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->execute(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QDBusConnectionDispatchEnabler::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDBusConnectionDispatchEnabler.data,
    qt_meta_data_QDBusConnectionDispatchEnabler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDBusConnectionDispatchEnabler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDBusConnectionDispatchEnabler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDBusConnectionDispatchEnabler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDBusConnectionDispatchEnabler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
