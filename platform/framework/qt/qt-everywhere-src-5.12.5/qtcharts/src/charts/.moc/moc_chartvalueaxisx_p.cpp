/****************************************************************************
** Meta object code from reading C++ file 'chartvalueaxisx_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../axis/valueaxis/chartvalueaxisx_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'chartvalueaxisx_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtCharts__ChartValueAxisX_t {
    QByteArrayData data[14];
    char stringdata0[229];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtCharts__ChartValueAxisX_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtCharts__ChartValueAxisX_t qt_meta_stringdata_QtCharts__ChartValueAxisX = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QtCharts::ChartValueAxisX"
QT_MOC_LITERAL(1, 26, 22), // "handleTickCountChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 4), // "tick"
QT_MOC_LITERAL(4, 55, 27), // "handleMinorTickCountChanged"
QT_MOC_LITERAL(5, 83, 24), // "handleLabelFormatChanged"
QT_MOC_LITERAL(6, 108, 6), // "format"
QT_MOC_LITERAL(7, 115, 25), // "handleTickIntervalChanged"
QT_MOC_LITERAL(8, 141, 8), // "interval"
QT_MOC_LITERAL(9, 150, 23), // "handleTickAnchorChanged"
QT_MOC_LITERAL(10, 174, 6), // "anchor"
QT_MOC_LITERAL(11, 181, 21), // "handleTickTypeChanged"
QT_MOC_LITERAL(12, 203, 20), // "QValueAxis::TickType"
QT_MOC_LITERAL(13, 224, 4) // "type"

    },
    "QtCharts::ChartValueAxisX\0"
    "handleTickCountChanged\0\0tick\0"
    "handleMinorTickCountChanged\0"
    "handleLabelFormatChanged\0format\0"
    "handleTickIntervalChanged\0interval\0"
    "handleTickAnchorChanged\0anchor\0"
    "handleTickTypeChanged\0QValueAxis::TickType\0"
    "type"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtCharts__ChartValueAxisX[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x08 /* Private */,
       4,    1,   47,    2, 0x08 /* Private */,
       5,    1,   50,    2, 0x08 /* Private */,
       7,    1,   53,    2, 0x08 /* Private */,
       9,    1,   56,    2, 0x08 /* Private */,
      11,    1,   59,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::QString,    6,
    QMetaType::Void, QMetaType::QReal,    8,
    QMetaType::Void, QMetaType::QReal,   10,
    QMetaType::Void, 0x80000000 | 12,   13,

       0        // eod
};

void QtCharts::ChartValueAxisX::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ChartValueAxisX *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->handleTickCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->handleMinorTickCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->handleLabelFormatChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->handleTickIntervalChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 4: _t->handleTickAnchorChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 5: _t->handleTickTypeChanged((*reinterpret_cast< QValueAxis::TickType(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QtCharts::ChartValueAxisX::staticMetaObject = { {
    &HorizontalAxis::staticMetaObject,
    qt_meta_stringdata_QtCharts__ChartValueAxisX.data,
    qt_meta_data_QtCharts__ChartValueAxisX,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtCharts::ChartValueAxisX::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtCharts::ChartValueAxisX::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtCharts__ChartValueAxisX.stringdata0))
        return static_cast<void*>(this);
    return HorizontalAxis::qt_metacast(_clname);
}

int QtCharts::ChartValueAxisX::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = HorizontalAxis::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
