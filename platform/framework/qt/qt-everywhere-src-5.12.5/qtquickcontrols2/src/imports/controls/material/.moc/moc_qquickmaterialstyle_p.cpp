/****************************************************************************
** Meta object code from reading C++ file 'qquickmaterialstyle_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickmaterialstyle_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmaterialstyle_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickMaterialStyle_t {
    QByteArrayData data[102];
    char stringdata0[1303];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMaterialStyle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMaterialStyle_t qt_meta_stringdata_QQuickMaterialStyle = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QQuickMaterialStyle"
QT_MOC_LITERAL(1, 20, 12), // "themeChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 14), // "primaryChanged"
QT_MOC_LITERAL(4, 49, 13), // "accentChanged"
QT_MOC_LITERAL(5, 63, 17), // "foregroundChanged"
QT_MOC_LITERAL(6, 81, 17), // "backgroundChanged"
QT_MOC_LITERAL(7, 99, 16), // "elevationChanged"
QT_MOC_LITERAL(8, 116, 14), // "paletteChanged"
QT_MOC_LITERAL(9, 131, 5), // "color"
QT_MOC_LITERAL(10, 137, 5), // "Color"
QT_MOC_LITERAL(11, 143, 5), // "Shade"
QT_MOC_LITERAL(12, 149, 5), // "shade"
QT_MOC_LITERAL(13, 155, 5), // "theme"
QT_MOC_LITERAL(14, 161, 5), // "Theme"
QT_MOC_LITERAL(15, 167, 7), // "primary"
QT_MOC_LITERAL(16, 175, 6), // "accent"
QT_MOC_LITERAL(17, 182, 10), // "foreground"
QT_MOC_LITERAL(18, 193, 10), // "background"
QT_MOC_LITERAL(19, 204, 9), // "elevation"
QT_MOC_LITERAL(20, 214, 12), // "primaryColor"
QT_MOC_LITERAL(21, 227, 11), // "accentColor"
QT_MOC_LITERAL(22, 239, 15), // "backgroundColor"
QT_MOC_LITERAL(23, 255, 16), // "primaryTextColor"
QT_MOC_LITERAL(24, 272, 27), // "primaryHighlightedTextColor"
QT_MOC_LITERAL(25, 300, 18), // "secondaryTextColor"
QT_MOC_LITERAL(26, 319, 13), // "hintTextColor"
QT_MOC_LITERAL(27, 333, 18), // "textSelectionColor"
QT_MOC_LITERAL(28, 352, 15), // "dropShadowColor"
QT_MOC_LITERAL(29, 368, 12), // "dividerColor"
QT_MOC_LITERAL(30, 381, 9), // "iconColor"
QT_MOC_LITERAL(31, 391, 17), // "iconDisabledColor"
QT_MOC_LITERAL(32, 409, 11), // "buttonColor"
QT_MOC_LITERAL(33, 421, 19), // "buttonDisabledColor"
QT_MOC_LITERAL(34, 441, 22), // "highlightedButtonColor"
QT_MOC_LITERAL(35, 464, 10), // "frameColor"
QT_MOC_LITERAL(36, 475, 11), // "rippleColor"
QT_MOC_LITERAL(37, 487, 22), // "highlightedRippleColor"
QT_MOC_LITERAL(38, 510, 25), // "switchUncheckedTrackColor"
QT_MOC_LITERAL(39, 536, 23), // "switchCheckedTrackColor"
QT_MOC_LITERAL(40, 560, 26), // "switchUncheckedHandleColor"
QT_MOC_LITERAL(41, 587, 24), // "switchCheckedHandleColor"
QT_MOC_LITERAL(42, 612, 24), // "switchDisabledTrackColor"
QT_MOC_LITERAL(43, 637, 25), // "switchDisabledHandleColor"
QT_MOC_LITERAL(44, 663, 14), // "scrollBarColor"
QT_MOC_LITERAL(45, 678, 21), // "scrollBarHoveredColor"
QT_MOC_LITERAL(46, 700, 21), // "scrollBarPressedColor"
QT_MOC_LITERAL(47, 722, 11), // "dialogColor"
QT_MOC_LITERAL(48, 734, 18), // "backgroundDimColor"
QT_MOC_LITERAL(49, 753, 18), // "listHighlightColor"
QT_MOC_LITERAL(50, 772, 12), // "tooltipColor"
QT_MOC_LITERAL(51, 785, 12), // "toolBarColor"
QT_MOC_LITERAL(52, 798, 13), // "toolTextColor"
QT_MOC_LITERAL(53, 812, 24), // "spinBoxDisabledIconColor"
QT_MOC_LITERAL(54, 837, 11), // "touchTarget"
QT_MOC_LITERAL(55, 849, 12), // "buttonHeight"
QT_MOC_LITERAL(56, 862, 14), // "delegateHeight"
QT_MOC_LITERAL(57, 877, 21), // "dialogButtonBoxHeight"
QT_MOC_LITERAL(58, 899, 20), // "frameVerticalPadding"
QT_MOC_LITERAL(59, 920, 14), // "menuItemHeight"
QT_MOC_LITERAL(60, 935, 23), // "menuItemVerticalPadding"
QT_MOC_LITERAL(61, 959, 29), // "switchDelegateVerticalPadding"
QT_MOC_LITERAL(62, 989, 13), // "tooltipHeight"
QT_MOC_LITERAL(63, 1003, 5), // "Light"
QT_MOC_LITERAL(64, 1009, 4), // "Dark"
QT_MOC_LITERAL(65, 1014, 6), // "System"
QT_MOC_LITERAL(66, 1021, 7), // "Variant"
QT_MOC_LITERAL(67, 1029, 6), // "Normal"
QT_MOC_LITERAL(68, 1036, 5), // "Dense"
QT_MOC_LITERAL(69, 1042, 3), // "Red"
QT_MOC_LITERAL(70, 1046, 4), // "Pink"
QT_MOC_LITERAL(71, 1051, 6), // "Purple"
QT_MOC_LITERAL(72, 1058, 10), // "DeepPurple"
QT_MOC_LITERAL(73, 1069, 6), // "Indigo"
QT_MOC_LITERAL(74, 1076, 4), // "Blue"
QT_MOC_LITERAL(75, 1081, 9), // "LightBlue"
QT_MOC_LITERAL(76, 1091, 4), // "Cyan"
QT_MOC_LITERAL(77, 1096, 4), // "Teal"
QT_MOC_LITERAL(78, 1101, 5), // "Green"
QT_MOC_LITERAL(79, 1107, 10), // "LightGreen"
QT_MOC_LITERAL(80, 1118, 4), // "Lime"
QT_MOC_LITERAL(81, 1123, 6), // "Yellow"
QT_MOC_LITERAL(82, 1130, 5), // "Amber"
QT_MOC_LITERAL(83, 1136, 6), // "Orange"
QT_MOC_LITERAL(84, 1143, 10), // "DeepOrange"
QT_MOC_LITERAL(85, 1154, 5), // "Brown"
QT_MOC_LITERAL(86, 1160, 4), // "Grey"
QT_MOC_LITERAL(87, 1165, 8), // "BlueGrey"
QT_MOC_LITERAL(88, 1174, 7), // "Shade50"
QT_MOC_LITERAL(89, 1182, 8), // "Shade100"
QT_MOC_LITERAL(90, 1191, 8), // "Shade200"
QT_MOC_LITERAL(91, 1200, 8), // "Shade300"
QT_MOC_LITERAL(92, 1209, 8), // "Shade400"
QT_MOC_LITERAL(93, 1218, 8), // "Shade500"
QT_MOC_LITERAL(94, 1227, 8), // "Shade600"
QT_MOC_LITERAL(95, 1236, 8), // "Shade700"
QT_MOC_LITERAL(96, 1245, 8), // "Shade800"
QT_MOC_LITERAL(97, 1254, 8), // "Shade900"
QT_MOC_LITERAL(98, 1263, 9), // "ShadeA100"
QT_MOC_LITERAL(99, 1273, 9), // "ShadeA200"
QT_MOC_LITERAL(100, 1283, 9), // "ShadeA400"
QT_MOC_LITERAL(101, 1293, 9) // "ShadeA700"

    },
    "QQuickMaterialStyle\0themeChanged\0\0"
    "primaryChanged\0accentChanged\0"
    "foregroundChanged\0backgroundChanged\0"
    "elevationChanged\0paletteChanged\0color\0"
    "Color\0Shade\0shade\0theme\0Theme\0primary\0"
    "accent\0foreground\0background\0elevation\0"
    "primaryColor\0accentColor\0backgroundColor\0"
    "primaryTextColor\0primaryHighlightedTextColor\0"
    "secondaryTextColor\0hintTextColor\0"
    "textSelectionColor\0dropShadowColor\0"
    "dividerColor\0iconColor\0iconDisabledColor\0"
    "buttonColor\0buttonDisabledColor\0"
    "highlightedButtonColor\0frameColor\0"
    "rippleColor\0highlightedRippleColor\0"
    "switchUncheckedTrackColor\0"
    "switchCheckedTrackColor\0"
    "switchUncheckedHandleColor\0"
    "switchCheckedHandleColor\0"
    "switchDisabledTrackColor\0"
    "switchDisabledHandleColor\0scrollBarColor\0"
    "scrollBarHoveredColor\0scrollBarPressedColor\0"
    "dialogColor\0backgroundDimColor\0"
    "listHighlightColor\0tooltipColor\0"
    "toolBarColor\0toolTextColor\0"
    "spinBoxDisabledIconColor\0touchTarget\0"
    "buttonHeight\0delegateHeight\0"
    "dialogButtonBoxHeight\0frameVerticalPadding\0"
    "menuItemHeight\0menuItemVerticalPadding\0"
    "switchDelegateVerticalPadding\0"
    "tooltipHeight\0Light\0Dark\0System\0Variant\0"
    "Normal\0Dense\0Red\0Pink\0Purple\0DeepPurple\0"
    "Indigo\0Blue\0LightBlue\0Cyan\0Teal\0Green\0"
    "LightGreen\0Lime\0Yellow\0Amber\0Orange\0"
    "DeepOrange\0Brown\0Grey\0BlueGrey\0Shade50\0"
    "Shade100\0Shade200\0Shade300\0Shade400\0"
    "Shade500\0Shade600\0Shade700\0Shade800\0"
    "Shade900\0ShadeA100\0ShadeA200\0ShadeA400\0"
    "ShadeA700"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMaterialStyle[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
      49,   84, // properties
       4,  280, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,
       3,    0,   65,    2, 0x06 /* Public */,
       4,    0,   66,    2, 0x06 /* Public */,
       5,    0,   67,    2, 0x06 /* Public */,
       6,    0,   68,    2, 0x06 /* Public */,
       7,    0,   69,    2, 0x06 /* Public */,
       8,    0,   70,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
       9,    2,   71,    2, 0x02 /* Public */,
       9,    1,   76,    2, 0x22 /* Public | MethodCloned */,
      12,    2,   79,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QColor, 0x80000000 | 10, 0x80000000 | 11,    9,   12,
    QMetaType::QColor, 0x80000000 | 10,    9,
    QMetaType::QColor, QMetaType::QColor, 0x80000000 | 11,    9,   12,

 // properties: name, type, flags
      13, 0x80000000 | 14, 0x0049590f,
      15, QMetaType::QVariant, 0x00495907,
      16, QMetaType::QVariant, 0x00495907,
      17, QMetaType::QVariant, 0x00495907,
      18, QMetaType::QVariant, 0x00495907,
      19, QMetaType::Int, 0x00495907,
      20, QMetaType::QColor, 0x00495801,
      21, QMetaType::QColor, 0x00495801,
      22, QMetaType::QColor, 0x00495801,
      23, QMetaType::QColor, 0x00495801,
      24, QMetaType::QColor, 0x00495801,
      25, QMetaType::QColor, 0x00495801,
      26, QMetaType::QColor, 0x00495801,
      27, QMetaType::QColor, 0x00495801,
      28, QMetaType::QColor, 0x00495801,
      29, QMetaType::QColor, 0x00495801,
      30, QMetaType::QColor, 0x00495801,
      31, QMetaType::QColor, 0x00495801,
      32, QMetaType::QColor, 0x00495801,
      33, QMetaType::QColor, 0x00495801,
      34, QMetaType::QColor, 0x00495801,
      35, QMetaType::QColor, 0x00495801,
      36, QMetaType::QColor, 0x00495801,
      37, QMetaType::QColor, 0x00495801,
      38, QMetaType::QColor, 0x00495801,
      39, QMetaType::QColor, 0x00495801,
      40, QMetaType::QColor, 0x00495801,
      41, QMetaType::QColor, 0x00495801,
      42, QMetaType::QColor, 0x00495801,
      43, QMetaType::QColor, 0x00495801,
      44, QMetaType::QColor, 0x00495801,
      45, QMetaType::QColor, 0x00495801,
      46, QMetaType::QColor, 0x00495801,
      47, QMetaType::QColor, 0x00495801,
      48, QMetaType::QColor, 0x00495801,
      49, QMetaType::QColor, 0x00495801,
      50, QMetaType::QColor, 0x00495801,
      51, QMetaType::QColor, 0x00495801,
      52, QMetaType::QColor, 0x00495801,
      53, QMetaType::QColor, 0x00495801,
      54, QMetaType::Int, 0x00095c01,
      55, QMetaType::Int, 0x00095c01,
      56, QMetaType::Int, 0x00095c01,
      57, QMetaType::Int, 0x00095c01,
      58, QMetaType::Int, 0x00095c01,
      59, QMetaType::Int, 0x00095c01,
      60, QMetaType::Int, 0x00095c01,
      61, QMetaType::Int, 0x00095c01,
      62, QMetaType::Int, 0x00095c01,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       1,
       2,
       4,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       6,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // enums: name, alias, flags, count, data
      14,   14, 0x0,    3,  300,
      66,   66, 0x0,    2,  306,
      10,   10, 0x0,   19,  310,
      11,   11, 0x0,   14,  348,

 // enum data: key, value
      63, uint(QQuickMaterialStyle::Light),
      64, uint(QQuickMaterialStyle::Dark),
      65, uint(QQuickMaterialStyle::System),
      67, uint(QQuickMaterialStyle::Normal),
      68, uint(QQuickMaterialStyle::Dense),
      69, uint(QQuickMaterialStyle::Red),
      70, uint(QQuickMaterialStyle::Pink),
      71, uint(QQuickMaterialStyle::Purple),
      72, uint(QQuickMaterialStyle::DeepPurple),
      73, uint(QQuickMaterialStyle::Indigo),
      74, uint(QQuickMaterialStyle::Blue),
      75, uint(QQuickMaterialStyle::LightBlue),
      76, uint(QQuickMaterialStyle::Cyan),
      77, uint(QQuickMaterialStyle::Teal),
      78, uint(QQuickMaterialStyle::Green),
      79, uint(QQuickMaterialStyle::LightGreen),
      80, uint(QQuickMaterialStyle::Lime),
      81, uint(QQuickMaterialStyle::Yellow),
      82, uint(QQuickMaterialStyle::Amber),
      83, uint(QQuickMaterialStyle::Orange),
      84, uint(QQuickMaterialStyle::DeepOrange),
      85, uint(QQuickMaterialStyle::Brown),
      86, uint(QQuickMaterialStyle::Grey),
      87, uint(QQuickMaterialStyle::BlueGrey),
      88, uint(QQuickMaterialStyle::Shade50),
      89, uint(QQuickMaterialStyle::Shade100),
      90, uint(QQuickMaterialStyle::Shade200),
      91, uint(QQuickMaterialStyle::Shade300),
      92, uint(QQuickMaterialStyle::Shade400),
      93, uint(QQuickMaterialStyle::Shade500),
      94, uint(QQuickMaterialStyle::Shade600),
      95, uint(QQuickMaterialStyle::Shade700),
      96, uint(QQuickMaterialStyle::Shade800),
      97, uint(QQuickMaterialStyle::Shade900),
      98, uint(QQuickMaterialStyle::ShadeA100),
      99, uint(QQuickMaterialStyle::ShadeA200),
     100, uint(QQuickMaterialStyle::ShadeA400),
     101, uint(QQuickMaterialStyle::ShadeA700),

       0        // eod
};

void QQuickMaterialStyle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickMaterialStyle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->themeChanged(); break;
        case 1: _t->primaryChanged(); break;
        case 2: _t->accentChanged(); break;
        case 3: _t->foregroundChanged(); break;
        case 4: _t->backgroundChanged(); break;
        case 5: _t->elevationChanged(); break;
        case 6: _t->paletteChanged(); break;
        case 7: { QColor _r = _t->color((*reinterpret_cast< Color(*)>(_a[1])),(*reinterpret_cast< Shade(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 8: { QColor _r = _t->color((*reinterpret_cast< Color(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 9: { QColor _r = _t->shade((*reinterpret_cast< const QColor(*)>(_a[1])),(*reinterpret_cast< Shade(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::themeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::primaryChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::accentChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::foregroundChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::backgroundChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::elevationChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickMaterialStyle::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMaterialStyle::paletteChanged)) {
                *result = 6;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickMaterialStyle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Theme*>(_v) = _t->theme(); break;
        case 1: *reinterpret_cast< QVariant*>(_v) = _t->primary(); break;
        case 2: *reinterpret_cast< QVariant*>(_v) = _t->accent(); break;
        case 3: *reinterpret_cast< QVariant*>(_v) = _t->foreground(); break;
        case 4: *reinterpret_cast< QVariant*>(_v) = _t->background(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->elevation(); break;
        case 6: *reinterpret_cast< QColor*>(_v) = _t->primaryColor(); break;
        case 7: *reinterpret_cast< QColor*>(_v) = _t->accentColor(); break;
        case 8: *reinterpret_cast< QColor*>(_v) = _t->backgroundColor(); break;
        case 9: *reinterpret_cast< QColor*>(_v) = _t->primaryTextColor(); break;
        case 10: *reinterpret_cast< QColor*>(_v) = _t->primaryHighlightedTextColor(); break;
        case 11: *reinterpret_cast< QColor*>(_v) = _t->secondaryTextColor(); break;
        case 12: *reinterpret_cast< QColor*>(_v) = _t->hintTextColor(); break;
        case 13: *reinterpret_cast< QColor*>(_v) = _t->textSelectionColor(); break;
        case 14: *reinterpret_cast< QColor*>(_v) = _t->dropShadowColor(); break;
        case 15: *reinterpret_cast< QColor*>(_v) = _t->dividerColor(); break;
        case 16: *reinterpret_cast< QColor*>(_v) = _t->iconColor(); break;
        case 17: *reinterpret_cast< QColor*>(_v) = _t->iconDisabledColor(); break;
        case 18: *reinterpret_cast< QColor*>(_v) = _t->buttonColor(); break;
        case 19: *reinterpret_cast< QColor*>(_v) = _t->buttonDisabledColor(); break;
        case 20: *reinterpret_cast< QColor*>(_v) = _t->highlightedButtonColor(); break;
        case 21: *reinterpret_cast< QColor*>(_v) = _t->frameColor(); break;
        case 22: *reinterpret_cast< QColor*>(_v) = _t->rippleColor(); break;
        case 23: *reinterpret_cast< QColor*>(_v) = _t->highlightedRippleColor(); break;
        case 24: *reinterpret_cast< QColor*>(_v) = _t->switchUncheckedTrackColor(); break;
        case 25: *reinterpret_cast< QColor*>(_v) = _t->switchCheckedTrackColor(); break;
        case 26: *reinterpret_cast< QColor*>(_v) = _t->switchUncheckedHandleColor(); break;
        case 27: *reinterpret_cast< QColor*>(_v) = _t->switchCheckedHandleColor(); break;
        case 28: *reinterpret_cast< QColor*>(_v) = _t->switchDisabledTrackColor(); break;
        case 29: *reinterpret_cast< QColor*>(_v) = _t->switchDisabledHandleColor(); break;
        case 30: *reinterpret_cast< QColor*>(_v) = _t->scrollBarColor(); break;
        case 31: *reinterpret_cast< QColor*>(_v) = _t->scrollBarHoveredColor(); break;
        case 32: *reinterpret_cast< QColor*>(_v) = _t->scrollBarPressedColor(); break;
        case 33: *reinterpret_cast< QColor*>(_v) = _t->dialogColor(); break;
        case 34: *reinterpret_cast< QColor*>(_v) = _t->backgroundDimColor(); break;
        case 35: *reinterpret_cast< QColor*>(_v) = _t->listHighlightColor(); break;
        case 36: *reinterpret_cast< QColor*>(_v) = _t->tooltipColor(); break;
        case 37: *reinterpret_cast< QColor*>(_v) = _t->toolBarColor(); break;
        case 38: *reinterpret_cast< QColor*>(_v) = _t->toolTextColor(); break;
        case 39: *reinterpret_cast< QColor*>(_v) = _t->spinBoxDisabledIconColor(); break;
        case 40: *reinterpret_cast< int*>(_v) = _t->touchTarget(); break;
        case 41: *reinterpret_cast< int*>(_v) = _t->buttonHeight(); break;
        case 42: *reinterpret_cast< int*>(_v) = _t->delegateHeight(); break;
        case 43: *reinterpret_cast< int*>(_v) = _t->dialogButtonBoxHeight(); break;
        case 44: *reinterpret_cast< int*>(_v) = _t->frameVerticalPadding(); break;
        case 45: *reinterpret_cast< int*>(_v) = _t->menuItemHeight(); break;
        case 46: *reinterpret_cast< int*>(_v) = _t->menuItemVerticalPadding(); break;
        case 47: *reinterpret_cast< int*>(_v) = _t->switchDelegateVerticalPadding(); break;
        case 48: *reinterpret_cast< int*>(_v) = _t->tooltipHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickMaterialStyle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTheme(*reinterpret_cast< Theme*>(_v)); break;
        case 1: _t->setPrimary(*reinterpret_cast< QVariant*>(_v)); break;
        case 2: _t->setAccent(*reinterpret_cast< QVariant*>(_v)); break;
        case 3: _t->setForeground(*reinterpret_cast< QVariant*>(_v)); break;
        case 4: _t->setBackground(*reinterpret_cast< QVariant*>(_v)); break;
        case 5: _t->setElevation(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickMaterialStyle *_t = static_cast<QQuickMaterialStyle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->resetTheme(); break;
        case 1: _t->resetPrimary(); break;
        case 2: _t->resetAccent(); break;
        case 3: _t->resetForeground(); break;
        case 4: _t->resetBackground(); break;
        case 5: _t->resetElevation(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickMaterialStyle::staticMetaObject = { {
    &QQuickAttachedObject::staticMetaObject,
    qt_meta_stringdata_QQuickMaterialStyle.data,
    qt_meta_data_QQuickMaterialStyle,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickMaterialStyle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMaterialStyle::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMaterialStyle.stringdata0))
        return static_cast<void*>(this);
    return QQuickAttachedObject::qt_metacast(_clname);
}

int QQuickMaterialStyle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAttachedObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 49;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 49;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 49;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 49;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 49;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 49;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMaterialStyle::themeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickMaterialStyle::primaryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickMaterialStyle::accentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickMaterialStyle::foregroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickMaterialStyle::backgroundChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickMaterialStyle::elevationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickMaterialStyle::paletteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
