/****************************************************************************
** Meta object code from reading C++ file 'qnavigationmanagerengine_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../maps/qnavigationmanagerengine_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qnavigationmanagerengine_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QAbstractNavigator_t {
    QByteArrayData data[17];
    char stringdata0[219];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAbstractNavigator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAbstractNavigator_t qt_meta_stringdata_QAbstractNavigator = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QAbstractNavigator"
QT_MOC_LITERAL(1, 19, 13), // "activeChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 6), // "active"
QT_MOC_LITERAL(4, 41, 15), // "waypointReached"
QT_MOC_LITERAL(5, 57, 30), // "const QDeclarativeGeoWaypoint*"
QT_MOC_LITERAL(6, 88, 3), // "pos"
QT_MOC_LITERAL(7, 92, 18), // "destinationReached"
QT_MOC_LITERAL(8, 111, 19), // "currentRouteChanged"
QT_MOC_LITERAL(9, 131, 9), // "QGeoRoute"
QT_MOC_LITERAL(10, 141, 5), // "route"
QT_MOC_LITERAL(11, 147, 21), // "currentSegmentChanged"
QT_MOC_LITERAL(12, 169, 7), // "segment"
QT_MOC_LITERAL(13, 177, 5), // "start"
QT_MOC_LITERAL(14, 183, 4), // "stop"
QT_MOC_LITERAL(15, 188, 16), // "setTrackPosition"
QT_MOC_LITERAL(16, 205, 13) // "trackPosition"

    },
    "QAbstractNavigator\0activeChanged\0\0"
    "active\0waypointReached\0"
    "const QDeclarativeGeoWaypoint*\0pos\0"
    "destinationReached\0currentRouteChanged\0"
    "QGeoRoute\0route\0currentSegmentChanged\0"
    "segment\0start\0stop\0setTrackPosition\0"
    "trackPosition"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAbstractNavigator[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       7,    0,   60,    2, 0x06 /* Public */,
       8,    1,   61,    2, 0x06 /* Public */,
      11,    1,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      13,    0,   67,    2, 0x0a /* Public */,
      14,    0,   68,    2, 0x0a /* Public */,
      15,    1,   69,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::Int,   12,

 // slots: parameters
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Void, QMetaType::Bool,   16,

       0        // eod
};

void QAbstractNavigator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAbstractNavigator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->activeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->waypointReached((*reinterpret_cast< const QDeclarativeGeoWaypoint*(*)>(_a[1]))); break;
        case 2: _t->destinationReached(); break;
        case 3: _t->currentRouteChanged((*reinterpret_cast< const QGeoRoute(*)>(_a[1]))); break;
        case 4: _t->currentSegmentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: { bool _r = _t->start();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->stop();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: _t->setTrackPosition((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAbstractNavigator::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractNavigator::activeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QAbstractNavigator::*)(const QDeclarativeGeoWaypoint * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractNavigator::waypointReached)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QAbstractNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractNavigator::destinationReached)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QAbstractNavigator::*)(const QGeoRoute & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractNavigator::currentRouteChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QAbstractNavigator::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractNavigator::currentSegmentChanged)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QAbstractNavigator::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QAbstractNavigator.data,
    qt_meta_data_QAbstractNavigator,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QAbstractNavigator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAbstractNavigator::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QAbstractNavigator.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QAbstractNavigator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void QAbstractNavigator::activeChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QAbstractNavigator::waypointReached(const QDeclarativeGeoWaypoint * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QAbstractNavigator::destinationReached()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QAbstractNavigator::currentRouteChanged(const QGeoRoute & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QAbstractNavigator::currentSegmentChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
struct qt_meta_stringdata_QNavigationManagerEngine_t {
    QByteArrayData data[3];
    char stringdata0[38];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QNavigationManagerEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QNavigationManagerEngine_t qt_meta_stringdata_QNavigationManagerEngine = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QNavigationManagerEngine"
QT_MOC_LITERAL(1, 25, 11), // "initialized"
QT_MOC_LITERAL(2, 37, 0) // ""

    },
    "QNavigationManagerEngine\0initialized\0"
    ""
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QNavigationManagerEngine[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

       0        // eod
};

void QNavigationManagerEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QNavigationManagerEngine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->initialized(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QNavigationManagerEngine::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QNavigationManagerEngine::initialized)) {
                *result = 0;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QNavigationManagerEngine::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QNavigationManagerEngine.data,
    qt_meta_data_QNavigationManagerEngine,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QNavigationManagerEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QNavigationManagerEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QNavigationManagerEngine.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QNavigationManagerEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void QNavigationManagerEngine::initialized()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
