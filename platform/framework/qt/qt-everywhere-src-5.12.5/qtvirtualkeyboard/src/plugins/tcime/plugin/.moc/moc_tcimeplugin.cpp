/****************************************************************************
** Meta object code from reading C++ file 'tcimeplugin.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tcimeplugin.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tcimeplugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtVirtualKeyboardTCImePlugin_t {
    QByteArrayData data[1];
    char stringdata0[29];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtVirtualKeyboardTCImePlugin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtVirtualKeyboardTCImePlugin_t qt_meta_stringdata_QtVirtualKeyboardTCImePlugin = {
    {
QT_MOC_LITERAL(0, 0, 28) // "QtVirtualKeyboardTCImePlugin"

    },
    "QtVirtualKeyboardTCImePlugin"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtVirtualKeyboardTCImePlugin[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QtVirtualKeyboardTCImePlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QtVirtualKeyboardTCImePlugin::staticMetaObject = { {
    &QVirtualKeyboardExtensionPlugin::staticMetaObject,
    qt_meta_stringdata_QtVirtualKeyboardTCImePlugin.data,
    qt_meta_data_QtVirtualKeyboardTCImePlugin,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtVirtualKeyboardTCImePlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtVirtualKeyboardTCImePlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtVirtualKeyboardTCImePlugin.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "org.qt-project.qt.virtualkeyboard.plugin/5.12"))
        return static_cast< QVirtualKeyboardExtensionPlugin*>(this);
    return QVirtualKeyboardExtensionPlugin::qt_metacast(_clname);
}

int QtVirtualKeyboardTCImePlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QVirtualKeyboardExtensionPlugin::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x2d,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  'v',  'i',  'r', 
    't',  'u',  'a',  'l',  'k',  'e',  'y',  'b', 
    'o',  'a',  'r',  'd',  '.',  'p',  'l',  'u', 
    'g',  'i',  'n',  '/',  '5',  '.',  '1',  '2', 
    // "className"
    0x03,  0x78,  0x1c,  'Q',  't',  'V',  'i',  'r', 
    't',  'u',  'a',  'l',  'K',  'e',  'y',  'b', 
    'o',  'a',  'r',  'd',  'T',  'C',  'I',  'm', 
    'e',  'P',  'l',  'u',  'g',  'i',  'n', 
    // "MetaData"
    0x04,  0xa4,  0x6b,  'I',  'n',  'p',  'u',  't', 
    'M',  'e',  't',  'h',  'o',  'd',  0x6d,  'T', 
    'C',  'I',  'n',  'p',  'u',  't',  'M',  'e', 
    't',  'h',  'o',  'd',  0x64,  'N',  'a',  'm', 
    'e',  0x73,  't',  'r',  'a',  'd',  'i',  't', 
    'i',  'o',  'n',  'a',  'l',  '_',  'c',  'h', 
    'i',  'n',  'e',  's',  'e',  0x68,  'P',  'r', 
    'o',  'v',  'i',  'd',  'e',  'r',  0x72,  'Q', 
    't',  ' ',  'T',  'C',  'I',  'M',  'E',  ' ', 
    'E',  'x',  't',  'e',  'n',  's',  'i',  'o', 
    'n',  0x67,  'V',  'e',  'r',  's',  'i',  'o', 
    'n',  0x18,  0x64, 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(QtVirtualKeyboardTCImePlugin, QtVirtualKeyboardTCImePlugin)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
