/* Generated by configure */
#include <GLES3/gl31.h>
int main(int argc, char **argv)
{
    (void)argc; (void)argv;
    /* BEGIN TEST: */
    glDispatchCompute(1, 1, 1);
    glProgramUniform1i(0, 0, 0);
    /* END TEST */
    return 0;
}
