/****************************************************************************
** Meta object code from reading C++ file 'qcandlestickmodelmapper_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../candlestickchart/qcandlestickmodelmapper_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qcandlestickmodelmapper_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate_t {
    QByteArrayData data[33];
    char stringdata0[513];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate_t qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate = {
    {
QT_MOC_LITERAL(0, 0, 40), // "QtCharts::QCandlestickModelMa..."
QT_MOC_LITERAL(1, 41, 16), // "timestampChanged"
QT_MOC_LITERAL(2, 58, 0), // ""
QT_MOC_LITERAL(3, 59, 11), // "openChanged"
QT_MOC_LITERAL(4, 71, 11), // "highChanged"
QT_MOC_LITERAL(5, 83, 10), // "lowChanged"
QT_MOC_LITERAL(6, 94, 12), // "closeChanged"
QT_MOC_LITERAL(7, 107, 22), // "firstSetSectionChanged"
QT_MOC_LITERAL(8, 130, 21), // "lastSetSectionChanged"
QT_MOC_LITERAL(9, 152, 30), // "initializeCandlestickFromModel"
QT_MOC_LITERAL(10, 183, 16), // "modelDataUpdated"
QT_MOC_LITERAL(11, 200, 11), // "QModelIndex"
QT_MOC_LITERAL(12, 212, 7), // "topLeft"
QT_MOC_LITERAL(13, 220, 11), // "bottomRight"
QT_MOC_LITERAL(14, 232, 22), // "modelHeaderDataUpdated"
QT_MOC_LITERAL(15, 255, 15), // "Qt::Orientation"
QT_MOC_LITERAL(16, 271, 11), // "orientation"
QT_MOC_LITERAL(17, 283, 5), // "first"
QT_MOC_LITERAL(18, 289, 4), // "last"
QT_MOC_LITERAL(19, 294, 17), // "modelRowsInserted"
QT_MOC_LITERAL(20, 312, 6), // "parent"
QT_MOC_LITERAL(21, 319, 5), // "start"
QT_MOC_LITERAL(22, 325, 3), // "end"
QT_MOC_LITERAL(23, 329, 16), // "modelRowsRemoved"
QT_MOC_LITERAL(24, 346, 20), // "modelColumnsInserted"
QT_MOC_LITERAL(25, 367, 19), // "modelColumnsRemoved"
QT_MOC_LITERAL(26, 387, 14), // "modelDestroyed"
QT_MOC_LITERAL(27, 402, 20), // "candlestickSetsAdded"
QT_MOC_LITERAL(28, 423, 23), // "QList<QCandlestickSet*>"
QT_MOC_LITERAL(29, 447, 4), // "sets"
QT_MOC_LITERAL(30, 452, 22), // "candlestickSetsRemoved"
QT_MOC_LITERAL(31, 475, 21), // "candlestickSetChanged"
QT_MOC_LITERAL(32, 497, 15) // "seriesDestroyed"

    },
    "QtCharts::QCandlestickModelMapperPrivate\0"
    "timestampChanged\0\0openChanged\0highChanged\0"
    "lowChanged\0closeChanged\0firstSetSectionChanged\0"
    "lastSetSectionChanged\0"
    "initializeCandlestickFromModel\0"
    "modelDataUpdated\0QModelIndex\0topLeft\0"
    "bottomRight\0modelHeaderDataUpdated\0"
    "Qt::Orientation\0orientation\0first\0"
    "last\0modelRowsInserted\0parent\0start\0"
    "end\0modelRowsRemoved\0modelColumnsInserted\0"
    "modelColumnsRemoved\0modelDestroyed\0"
    "candlestickSetsAdded\0QList<QCandlestickSet*>\0"
    "sets\0candlestickSetsRemoved\0"
    "candlestickSetChanged\0seriesDestroyed"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtCharts__QCandlestickModelMapperPrivate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,
       3,    0,  110,    2, 0x06 /* Public */,
       4,    0,  111,    2, 0x06 /* Public */,
       5,    0,  112,    2, 0x06 /* Public */,
       6,    0,  113,    2, 0x06 /* Public */,
       7,    0,  114,    2, 0x06 /* Public */,
       8,    0,  115,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    0,  116,    2, 0x08 /* Private */,
      10,    2,  117,    2, 0x08 /* Private */,
      14,    3,  122,    2, 0x08 /* Private */,
      19,    3,  129,    2, 0x08 /* Private */,
      23,    3,  136,    2, 0x08 /* Private */,
      24,    3,  143,    2, 0x08 /* Private */,
      25,    3,  150,    2, 0x08 /* Private */,
      26,    0,  157,    2, 0x08 /* Private */,
      27,    1,  158,    2, 0x08 /* Private */,
      30,    1,  161,    2, 0x08 /* Private */,
      31,    0,  164,    2, 0x08 /* Private */,
      32,    0,  165,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11, 0x80000000 | 11,   12,   13,
    QMetaType::Void, 0x80000000 | 15, QMetaType::Int, QMetaType::Int,   16,   17,   18,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Int, QMetaType::Int,   20,   21,   22,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Int, QMetaType::Int,   20,   21,   22,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Int, QMetaType::Int,   20,   21,   22,
    QMetaType::Void, 0x80000000 | 11, QMetaType::Int, QMetaType::Int,   20,   21,   22,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QtCharts::QCandlestickModelMapperPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QCandlestickModelMapperPrivate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->timestampChanged(); break;
        case 1: _t->openChanged(); break;
        case 2: _t->highChanged(); break;
        case 3: _t->lowChanged(); break;
        case 4: _t->closeChanged(); break;
        case 5: _t->firstSetSectionChanged(); break;
        case 6: _t->lastSetSectionChanged(); break;
        case 7: _t->initializeCandlestickFromModel(); break;
        case 8: _t->modelDataUpdated((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< QModelIndex(*)>(_a[2]))); break;
        case 9: _t->modelHeaderDataUpdated((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 10: _t->modelRowsInserted((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 11: _t->modelRowsRemoved((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 12: _t->modelColumnsInserted((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 13: _t->modelColumnsRemoved((*reinterpret_cast< QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 14: _t->modelDestroyed(); break;
        case 15: _t->candlestickSetsAdded((*reinterpret_cast< const QList<QCandlestickSet*>(*)>(_a[1]))); break;
        case 16: _t->candlestickSetsRemoved((*reinterpret_cast< const QList<QCandlestickSet*>(*)>(_a[1]))); break;
        case 17: _t->candlestickSetChanged(); break;
        case 18: _t->seriesDestroyed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::timestampChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::openChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::highChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::lowChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::closeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::firstSetSectionChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QCandlestickModelMapperPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QCandlestickModelMapperPrivate::lastSetSectionChanged)) {
                *result = 6;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QtCharts::QCandlestickModelMapperPrivate::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate.data,
    qt_meta_data_QtCharts__QCandlestickModelMapperPrivate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtCharts::QCandlestickModelMapperPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtCharts::QCandlestickModelMapperPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtCharts__QCandlestickModelMapperPrivate.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QtCharts::QCandlestickModelMapperPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
    return _id;
}

// SIGNAL 0
void QtCharts::QCandlestickModelMapperPrivate::timestampChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QtCharts::QCandlestickModelMapperPrivate::openChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QtCharts::QCandlestickModelMapperPrivate::highChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QtCharts::QCandlestickModelMapperPrivate::lowChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QtCharts::QCandlestickModelMapperPrivate::closeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QtCharts::QCandlestickModelMapperPrivate::firstSetSectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QtCharts::QCandlestickModelMapperPrivate::lastSetSectionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
