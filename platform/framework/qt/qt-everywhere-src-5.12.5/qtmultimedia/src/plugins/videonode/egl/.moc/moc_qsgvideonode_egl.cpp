/****************************************************************************
** Meta object code from reading C++ file 'qsgvideonode_egl.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qsgvideonode_egl.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qsgvideonode_egl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QSGVideoNodeFactory_EGL_t {
    QByteArrayData data[1];
    char stringdata0[24];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QSGVideoNodeFactory_EGL_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QSGVideoNodeFactory_EGL_t qt_meta_stringdata_QSGVideoNodeFactory_EGL = {
    {
QT_MOC_LITERAL(0, 0, 23) // "QSGVideoNodeFactory_EGL"

    },
    "QSGVideoNodeFactory_EGL"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QSGVideoNodeFactory_EGL[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QSGVideoNodeFactory_EGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QSGVideoNodeFactory_EGL::staticMetaObject = { {
    &QSGVideoNodeFactoryPlugin::staticMetaObject,
    qt_meta_stringdata_QSGVideoNodeFactory_EGL.data,
    qt_meta_data_QSGVideoNodeFactory_EGL,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QSGVideoNodeFactory_EGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QSGVideoNodeFactory_EGL::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QSGVideoNodeFactory_EGL.stringdata0))
        return static_cast<void*>(this);
    return QSGVideoNodeFactoryPlugin::qt_metacast(_clname);
}

int QSGVideoNodeFactory_EGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QSGVideoNodeFactoryPlugin::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x28,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  's',  'g',  'v', 
    'i',  'd',  'e',  'o',  'n',  'o',  'd',  'e', 
    'f',  'a',  'c',  't',  'o',  'r',  'y',  '/', 
    '5',  '.',  '2', 
    // "className"
    0x03,  0x77,  'Q',  'S',  'G',  'V',  'i',  'd', 
    'e',  'o',  'N',  'o',  'd',  'e',  'F',  'a', 
    'c',  't',  'o',  'r',  'y',  '_',  'E',  'G', 
    'L', 
    // "MetaData"
    0x04,  0xa1,  0x64,  'K',  'e',  'y',  's',  0x81, 
    0x63,  'e',  'g',  'l', 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(QSGVideoNodeFactory_EGL, QSGVideoNodeFactory_EGL)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
