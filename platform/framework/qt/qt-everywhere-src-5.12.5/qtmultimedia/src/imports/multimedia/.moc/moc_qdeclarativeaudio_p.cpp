/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativeaudio_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qdeclarativeaudio_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativeaudio_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeAudio_t {
    QByteArrayData data[107];
    char stringdata0[1372];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeAudio_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeAudio_t qt_meta_stringdata_QDeclarativeAudio = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QDeclarativeAudio"
QT_MOC_LITERAL(1, 18, 15), // "playlistChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 13), // "sourceChanged"
QT_MOC_LITERAL(4, 49, 15), // "autoLoadChanged"
QT_MOC_LITERAL(5, 65, 16), // "loopCountChanged"
QT_MOC_LITERAL(6, 82, 20), // "playbackStateChanged"
QT_MOC_LITERAL(7, 103, 15), // "autoPlayChanged"
QT_MOC_LITERAL(8, 119, 6), // "paused"
QT_MOC_LITERAL(9, 126, 7), // "stopped"
QT_MOC_LITERAL(10, 134, 7), // "playing"
QT_MOC_LITERAL(11, 142, 13), // "statusChanged"
QT_MOC_LITERAL(12, 156, 15), // "durationChanged"
QT_MOC_LITERAL(13, 172, 15), // "positionChanged"
QT_MOC_LITERAL(14, 188, 13), // "volumeChanged"
QT_MOC_LITERAL(15, 202, 12), // "mutedChanged"
QT_MOC_LITERAL(16, 215, 15), // "hasAudioChanged"
QT_MOC_LITERAL(17, 231, 15), // "hasVideoChanged"
QT_MOC_LITERAL(18, 247, 21), // "bufferProgressChanged"
QT_MOC_LITERAL(19, 269, 15), // "seekableChanged"
QT_MOC_LITERAL(20, 285, 19), // "playbackRateChanged"
QT_MOC_LITERAL(21, 305, 16), // "audioRoleChanged"
QT_MOC_LITERAL(22, 322, 22), // "customAudioRoleChanged"
QT_MOC_LITERAL(23, 345, 19), // "availabilityChanged"
QT_MOC_LITERAL(24, 365, 12), // "Availability"
QT_MOC_LITERAL(25, 378, 12), // "availability"
QT_MOC_LITERAL(26, 391, 12), // "errorChanged"
QT_MOC_LITERAL(27, 404, 5), // "error"
QT_MOC_LITERAL(28, 410, 24), // "QDeclarativeAudio::Error"
QT_MOC_LITERAL(29, 435, 11), // "errorString"
QT_MOC_LITERAL(30, 447, 18), // "mediaObjectChanged"
QT_MOC_LITERAL(31, 466, 21), // "notifyIntervalChanged"
QT_MOC_LITERAL(32, 488, 4), // "play"
QT_MOC_LITERAL(33, 493, 5), // "pause"
QT_MOC_LITERAL(34, 499, 4), // "stop"
QT_MOC_LITERAL(35, 504, 4), // "seek"
QT_MOC_LITERAL(36, 509, 8), // "position"
QT_MOC_LITERAL(37, 518, 19), // "supportedAudioRoles"
QT_MOC_LITERAL(38, 538, 8), // "QJSValue"
QT_MOC_LITERAL(39, 547, 8), // "_q_error"
QT_MOC_LITERAL(40, 556, 19), // "QMediaPlayer::Error"
QT_MOC_LITERAL(41, 576, 22), // "_q_availabilityChanged"
QT_MOC_LITERAL(42, 599, 31), // "QMultimedia::AvailabilityStatus"
QT_MOC_LITERAL(43, 631, 16), // "_q_statusChanged"
QT_MOC_LITERAL(44, 648, 15), // "_q_mediaChanged"
QT_MOC_LITERAL(45, 664, 13), // "QMediaContent"
QT_MOC_LITERAL(46, 678, 6), // "source"
QT_MOC_LITERAL(47, 685, 8), // "playlist"
QT_MOC_LITERAL(48, 694, 21), // "QDeclarativePlaylist*"
QT_MOC_LITERAL(49, 716, 5), // "loops"
QT_MOC_LITERAL(50, 722, 13), // "playbackState"
QT_MOC_LITERAL(51, 736, 13), // "PlaybackState"
QT_MOC_LITERAL(52, 750, 8), // "autoPlay"
QT_MOC_LITERAL(53, 759, 8), // "autoLoad"
QT_MOC_LITERAL(54, 768, 6), // "status"
QT_MOC_LITERAL(55, 775, 6), // "Status"
QT_MOC_LITERAL(56, 782, 8), // "duration"
QT_MOC_LITERAL(57, 791, 6), // "volume"
QT_MOC_LITERAL(58, 798, 5), // "muted"
QT_MOC_LITERAL(59, 804, 8), // "hasAudio"
QT_MOC_LITERAL(60, 813, 8), // "hasVideo"
QT_MOC_LITERAL(61, 822, 14), // "bufferProgress"
QT_MOC_LITERAL(62, 837, 8), // "seekable"
QT_MOC_LITERAL(63, 846, 12), // "playbackRate"
QT_MOC_LITERAL(64, 859, 5), // "Error"
QT_MOC_LITERAL(65, 865, 8), // "metaData"
QT_MOC_LITERAL(66, 874, 26), // "QDeclarativeMediaMetaData*"
QT_MOC_LITERAL(67, 901, 11), // "mediaObject"
QT_MOC_LITERAL(68, 913, 9), // "audioRole"
QT_MOC_LITERAL(69, 923, 9), // "AudioRole"
QT_MOC_LITERAL(70, 933, 15), // "customAudioRole"
QT_MOC_LITERAL(71, 949, 14), // "notifyInterval"
QT_MOC_LITERAL(72, 964, 13), // "UnknownStatus"
QT_MOC_LITERAL(73, 978, 7), // "NoMedia"
QT_MOC_LITERAL(74, 986, 7), // "Loading"
QT_MOC_LITERAL(75, 994, 6), // "Loaded"
QT_MOC_LITERAL(76, 1001, 7), // "Stalled"
QT_MOC_LITERAL(77, 1009, 9), // "Buffering"
QT_MOC_LITERAL(78, 1019, 8), // "Buffered"
QT_MOC_LITERAL(79, 1028, 10), // "EndOfMedia"
QT_MOC_LITERAL(80, 1039, 12), // "InvalidMedia"
QT_MOC_LITERAL(81, 1052, 7), // "NoError"
QT_MOC_LITERAL(82, 1060, 13), // "ResourceError"
QT_MOC_LITERAL(83, 1074, 11), // "FormatError"
QT_MOC_LITERAL(84, 1086, 12), // "NetworkError"
QT_MOC_LITERAL(85, 1099, 12), // "AccessDenied"
QT_MOC_LITERAL(86, 1112, 14), // "ServiceMissing"
QT_MOC_LITERAL(87, 1127, 4), // "Loop"
QT_MOC_LITERAL(88, 1132, 8), // "Infinite"
QT_MOC_LITERAL(89, 1141, 12), // "PlayingState"
QT_MOC_LITERAL(90, 1154, 11), // "PausedState"
QT_MOC_LITERAL(91, 1166, 12), // "StoppedState"
QT_MOC_LITERAL(92, 1179, 9), // "Available"
QT_MOC_LITERAL(93, 1189, 4), // "Busy"
QT_MOC_LITERAL(94, 1194, 11), // "Unavailable"
QT_MOC_LITERAL(95, 1206, 15), // "ResourceMissing"
QT_MOC_LITERAL(96, 1222, 11), // "UnknownRole"
QT_MOC_LITERAL(97, 1234, 17), // "AccessibilityRole"
QT_MOC_LITERAL(98, 1252, 9), // "AlarmRole"
QT_MOC_LITERAL(99, 1262, 10), // "CustomRole"
QT_MOC_LITERAL(100, 1273, 8), // "GameRole"
QT_MOC_LITERAL(101, 1282, 9), // "MusicRole"
QT_MOC_LITERAL(102, 1292, 16), // "NotificationRole"
QT_MOC_LITERAL(103, 1309, 12), // "RingtoneRole"
QT_MOC_LITERAL(104, 1322, 16), // "SonificationRole"
QT_MOC_LITERAL(105, 1339, 9), // "VideoRole"
QT_MOC_LITERAL(106, 1349, 22) // "VoiceCommunicationRole"

    },
    "QDeclarativeAudio\0playlistChanged\0\0"
    "sourceChanged\0autoLoadChanged\0"
    "loopCountChanged\0playbackStateChanged\0"
    "autoPlayChanged\0paused\0stopped\0playing\0"
    "statusChanged\0durationChanged\0"
    "positionChanged\0volumeChanged\0"
    "mutedChanged\0hasAudioChanged\0"
    "hasVideoChanged\0bufferProgressChanged\0"
    "seekableChanged\0playbackRateChanged\0"
    "audioRoleChanged\0customAudioRoleChanged\0"
    "availabilityChanged\0Availability\0"
    "availability\0errorChanged\0error\0"
    "QDeclarativeAudio::Error\0errorString\0"
    "mediaObjectChanged\0notifyIntervalChanged\0"
    "play\0pause\0stop\0seek\0position\0"
    "supportedAudioRoles\0QJSValue\0_q_error\0"
    "QMediaPlayer::Error\0_q_availabilityChanged\0"
    "QMultimedia::AvailabilityStatus\0"
    "_q_statusChanged\0_q_mediaChanged\0"
    "QMediaContent\0source\0playlist\0"
    "QDeclarativePlaylist*\0loops\0playbackState\0"
    "PlaybackState\0autoPlay\0autoLoad\0status\0"
    "Status\0duration\0volume\0muted\0hasAudio\0"
    "hasVideo\0bufferProgress\0seekable\0"
    "playbackRate\0Error\0metaData\0"
    "QDeclarativeMediaMetaData*\0mediaObject\0"
    "audioRole\0AudioRole\0customAudioRole\0"
    "notifyInterval\0UnknownStatus\0NoMedia\0"
    "Loading\0Loaded\0Stalled\0Buffering\0"
    "Buffered\0EndOfMedia\0InvalidMedia\0"
    "NoError\0ResourceError\0FormatError\0"
    "NetworkError\0AccessDenied\0ServiceMissing\0"
    "Loop\0Infinite\0PlayingState\0PausedState\0"
    "StoppedState\0Available\0Busy\0Unavailable\0"
    "ResourceMissing\0UnknownRole\0"
    "AccessibilityRole\0AlarmRole\0CustomRole\0"
    "GameRole\0MusicRole\0NotificationRole\0"
    "RingtoneRole\0SonificationRole\0VideoRole\0"
    "VoiceCommunicationRole"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeAudio[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      35,   14, // methods
      24,  273, // properties
       6,  393, // enums/sets
       0,    0, // constructors
       0,       // flags
      26,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  224,    2, 0x86 /* Public | MethodRevisioned */,
       3,    0,  225,    2, 0x06 /* Public */,
       4,    0,  226,    2, 0x06 /* Public */,
       5,    0,  227,    2, 0x06 /* Public */,
       6,    0,  228,    2, 0x06 /* Public */,
       7,    0,  229,    2, 0x06 /* Public */,
       8,    0,  230,    2, 0x06 /* Public */,
       9,    0,  231,    2, 0x06 /* Public */,
      10,    0,  232,    2, 0x06 /* Public */,
      11,    0,  233,    2, 0x06 /* Public */,
      12,    0,  234,    2, 0x06 /* Public */,
      13,    0,  235,    2, 0x06 /* Public */,
      14,    0,  236,    2, 0x06 /* Public */,
      15,    0,  237,    2, 0x06 /* Public */,
      16,    0,  238,    2, 0x06 /* Public */,
      17,    0,  239,    2, 0x06 /* Public */,
      18,    0,  240,    2, 0x06 /* Public */,
      19,    0,  241,    2, 0x06 /* Public */,
      20,    0,  242,    2, 0x06 /* Public */,
      21,    0,  243,    2, 0x86 /* Public | MethodRevisioned */,
      22,    0,  244,    2, 0x86 /* Public | MethodRevisioned */,
      23,    1,  245,    2, 0x06 /* Public */,
      26,    0,  248,    2, 0x06 /* Public */,
      27,    2,  249,    2, 0x06 /* Public */,
      30,    0,  254,    2, 0x06 /* Public */,
      31,    0,  255,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      32,    0,  256,    2, 0x0a /* Public */,
      33,    0,  257,    2, 0x0a /* Public */,
      34,    0,  258,    2, 0x0a /* Public */,
      35,    1,  259,    2, 0x0a /* Public */,
      37,    0,  262,    2, 0x8a /* Public | MethodRevisioned */,
      39,    1,  263,    2, 0x08 /* Private */,
      41,    1,  266,    2, 0x08 /* Private */,
      43,    0,  269,    2, 0x08 /* Private */,
      44,    1,  270,    2, 0x08 /* Private */,

 // signals: revision
       1,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       3,
       0,
       0,
       0,
       0,
       2,

 // slots: revision
       0,
       0,
       0,
       0,
       1,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 28, QMetaType::QString,   27,   29,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   36,
    0x80000000 | 38,
    QMetaType::Void, 0x80000000 | 40,    2,
    QMetaType::Void, 0x80000000 | 42,    2,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 45,    2,

 // properties: name, type, flags
      46, QMetaType::QUrl, 0x00495103,
      47, 0x80000000 | 48, 0x00c9510b,
      49, QMetaType::Int, 0x00495003,
      50, 0x80000000 | 51, 0x00495009,
      52, QMetaType::Bool, 0x00495103,
      53, QMetaType::Bool, 0x00495103,
      54, 0x80000000 | 55, 0x00495009,
      56, QMetaType::Int, 0x00495001,
      36, QMetaType::Int, 0x00495001,
      57, QMetaType::QReal, 0x00495103,
      58, QMetaType::Bool, 0x00495103,
      59, QMetaType::Bool, 0x00495001,
      60, QMetaType::Bool, 0x00495001,
      61, QMetaType::QReal, 0x00495001,
      62, QMetaType::Bool, 0x00495001,
      63, QMetaType::QReal, 0x00495103,
      27, 0x80000000 | 64, 0x00495009,
      29, QMetaType::QString, 0x00495001,
      65, 0x80000000 | 66, 0x00095409,
      67, QMetaType::QObjectStar, 0x00490001,
      25, 0x80000000 | 24, 0x00495009,
      68, 0x80000000 | 69, 0x00c9510b,
      70, QMetaType::QString, 0x00c95103,
      71, QMetaType::Int, 0x00c95103,

 // properties: notify_signal_id
       1,
       0,
       3,
       4,
       5,
       2,
       9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      22,
      22,
       0,
      24,
      21,
      19,
      20,
      25,

 // properties: revision
       0,
       1,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       3,
       2,

 // enums: name, alias, flags, count, data
      55,   55, 0x0,    9,  423,
      64,   64, 0x0,    6,  441,
      87,   87, 0x0,    1,  453,
      51,   51, 0x0,    3,  455,
      24,   24, 0x0,    4,  461,
      69,   69, 0x0,   11,  469,

 // enum data: key, value
      72, uint(QDeclarativeAudio::UnknownStatus),
      73, uint(QDeclarativeAudio::NoMedia),
      74, uint(QDeclarativeAudio::Loading),
      75, uint(QDeclarativeAudio::Loaded),
      76, uint(QDeclarativeAudio::Stalled),
      77, uint(QDeclarativeAudio::Buffering),
      78, uint(QDeclarativeAudio::Buffered),
      79, uint(QDeclarativeAudio::EndOfMedia),
      80, uint(QDeclarativeAudio::InvalidMedia),
      81, uint(QDeclarativeAudio::NoError),
      82, uint(QDeclarativeAudio::ResourceError),
      83, uint(QDeclarativeAudio::FormatError),
      84, uint(QDeclarativeAudio::NetworkError),
      85, uint(QDeclarativeAudio::AccessDenied),
      86, uint(QDeclarativeAudio::ServiceMissing),
      88, uint(QDeclarativeAudio::Infinite),
      89, uint(QDeclarativeAudio::PlayingState),
      90, uint(QDeclarativeAudio::PausedState),
      91, uint(QDeclarativeAudio::StoppedState),
      92, uint(QDeclarativeAudio::Available),
      93, uint(QDeclarativeAudio::Busy),
      94, uint(QDeclarativeAudio::Unavailable),
      95, uint(QDeclarativeAudio::ResourceMissing),
      96, uint(QDeclarativeAudio::UnknownRole),
      97, uint(QDeclarativeAudio::AccessibilityRole),
      98, uint(QDeclarativeAudio::AlarmRole),
      99, uint(QDeclarativeAudio::CustomRole),
     100, uint(QDeclarativeAudio::GameRole),
     101, uint(QDeclarativeAudio::MusicRole),
     102, uint(QDeclarativeAudio::NotificationRole),
     103, uint(QDeclarativeAudio::RingtoneRole),
     104, uint(QDeclarativeAudio::SonificationRole),
     105, uint(QDeclarativeAudio::VideoRole),
     106, uint(QDeclarativeAudio::VoiceCommunicationRole),

       0        // eod
};

void QDeclarativeAudio::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->playlistChanged(); break;
        case 1: _t->sourceChanged(); break;
        case 2: _t->autoLoadChanged(); break;
        case 3: _t->loopCountChanged(); break;
        case 4: _t->playbackStateChanged(); break;
        case 5: _t->autoPlayChanged(); break;
        case 6: _t->paused(); break;
        case 7: _t->stopped(); break;
        case 8: _t->playing(); break;
        case 9: _t->statusChanged(); break;
        case 10: _t->durationChanged(); break;
        case 11: _t->positionChanged(); break;
        case 12: _t->volumeChanged(); break;
        case 13: _t->mutedChanged(); break;
        case 14: _t->hasAudioChanged(); break;
        case 15: _t->hasVideoChanged(); break;
        case 16: _t->bufferProgressChanged(); break;
        case 17: _t->seekableChanged(); break;
        case 18: _t->playbackRateChanged(); break;
        case 19: _t->audioRoleChanged(); break;
        case 20: _t->customAudioRoleChanged(); break;
        case 21: _t->availabilityChanged((*reinterpret_cast< Availability(*)>(_a[1]))); break;
        case 22: _t->errorChanged(); break;
        case 23: _t->error((*reinterpret_cast< QDeclarativeAudio::Error(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 24: _t->mediaObjectChanged(); break;
        case 25: _t->notifyIntervalChanged(); break;
        case 26: _t->play(); break;
        case 27: _t->pause(); break;
        case 28: _t->stop(); break;
        case 29: _t->seek((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 30: { QJSValue _r = _t->supportedAudioRoles();
            if (_a[0]) *reinterpret_cast< QJSValue*>(_a[0]) = std::move(_r); }  break;
        case 31: _t->_q_error((*reinterpret_cast< QMediaPlayer::Error(*)>(_a[1]))); break;
        case 32: _t->_q_availabilityChanged((*reinterpret_cast< QMultimedia::AvailabilityStatus(*)>(_a[1]))); break;
        case 33: _t->_q_statusChanged(); break;
        case 34: _t->_q_mediaChanged((*reinterpret_cast< const QMediaContent(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 31:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaPlayer::Error >(); break;
            }
            break;
        case 32:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMultimedia::AvailabilityStatus >(); break;
            }
            break;
        case 34:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QMediaContent >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::playlistChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::sourceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::autoLoadChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::loopCountChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::playbackStateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::autoPlayChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::paused)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::stopped)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::playing)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::statusChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::durationChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::positionChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::volumeChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::mutedChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::hasAudioChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::hasVideoChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::bufferProgressChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::seekableChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::playbackRateChanged)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::audioRoleChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::customAudioRoleChanged)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)(Availability );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::availabilityChanged)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::errorChanged)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)(QDeclarativeAudio::Error , const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::error)) {
                *result = 23;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::mediaObjectChanged)) {
                *result = 24;
                return;
            }
        }
        {
            using _t = void (QDeclarativeAudio::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeAudio::notifyIntervalChanged)) {
                *result = 25;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QUrl*>(_v) = _t->source(); break;
        case 1: *reinterpret_cast< QDeclarativePlaylist**>(_v) = _t->playlist(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->loopCount(); break;
        case 3: *reinterpret_cast< PlaybackState*>(_v) = _t->playbackState(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->autoPlay(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->isAutoLoad(); break;
        case 6: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->duration(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->position(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->volume(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->isMuted(); break;
        case 11: *reinterpret_cast< bool*>(_v) = _t->hasAudio(); break;
        case 12: *reinterpret_cast< bool*>(_v) = _t->hasVideo(); break;
        case 13: *reinterpret_cast< qreal*>(_v) = _t->bufferProgress(); break;
        case 14: *reinterpret_cast< bool*>(_v) = _t->isSeekable(); break;
        case 15: *reinterpret_cast< qreal*>(_v) = _t->playbackRate(); break;
        case 16: *reinterpret_cast< Error*>(_v) = _t->error(); break;
        case 17: *reinterpret_cast< QString*>(_v) = _t->errorString(); break;
        case 18: *reinterpret_cast< QDeclarativeMediaMetaData**>(_v) = _t->metaData(); break;
        case 19: *reinterpret_cast< QObject**>(_v) = _t->mediaObject(); break;
        case 20: *reinterpret_cast< Availability*>(_v) = _t->availability(); break;
        case 21: *reinterpret_cast< AudioRole*>(_v) = _t->audioRole(); break;
        case 22: *reinterpret_cast< QString*>(_v) = _t->customAudioRole(); break;
        case 23: *reinterpret_cast< int*>(_v) = _t->notifyInterval(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeAudio *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSource(*reinterpret_cast< QUrl*>(_v)); break;
        case 1: _t->setPlaylist(*reinterpret_cast< QDeclarativePlaylist**>(_v)); break;
        case 2: _t->setLoopCount(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setAutoPlay(*reinterpret_cast< bool*>(_v)); break;
        case 5: _t->setAutoLoad(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setVolume(*reinterpret_cast< qreal*>(_v)); break;
        case 10: _t->setMuted(*reinterpret_cast< bool*>(_v)); break;
        case 15: _t->setPlaybackRate(*reinterpret_cast< qreal*>(_v)); break;
        case 21: _t->setAudioRole(*reinterpret_cast< AudioRole*>(_v)); break;
        case 22: _t->setCustomAudioRole(*reinterpret_cast< QString*>(_v)); break;
        case 23: _t->setNotifyInterval(*reinterpret_cast< int*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeAudio::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeAudio.data,
    qt_meta_data_QDeclarativeAudio,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeAudio::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeAudio::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeAudio.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeAudio::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 35)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 35;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 24;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeAudio::playlistChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeAudio::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeAudio::autoLoadChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeAudio::loopCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QDeclarativeAudio::playbackStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeAudio::autoPlayChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeAudio::paused()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeAudio::stopped()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeAudio::playing()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeAudio::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QDeclarativeAudio::durationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void QDeclarativeAudio::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void QDeclarativeAudio::volumeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void QDeclarativeAudio::mutedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 13, nullptr);
}

// SIGNAL 14
void QDeclarativeAudio::hasAudioChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 14, nullptr);
}

// SIGNAL 15
void QDeclarativeAudio::hasVideoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 15, nullptr);
}

// SIGNAL 16
void QDeclarativeAudio::bufferProgressChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 16, nullptr);
}

// SIGNAL 17
void QDeclarativeAudio::seekableChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 17, nullptr);
}

// SIGNAL 18
void QDeclarativeAudio::playbackRateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 18, nullptr);
}

// SIGNAL 19
void QDeclarativeAudio::audioRoleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 19, nullptr);
}

// SIGNAL 20
void QDeclarativeAudio::customAudioRoleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 20, nullptr);
}

// SIGNAL 21
void QDeclarativeAudio::availabilityChanged(Availability _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void QDeclarativeAudio::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void QDeclarativeAudio::error(QDeclarativeAudio::Error _t1, const QString & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 23, _a);
}

// SIGNAL 24
void QDeclarativeAudio::mediaObjectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 24, nullptr);
}

// SIGNAL 25
void QDeclarativeAudio::notifyIntervalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 25, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
