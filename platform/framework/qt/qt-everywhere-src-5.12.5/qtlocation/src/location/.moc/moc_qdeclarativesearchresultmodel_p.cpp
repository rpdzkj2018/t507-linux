/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativesearchresultmodel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativeplaces/qdeclarativesearchresultmodel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativesearchresultmodel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeSearchResultModel_t {
    QByteArrayData data[45];
    char stringdata0[758];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeSearchResultModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeSearchResultModel_t qt_meta_stringdata_QDeclarativeSearchResultModel = {
    {
QT_MOC_LITERAL(0, 0, 29), // "QDeclarativeSearchResultModel"
QT_MOC_LITERAL(1, 30, 17), // "searchTermChanged"
QT_MOC_LITERAL(2, 48, 0), // ""
QT_MOC_LITERAL(3, 49, 17), // "categoriesChanged"
QT_MOC_LITERAL(4, 67, 23), // "recommendationIdChanged"
QT_MOC_LITERAL(5, 91, 20), // "relevanceHintChanged"
QT_MOC_LITERAL(6, 112, 22), // "visibilityScopeChanged"
QT_MOC_LITERAL(7, 135, 15), // "rowCountChanged"
QT_MOC_LITERAL(8, 151, 22), // "favoritesPluginChanged"
QT_MOC_LITERAL(9, 174, 31), // "favoritesMatchParametersChanged"
QT_MOC_LITERAL(10, 206, 11), // "dataChanged"
QT_MOC_LITERAL(11, 218, 18), // "incrementalChanged"
QT_MOC_LITERAL(12, 237, 13), // "queryFinished"
QT_MOC_LITERAL(13, 251, 16), // "onContentUpdated"
QT_MOC_LITERAL(14, 268, 12), // "updateLayout"
QT_MOC_LITERAL(15, 281, 13), // "QList<QPlace>"
QT_MOC_LITERAL(16, 295, 14), // "favoritePlaces"
QT_MOC_LITERAL(17, 310, 12), // "placeUpdated"
QT_MOC_LITERAL(18, 323, 7), // "placeId"
QT_MOC_LITERAL(19, 331, 12), // "placeRemoved"
QT_MOC_LITERAL(20, 344, 4), // "data"
QT_MOC_LITERAL(21, 349, 5), // "index"
QT_MOC_LITERAL(22, 355, 8), // "roleName"
QT_MOC_LITERAL(23, 364, 10), // "updateWith"
QT_MOC_LITERAL(24, 375, 19), // "proposedSearchIndex"
QT_MOC_LITERAL(25, 395, 10), // "searchTerm"
QT_MOC_LITERAL(26, 406, 10), // "categories"
QT_MOC_LITERAL(27, 417, 38), // "QQmlListProperty<QDeclarative..."
QT_MOC_LITERAL(28, 456, 16), // "recommendationId"
QT_MOC_LITERAL(29, 473, 13), // "relevanceHint"
QT_MOC_LITERAL(30, 487, 13), // "RelevanceHint"
QT_MOC_LITERAL(31, 501, 15), // "visibilityScope"
QT_MOC_LITERAL(32, 517, 29), // "QDeclarativePlace::Visibility"
QT_MOC_LITERAL(33, 547, 5), // "count"
QT_MOC_LITERAL(34, 553, 15), // "favoritesPlugin"
QT_MOC_LITERAL(35, 569, 31), // "QDeclarativeGeoServiceProvider*"
QT_MOC_LITERAL(36, 601, 24), // "favoritesMatchParameters"
QT_MOC_LITERAL(37, 626, 11), // "incremental"
QT_MOC_LITERAL(38, 638, 16), // "SearchResultType"
QT_MOC_LITERAL(39, 655, 19), // "UnknownSearchResult"
QT_MOC_LITERAL(40, 675, 11), // "PlaceResult"
QT_MOC_LITERAL(41, 687, 20), // "ProposedSearchResult"
QT_MOC_LITERAL(42, 708, 15), // "UnspecifiedHint"
QT_MOC_LITERAL(43, 724, 12), // "DistanceHint"
QT_MOC_LITERAL(44, 737, 20) // "LexicalPlaceNameHint"

    },
    "QDeclarativeSearchResultModel\0"
    "searchTermChanged\0\0categoriesChanged\0"
    "recommendationIdChanged\0relevanceHintChanged\0"
    "visibilityScopeChanged\0rowCountChanged\0"
    "favoritesPluginChanged\0"
    "favoritesMatchParametersChanged\0"
    "dataChanged\0incrementalChanged\0"
    "queryFinished\0onContentUpdated\0"
    "updateLayout\0QList<QPlace>\0favoritePlaces\0"
    "placeUpdated\0placeId\0placeRemoved\0"
    "data\0index\0roleName\0updateWith\0"
    "proposedSearchIndex\0searchTerm\0"
    "categories\0QQmlListProperty<QDeclarativeCategory>\0"
    "recommendationId\0relevanceHint\0"
    "RelevanceHint\0visibilityScope\0"
    "QDeclarativePlace::Visibility\0count\0"
    "favoritesPlugin\0QDeclarativeGeoServiceProvider*\0"
    "favoritesMatchParameters\0incremental\0"
    "SearchResultType\0UnknownSearchResult\0"
    "PlaceResult\0ProposedSearchResult\0"
    "UnspecifiedHint\0DistanceHint\0"
    "LexicalPlaceNameHint"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeSearchResultModel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       9,  134, // properties
       2,  179, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06 /* Public */,
       3,    0,  105,    2, 0x06 /* Public */,
       4,    0,  106,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,
       8,    0,  110,    2, 0x06 /* Public */,
       9,    0,  111,    2, 0x06 /* Public */,
      10,    0,  112,    2, 0x06 /* Public */,
      11,    0,  113,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,  114,    2, 0x09 /* Protected */,
      13,    0,  115,    2, 0x09 /* Protected */,
      14,    1,  116,    2, 0x08 /* Private */,
      14,    0,  119,    2, 0x28 /* Private | MethodCloned */,
      17,    1,  120,    2, 0x08 /* Private */,
      19,    1,  123,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      20,    2,  126,    2, 0x02 /* Public */,
      23,    1,  131,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,   16,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void, QMetaType::QString,   18,

 // methods: parameters
    QMetaType::QVariant, QMetaType::Int, QMetaType::QString,   21,   22,
    QMetaType::Void, QMetaType::Int,   24,

 // properties: name, type, flags
      25, QMetaType::QString, 0x00495103,
      26, 0x80000000 | 27, 0x00495009,
      28, QMetaType::QString, 0x00495103,
      29, 0x80000000 | 30, 0x0049510b,
      31, 0x80000000 | 32, 0x0049510b,
      33, QMetaType::Int, 0x00495001,
      34, 0x80000000 | 35, 0x0049510b,
      36, QMetaType::QVariantMap, 0x00495103,
      37, QMetaType::Bool, 0x00c95003,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       9,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      12,

 // enums: name, alias, flags, count, data
      38,   38, 0x0,    3,  189,
      30,   30, 0x0,    3,  195,

 // enum data: key, value
      39, uint(QDeclarativeSearchResultModel::UnknownSearchResult),
      40, uint(QDeclarativeSearchResultModel::PlaceResult),
      41, uint(QDeclarativeSearchResultModel::ProposedSearchResult),
      42, uint(QDeclarativeSearchResultModel::UnspecifiedHint),
      43, uint(QDeclarativeSearchResultModel::DistanceHint),
      44, uint(QDeclarativeSearchResultModel::LexicalPlaceNameHint),

       0        // eod
};

void QDeclarativeSearchResultModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeSearchResultModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->searchTermChanged(); break;
        case 1: _t->categoriesChanged(); break;
        case 2: _t->recommendationIdChanged(); break;
        case 3: _t->relevanceHintChanged(); break;
        case 4: _t->visibilityScopeChanged(); break;
        case 5: _t->rowCountChanged(); break;
        case 6: _t->favoritesPluginChanged(); break;
        case 7: _t->favoritesMatchParametersChanged(); break;
        case 8: _t->dataChanged(); break;
        case 9: _t->incrementalChanged(); break;
        case 10: _t->queryFinished(); break;
        case 11: _t->onContentUpdated(); break;
        case 12: _t->updateLayout((*reinterpret_cast< const QList<QPlace>(*)>(_a[1]))); break;
        case 13: _t->updateLayout(); break;
        case 14: _t->placeUpdated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->placeRemoved((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: { QVariant _r = _t->data((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QVariant*>(_a[0]) = std::move(_r); }  break;
        case 17: _t->updateWith((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QPlace> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::searchTermChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::categoriesChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::recommendationIdChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::relevanceHintChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::visibilityScopeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::rowCountChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::favoritesPluginChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::favoritesMatchParametersChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::dataChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeSearchResultModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeSearchResultModel::incrementalChanged)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoServiceProvider* >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QDeclarativeCategory> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeSearchResultModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->searchTerm(); break;
        case 1: *reinterpret_cast< QQmlListProperty<QDeclarativeCategory>*>(_v) = _t->categories(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->recommendationId(); break;
        case 3: *reinterpret_cast< RelevanceHint*>(_v) = _t->relevanceHint(); break;
        case 4: *reinterpret_cast< QDeclarativePlace::Visibility*>(_v) = _t->visibilityScope(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->rowCount(); break;
        case 6: *reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v) = _t->favoritesPlugin(); break;
        case 7: *reinterpret_cast< QVariantMap*>(_v) = _t->favoritesMatchParameters(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->m_incremental; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeSearchResultModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSearchTerm(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setRecommendationId(*reinterpret_cast< QString*>(_v)); break;
        case 3: _t->setRelevanceHint(*reinterpret_cast< RelevanceHint*>(_v)); break;
        case 4: _t->setVisibilityScope(*reinterpret_cast< QDeclarativePlace::Visibility*>(_v)); break;
        case 6: _t->setFavoritesPlugin(*reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v)); break;
        case 7: _t->setFavoritesMatchParameters(*reinterpret_cast< QVariantMap*>(_v)); break;
        case 8:
            if (_t->m_incremental != *reinterpret_cast< bool*>(_v)) {
                _t->m_incremental = *reinterpret_cast< bool*>(_v);
                Q_EMIT _t->incrementalChanged();
            }
            break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QDeclarativeSearchResultModel[] = {
        &QDeclarativePlace::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QDeclarativeSearchResultModel::staticMetaObject = { {
    &QDeclarativeSearchModelBase::staticMetaObject,
    qt_meta_stringdata_QDeclarativeSearchResultModel.data,
    qt_meta_data_QDeclarativeSearchResultModel,
    qt_static_metacall,
    qt_meta_extradata_QDeclarativeSearchResultModel,
    nullptr
} };


const QMetaObject *QDeclarativeSearchResultModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeSearchResultModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeSearchResultModel.stringdata0))
        return static_cast<void*>(this);
    return QDeclarativeSearchModelBase::qt_metacast(_clname);
}

int QDeclarativeSearchResultModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDeclarativeSearchModelBase::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeSearchResultModel::searchTermChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeSearchResultModel::categoriesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeSearchResultModel::recommendationIdChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeSearchResultModel::relevanceHintChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QDeclarativeSearchResultModel::visibilityScopeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeSearchResultModel::rowCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeSearchResultModel::favoritesPluginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeSearchResultModel::favoritesMatchParametersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeSearchResultModel::dataChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeSearchResultModel::incrementalChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
