/****************************************************************************
** Meta object code from reading C++ file 'qremoteobjectabstractitemmodelreplica_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qremoteobjectabstractitemmodelreplica_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qremoteobjectabstractitemmodelreplica_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SizeWatcher_t {
    QByteArrayData data[1];
    char stringdata0[12];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SizeWatcher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SizeWatcher_t qt_meta_stringdata_SizeWatcher = {
    {
QT_MOC_LITERAL(0, 0, 11) // "SizeWatcher"

    },
    "SizeWatcher"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SizeWatcher[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void SizeWatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject SizeWatcher::staticMetaObject = { {
    &QRemoteObjectPendingCallWatcher::staticMetaObject,
    qt_meta_stringdata_SizeWatcher.data,
    qt_meta_data_SizeWatcher,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SizeWatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SizeWatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SizeWatcher.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectPendingCallWatcher::qt_metacast(_clname);
}

int SizeWatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectPendingCallWatcher::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_RowWatcher_t {
    QByteArrayData data[1];
    char stringdata0[11];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_RowWatcher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_RowWatcher_t qt_meta_stringdata_RowWatcher = {
    {
QT_MOC_LITERAL(0, 0, 10) // "RowWatcher"

    },
    "RowWatcher"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_RowWatcher[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void RowWatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject RowWatcher::staticMetaObject = { {
    &QRemoteObjectPendingCallWatcher::staticMetaObject,
    qt_meta_stringdata_RowWatcher.data,
    qt_meta_data_RowWatcher,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *RowWatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *RowWatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_RowWatcher.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectPendingCallWatcher::qt_metacast(_clname);
}

int RowWatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectPendingCallWatcher::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_HeaderWatcher_t {
    QByteArrayData data[1];
    char stringdata0[14];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_HeaderWatcher_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_HeaderWatcher_t qt_meta_stringdata_HeaderWatcher = {
    {
QT_MOC_LITERAL(0, 0, 13) // "HeaderWatcher"

    },
    "HeaderWatcher"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_HeaderWatcher[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void HeaderWatcher::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject HeaderWatcher::staticMetaObject = { {
    &QRemoteObjectPendingCallWatcher::staticMetaObject,
    qt_meta_stringdata_HeaderWatcher.data,
    qt_meta_data_HeaderWatcher,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *HeaderWatcher::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *HeaderWatcher::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_HeaderWatcher.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectPendingCallWatcher::qt_metacast(_clname);
}

int HeaderWatcher::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectPendingCallWatcher::qt_metacall(_c, _id, _a);
    return _id;
}
struct qt_meta_stringdata_QAbstractItemModelReplicaImplementation_t {
    QByteArrayData data[81];
    char stringdata0[1186];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QAbstractItemModelReplicaImplementation_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QAbstractItemModelReplicaImplementation_t qt_meta_stringdata_QAbstractItemModelReplicaImplementation = {
    {
QT_MOC_LITERAL(0, 0, 39), // "QAbstractItemModelReplicaImpl..."
QT_MOC_LITERAL(1, 40, 17), // "RemoteObject Type"
QT_MOC_LITERAL(2, 58, 18), // "ServerModelAdapter"
QT_MOC_LITERAL(3, 77, 21), // "availableRolesChanged"
QT_MOC_LITERAL(4, 99, 0), // ""
QT_MOC_LITERAL(5, 100, 11), // "dataChanged"
QT_MOC_LITERAL(6, 112, 9), // "IndexList"
QT_MOC_LITERAL(7, 122, 7), // "topLeft"
QT_MOC_LITERAL(8, 130, 11), // "bottomRight"
QT_MOC_LITERAL(9, 142, 12), // "QVector<int>"
QT_MOC_LITERAL(10, 155, 5), // "roles"
QT_MOC_LITERAL(11, 161, 12), // "rowsInserted"
QT_MOC_LITERAL(12, 174, 6), // "parent"
QT_MOC_LITERAL(13, 181, 5), // "first"
QT_MOC_LITERAL(14, 187, 4), // "last"
QT_MOC_LITERAL(15, 192, 11), // "rowsRemoved"
QT_MOC_LITERAL(16, 204, 9), // "rowsMoved"
QT_MOC_LITERAL(17, 214, 5), // "start"
QT_MOC_LITERAL(18, 220, 3), // "end"
QT_MOC_LITERAL(19, 224, 11), // "destination"
QT_MOC_LITERAL(20, 236, 3), // "row"
QT_MOC_LITERAL(21, 240, 14), // "currentChanged"
QT_MOC_LITERAL(22, 255, 7), // "current"
QT_MOC_LITERAL(23, 263, 8), // "previous"
QT_MOC_LITERAL(24, 272, 10), // "modelReset"
QT_MOC_LITERAL(25, 283, 17), // "headerDataChanged"
QT_MOC_LITERAL(26, 301, 15), // "Qt::Orientation"
QT_MOC_LITERAL(27, 317, 15), // "columnsInserted"
QT_MOC_LITERAL(28, 333, 18), // "replicaSizeRequest"
QT_MOC_LITERAL(29, 352, 32), // "QRemoteObjectPendingReply<QSize>"
QT_MOC_LITERAL(30, 385, 10), // "parentList"
QT_MOC_LITERAL(31, 396, 17), // "replicaRowRequest"
QT_MOC_LITERAL(32, 414, 38), // "QRemoteObjectPendingReply<Dat..."
QT_MOC_LITERAL(33, 453, 20), // "replicaHeaderRequest"
QT_MOC_LITERAL(34, 474, 39), // "QRemoteObjectPendingReply<QVa..."
QT_MOC_LITERAL(35, 514, 24), // "QVector<Qt::Orientation>"
QT_MOC_LITERAL(36, 539, 12), // "orientations"
QT_MOC_LITERAL(37, 552, 8), // "sections"
QT_MOC_LITERAL(38, 561, 22), // "replicaSetCurrentIndex"
QT_MOC_LITERAL(39, 584, 5), // "index"
QT_MOC_LITERAL(40, 590, 35), // "QItemSelectionModel::Selectio..."
QT_MOC_LITERAL(41, 626, 7), // "command"
QT_MOC_LITERAL(42, 634, 14), // "replicaSetData"
QT_MOC_LITERAL(43, 649, 5), // "value"
QT_MOC_LITERAL(44, 655, 4), // "role"
QT_MOC_LITERAL(45, 660, 19), // "replicaCacheRequest"
QT_MOC_LITERAL(46, 680, 45), // "QRemoteObjectPendingReply<Met..."
QT_MOC_LITERAL(47, 726, 6), // "size_t"
QT_MOC_LITERAL(48, 733, 4), // "size"
QT_MOC_LITERAL(49, 738, 19), // "onHeaderDataChanged"
QT_MOC_LITERAL(50, 758, 11), // "orientation"
QT_MOC_LITERAL(51, 770, 13), // "onDataChanged"
QT_MOC_LITERAL(52, 784, 14), // "onRowsInserted"
QT_MOC_LITERAL(53, 799, 13), // "onRowsRemoved"
QT_MOC_LITERAL(54, 813, 17), // "onColumnsInserted"
QT_MOC_LITERAL(55, 831, 11), // "onRowsMoved"
QT_MOC_LITERAL(56, 843, 9), // "srcParent"
QT_MOC_LITERAL(57, 853, 6), // "srcRow"
QT_MOC_LITERAL(58, 860, 5), // "count"
QT_MOC_LITERAL(59, 866, 10), // "destParent"
QT_MOC_LITERAL(60, 877, 7), // "destRow"
QT_MOC_LITERAL(61, 885, 16), // "onCurrentChanged"
QT_MOC_LITERAL(62, 902, 12), // "onModelReset"
QT_MOC_LITERAL(63, 915, 13), // "requestedData"
QT_MOC_LITERAL(64, 929, 32), // "QRemoteObjectPendingCallWatcher*"
QT_MOC_LITERAL(65, 962, 19), // "requestedHeaderData"
QT_MOC_LITERAL(66, 982, 4), // "init"
QT_MOC_LITERAL(67, 987, 16), // "fetchPendingData"
QT_MOC_LITERAL(68, 1004, 22), // "fetchPendingHeaderData"
QT_MOC_LITERAL(69, 1027, 14), // "handleInitDone"
QT_MOC_LITERAL(70, 1042, 7), // "watcher"
QT_MOC_LITERAL(71, 1050, 20), // "handleModelResetDone"
QT_MOC_LITERAL(72, 1071, 14), // "handleSizeDone"
QT_MOC_LITERAL(73, 1086, 23), // "onReplicaCurrentChanged"
QT_MOC_LITERAL(74, 1110, 11), // "QModelIndex"
QT_MOC_LITERAL(75, 1122, 9), // "fillCache"
QT_MOC_LITERAL(76, 1132, 14), // "IndexValuePair"
QT_MOC_LITERAL(77, 1147, 4), // "pair"
QT_MOC_LITERAL(78, 1152, 14), // "availableRoles"
QT_MOC_LITERAL(79, 1167, 9), // "roleNames"
QT_MOC_LITERAL(80, 1177, 8) // "QIntHash"

    },
    "QAbstractItemModelReplicaImplementation\0"
    "RemoteObject Type\0ServerModelAdapter\0"
    "availableRolesChanged\0\0dataChanged\0"
    "IndexList\0topLeft\0bottomRight\0"
    "QVector<int>\0roles\0rowsInserted\0parent\0"
    "first\0last\0rowsRemoved\0rowsMoved\0start\0"
    "end\0destination\0row\0currentChanged\0"
    "current\0previous\0modelReset\0"
    "headerDataChanged\0Qt::Orientation\0"
    "columnsInserted\0replicaSizeRequest\0"
    "QRemoteObjectPendingReply<QSize>\0"
    "parentList\0replicaRowRequest\0"
    "QRemoteObjectPendingReply<DataEntries>\0"
    "replicaHeaderRequest\0"
    "QRemoteObjectPendingReply<QVariantList>\0"
    "QVector<Qt::Orientation>\0orientations\0"
    "sections\0replicaSetCurrentIndex\0index\0"
    "QItemSelectionModel::SelectionFlags\0"
    "command\0replicaSetData\0value\0role\0"
    "replicaCacheRequest\0"
    "QRemoteObjectPendingReply<MetaAndDataEntries>\0"
    "size_t\0size\0onHeaderDataChanged\0"
    "orientation\0onDataChanged\0onRowsInserted\0"
    "onRowsRemoved\0onColumnsInserted\0"
    "onRowsMoved\0srcParent\0srcRow\0count\0"
    "destParent\0destRow\0onCurrentChanged\0"
    "onModelReset\0requestedData\0"
    "QRemoteObjectPendingCallWatcher*\0"
    "requestedHeaderData\0init\0fetchPendingData\0"
    "fetchPendingHeaderData\0handleInitDone\0"
    "watcher\0handleModelResetDone\0"
    "handleSizeDone\0onReplicaCurrentChanged\0"
    "QModelIndex\0fillCache\0IndexValuePair\0"
    "pair\0availableRoles\0roleNames\0QIntHash"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QAbstractItemModelReplicaImplementation[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      33,   16, // methods
       2,  348, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  181,    4, 0x06 /* Public */,
       5,    3,  182,    4, 0x06 /* Public */,
      11,    3,  189,    4, 0x06 /* Public */,
      15,    3,  196,    4, 0x06 /* Public */,
      16,    5,  203,    4, 0x06 /* Public */,
      21,    2,  214,    4, 0x06 /* Public */,
      24,    0,  219,    4, 0x06 /* Public */,
      25,    3,  220,    4, 0x06 /* Public */,
      27,    3,  227,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      28,    1,  234,    4, 0x0a /* Public */,
      31,    3,  237,    4, 0x0a /* Public */,
      33,    3,  244,    4, 0x0a /* Public */,
      38,    2,  251,    4, 0x0a /* Public */,
      42,    3,  256,    4, 0x0a /* Public */,
      45,    2,  263,    4, 0x0a /* Public */,
      49,    3,  268,    4, 0x0a /* Public */,
      51,    3,  275,    4, 0x0a /* Public */,
      52,    3,  282,    4, 0x0a /* Public */,
      53,    3,  289,    4, 0x0a /* Public */,
      54,    3,  296,    4, 0x0a /* Public */,
      55,    5,  303,    4, 0x0a /* Public */,
      61,    2,  314,    4, 0x0a /* Public */,
      62,    0,  319,    4, 0x0a /* Public */,
      63,    1,  320,    4, 0x0a /* Public */,
      65,    1,  323,    4, 0x0a /* Public */,
      66,    0,  326,    4, 0x0a /* Public */,
      67,    0,  327,    4, 0x0a /* Public */,
      68,    0,  328,    4, 0x0a /* Public */,
      69,    1,  329,    4, 0x0a /* Public */,
      71,    1,  332,    4, 0x0a /* Public */,
      72,    1,  335,    4, 0x0a /* Public */,
      73,    2,  338,    4, 0x0a /* Public */,
      75,    2,  343,    4, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 9,    7,    8,   10,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   13,   14,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int, 0x80000000 | 6, QMetaType::Int,   12,   17,   18,   19,   20,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6,   22,   23,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 26, QMetaType::Int, QMetaType::Int,    4,    4,    4,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   13,   14,

 // slots: parameters
    0x80000000 | 29, 0x80000000 | 6,   30,
    0x80000000 | 32, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 9,   17,   18,   10,
    0x80000000 | 34, 0x80000000 | 35, 0x80000000 | 9, 0x80000000 | 9,   36,   37,   10,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 40,   39,   41,
    QMetaType::Void, 0x80000000 | 6, QMetaType::QVariant, QMetaType::Int,   39,   43,   44,
    0x80000000 | 46, 0x80000000 | 47, 0x80000000 | 9,   48,   10,
    QMetaType::Void, 0x80000000 | 26, QMetaType::Int, QMetaType::Int,   50,   13,   14,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 9,   17,   18,   10,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   17,   18,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   17,   18,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,   12,   17,   18,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int, 0x80000000 | 6, QMetaType::Int,   56,   57,   58,   59,   60,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6,   22,   23,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 64,    4,
    QMetaType::Void, 0x80000000 | 64,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 64,   70,
    QMetaType::Void, 0x80000000 | 64,   70,
    QMetaType::Void, 0x80000000 | 64,   70,
    QMetaType::Void, 0x80000000 | 74, 0x80000000 | 74,   22,   23,
    QMetaType::Void, 0x80000000 | 76, 0x80000000 | 9,   77,   10,

 // properties: name, type, flags
      78, 0x80000000 | 9, 0x00495009,
      79, 0x80000000 | 80, 0x00095009,

 // properties: notify_signal_id
       0,
       0,

       0        // eod
};

void QAbstractItemModelReplicaImplementation::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QAbstractItemModelReplicaImplementation *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->availableRolesChanged(); break;
        case 1: _t->dataChanged((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3]))); break;
        case 2: _t->rowsInserted((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 3: _t->rowsRemoved((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->rowsMoved((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< IndexList(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 5: _t->currentChanged((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2]))); break;
        case 6: _t->modelReset(); break;
        case 7: _t->headerDataChanged((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 8: _t->columnsInserted((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 9: { QRemoteObjectPendingReply<QSize> _r = _t->replicaSizeRequest((*reinterpret_cast< IndexList(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QRemoteObjectPendingReply<QSize>*>(_a[0]) = std::move(_r); }  break;
        case 10: { QRemoteObjectPendingReply<DataEntries> _r = _t->replicaRowRequest((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QRemoteObjectPendingReply<DataEntries>*>(_a[0]) = std::move(_r); }  break;
        case 11: { QRemoteObjectPendingReply<QVariantList> _r = _t->replicaHeaderRequest((*reinterpret_cast< QVector<Qt::Orientation>(*)>(_a[1])),(*reinterpret_cast< QVector<int>(*)>(_a[2])),(*reinterpret_cast< QVector<int>(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QRemoteObjectPendingReply<QVariantList>*>(_a[0]) = std::move(_r); }  break;
        case 12: _t->replicaSetCurrentIndex((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< QItemSelectionModel::SelectionFlags(*)>(_a[2]))); break;
        case 13: _t->replicaSetData((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< const QVariant(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 14: { QRemoteObjectPendingReply<MetaAndDataEntries> _r = _t->replicaCacheRequest((*reinterpret_cast< size_t(*)>(_a[1])),(*reinterpret_cast< QVector<int>(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QRemoteObjectPendingReply<MetaAndDataEntries>*>(_a[0]) = std::move(_r); }  break;
        case 15: _t->onHeaderDataChanged((*reinterpret_cast< Qt::Orientation(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 16: _t->onDataChanged((*reinterpret_cast< const IndexList(*)>(_a[1])),(*reinterpret_cast< const IndexList(*)>(_a[2])),(*reinterpret_cast< const QVector<int>(*)>(_a[3]))); break;
        case 17: _t->onRowsInserted((*reinterpret_cast< const IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 18: _t->onRowsRemoved((*reinterpret_cast< const IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 19: _t->onColumnsInserted((*reinterpret_cast< const IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 20: _t->onRowsMoved((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< IndexList(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 21: _t->onCurrentChanged((*reinterpret_cast< IndexList(*)>(_a[1])),(*reinterpret_cast< IndexList(*)>(_a[2]))); break;
        case 22: _t->onModelReset(); break;
        case 23: _t->requestedData((*reinterpret_cast< QRemoteObjectPendingCallWatcher*(*)>(_a[1]))); break;
        case 24: _t->requestedHeaderData((*reinterpret_cast< QRemoteObjectPendingCallWatcher*(*)>(_a[1]))); break;
        case 25: _t->init(); break;
        case 26: _t->fetchPendingData(); break;
        case 27: _t->fetchPendingHeaderData(); break;
        case 28: _t->handleInitDone((*reinterpret_cast< QRemoteObjectPendingCallWatcher*(*)>(_a[1]))); break;
        case 29: _t->handleModelResetDone((*reinterpret_cast< QRemoteObjectPendingCallWatcher*(*)>(_a[1]))); break;
        case 30: _t->handleSizeDone((*reinterpret_cast< QRemoteObjectPendingCallWatcher*(*)>(_a[1]))); break;
        case 31: _t->onReplicaCurrentChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 32: _t->fillCache((*reinterpret_cast< const IndexValuePair(*)>(_a[1])),(*reinterpret_cast< const QVector<int>(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 7:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt::Orientation >(); break;
            }
            break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<Qt::Orientation> >(); break;
            case 2:
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QItemSelectionModel::SelectionFlags >(); break;
            }
            break;
        case 13:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt::Orientation >(); break;
            }
            break;
        case 16:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 3:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 21:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexList >(); break;
            }
            break;
        case 23:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectPendingCallWatcher* >(); break;
            }
            break;
        case 24:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectPendingCallWatcher* >(); break;
            }
            break;
        case 28:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectPendingCallWatcher* >(); break;
            }
            break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectPendingCallWatcher* >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QRemoteObjectPendingCallWatcher* >(); break;
            }
            break;
        case 32:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< IndexValuePair >(); break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::availableRolesChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , IndexList , QVector<int> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::dataChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::rowsInserted)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::rowsRemoved)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , int , int , IndexList , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::rowsMoved)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , IndexList );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::currentChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::modelReset)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(Qt::Orientation , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::headerDataChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QAbstractItemModelReplicaImplementation::*)(IndexList , int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QAbstractItemModelReplicaImplementation::columnsInserted)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QIntHash >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QAbstractItemModelReplicaImplementation *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVector<int>*>(_v) = _t->availableRoles(); break;
        case 1: *reinterpret_cast< QIntHash*>(_v) = _t->roleNames(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QAbstractItemModelReplicaImplementation::staticMetaObject = { {
    &QRemoteObjectReplica::staticMetaObject,
    qt_meta_stringdata_QAbstractItemModelReplicaImplementation.data,
    qt_meta_data_QAbstractItemModelReplicaImplementation,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QAbstractItemModelReplicaImplementation::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QAbstractItemModelReplicaImplementation::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QAbstractItemModelReplicaImplementation.stringdata0))
        return static_cast<void*>(this);
    return QRemoteObjectReplica::qt_metacast(_clname);
}

int QAbstractItemModelReplicaImplementation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QRemoteObjectReplica::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QAbstractItemModelReplicaImplementation::availableRolesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QAbstractItemModelReplicaImplementation::dataChanged(IndexList _t1, IndexList _t2, QVector<int> _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QAbstractItemModelReplicaImplementation::rowsInserted(IndexList _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QAbstractItemModelReplicaImplementation::rowsRemoved(IndexList _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QAbstractItemModelReplicaImplementation::rowsMoved(IndexList _t1, int _t2, int _t3, IndexList _t4, int _t5)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)), const_cast<void*>(reinterpret_cast<const void*>(&_t4)), const_cast<void*>(reinterpret_cast<const void*>(&_t5)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QAbstractItemModelReplicaImplementation::currentChanged(IndexList _t1, IndexList _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QAbstractItemModelReplicaImplementation::modelReset()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QAbstractItemModelReplicaImplementation::headerDataChanged(Qt::Orientation _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QAbstractItemModelReplicaImplementation::columnsInserted(IndexList _t1, int _t2, int _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
