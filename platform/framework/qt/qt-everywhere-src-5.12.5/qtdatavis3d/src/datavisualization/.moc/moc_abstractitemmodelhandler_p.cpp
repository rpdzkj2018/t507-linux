/****************************************************************************
** Meta object code from reading C++ file 'abstractitemmodelhandler_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../data/abstractitemmodelhandler_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'abstractitemmodelhandler_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler_t {
    QByteArrayData data[34];
    char stringdata0[545];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler_t qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler = {
    {
QT_MOC_LITERAL(0, 0, 45), // "QtDataVisualization::Abstract..."
QT_MOC_LITERAL(1, 46, 16), // "itemModelChanged"
QT_MOC_LITERAL(2, 63, 0), // ""
QT_MOC_LITERAL(3, 64, 25), // "const QAbstractItemModel*"
QT_MOC_LITERAL(4, 90, 9), // "itemModel"
QT_MOC_LITERAL(5, 100, 21), // "handleColumnsInserted"
QT_MOC_LITERAL(6, 122, 11), // "QModelIndex"
QT_MOC_LITERAL(7, 134, 6), // "parent"
QT_MOC_LITERAL(8, 141, 5), // "start"
QT_MOC_LITERAL(9, 147, 3), // "end"
QT_MOC_LITERAL(10, 151, 18), // "handleColumnsMoved"
QT_MOC_LITERAL(11, 170, 12), // "sourceParent"
QT_MOC_LITERAL(12, 183, 11), // "sourceStart"
QT_MOC_LITERAL(13, 195, 9), // "sourceEnd"
QT_MOC_LITERAL(14, 205, 17), // "destinationParent"
QT_MOC_LITERAL(15, 223, 17), // "destinationColumn"
QT_MOC_LITERAL(16, 241, 20), // "handleColumnsRemoved"
QT_MOC_LITERAL(17, 262, 17), // "handleDataChanged"
QT_MOC_LITERAL(18, 280, 7), // "topLeft"
QT_MOC_LITERAL(19, 288, 11), // "bottomRight"
QT_MOC_LITERAL(20, 300, 12), // "QVector<int>"
QT_MOC_LITERAL(21, 313, 5), // "roles"
QT_MOC_LITERAL(22, 319, 19), // "handleLayoutChanged"
QT_MOC_LITERAL(23, 339, 28), // "QList<QPersistentModelIndex>"
QT_MOC_LITERAL(24, 368, 7), // "parents"
QT_MOC_LITERAL(25, 376, 36), // "QAbstractItemModel::LayoutCha..."
QT_MOC_LITERAL(26, 413, 4), // "hint"
QT_MOC_LITERAL(27, 418, 16), // "handleModelReset"
QT_MOC_LITERAL(28, 435, 18), // "handleRowsInserted"
QT_MOC_LITERAL(29, 454, 15), // "handleRowsMoved"
QT_MOC_LITERAL(30, 470, 14), // "destinationRow"
QT_MOC_LITERAL(31, 485, 17), // "handleRowsRemoved"
QT_MOC_LITERAL(32, 503, 20), // "handleMappingChanged"
QT_MOC_LITERAL(33, 524, 20) // "handlePendingResolve"

    },
    "QtDataVisualization::AbstractItemModelHandler\0"
    "itemModelChanged\0\0const QAbstractItemModel*\0"
    "itemModel\0handleColumnsInserted\0"
    "QModelIndex\0parent\0start\0end\0"
    "handleColumnsMoved\0sourceParent\0"
    "sourceStart\0sourceEnd\0destinationParent\0"
    "destinationColumn\0handleColumnsRemoved\0"
    "handleDataChanged\0topLeft\0bottomRight\0"
    "QVector<int>\0roles\0handleLayoutChanged\0"
    "QList<QPersistentModelIndex>\0parents\0"
    "QAbstractItemModel::LayoutChangeHint\0"
    "hint\0handleModelReset\0handleRowsInserted\0"
    "handleRowsMoved\0destinationRow\0"
    "handleRowsRemoved\0handleMappingChanged\0"
    "handlePendingResolve"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtDataVisualization__AbstractItemModelHandler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    3,   92,    2, 0x0a /* Public */,
      10,    5,   99,    2, 0x0a /* Public */,
      16,    3,  110,    2, 0x0a /* Public */,
      17,    3,  117,    2, 0x0a /* Public */,
      17,    2,  124,    2, 0x2a /* Public | MethodCloned */,
      22,    2,  129,    2, 0x0a /* Public */,
      22,    1,  134,    2, 0x2a /* Public | MethodCloned */,
      22,    0,  137,    2, 0x2a /* Public | MethodCloned */,
      27,    0,  138,    2, 0x0a /* Public */,
      28,    3,  139,    2, 0x0a /* Public */,
      29,    5,  146,    2, 0x0a /* Public */,
      31,    3,  157,    2, 0x0a /* Public */,
      32,    0,  164,    2, 0x0a /* Public */,
      33,    0,  165,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,    7,    8,    9,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int, 0x80000000 | 6, QMetaType::Int,   11,   12,   13,   14,   15,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,    7,    8,    9,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6, 0x80000000 | 20,   18,   19,   21,
    QMetaType::Void, 0x80000000 | 6, 0x80000000 | 6,   18,   19,
    QMetaType::Void, 0x80000000 | 23, 0x80000000 | 25,   24,   26,
    QMetaType::Void, 0x80000000 | 23,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,    7,    8,    9,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int, 0x80000000 | 6, QMetaType::Int,   11,   12,   13,   14,   30,
    QMetaType::Void, 0x80000000 | 6, QMetaType::Int, QMetaType::Int,    7,    8,    9,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QtDataVisualization::AbstractItemModelHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<AbstractItemModelHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->itemModelChanged((*reinterpret_cast< const QAbstractItemModel*(*)>(_a[1]))); break;
        case 1: _t->handleColumnsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 2: _t->handleColumnsMoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QModelIndex(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 3: _t->handleColumnsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 4: _t->handleDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2])),(*reinterpret_cast< const QVector<int>(*)>(_a[3]))); break;
        case 5: _t->handleDataChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< const QModelIndex(*)>(_a[2]))); break;
        case 6: _t->handleLayoutChanged((*reinterpret_cast< const QList<QPersistentModelIndex>(*)>(_a[1])),(*reinterpret_cast< QAbstractItemModel::LayoutChangeHint(*)>(_a[2]))); break;
        case 7: _t->handleLayoutChanged((*reinterpret_cast< const QList<QPersistentModelIndex>(*)>(_a[1]))); break;
        case 8: _t->handleLayoutChanged(); break;
        case 9: _t->handleModelReset(); break;
        case 10: _t->handleRowsInserted((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 11: _t->handleRowsMoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< const QModelIndex(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5]))); break;
        case 12: _t->handleRowsRemoved((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 13: _t->handleMappingChanged(); break;
        case 14: _t->handlePendingResolve(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<int> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (AbstractItemModelHandler::*)(const QAbstractItemModel * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&AbstractItemModelHandler::itemModelChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QtDataVisualization::AbstractItemModelHandler::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler.data,
    qt_meta_data_QtDataVisualization__AbstractItemModelHandler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtDataVisualization::AbstractItemModelHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtDataVisualization::AbstractItemModelHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtDataVisualization__AbstractItemModelHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QtDataVisualization::AbstractItemModelHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void QtDataVisualization::AbstractItemModelHandler::itemModelChanged(const QAbstractItemModel * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
