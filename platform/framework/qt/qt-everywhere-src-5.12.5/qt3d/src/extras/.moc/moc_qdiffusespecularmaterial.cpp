/****************************************************************************
** Meta object code from reading C++ file 'qdiffusespecularmaterial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../defaults/qdiffusespecularmaterial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdiffusespecularmaterial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial_t {
    QByteArrayData data[24];
    char stringdata0[337];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial_t qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial = {
    {
QT_MOC_LITERAL(0, 0, 36), // "Qt3DExtras::QDiffuseSpecularM..."
QT_MOC_LITERAL(1, 37, 14), // "ambientChanged"
QT_MOC_LITERAL(2, 52, 0), // ""
QT_MOC_LITERAL(3, 53, 7), // "ambient"
QT_MOC_LITERAL(4, 61, 14), // "diffuseChanged"
QT_MOC_LITERAL(5, 76, 7), // "diffuse"
QT_MOC_LITERAL(6, 84, 15), // "specularChanged"
QT_MOC_LITERAL(7, 100, 8), // "specular"
QT_MOC_LITERAL(8, 109, 16), // "shininessChanged"
QT_MOC_LITERAL(9, 126, 9), // "shininess"
QT_MOC_LITERAL(10, 136, 13), // "normalChanged"
QT_MOC_LITERAL(11, 150, 6), // "normal"
QT_MOC_LITERAL(12, 157, 19), // "textureScaleChanged"
QT_MOC_LITERAL(13, 177, 12), // "textureScale"
QT_MOC_LITERAL(14, 190, 27), // "alphaBlendingEnabledChanged"
QT_MOC_LITERAL(15, 218, 7), // "enabled"
QT_MOC_LITERAL(16, 226, 10), // "setAmbient"
QT_MOC_LITERAL(17, 237, 10), // "setDiffuse"
QT_MOC_LITERAL(18, 248, 11), // "setSpecular"
QT_MOC_LITERAL(19, 260, 12), // "setShininess"
QT_MOC_LITERAL(20, 273, 9), // "setNormal"
QT_MOC_LITERAL(21, 283, 15), // "setTextureScale"
QT_MOC_LITERAL(22, 299, 23), // "setAlphaBlendingEnabled"
QT_MOC_LITERAL(23, 323, 13) // "alphaBlending"

    },
    "Qt3DExtras::QDiffuseSpecularMaterial\0"
    "ambientChanged\0\0ambient\0diffuseChanged\0"
    "diffuse\0specularChanged\0specular\0"
    "shininessChanged\0shininess\0normalChanged\0"
    "normal\0textureScaleChanged\0textureScale\0"
    "alphaBlendingEnabledChanged\0enabled\0"
    "setAmbient\0setDiffuse\0setSpecular\0"
    "setShininess\0setNormal\0setTextureScale\0"
    "setAlphaBlendingEnabled\0alphaBlending"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DExtras__QDiffuseSpecularMaterial[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       7,  126, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   84,    2, 0x06 /* Public */,
       4,    1,   87,    2, 0x06 /* Public */,
       6,    1,   90,    2, 0x06 /* Public */,
       8,    1,   93,    2, 0x06 /* Public */,
      10,    1,   96,    2, 0x06 /* Public */,
      12,    1,   99,    2, 0x06 /* Public */,
      14,    1,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      16,    1,  105,    2, 0x0a /* Public */,
      17,    1,  108,    2, 0x0a /* Public */,
      18,    1,  111,    2, 0x0a /* Public */,
      19,    1,  114,    2, 0x0a /* Public */,
      20,    1,  117,    2, 0x0a /* Public */,
      21,    1,  120,    2, 0x0a /* Public */,
      22,    1,  123,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, QMetaType::QVariant,    5,
    QMetaType::Void, QMetaType::QVariant,    7,
    QMetaType::Void, QMetaType::Float,    9,
    QMetaType::Void, QMetaType::QVariant,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Bool,   15,

 // slots: parameters
    QMetaType::Void, QMetaType::QColor,    3,
    QMetaType::Void, QMetaType::QVariant,    5,
    QMetaType::Void, QMetaType::QVariant,    7,
    QMetaType::Void, QMetaType::Float,    9,
    QMetaType::Void, QMetaType::QVariant,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Bool,   15,

 // properties: name, type, flags
       3, QMetaType::QColor, 0x00495103,
       5, QMetaType::QVariant, 0x00495103,
       7, QMetaType::QVariant, 0x00495103,
       9, QMetaType::Float, 0x00495103,
      11, QMetaType::QVariant, 0x00495103,
      13, QMetaType::Float, 0x00495103,
      23, QMetaType::Bool, 0x00495003,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,

       0        // eod
};

void Qt3DExtras::QDiffuseSpecularMaterial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDiffuseSpecularMaterial *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ambientChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 1: _t->diffuseChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 2: _t->specularChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 3: _t->shininessChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 4: _t->normalChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 5: _t->textureScaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->alphaBlendingEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: _t->setAmbient((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 8: _t->setDiffuse((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 9: _t->setSpecular((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 10: _t->setShininess((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 11: _t->setNormal((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 12: _t->setTextureScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 13: _t->setAlphaBlendingEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDiffuseSpecularMaterial::*)(const QColor & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::ambientChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::diffuseChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::specularChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::shininessChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::normalChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::textureScaleChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDiffuseSpecularMaterial::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDiffuseSpecularMaterial::alphaBlendingEnabledChanged)) {
                *result = 6;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDiffuseSpecularMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->ambient(); break;
        case 1: *reinterpret_cast< QVariant*>(_v) = _t->diffuse(); break;
        case 2: *reinterpret_cast< QVariant*>(_v) = _t->specular(); break;
        case 3: *reinterpret_cast< float*>(_v) = _t->shininess(); break;
        case 4: *reinterpret_cast< QVariant*>(_v) = _t->normal(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->textureScale(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isAlphaBlendingEnabled(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDiffuseSpecularMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setAmbient(*reinterpret_cast< QColor*>(_v)); break;
        case 1: _t->setDiffuse(*reinterpret_cast< QVariant*>(_v)); break;
        case 2: _t->setSpecular(*reinterpret_cast< QVariant*>(_v)); break;
        case 3: _t->setShininess(*reinterpret_cast< float*>(_v)); break;
        case 4: _t->setNormal(*reinterpret_cast< QVariant*>(_v)); break;
        case 5: _t->setTextureScale(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setAlphaBlendingEnabled(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DExtras::QDiffuseSpecularMaterial::staticMetaObject = { {
    &Qt3DRender::QMaterial::staticMetaObject,
    qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial.data,
    qt_meta_data_Qt3DExtras__QDiffuseSpecularMaterial,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DExtras::QDiffuseSpecularMaterial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DExtras::QDiffuseSpecularMaterial::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DExtras__QDiffuseSpecularMaterial.stringdata0))
        return static_cast<void*>(this);
    return Qt3DRender::QMaterial::qt_metacast(_clname);
}

int Qt3DExtras::QDiffuseSpecularMaterial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DRender::QMaterial::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DExtras::QDiffuseSpecularMaterial::ambientChanged(const QColor & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DExtras::QDiffuseSpecularMaterial::diffuseChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DExtras::QDiffuseSpecularMaterial::specularChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DExtras::QDiffuseSpecularMaterial::shininessChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DExtras::QDiffuseSpecularMaterial::normalChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DExtras::QDiffuseSpecularMaterial::textureScaleChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DExtras::QDiffuseSpecularMaterial::alphaBlendingEnabledChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
