/****************************************************************************
** Meta object code from reading C++ file 'qquicktumbler_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquicktumbler_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquicktumbler_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickTumbler_t {
    QByteArrayData data[36];
    char stringdata0[482];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTumbler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTumbler_t qt_meta_stringdata_QQuickTumbler = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QQuickTumbler"
QT_MOC_LITERAL(1, 14, 12), // "modelChanged"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 12), // "countChanged"
QT_MOC_LITERAL(4, 41, 19), // "currentIndexChanged"
QT_MOC_LITERAL(5, 61, 18), // "currentItemChanged"
QT_MOC_LITERAL(6, 80, 15), // "delegateChanged"
QT_MOC_LITERAL(7, 96, 23), // "visibleItemCountChanged"
QT_MOC_LITERAL(8, 120, 11), // "wrapChanged"
QT_MOC_LITERAL(9, 132, 13), // "movingChanged"
QT_MOC_LITERAL(10, 146, 19), // "_q_updateItemWidths"
QT_MOC_LITERAL(11, 166, 20), // "_q_updateItemHeights"
QT_MOC_LITERAL(12, 187, 28), // "_q_onViewCurrentIndexChanged"
QT_MOC_LITERAL(13, 216, 21), // "_q_onViewCountChanged"
QT_MOC_LITERAL(14, 238, 22), // "_q_onViewOffsetChanged"
QT_MOC_LITERAL(15, 261, 24), // "_q_onViewContentYChanged"
QT_MOC_LITERAL(16, 286, 19), // "positionViewAtIndex"
QT_MOC_LITERAL(17, 306, 5), // "index"
QT_MOC_LITERAL(18, 312, 12), // "PositionMode"
QT_MOC_LITERAL(19, 325, 4), // "mode"
QT_MOC_LITERAL(20, 330, 5), // "model"
QT_MOC_LITERAL(21, 336, 5), // "count"
QT_MOC_LITERAL(22, 342, 12), // "currentIndex"
QT_MOC_LITERAL(23, 355, 11), // "currentItem"
QT_MOC_LITERAL(24, 367, 11), // "QQuickItem*"
QT_MOC_LITERAL(25, 379, 8), // "delegate"
QT_MOC_LITERAL(26, 388, 14), // "QQmlComponent*"
QT_MOC_LITERAL(27, 403, 16), // "visibleItemCount"
QT_MOC_LITERAL(28, 420, 4), // "wrap"
QT_MOC_LITERAL(29, 425, 6), // "moving"
QT_MOC_LITERAL(30, 432, 9), // "Beginning"
QT_MOC_LITERAL(31, 442, 6), // "Center"
QT_MOC_LITERAL(32, 449, 3), // "End"
QT_MOC_LITERAL(33, 453, 7), // "Visible"
QT_MOC_LITERAL(34, 461, 7), // "Contain"
QT_MOC_LITERAL(35, 469, 12) // "SnapPosition"

    },
    "QQuickTumbler\0modelChanged\0\0countChanged\0"
    "currentIndexChanged\0currentItemChanged\0"
    "delegateChanged\0visibleItemCountChanged\0"
    "wrapChanged\0movingChanged\0_q_updateItemWidths\0"
    "_q_updateItemHeights\0_q_onViewCurrentIndexChanged\0"
    "_q_onViewCountChanged\0_q_onViewOffsetChanged\0"
    "_q_onViewContentYChanged\0positionViewAtIndex\0"
    "index\0PositionMode\0mode\0model\0count\0"
    "currentIndex\0currentItem\0QQuickItem*\0"
    "delegate\0QQmlComponent*\0visibleItemCount\0"
    "wrap\0moving\0Beginning\0Center\0End\0"
    "Visible\0Contain\0SnapPosition"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTumbler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       8,  123, // properties
       1,  163, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06 /* Public */,
       3,    0,  105,    2, 0x06 /* Public */,
       4,    0,  106,    2, 0x06 /* Public */,
       5,    0,  107,    2, 0x06 /* Public */,
       6,    0,  108,    2, 0x06 /* Public */,
       7,    0,  109,    2, 0x06 /* Public */,
       8,    0,  110,    2, 0x86 /* Public | MethodRevisioned */,
       9,    0,  111,    2, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      10,    0,  112,    2, 0x08 /* Private */,
      11,    0,  113,    2, 0x08 /* Private */,
      12,    0,  114,    2, 0x08 /* Private */,
      13,    0,  115,    2, 0x08 /* Private */,
      14,    0,  116,    2, 0x08 /* Private */,
      15,    0,  117,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      16,    2,  118,    2, 0x82 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       2,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,

 // methods: revision
       5,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 18,   17,   19,

 // properties: name, type, flags
      20, QMetaType::QVariant, 0x00495903,
      21, QMetaType::Int, 0x00495801,
      22, QMetaType::Int, 0x00495903,
      23, 0x80000000 | 24, 0x00495809,
      25, 0x80000000 | 26, 0x0049590b,
      27, QMetaType::Int, 0x00495903,
      28, QMetaType::Bool, 0x00c95907,
      29, QMetaType::Bool, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       2,

 // enums: name, alias, flags, count, data
      18,   18, 0x0,    6,  168,

 // enum data: key, value
      30, uint(QQuickTumbler::Beginning),
      31, uint(QQuickTumbler::Center),
      32, uint(QQuickTumbler::End),
      33, uint(QQuickTumbler::Visible),
      34, uint(QQuickTumbler::Contain),
      35, uint(QQuickTumbler::SnapPosition),

       0        // eod
};

void QQuickTumbler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTumbler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelChanged(); break;
        case 1: _t->countChanged(); break;
        case 2: _t->currentIndexChanged(); break;
        case 3: _t->currentItemChanged(); break;
        case 4: _t->delegateChanged(); break;
        case 5: _t->visibleItemCountChanged(); break;
        case 6: _t->wrapChanged(); break;
        case 7: _t->movingChanged(); break;
        case 8: _t->d_func()->_q_updateItemWidths(); break;
        case 9: _t->d_func()->_q_updateItemHeights(); break;
        case 10: _t->d_func()->_q_onViewCurrentIndexChanged(); break;
        case 11: _t->d_func()->_q_onViewCountChanged(); break;
        case 12: _t->d_func()->_q_onViewOffsetChanged(); break;
        case 13: _t->d_func()->_q_onViewContentYChanged(); break;
        case 14: _t->positionViewAtIndex((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< PositionMode(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::modelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::countChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::currentIndexChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::currentItemChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::delegateChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::visibleItemCountChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::wrapChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QQuickTumbler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumbler::movingChanged)) {
                *result = 7;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTumbler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->currentIndex(); break;
        case 3: *reinterpret_cast< QQuickItem**>(_v) = _t->currentItem(); break;
        case 4: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->visibleItemCount(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->wrap(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->isMoving(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickTumbler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QVariant*>(_v)); break;
        case 2: _t->setCurrentIndex(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 5: _t->setVisibleItemCount(*reinterpret_cast< int*>(_v)); break;
        case 6: _t->setWrap(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickTumbler *_t = static_cast<QQuickTumbler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 6: _t->resetWrap(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickTumbler::staticMetaObject = { {
    &QQuickControl::staticMetaObject,
    qt_meta_stringdata_QQuickTumbler.data,
    qt_meta_data_QQuickTumbler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTumbler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTumbler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTumbler.stringdata0))
        return static_cast<void*>(this);
    return QQuickControl::qt_metacast(_clname);
}

int QQuickTumbler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickControl::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 15)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 15;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTumbler::modelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickTumbler::countChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickTumbler::currentIndexChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickTumbler::currentItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickTumbler::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickTumbler::visibleItemCountChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickTumbler::wrapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QQuickTumbler::movingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
struct qt_meta_stringdata_QQuickTumblerAttached_t {
    QByteArrayData data[6];
    char stringdata0[79];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickTumblerAttached_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickTumblerAttached_t qt_meta_stringdata_QQuickTumblerAttached = {
    {
QT_MOC_LITERAL(0, 0, 21), // "QQuickTumblerAttached"
QT_MOC_LITERAL(1, 22, 19), // "displacementChanged"
QT_MOC_LITERAL(2, 42, 0), // ""
QT_MOC_LITERAL(3, 43, 7), // "tumbler"
QT_MOC_LITERAL(4, 51, 14), // "QQuickTumbler*"
QT_MOC_LITERAL(5, 66, 12) // "displacement"

    },
    "QQuickTumblerAttached\0displacementChanged\0"
    "\0tumbler\0QQuickTumbler*\0displacement"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickTumblerAttached[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       2,   20, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, 0x80000000 | 4, 0x00095c09,
       5, QMetaType::QReal, 0x00495801,

 // properties: notify_signal_id
       0,
       0,

       0        // eod
};

void QQuickTumblerAttached::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickTumblerAttached *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->displacementChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickTumblerAttached::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickTumblerAttached::displacementChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickTumbler* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickTumblerAttached *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickTumbler**>(_v) = _t->tumbler(); break;
        case 1: *reinterpret_cast< qreal*>(_v) = _t->displacement(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickTumblerAttached::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickTumblerAttached.data,
    qt_meta_data_QQuickTumblerAttached,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickTumblerAttached::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickTumblerAttached::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickTumblerAttached.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickTumblerAttached::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickTumblerAttached::displacementChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
