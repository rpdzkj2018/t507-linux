/****************************************************************************
** Meta object code from reading C++ file 'qremoteobjectpacket_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qremoteobjectpacket_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qremoteobjectpacket_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QRemoteObjectPackets_t {
    QByteArrayData data[5];
    char stringdata0[51];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRemoteObjectPackets_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRemoteObjectPackets_t qt_meta_stringdata_QRemoteObjectPackets = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QRemoteObjectPackets"
QT_MOC_LITERAL(1, 21, 10), // "ObjectType"
QT_MOC_LITERAL(2, 32, 5), // "CLASS"
QT_MOC_LITERAL(3, 38, 5), // "MODEL"
QT_MOC_LITERAL(4, 44, 6) // "GADGET"

    },
    "QRemoteObjectPackets\0ObjectType\0CLASS\0"
    "MODEL\0GADGET"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRemoteObjectPackets[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x2,    3,   19,

 // enum data: key, value
       2, uint(QRemoteObjectPackets::ObjectType::CLASS),
       3, uint(QRemoteObjectPackets::ObjectType::MODEL),
       4, uint(QRemoteObjectPackets::ObjectType::GADGET),

       0        // eod
};

QT_INIT_METAOBJECT const QMetaObject QRemoteObjectPackets::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_QRemoteObjectPackets.data,
    qt_meta_data_QRemoteObjectPackets,
    nullptr,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
