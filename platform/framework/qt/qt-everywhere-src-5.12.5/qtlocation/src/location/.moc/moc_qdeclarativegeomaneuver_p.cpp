/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeomaneuver_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qdeclarativegeomaneuver_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeomaneuver_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeGeoManeuver_t {
    QByteArrayData data[26];
    char stringdata0[417];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoManeuver_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoManeuver_t qt_meta_stringdata_QDeclarativeGeoManeuver = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QDeclarativeGeoManeuver"
QT_MOC_LITERAL(1, 24, 25), // "extendedAttributesChanged"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 5), // "valid"
QT_MOC_LITERAL(4, 57, 8), // "position"
QT_MOC_LITERAL(5, 66, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(6, 81, 15), // "instructionText"
QT_MOC_LITERAL(7, 97, 9), // "direction"
QT_MOC_LITERAL(8, 107, 9), // "Direction"
QT_MOC_LITERAL(9, 117, 21), // "timeToNextInstruction"
QT_MOC_LITERAL(10, 139, 25), // "distanceToNextInstruction"
QT_MOC_LITERAL(11, 165, 8), // "waypoint"
QT_MOC_LITERAL(12, 174, 13), // "waypointValid"
QT_MOC_LITERAL(13, 188, 18), // "extendedAttributes"
QT_MOC_LITERAL(14, 207, 11), // "NoDirection"
QT_MOC_LITERAL(15, 219, 16), // "DirectionForward"
QT_MOC_LITERAL(16, 236, 18), // "DirectionBearRight"
QT_MOC_LITERAL(17, 255, 19), // "DirectionLightRight"
QT_MOC_LITERAL(18, 275, 14), // "DirectionRight"
QT_MOC_LITERAL(19, 290, 18), // "DirectionHardRight"
QT_MOC_LITERAL(20, 309, 19), // "DirectionUTurnRight"
QT_MOC_LITERAL(21, 329, 18), // "DirectionUTurnLeft"
QT_MOC_LITERAL(22, 348, 17), // "DirectionHardLeft"
QT_MOC_LITERAL(23, 366, 13), // "DirectionLeft"
QT_MOC_LITERAL(24, 380, 18), // "DirectionLightLeft"
QT_MOC_LITERAL(25, 399, 17) // "DirectionBearLeft"

    },
    "QDeclarativeGeoManeuver\0"
    "extendedAttributesChanged\0\0valid\0"
    "position\0QGeoCoordinate\0instructionText\0"
    "direction\0Direction\0timeToNextInstruction\0"
    "distanceToNextInstruction\0waypoint\0"
    "waypointValid\0extendedAttributes\0"
    "NoDirection\0DirectionForward\0"
    "DirectionBearRight\0DirectionLightRight\0"
    "DirectionRight\0DirectionHardRight\0"
    "DirectionUTurnRight\0DirectionUTurnLeft\0"
    "DirectionHardLeft\0DirectionLeft\0"
    "DirectionLightLeft\0DirectionBearLeft"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoManeuver[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       9,   20, // properties
       1,   65, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::Bool, 0x00095401,
       4, 0x80000000 | 5, 0x00095409,
       6, QMetaType::QString, 0x00095401,
       7, 0x80000000 | 8, 0x00095409,
       9, QMetaType::Int, 0x00095401,
      10, QMetaType::QReal, 0x00095401,
      11, 0x80000000 | 5, 0x00095409,
      12, QMetaType::Bool, 0x00095401,
      13, QMetaType::QObjectStar, 0x00c95001,

 // properties: notify_signal_id
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      11,

 // enums: name, alias, flags, count, data
       8,    8, 0x0,   12,   70,

 // enum data: key, value
      14, uint(QDeclarativeGeoManeuver::NoDirection),
      15, uint(QDeclarativeGeoManeuver::DirectionForward),
      16, uint(QDeclarativeGeoManeuver::DirectionBearRight),
      17, uint(QDeclarativeGeoManeuver::DirectionLightRight),
      18, uint(QDeclarativeGeoManeuver::DirectionRight),
      19, uint(QDeclarativeGeoManeuver::DirectionHardRight),
      20, uint(QDeclarativeGeoManeuver::DirectionUTurnRight),
      21, uint(QDeclarativeGeoManeuver::DirectionUTurnLeft),
      22, uint(QDeclarativeGeoManeuver::DirectionHardLeft),
      23, uint(QDeclarativeGeoManeuver::DirectionLeft),
      24, uint(QDeclarativeGeoManeuver::DirectionLightLeft),
      25, uint(QDeclarativeGeoManeuver::DirectionBearLeft),

       0        // eod
};

void QDeclarativeGeoManeuver::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoManeuver *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->extendedAttributesChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoManeuver::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoManeuver::extendedAttributesChanged)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoManeuver *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->valid(); break;
        case 1: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->position(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->instructionText(); break;
        case 3: *reinterpret_cast< Direction*>(_v) = _t->direction(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->timeToNextInstruction(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->distanceToNextInstruction(); break;
        case 6: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->waypoint(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->waypointValid(); break;
        case 8: *reinterpret_cast< QObject**>(_v) = _t->extendedAttributes(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoManeuver::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoManeuver.data,
    qt_meta_data_QDeclarativeGeoManeuver,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoManeuver::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoManeuver::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoManeuver.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeGeoManeuver::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoManeuver::extendedAttributesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
