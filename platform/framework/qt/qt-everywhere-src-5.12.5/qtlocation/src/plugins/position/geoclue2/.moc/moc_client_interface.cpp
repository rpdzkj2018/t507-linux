/****************************************************************************
** Meta object code from reading C++ file 'client_interface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../client_interface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'client_interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface_t {
    QByteArrayData data[15];
    char stringdata0[207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface_t qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface = {
    {
QT_MOC_LITERAL(0, 0, 37), // "OrgFreedesktopGeoClue2ClientI..."
QT_MOC_LITERAL(1, 38, 15), // "LocationUpdated"
QT_MOC_LITERAL(2, 54, 0), // ""
QT_MOC_LITERAL(3, 55, 15), // "QDBusObjectPath"
QT_MOC_LITERAL(4, 71, 11), // "oldLocation"
QT_MOC_LITERAL(5, 83, 11), // "newLocation"
QT_MOC_LITERAL(6, 95, 5), // "Start"
QT_MOC_LITERAL(7, 101, 19), // "QDBusPendingReply<>"
QT_MOC_LITERAL(8, 121, 4), // "Stop"
QT_MOC_LITERAL(9, 126, 6), // "Active"
QT_MOC_LITERAL(10, 133, 9), // "DesktopId"
QT_MOC_LITERAL(11, 143, 17), // "DistanceThreshold"
QT_MOC_LITERAL(12, 161, 8), // "Location"
QT_MOC_LITERAL(13, 170, 22), // "RequestedAccuracyLevel"
QT_MOC_LITERAL(14, 193, 13) // "TimeThreshold"

    },
    "OrgFreedesktopGeoClue2ClientInterface\0"
    "LocationUpdated\0\0QDBusObjectPath\0"
    "oldLocation\0newLocation\0Start\0"
    "QDBusPendingReply<>\0Stop\0Active\0"
    "DesktopId\0DistanceThreshold\0Location\0"
    "RequestedAccuracyLevel\0TimeThreshold"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OrgFreedesktopGeoClue2ClientInterface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       6,   36, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   29,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   34,    2, 0x0a /* Public */,
       8,    0,   35,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,

 // slots: parameters
    0x80000000 | 7,
    0x80000000 | 7,

 // properties: name, type, flags
       9, QMetaType::Bool, 0x00095001,
      10, QMetaType::QString, 0x00095103,
      11, QMetaType::UInt, 0x00095103,
      12, 0x80000000 | 3, 0x00095009,
      13, QMetaType::UInt, 0x00095103,
      14, QMetaType::UInt, 0x00095103,

       0        // eod
};

void OrgFreedesktopGeoClue2ClientInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OrgFreedesktopGeoClue2ClientInterface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->LocationUpdated((*reinterpret_cast< const QDBusObjectPath(*)>(_a[1])),(*reinterpret_cast< const QDBusObjectPath(*)>(_a[2]))); break;
        case 1: { QDBusPendingReply<> _r = _t->Start();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 2: { QDBusPendingReply<> _r = _t->Stop();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusObjectPath >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (OrgFreedesktopGeoClue2ClientInterface::*)(const QDBusObjectPath & , const QDBusObjectPath & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OrgFreedesktopGeoClue2ClientInterface::LocationUpdated)) {
                *result = 0;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusObjectPath >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<OrgFreedesktopGeoClue2ClientInterface *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->active(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->desktopId(); break;
        case 2: *reinterpret_cast< uint*>(_v) = _t->distanceThreshold(); break;
        case 3: *reinterpret_cast< QDBusObjectPath*>(_v) = _t->location(); break;
        case 4: *reinterpret_cast< uint*>(_v) = _t->requestedAccuracyLevel(); break;
        case 5: *reinterpret_cast< uint*>(_v) = _t->timeThreshold(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<OrgFreedesktopGeoClue2ClientInterface *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setDesktopId(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setDistanceThreshold(*reinterpret_cast< uint*>(_v)); break;
        case 4: _t->setRequestedAccuracyLevel(*reinterpret_cast< uint*>(_v)); break;
        case 5: _t->setTimeThreshold(*reinterpret_cast< uint*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject OrgFreedesktopGeoClue2ClientInterface::staticMetaObject = { {
    &QDBusAbstractInterface::staticMetaObject,
    qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface.data,
    qt_meta_data_OrgFreedesktopGeoClue2ClientInterface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OrgFreedesktopGeoClue2ClientInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OrgFreedesktopGeoClue2ClientInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OrgFreedesktopGeoClue2ClientInterface.stringdata0))
        return static_cast<void*>(this);
    return QDBusAbstractInterface::qt_metacast(_clname);
}

int OrgFreedesktopGeoClue2ClientInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDBusAbstractInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void OrgFreedesktopGeoClue2ClientInterface::LocationUpdated(const QDBusObjectPath & _t1, const QDBusObjectPath & _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
