/****************************************************************************
** Meta object code from reading C++ file 'qblitframebuffer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../framegraph/qblitframebuffer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qblitframebuffer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QBlitFramebuffer_t {
    QByteArrayData data[21];
    char stringdata0[420];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QBlitFramebuffer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QBlitFramebuffer_t qt_meta_stringdata_Qt3DRender__QBlitFramebuffer = {
    {
QT_MOC_LITERAL(0, 0, 28), // "Qt3DRender::QBlitFramebuffer"
QT_MOC_LITERAL(1, 29, 13), // "sourceChanged"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 18), // "destinationChanged"
QT_MOC_LITERAL(4, 63, 17), // "sourceRectChanged"
QT_MOC_LITERAL(5, 81, 22), // "destinationRectChanged"
QT_MOC_LITERAL(6, 104, 28), // "sourceAttachmentPointChanged"
QT_MOC_LITERAL(7, 133, 33), // "destinationAttachmentPointCha..."
QT_MOC_LITERAL(8, 167, 26), // "interpolationMethodChanged"
QT_MOC_LITERAL(9, 194, 6), // "source"
QT_MOC_LITERAL(10, 201, 26), // "Qt3DRender::QRenderTarget*"
QT_MOC_LITERAL(11, 228, 11), // "destination"
QT_MOC_LITERAL(12, 240, 10), // "sourceRect"
QT_MOC_LITERAL(13, 251, 15), // "destinationRect"
QT_MOC_LITERAL(14, 267, 21), // "sourceAttachmentPoint"
QT_MOC_LITERAL(15, 289, 48), // "Qt3DRender::QRenderTargetOutp..."
QT_MOC_LITERAL(16, 338, 26), // "destinationAttachmentPoint"
QT_MOC_LITERAL(17, 365, 19), // "interpolationMethod"
QT_MOC_LITERAL(18, 385, 19), // "InterpolationMethod"
QT_MOC_LITERAL(19, 405, 7), // "Nearest"
QT_MOC_LITERAL(20, 413, 6) // "Linear"

    },
    "Qt3DRender::QBlitFramebuffer\0sourceChanged\0"
    "\0destinationChanged\0sourceRectChanged\0"
    "destinationRectChanged\0"
    "sourceAttachmentPointChanged\0"
    "destinationAttachmentPointChanged\0"
    "interpolationMethodChanged\0source\0"
    "Qt3DRender::QRenderTarget*\0destination\0"
    "sourceRect\0destinationRect\0"
    "sourceAttachmentPoint\0"
    "Qt3DRender::QRenderTargetOutput::AttachmentPoint\0"
    "destinationAttachmentPoint\0"
    "interpolationMethod\0InterpolationMethod\0"
    "Nearest\0Linear"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QBlitFramebuffer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       7,   56, // properties
       1,   84, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,
       5,    0,   52,    2, 0x06 /* Public */,
       6,    0,   53,    2, 0x06 /* Public */,
       7,    0,   54,    2, 0x06 /* Public */,
       8,    0,   55,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       9, 0x80000000 | 10, 0x0049510b,
      11, 0x80000000 | 10, 0x0049510b,
      12, QMetaType::QRectF, 0x00495103,
      13, QMetaType::QRectF, 0x00495103,
      14, 0x80000000 | 15, 0x0049510b,
      16, 0x80000000 | 15, 0x0049510b,
      17, 0x80000000 | 18, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,

 // enums: name, alias, flags, count, data
      18,   18, 0x0,    2,   89,

 // enum data: key, value
      19, uint(Qt3DRender::QBlitFramebuffer::Nearest),
      20, uint(Qt3DRender::QBlitFramebuffer::Linear),

       0        // eod
};

void Qt3DRender::QBlitFramebuffer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QBlitFramebuffer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sourceChanged(); break;
        case 1: _t->destinationChanged(); break;
        case 2: _t->sourceRectChanged(); break;
        case 3: _t->destinationRectChanged(); break;
        case 4: _t->sourceAttachmentPointChanged(); break;
        case 5: _t->destinationAttachmentPointChanged(); break;
        case 6: _t->interpolationMethodChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::sourceChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::destinationChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::sourceRectChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::destinationRectChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::sourceAttachmentPointChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::destinationAttachmentPointChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QBlitFramebuffer::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBlitFramebuffer::interpolationMethodChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
        case 4:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DRender::QRenderTargetOutput::AttachmentPoint >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QBlitFramebuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QRenderTarget**>(_v) = _t->source(); break;
        case 1: *reinterpret_cast< Qt3DRender::QRenderTarget**>(_v) = _t->destination(); break;
        case 2: *reinterpret_cast< QRectF*>(_v) = _t->sourceRect(); break;
        case 3: *reinterpret_cast< QRectF*>(_v) = _t->destinationRect(); break;
        case 4: *reinterpret_cast< Qt3DRender::QRenderTargetOutput::AttachmentPoint*>(_v) = _t->sourceAttachmentPoint(); break;
        case 5: *reinterpret_cast< Qt3DRender::QRenderTargetOutput::AttachmentPoint*>(_v) = _t->destinationAttachmentPoint(); break;
        case 6: *reinterpret_cast< InterpolationMethod*>(_v) = _t->interpolationMethod(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QBlitFramebuffer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSource(*reinterpret_cast< Qt3DRender::QRenderTarget**>(_v)); break;
        case 1: _t->setDestination(*reinterpret_cast< Qt3DRender::QRenderTarget**>(_v)); break;
        case 2: _t->setSourceRect(*reinterpret_cast< QRectF*>(_v)); break;
        case 3: _t->setDestinationRect(*reinterpret_cast< QRectF*>(_v)); break;
        case 4: _t->setSourceAttachmentPoint(*reinterpret_cast< Qt3DRender::QRenderTargetOutput::AttachmentPoint*>(_v)); break;
        case 5: _t->setDestinationAttachmentPoint(*reinterpret_cast< Qt3DRender::QRenderTargetOutput::AttachmentPoint*>(_v)); break;
        case 6: _t->setInterpolationMethod(*reinterpret_cast< InterpolationMethod*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_Qt3DRender__QBlitFramebuffer[] = {
        &Qt3DRender::QRenderTargetOutput::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QBlitFramebuffer::staticMetaObject = { {
    &QFrameGraphNode::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QBlitFramebuffer.data,
    qt_meta_data_Qt3DRender__QBlitFramebuffer,
    qt_static_metacall,
    qt_meta_extradata_Qt3DRender__QBlitFramebuffer,
    nullptr
} };


const QMetaObject *Qt3DRender::QBlitFramebuffer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QBlitFramebuffer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QBlitFramebuffer.stringdata0))
        return static_cast<void*>(this);
    return QFrameGraphNode::qt_metacast(_clname);
}

int Qt3DRender::QBlitFramebuffer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QFrameGraphNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QBlitFramebuffer::sourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Qt3DRender::QBlitFramebuffer::destinationChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Qt3DRender::QBlitFramebuffer::sourceRectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void Qt3DRender::QBlitFramebuffer::destinationRectChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void Qt3DRender::QBlitFramebuffer::sourceAttachmentPointChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void Qt3DRender::QBlitFramebuffer::destinationAttachmentPointChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void Qt3DRender::QBlitFramebuffer::interpolationMethodChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
