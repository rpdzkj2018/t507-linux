/****************************************************************************
** Meta object code from reading C++ file 'qquickiconlabel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickiconlabel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickiconlabel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickIconLabel_t {
    QByteArrayData data[20];
    char stringdata0[202];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickIconLabel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickIconLabel_t qt_meta_stringdata_QQuickIconLabel = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QQuickIconLabel"
QT_MOC_LITERAL(1, 16, 4), // "icon"
QT_MOC_LITERAL(2, 21, 10), // "QQuickIcon"
QT_MOC_LITERAL(3, 32, 4), // "text"
QT_MOC_LITERAL(4, 37, 4), // "font"
QT_MOC_LITERAL(5, 42, 5), // "color"
QT_MOC_LITERAL(6, 48, 7), // "display"
QT_MOC_LITERAL(7, 56, 7), // "Display"
QT_MOC_LITERAL(8, 64, 7), // "spacing"
QT_MOC_LITERAL(9, 72, 8), // "mirrored"
QT_MOC_LITERAL(10, 81, 9), // "alignment"
QT_MOC_LITERAL(11, 91, 13), // "Qt::Alignment"
QT_MOC_LITERAL(12, 105, 10), // "topPadding"
QT_MOC_LITERAL(13, 116, 11), // "leftPadding"
QT_MOC_LITERAL(14, 128, 12), // "rightPadding"
QT_MOC_LITERAL(15, 141, 13), // "bottomPadding"
QT_MOC_LITERAL(16, 155, 8), // "IconOnly"
QT_MOC_LITERAL(17, 164, 8), // "TextOnly"
QT_MOC_LITERAL(18, 173, 14), // "TextBesideIcon"
QT_MOC_LITERAL(19, 188, 13) // "TextUnderIcon"

    },
    "QQuickIconLabel\0icon\0QQuickIcon\0text\0"
    "font\0color\0display\0Display\0spacing\0"
    "mirrored\0alignment\0Qt::Alignment\0"
    "topPadding\0leftPadding\0rightPadding\0"
    "bottomPadding\0IconOnly\0TextOnly\0"
    "TextBesideIcon\0TextUnderIcon"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickIconLabel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
      12,   14, // properties
       1,   50, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, 0x80000000 | 2, 0x0009590b,
       3, QMetaType::QString, 0x00095903,
       4, QMetaType::QFont, 0x00095903,
       5, QMetaType::QColor, 0x00095903,
       6, 0x80000000 | 7, 0x0009590b,
       8, QMetaType::QReal, 0x00095903,
       9, QMetaType::Bool, 0x00095903,
      10, 0x80000000 | 11, 0x0009590b,
      12, QMetaType::QReal, 0x00095907,
      13, QMetaType::QReal, 0x00095907,
      14, QMetaType::QReal, 0x00095907,
      15, QMetaType::QReal, 0x00095907,

 // enums: name, alias, flags, count, data
       7,    7, 0x0,    4,   55,

 // enum data: key, value
      16, uint(QQuickIconLabel::IconOnly),
      17, uint(QQuickIconLabel::TextOnly),
      18, uint(QQuickIconLabel::TextBesideIcon),
      19, uint(QQuickIconLabel::TextUnderIcon),

       0        // eod
};

void QQuickIconLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{

#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickIconLabel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickIcon*>(_v) = _t->icon(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        case 2: *reinterpret_cast< QFont*>(_v) = _t->font(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 4: *reinterpret_cast< Display*>(_v) = _t->display(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->spacing(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->isMirrored(); break;
        case 7: *reinterpret_cast< Qt::Alignment*>(_v) = _t->alignment(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->topPadding(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->leftPadding(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->rightPadding(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->bottomPadding(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickIconLabel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setIcon(*reinterpret_cast< QQuickIcon*>(_v)); break;
        case 1: _t->setText(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->setFont(*reinterpret_cast< QFont*>(_v)); break;
        case 3: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 4: _t->setDisplay(*reinterpret_cast< Display*>(_v)); break;
        case 5: _t->setSpacing(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setMirrored(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setAlignment(*reinterpret_cast< Qt::Alignment*>(_v)); break;
        case 8: _t->setTopPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setLeftPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 10: _t->setRightPadding(*reinterpret_cast< qreal*>(_v)); break;
        case 11: _t->setBottomPadding(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
        QQuickIconLabel *_t = static_cast<QQuickIconLabel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 8: _t->resetTopPadding(); break;
        case 9: _t->resetLeftPadding(); break;
        case 10: _t->resetRightPadding(); break;
        case 11: _t->resetBottomPadding(); break;
        default: break;
        }
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQuickIconLabel::staticMetaObject = { {
    &QQuickItem::staticMetaObject,
    qt_meta_stringdata_QQuickIconLabel.data,
    qt_meta_data_QQuickIconLabel,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickIconLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickIconLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickIconLabel.stringdata0))
        return static_cast<void*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int QQuickIconLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 12;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 12;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 12;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 12;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 12;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
