/****************************************************************************
** Meta object code from reading C++ file 'qvirtualkeyboardinputcontext_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qvirtualkeyboardinputcontext_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qvirtualkeyboardinputcontext_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QVirtualKeyboardInputContextPrivate_t {
    QByteArrayData data[40];
    char stringdata0[630];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QVirtualKeyboardInputContextPrivate_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QVirtualKeyboardInputContextPrivate_t qt_meta_stringdata_QVirtualKeyboardInputContextPrivate = {
    {
QT_MOC_LITERAL(0, 0, 35), // "QVirtualKeyboardInputContextP..."
QT_MOC_LITERAL(1, 36, 12), // "focusChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 24), // "keyboardRectangleChanged"
QT_MOC_LITERAL(4, 75, 23), // "previewRectangleChanged"
QT_MOC_LITERAL(5, 99, 21), // "previewVisibleChanged"
QT_MOC_LITERAL(6, 121, 13), // "localeChanged"
QT_MOC_LITERAL(7, 135, 16), // "inputItemChanged"
QT_MOC_LITERAL(8, 152, 20), // "navigationKeyPressed"
QT_MOC_LITERAL(9, 173, 3), // "key"
QT_MOC_LITERAL(10, 177, 12), // "isAutoRepeat"
QT_MOC_LITERAL(11, 190, 21), // "navigationKeyReleased"
QT_MOC_LITERAL(12, 212, 14), // "hideInputPanel"
QT_MOC_LITERAL(13, 227, 22), // "updateAvailableLocales"
QT_MOC_LITERAL(14, 250, 16), // "availableLocales"
QT_MOC_LITERAL(15, 267, 19), // "forceCursorPosition"
QT_MOC_LITERAL(16, 287, 14), // "anchorPosition"
QT_MOC_LITERAL(17, 302, 14), // "cursorPosition"
QT_MOC_LITERAL(18, 317, 18), // "onInputItemChanged"
QT_MOC_LITERAL(19, 336, 10), // "fileExists"
QT_MOC_LITERAL(20, 347, 7), // "fileUrl"
QT_MOC_LITERAL(21, 355, 17), // "hasEnterKeyAction"
QT_MOC_LITERAL(22, 373, 4), // "item"
QT_MOC_LITERAL(23, 378, 5), // "focus"
QT_MOC_LITERAL(24, 384, 17), // "keyboardRectangle"
QT_MOC_LITERAL(25, 402, 16), // "previewRectangle"
QT_MOC_LITERAL(26, 419, 14), // "previewVisible"
QT_MOC_LITERAL(27, 434, 6), // "locale"
QT_MOC_LITERAL(28, 441, 9), // "inputItem"
QT_MOC_LITERAL(29, 451, 12), // "shiftHandler"
QT_MOC_LITERAL(30, 464, 32), // "QtVirtualKeyboard::ShiftHandler*"
QT_MOC_LITERAL(31, 497, 6), // "shadow"
QT_MOC_LITERAL(32, 504, 38), // "QtVirtualKeyboard::ShadowInpu..."
QT_MOC_LITERAL(33, 543, 12), // "inputMethods"
QT_MOC_LITERAL(34, 556, 5), // "State"
QT_MOC_LITERAL(35, 562, 8), // "Reselect"
QT_MOC_LITERAL(36, 571, 16), // "InputMethodEvent"
QT_MOC_LITERAL(37, 588, 8), // "KeyEvent"
QT_MOC_LITERAL(38, 597, 16), // "InputMethodClick"
QT_MOC_LITERAL(39, 614, 15) // "SyncShadowInput"

    },
    "QVirtualKeyboardInputContextPrivate\0"
    "focusChanged\0\0keyboardRectangleChanged\0"
    "previewRectangleChanged\0previewVisibleChanged\0"
    "localeChanged\0inputItemChanged\0"
    "navigationKeyPressed\0key\0isAutoRepeat\0"
    "navigationKeyReleased\0hideInputPanel\0"
    "updateAvailableLocales\0availableLocales\0"
    "forceCursorPosition\0anchorPosition\0"
    "cursorPosition\0onInputItemChanged\0"
    "fileExists\0fileUrl\0hasEnterKeyAction\0"
    "item\0focus\0keyboardRectangle\0"
    "previewRectangle\0previewVisible\0locale\0"
    "inputItem\0shiftHandler\0"
    "QtVirtualKeyboard::ShiftHandler*\0"
    "shadow\0QtVirtualKeyboard::ShadowInputContext*\0"
    "inputMethods\0State\0Reselect\0"
    "InputMethodEvent\0KeyEvent\0InputMethodClick\0"
    "SyncShadowInput"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QVirtualKeyboardInputContextPrivate[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       9,  116, // properties
       1,  152, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x06 /* Public */,
       3,    0,   85,    2, 0x06 /* Public */,
       4,    0,   86,    2, 0x06 /* Public */,
       5,    0,   87,    2, 0x06 /* Public */,
       6,    0,   88,    2, 0x06 /* Public */,
       7,    0,   89,    2, 0x06 /* Public */,
       8,    2,   90,    2, 0x06 /* Public */,
      11,    2,   95,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,  100,    2, 0x0a /* Public */,
      13,    1,  101,    2, 0x0a /* Public */,
      15,    2,  104,    2, 0x0a /* Public */,
      18,    0,  109,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      19,    1,  110,    2, 0x02 /* Public */,
      21,    1,  113,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,    9,   10,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,    9,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QStringList,   14,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   16,   17,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Bool, QMetaType::QUrl,   20,
    QMetaType::Bool, QMetaType::QObjectStar,   22,

 // properties: name, type, flags
      23, QMetaType::Bool, 0x00495103,
      24, QMetaType::QRectF, 0x00495103,
      25, QMetaType::QRectF, 0x00495103,
      26, QMetaType::Bool, 0x00495103,
      27, QMetaType::QString, 0x00495103,
      28, QMetaType::QObjectStar, 0x00495001,
      29, 0x80000000 | 30, 0x00095409,
      31, 0x80000000 | 32, 0x00095409,
      33, QMetaType::QStringList, 0x00095401,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       0,
       0,
       0,

 // enums: name, alias, flags, count, data
      34,   34, 0x3,    5,  157,

 // enum data: key, value
      35, uint(QVirtualKeyboardInputContextPrivate::State::Reselect),
      36, uint(QVirtualKeyboardInputContextPrivate::State::InputMethodEvent),
      37, uint(QVirtualKeyboardInputContextPrivate::State::KeyEvent),
      38, uint(QVirtualKeyboardInputContextPrivate::State::InputMethodClick),
      39, uint(QVirtualKeyboardInputContextPrivate::State::SyncShadowInput),

       0        // eod
};

void QVirtualKeyboardInputContextPrivate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QVirtualKeyboardInputContextPrivate *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->focusChanged(); break;
        case 1: _t->keyboardRectangleChanged(); break;
        case 2: _t->previewRectangleChanged(); break;
        case 3: _t->previewVisibleChanged(); break;
        case 4: _t->localeChanged(); break;
        case 5: _t->inputItemChanged(); break;
        case 6: _t->navigationKeyPressed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 7: _t->navigationKeyReleased((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 8: _t->hideInputPanel(); break;
        case 9: _t->updateAvailableLocales((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 10: _t->forceCursorPosition((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: _t->onInputItemChanged(); break;
        case 12: { bool _r = _t->fileExists((*reinterpret_cast< const QUrl(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 13: { bool _r = _t->hasEnterKeyAction((*reinterpret_cast< QObject*(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::focusChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::keyboardRectangleChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::previewRectangleChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::previewVisibleChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::localeChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::inputItemChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)(int , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::navigationKeyPressed)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QVirtualKeyboardInputContextPrivate::*)(int , bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QVirtualKeyboardInputContextPrivate::navigationKeyReleased)) {
                *result = 7;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QtVirtualKeyboard::ShadowInputContext* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QVirtualKeyboardInputContextPrivate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->focus(); break;
        case 1: *reinterpret_cast< QRectF*>(_v) = _t->keyboardRectangle(); break;
        case 2: *reinterpret_cast< QRectF*>(_v) = _t->previewRectangle(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->previewVisible(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->locale(); break;
        case 5: *reinterpret_cast< QObject**>(_v) = _t->inputItem(); break;
        case 6: *reinterpret_cast< QtVirtualKeyboard::ShiftHandler**>(_v) = _t->shiftHandler(); break;
        case 7: *reinterpret_cast< QtVirtualKeyboard::ShadowInputContext**>(_v) = _t->shadow(); break;
        case 8: *reinterpret_cast< QStringList*>(_v) = _t->inputMethods(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QVirtualKeyboardInputContextPrivate *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setFocus(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setKeyboardRectangle(*reinterpret_cast< QRectF*>(_v)); break;
        case 2: _t->setPreviewRectangle(*reinterpret_cast< QRectF*>(_v)); break;
        case 3: _t->setPreviewVisible(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->setLocale(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QVirtualKeyboardInputContextPrivate::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QVirtualKeyboardInputContextPrivate.data,
    qt_meta_data_QVirtualKeyboardInputContextPrivate,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QVirtualKeyboardInputContextPrivate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QVirtualKeyboardInputContextPrivate::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QVirtualKeyboardInputContextPrivate.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QVirtualKeyboardInputContextPrivate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QVirtualKeyboardInputContextPrivate::focusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QVirtualKeyboardInputContextPrivate::keyboardRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QVirtualKeyboardInputContextPrivate::previewRectangleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QVirtualKeyboardInputContextPrivate::previewVisibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QVirtualKeyboardInputContextPrivate::localeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QVirtualKeyboardInputContextPrivate::inputItemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QVirtualKeyboardInputContextPrivate::navigationKeyPressed(int _t1, bool _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QVirtualKeyboardInputContextPrivate::navigationKeyReleased(int _t1, bool _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
