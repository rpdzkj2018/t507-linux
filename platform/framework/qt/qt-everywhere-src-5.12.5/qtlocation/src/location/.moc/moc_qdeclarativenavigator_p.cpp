/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativenavigator_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../labs/qdeclarativenavigator_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativenavigator_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeNavigator_t {
    QByteArrayData data[33];
    char stringdata0[532];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeNavigator_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeNavigator_t qt_meta_stringdata_QDeclarativeNavigator = {
    {
QT_MOC_LITERAL(0, 0, 21), // "QDeclarativeNavigator"
QT_MOC_LITERAL(1, 22, 21), // "navigatorReadyChanged"
QT_MOC_LITERAL(2, 44, 0), // ""
QT_MOC_LITERAL(3, 45, 5), // "ready"
QT_MOC_LITERAL(4, 51, 26), // "trackPositionSourceChanged"
QT_MOC_LITERAL(5, 78, 19), // "trackPositionSource"
QT_MOC_LITERAL(6, 98, 13), // "activeChanged"
QT_MOC_LITERAL(7, 112, 6), // "active"
QT_MOC_LITERAL(8, 119, 15), // "waypointReached"
QT_MOC_LITERAL(9, 135, 30), // "const QDeclarativeGeoWaypoint*"
QT_MOC_LITERAL(10, 166, 3), // "pos"
QT_MOC_LITERAL(11, 170, 18), // "destinationReached"
QT_MOC_LITERAL(12, 189, 13), // "pluginChanged"
QT_MOC_LITERAL(13, 203, 10), // "mapChanged"
QT_MOC_LITERAL(14, 214, 12), // "routeChanged"
QT_MOC_LITERAL(15, 227, 21), // "positionSourceChanged"
QT_MOC_LITERAL(16, 249, 19), // "currentRouteChanged"
QT_MOC_LITERAL(17, 269, 21), // "currentSegmentChanged"
QT_MOC_LITERAL(18, 291, 21), // "onCurrentRouteChanged"
QT_MOC_LITERAL(19, 313, 9), // "QGeoRoute"
QT_MOC_LITERAL(20, 323, 5), // "route"
QT_MOC_LITERAL(21, 329, 23), // "onCurrentSegmentChanged"
QT_MOC_LITERAL(22, 353, 7), // "segment"
QT_MOC_LITERAL(23, 361, 6), // "plugin"
QT_MOC_LITERAL(24, 368, 31), // "QDeclarativeGeoServiceProvider*"
QT_MOC_LITERAL(25, 400, 3), // "map"
QT_MOC_LITERAL(26, 404, 19), // "QDeclarativeGeoMap*"
QT_MOC_LITERAL(27, 424, 21), // "QDeclarativeGeoRoute*"
QT_MOC_LITERAL(28, 446, 14), // "positionSource"
QT_MOC_LITERAL(29, 461, 27), // "QDeclarativePositionSource*"
QT_MOC_LITERAL(30, 489, 14), // "navigatorReady"
QT_MOC_LITERAL(31, 504, 12), // "currentRoute"
QT_MOC_LITERAL(32, 517, 14) // "currentSegment"

    },
    "QDeclarativeNavigator\0navigatorReadyChanged\0"
    "\0ready\0trackPositionSourceChanged\0"
    "trackPositionSource\0activeChanged\0"
    "active\0waypointReached\0"
    "const QDeclarativeGeoWaypoint*\0pos\0"
    "destinationReached\0pluginChanged\0"
    "mapChanged\0routeChanged\0positionSourceChanged\0"
    "currentRouteChanged\0currentSegmentChanged\0"
    "onCurrentRouteChanged\0QGeoRoute\0route\0"
    "onCurrentSegmentChanged\0segment\0plugin\0"
    "QDeclarativeGeoServiceProvider*\0map\0"
    "QDeclarativeGeoMap*\0QDeclarativeGeoRoute*\0"
    "positionSource\0QDeclarativePositionSource*\0"
    "navigatorReady\0currentRoute\0currentSegment"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeNavigator[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       9,  104, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x06 /* Public */,
       4,    1,   82,    2, 0x06 /* Public */,
       6,    1,   85,    2, 0x06 /* Public */,
       8,    1,   88,    2, 0x06 /* Public */,
      11,    0,   91,    2, 0x06 /* Public */,
      12,    0,   92,    2, 0x06 /* Public */,
      13,    0,   93,    2, 0x06 /* Public */,
      14,    0,   94,    2, 0x06 /* Public */,
      15,    0,   95,    2, 0x06 /* Public */,
      16,    0,   96,    2, 0x06 /* Public */,
      17,    0,   97,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      18,    1,   98,    2, 0x08 /* Private */,
      21,    1,  101,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void, QMetaType::Bool,    5,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 19,   20,
    QMetaType::Void, QMetaType::Int,   22,

 // properties: name, type, flags
      23, 0x80000000 | 24, 0x0049510b,
      25, 0x80000000 | 26, 0x0049510b,
      20, 0x80000000 | 27, 0x0049510b,
      28, 0x80000000 | 29, 0x0049510b,
       7, QMetaType::Bool, 0x00495103,
      30, QMetaType::Bool, 0x00495001,
       5, QMetaType::Bool, 0x00495103,
      31, 0x80000000 | 27, 0x00495009,
      32, QMetaType::Int, 0x00495001,

 // properties: notify_signal_id
       5,
       6,
       7,
       8,
       2,
       0,
       1,
       9,
      10,

       0        // eod
};

void QDeclarativeNavigator::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeNavigator *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->navigatorReadyChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->trackPositionSourceChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->activeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->waypointReached((*reinterpret_cast< const QDeclarativeGeoWaypoint*(*)>(_a[1]))); break;
        case 4: _t->destinationReached(); break;
        case 5: _t->pluginChanged(); break;
        case 6: _t->mapChanged(); break;
        case 7: _t->routeChanged(); break;
        case 8: _t->positionSourceChanged(); break;
        case 9: _t->currentRouteChanged(); break;
        case 10: _t->currentSegmentChanged(); break;
        case 11: _t->onCurrentRouteChanged((*reinterpret_cast< const QGeoRoute(*)>(_a[1]))); break;
        case 12: _t->onCurrentSegmentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeNavigator::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::navigatorReadyChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::trackPositionSourceChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::activeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)(const QDeclarativeGeoWaypoint * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::waypointReached)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::destinationReached)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::pluginChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::mapChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::routeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::positionSourceChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::currentRouteChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QDeclarativeNavigator::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeNavigator::currentSegmentChanged)) {
                *result = 10;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeNavigator *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v) = _t->plugin(); break;
        case 1: *reinterpret_cast< QDeclarativeGeoMap**>(_v) = _t->map(); break;
        case 2: *reinterpret_cast< QDeclarativeGeoRoute**>(_v) = _t->route(); break;
        case 3: *reinterpret_cast< QDeclarativePositionSource**>(_v) = _t->positionSource(); break;
        case 4: *reinterpret_cast< bool*>(_v) = _t->active(); break;
        case 5: *reinterpret_cast< bool*>(_v) = _t->navigatorReady(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->trackPositionSource(); break;
        case 7: *reinterpret_cast< QDeclarativeGeoRoute**>(_v) = _t->currentRoute(); break;
        case 8: *reinterpret_cast< int*>(_v) = _t->currentSegment(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeNavigator *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPlugin(*reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v)); break;
        case 1: _t->setMap(*reinterpret_cast< QDeclarativeGeoMap**>(_v)); break;
        case 2: _t->setRoute(*reinterpret_cast< QDeclarativeGeoRoute**>(_v)); break;
        case 3: _t->setPositionSource(*reinterpret_cast< QDeclarativePositionSource**>(_v)); break;
        case 4: _t->setActive(*reinterpret_cast< bool*>(_v)); break;
        case 6: _t->setTrackPositionSource(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeNavigator::staticMetaObject = { {
    &QParameterizableObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeNavigator.data,
    qt_meta_data_QDeclarativeNavigator,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeNavigator::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeNavigator::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeNavigator.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QParameterizableObject::qt_metacast(_clname);
}

int QDeclarativeNavigator::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QParameterizableObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeNavigator::navigatorReadyChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeNavigator::trackPositionSourceChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeNavigator::activeChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeNavigator::waypointReached(const QDeclarativeGeoWaypoint * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QDeclarativeNavigator::destinationReached()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeNavigator::pluginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeNavigator::mapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeNavigator::routeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeNavigator::positionSourceChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeNavigator::currentRouteChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void QDeclarativeNavigator::currentSegmentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
