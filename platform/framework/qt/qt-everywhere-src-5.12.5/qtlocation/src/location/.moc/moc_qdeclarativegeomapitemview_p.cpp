/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeomapitemview_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qdeclarativegeomapitemview_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeomapitemview_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeGeoMapItemView_t {
    QByteArrayData data[26];
    char stringdata0[341];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoMapItemView_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoMapItemView_t qt_meta_stringdata_QDeclarativeGeoMapItemView = {
    {
QT_MOC_LITERAL(0, 0, 26), // "QDeclarativeGeoMapItemView"
QT_MOC_LITERAL(1, 27, 12), // "modelChanged"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 15), // "delegateChanged"
QT_MOC_LITERAL(4, 57, 22), // "autoFitViewportChanged"
QT_MOC_LITERAL(5, 80, 24), // "incubateDelegatesChanged"
QT_MOC_LITERAL(6, 105, 14), // "destroyingItem"
QT_MOC_LITERAL(7, 120, 6), // "object"
QT_MOC_LITERAL(8, 127, 8), // "initItem"
QT_MOC_LITERAL(9, 136, 5), // "index"
QT_MOC_LITERAL(10, 142, 11), // "createdItem"
QT_MOC_LITERAL(11, 154, 12), // "modelUpdated"
QT_MOC_LITERAL(12, 167, 13), // "QQmlChangeSet"
QT_MOC_LITERAL(13, 181, 9), // "changeSet"
QT_MOC_LITERAL(14, 191, 5), // "reset"
QT_MOC_LITERAL(15, 197, 22), // "exitTransitionFinished"
QT_MOC_LITERAL(16, 220, 5), // "model"
QT_MOC_LITERAL(17, 226, 8), // "delegate"
QT_MOC_LITERAL(18, 235, 14), // "QQmlComponent*"
QT_MOC_LITERAL(19, 250, 15), // "autoFitViewport"
QT_MOC_LITERAL(20, 266, 3), // "add"
QT_MOC_LITERAL(21, 270, 17), // "QQuickTransition*"
QT_MOC_LITERAL(22, 288, 6), // "remove"
QT_MOC_LITERAL(23, 295, 8), // "mapItems"
QT_MOC_LITERAL(24, 304, 18), // "QList<QQuickItem*>"
QT_MOC_LITERAL(25, 323, 17) // "incubateDelegates"

    },
    "QDeclarativeGeoMapItemView\0modelChanged\0"
    "\0delegateChanged\0autoFitViewportChanged\0"
    "incubateDelegatesChanged\0destroyingItem\0"
    "object\0initItem\0index\0createdItem\0"
    "modelUpdated\0QQmlChangeSet\0changeSet\0"
    "reset\0exitTransitionFinished\0model\0"
    "delegate\0QQmlComponent*\0autoFitViewport\0"
    "add\0QQuickTransition*\0remove\0mapItems\0"
    "QList<QQuickItem*>\0incubateDelegates"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoMapItemView[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       7,   82, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    0,   60,    2, 0x06 /* Public */,
       4,    0,   61,    2, 0x06 /* Public */,
       5,    0,   62,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    1,   63,    2, 0x08 /* Private */,
       8,    2,   66,    2, 0x08 /* Private */,
      10,    2,   71,    2, 0x08 /* Private */,
      11,    2,   76,    2, 0x08 /* Private */,
      15,    0,   81,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QObjectStar,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,    9,    7,
    QMetaType::Void, QMetaType::Int, QMetaType::QObjectStar,    9,    7,
    QMetaType::Void, 0x80000000 | 12, QMetaType::Bool,   13,   14,
    QMetaType::Void,

 // properties: name, type, flags
      16, QMetaType::QVariant, 0x00495103,
      17, 0x80000000 | 18, 0x0049510b,
      19, QMetaType::Bool, 0x00495103,
      20, 0x80000000 | 21, 0x0089500b,
      22, 0x80000000 | 21, 0x0089500b,
      23, 0x80000000 | 24, 0x00895009,
      25, QMetaType::Bool, 0x00c95103,

 // properties: notify_signal_id
       0,
       1,
       2,
       0,
       0,
       0,
       3,

 // properties: revision
       0,
       0,
       0,
      12,
      12,
      12,
      12,

       0        // eod
};

void QDeclarativeGeoMapItemView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoMapItemView *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelChanged(); break;
        case 1: _t->delegateChanged(); break;
        case 2: _t->autoFitViewportChanged(); break;
        case 3: _t->incubateDelegatesChanged(); break;
        case 4: _t->destroyingItem((*reinterpret_cast< QObject*(*)>(_a[1]))); break;
        case 5: _t->initItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 6: _t->createdItem((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QObject*(*)>(_a[2]))); break;
        case 7: _t->modelUpdated((*reinterpret_cast< const QQmlChangeSet(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 8: _t->exitTransitionFinished(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoMapItemView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMapItemView::modelChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMapItemView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMapItemView::delegateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMapItemView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMapItemView::autoFitViewportChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMapItemView::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMapItemView::incubateDelegatesChanged)) {
                *result = 3;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 5:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QQuickItem*> >(); break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlComponent* >(); break;
        case 4:
        case 3:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickTransition* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoMapItemView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->model(); break;
        case 1: *reinterpret_cast< QQmlComponent**>(_v) = _t->delegate(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->autoFitViewport(); break;
        case 3: *reinterpret_cast< QQuickTransition**>(_v) = _t->m_enter; break;
        case 4: *reinterpret_cast< QQuickTransition**>(_v) = _t->m_exit; break;
        case 5: *reinterpret_cast< QList<QQuickItem*>*>(_v) = _t->mapItems(); break;
        case 6: *reinterpret_cast< bool*>(_v) = _t->incubateDelegates(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoMapItemView *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setModel(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setDelegate(*reinterpret_cast< QQmlComponent**>(_v)); break;
        case 2: _t->setAutoFitViewport(*reinterpret_cast< bool*>(_v)); break;
        case 3:
            if (_t->m_enter != *reinterpret_cast< QQuickTransition**>(_v)) {
                _t->m_enter = *reinterpret_cast< QQuickTransition**>(_v);
            }
            break;
        case 4:
            if (_t->m_exit != *reinterpret_cast< QQuickTransition**>(_v)) {
                _t->m_exit = *reinterpret_cast< QQuickTransition**>(_v);
            }
            break;
        case 6: _t->setIncubateDelegates(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoMapItemView::staticMetaObject = { {
    &QDeclarativeGeoMapItemGroup::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoMapItemView.data,
    qt_meta_data_QDeclarativeGeoMapItemView,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoMapItemView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoMapItemView::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoMapItemView.stringdata0))
        return static_cast<void*>(this);
    return QDeclarativeGeoMapItemGroup::qt_metacast(_clname);
}

int QDeclarativeGeoMapItemView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDeclarativeGeoMapItemGroup::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoMapItemView::modelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeGeoMapItemView::delegateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeGeoMapItemView::autoFitViewportChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeGeoMapItemView::incubateDelegatesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
