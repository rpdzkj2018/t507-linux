/****************************************************************************
** Meta object code from reading C++ file 'qmetalroughmaterial.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../defaults/qmetalroughmaterial.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qmetalroughmaterial.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial_t {
    QByteArrayData data[20];
    char stringdata0[294];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial_t qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial = {
    {
QT_MOC_LITERAL(0, 0, 31), // "Qt3DExtras::QMetalRoughMaterial"
QT_MOC_LITERAL(1, 32, 16), // "baseColorChanged"
QT_MOC_LITERAL(2, 49, 0), // ""
QT_MOC_LITERAL(3, 50, 9), // "baseColor"
QT_MOC_LITERAL(4, 60, 16), // "metalnessChanged"
QT_MOC_LITERAL(5, 77, 9), // "metalness"
QT_MOC_LITERAL(6, 87, 16), // "roughnessChanged"
QT_MOC_LITERAL(7, 104, 9), // "roughness"
QT_MOC_LITERAL(8, 114, 23), // "ambientOcclusionChanged"
QT_MOC_LITERAL(9, 138, 16), // "ambientOcclusion"
QT_MOC_LITERAL(10, 155, 13), // "normalChanged"
QT_MOC_LITERAL(11, 169, 6), // "normal"
QT_MOC_LITERAL(12, 176, 19), // "textureScaleChanged"
QT_MOC_LITERAL(13, 196, 12), // "textureScale"
QT_MOC_LITERAL(14, 209, 12), // "setBaseColor"
QT_MOC_LITERAL(15, 222, 12), // "setMetalness"
QT_MOC_LITERAL(16, 235, 12), // "setRoughness"
QT_MOC_LITERAL(17, 248, 19), // "setAmbientOcclusion"
QT_MOC_LITERAL(18, 268, 9), // "setNormal"
QT_MOC_LITERAL(19, 278, 15) // "setTextureScale"

    },
    "Qt3DExtras::QMetalRoughMaterial\0"
    "baseColorChanged\0\0baseColor\0"
    "metalnessChanged\0metalness\0roughnessChanged\0"
    "roughness\0ambientOcclusionChanged\0"
    "ambientOcclusion\0normalChanged\0normal\0"
    "textureScaleChanged\0textureScale\0"
    "setBaseColor\0setMetalness\0setRoughness\0"
    "setAmbientOcclusion\0setNormal\0"
    "setTextureScale"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DExtras__QMetalRoughMaterial[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       6,  110, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    1,   77,    2, 0x06 /* Public */,
       6,    1,   80,    2, 0x06 /* Public */,
       8,    1,   83,    2, 0x06 /* Public */,
      10,    1,   86,    2, 0x06 /* Public */,
      12,    1,   89,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,   92,    2, 0x0a /* Public */,
      15,    1,   95,    2, 0x0a /* Public */,
      16,    1,   98,    2, 0x0a /* Public */,
      17,    1,  101,    2, 0x0a /* Public */,
      18,    1,  104,    2, 0x0a /* Public */,
      19,    1,  107,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVariant,    3,
    QMetaType::Void, QMetaType::QVariant,    5,
    QMetaType::Void, QMetaType::QVariant,    7,
    QMetaType::Void, QMetaType::QVariant,    9,
    QMetaType::Void, QMetaType::QVariant,   11,
    QMetaType::Void, QMetaType::Float,   13,

 // slots: parameters
    QMetaType::Void, QMetaType::QVariant,    3,
    QMetaType::Void, QMetaType::QVariant,    5,
    QMetaType::Void, QMetaType::QVariant,    7,
    QMetaType::Void, QMetaType::QVariant,    9,
    QMetaType::Void, QMetaType::QVariant,   11,
    QMetaType::Void, QMetaType::Float,   13,

 // properties: name, type, flags
       3, QMetaType::QVariant, 0x00495103,
       5, QMetaType::QVariant, 0x00495103,
       7, QMetaType::QVariant, 0x00495103,
       9, QMetaType::QVariant, 0x00c95103,
      11, QMetaType::QVariant, 0x00c95103,
      13, QMetaType::Float, 0x00c95103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,

 // properties: revision
       0,
       0,
       0,
      10,
      10,
      10,

       0        // eod
};

void Qt3DExtras::QMetalRoughMaterial::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QMetalRoughMaterial *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->baseColorChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 1: _t->metalnessChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 2: _t->roughnessChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 3: _t->ambientOcclusionChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 4: _t->normalChanged((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 5: _t->textureScaleChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->setBaseColor((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 7: _t->setMetalness((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 8: _t->setRoughness((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 9: _t->setAmbientOcclusion((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 10: _t->setNormal((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 11: _t->setTextureScale((*reinterpret_cast< float(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QMetalRoughMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::baseColorChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QMetalRoughMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::metalnessChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QMetalRoughMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::roughnessChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QMetalRoughMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::ambientOcclusionChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QMetalRoughMaterial::*)(const QVariant & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::normalChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QMetalRoughMaterial::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QMetalRoughMaterial::textureScaleChanged)) {
                *result = 5;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QMetalRoughMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariant*>(_v) = _t->baseColor(); break;
        case 1: *reinterpret_cast< QVariant*>(_v) = _t->metalness(); break;
        case 2: *reinterpret_cast< QVariant*>(_v) = _t->roughness(); break;
        case 3: *reinterpret_cast< QVariant*>(_v) = _t->ambientOcclusion(); break;
        case 4: *reinterpret_cast< QVariant*>(_v) = _t->normal(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->textureScale(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QMetalRoughMaterial *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setBaseColor(*reinterpret_cast< QVariant*>(_v)); break;
        case 1: _t->setMetalness(*reinterpret_cast< QVariant*>(_v)); break;
        case 2: _t->setRoughness(*reinterpret_cast< QVariant*>(_v)); break;
        case 3: _t->setAmbientOcclusion(*reinterpret_cast< QVariant*>(_v)); break;
        case 4: _t->setNormal(*reinterpret_cast< QVariant*>(_v)); break;
        case 5: _t->setTextureScale(*reinterpret_cast< float*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DExtras::QMetalRoughMaterial::staticMetaObject = { {
    &Qt3DRender::QMaterial::staticMetaObject,
    qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial.data,
    qt_meta_data_Qt3DExtras__QMetalRoughMaterial,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DExtras::QMetalRoughMaterial::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DExtras::QMetalRoughMaterial::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DExtras__QMetalRoughMaterial.stringdata0))
        return static_cast<void*>(this);
    return Qt3DRender::QMaterial::qt_metacast(_clname);
}

int Qt3DExtras::QMetalRoughMaterial::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DRender::QMaterial::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 6;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 6;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DExtras::QMetalRoughMaterial::baseColorChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DExtras::QMetalRoughMaterial::metalnessChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DExtras::QMetalRoughMaterial::roughnessChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DExtras::QMetalRoughMaterial::ambientOcclusionChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DExtras::QMetalRoughMaterial::normalChanged(const QVariant & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DExtras::QMetalRoughMaterial::textureScaleChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
