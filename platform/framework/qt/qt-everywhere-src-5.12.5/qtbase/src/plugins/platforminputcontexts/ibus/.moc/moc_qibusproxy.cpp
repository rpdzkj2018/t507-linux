/****************************************************************************
** Meta object code from reading C++ file 'qibusproxy.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qibusproxy.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qibusproxy.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QIBusProxy_t {
    QByteArrayData data[22];
    char stringdata0[301];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QIBusProxy_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QIBusProxy_t qt_meta_stringdata_QIBusProxy = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QIBusProxy"
QT_MOC_LITERAL(1, 11, 19), // "GlobalEngineChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 11), // "engine_name"
QT_MOC_LITERAL(4, 44, 18), // "CreateInputContext"
QT_MOC_LITERAL(5, 63, 34), // "QDBusPendingReply<QDBusObject..."
QT_MOC_LITERAL(6, 98, 4), // "name"
QT_MOC_LITERAL(7, 103, 4), // "Exit"
QT_MOC_LITERAL(8, 108, 19), // "QDBusPendingReply<>"
QT_MOC_LITERAL(9, 128, 7), // "restart"
QT_MOC_LITERAL(10, 136, 4), // "Ping"
QT_MOC_LITERAL(11, 141, 31), // "QDBusPendingReply<QDBusVariant>"
QT_MOC_LITERAL(12, 173, 12), // "QDBusVariant"
QT_MOC_LITERAL(13, 186, 4), // "data"
QT_MOC_LITERAL(14, 191, 17), // "RegisterComponent"
QT_MOC_LITERAL(15, 209, 10), // "components"
QT_MOC_LITERAL(16, 220, 11), // "GetProperty"
QT_MOC_LITERAL(17, 232, 16), // "QDBusPendingCall"
QT_MOC_LITERAL(18, 249, 6), // "method"
QT_MOC_LITERAL(19, 256, 12), // "GlobalEngine"
QT_MOC_LITERAL(20, 269, 15), // "getGlobalEngine"
QT_MOC_LITERAL(21, 285, 15) // "QIBusEngineDesc"

    },
    "QIBusProxy\0GlobalEngineChanged\0\0"
    "engine_name\0CreateInputContext\0"
    "QDBusPendingReply<QDBusObjectPath>\0"
    "name\0Exit\0QDBusPendingReply<>\0restart\0"
    "Ping\0QDBusPendingReply<QDBusVariant>\0"
    "QDBusVariant\0data\0RegisterComponent\0"
    "components\0GetProperty\0QDBusPendingCall\0"
    "method\0GlobalEngine\0getGlobalEngine\0"
    "QIBusEngineDesc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QIBusProxy[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    1,   57,    2, 0x0a /* Public */,
       7,    1,   60,    2, 0x0a /* Public */,
      10,    1,   63,    2, 0x0a /* Public */,
      14,    1,   66,    2, 0x0a /* Public */,
      16,    1,   69,    2, 0x0a /* Public */,
      19,    0,   72,    2, 0x0a /* Public */,
      20,    0,   73,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,

 // slots: parameters
    0x80000000 | 5, QMetaType::QString,    6,
    0x80000000 | 8, QMetaType::Bool,    9,
    0x80000000 | 11, 0x80000000 | 12,   13,
    0x80000000 | 8, 0x80000000 | 12,   15,
    0x80000000 | 17, QMetaType::QString,   18,
    0x80000000 | 17,
    0x80000000 | 21,

       0        // eod
};

void QIBusProxy::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QIBusProxy *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->GlobalEngineChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: { QDBusPendingReply<QDBusObjectPath> _r = _t->CreateInputContext((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<QDBusObjectPath>*>(_a[0]) = std::move(_r); }  break;
        case 2: { QDBusPendingReply<> _r = _t->Exit((*reinterpret_cast< bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 3: { QDBusPendingReply<QDBusVariant> _r = _t->Ping((*reinterpret_cast< const QDBusVariant(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<QDBusVariant>*>(_a[0]) = std::move(_r); }  break;
        case 4: { QDBusPendingReply<> _r = _t->RegisterComponent((*reinterpret_cast< const QDBusVariant(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 5: { QDBusPendingCall _r = _t->GetProperty((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDBusPendingCall*>(_a[0]) = std::move(_r); }  break;
        case 6: { QDBusPendingCall _r = _t->GlobalEngine();
            if (_a[0]) *reinterpret_cast< QDBusPendingCall*>(_a[0]) = std::move(_r); }  break;
        case 7: { QIBusEngineDesc _r = _t->getGlobalEngine();
            if (_a[0]) *reinterpret_cast< QIBusEngineDesc*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusVariant >(); break;
            }
            break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDBusVariant >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QIBusProxy::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QIBusProxy::GlobalEngineChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QIBusProxy::staticMetaObject = { {
    &QDBusAbstractInterface::staticMetaObject,
    qt_meta_stringdata_QIBusProxy.data,
    qt_meta_data_QIBusProxy,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QIBusProxy::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QIBusProxy::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QIBusProxy.stringdata0))
        return static_cast<void*>(this);
    return QDBusAbstractInterface::qt_metacast(_clname);
}

int QIBusProxy::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDBusAbstractInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void QIBusProxy::GlobalEngineChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
