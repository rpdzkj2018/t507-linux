/****************************************************************************
** Meta object code from reading C++ file 'qraycasterhit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../picking/qraycasterhit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qraycasterhit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QRayCasterHit_t {
    QByteArrayData data[6];
    char stringdata0[73];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QRayCasterHit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QRayCasterHit_t qt_meta_stringdata_Qt3DRender__QRayCasterHit = {
    {
QT_MOC_LITERAL(0, 0, 25), // "Qt3DRender::QRayCasterHit"
QT_MOC_LITERAL(1, 26, 7), // "HitType"
QT_MOC_LITERAL(2, 34, 11), // "TriangleHit"
QT_MOC_LITERAL(3, 46, 7), // "LineHit"
QT_MOC_LITERAL(4, 54, 8), // "PointHit"
QT_MOC_LITERAL(5, 63, 9) // "EntityHit"

    },
    "Qt3DRender::QRayCasterHit\0HitType\0"
    "TriangleHit\0LineHit\0PointHit\0EntityHit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QRayCasterHit[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       1,   14, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    4,   19,

 // enum data: key, value
       2, uint(Qt3DRender::QRayCasterHit::TriangleHit),
       3, uint(Qt3DRender::QRayCasterHit::LineHit),
       4, uint(Qt3DRender::QRayCasterHit::PointHit),
       5, uint(Qt3DRender::QRayCasterHit::EntityHit),

       0        // eod
};

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QRayCasterHit::staticMetaObject = { {
    nullptr,
    qt_meta_stringdata_Qt3DRender__QRayCasterHit.data,
    qt_meta_data_Qt3DRender__QRayCasterHit,
    nullptr,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
