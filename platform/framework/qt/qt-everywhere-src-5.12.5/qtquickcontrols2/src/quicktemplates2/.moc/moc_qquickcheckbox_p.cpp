/****************************************************************************
** Meta object code from reading C++ file 'qquickcheckbox_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickcheckbox_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickcheckbox_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickCheckBox_t {
    QByteArrayData data[10];
    char stringdata0[131];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickCheckBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickCheckBox_t qt_meta_stringdata_QQuickCheckBox = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QQuickCheckBox"
QT_MOC_LITERAL(1, 15, 15), // "tristateChanged"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 17), // "checkStateChanged"
QT_MOC_LITERAL(4, 50, 21), // "nextCheckStateChanged"
QT_MOC_LITERAL(5, 72, 8), // "tristate"
QT_MOC_LITERAL(6, 81, 10), // "checkState"
QT_MOC_LITERAL(7, 92, 14), // "Qt::CheckState"
QT_MOC_LITERAL(8, 107, 14), // "nextCheckState"
QT_MOC_LITERAL(9, 122, 8) // "QJSValue"

    },
    "QQuickCheckBox\0tristateChanged\0\0"
    "checkStateChanged\0nextCheckStateChanged\0"
    "tristate\0checkState\0Qt::CheckState\0"
    "nextCheckState\0QJSValue"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickCheckBox[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       3,   35, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   32,    2, 0x06 /* Public */,
       3,    0,   33,    2, 0x06 /* Public */,
       4,    0,   34,    2, 0x86 /* Public | MethodRevisioned */,

 // signals: revision
       0,
       0,
       4,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::Bool, 0x00495903,
       6, 0x80000000 | 7, 0x0049590b,
       8, 0x80000000 | 9, 0x00c9590b,

 // properties: notify_signal_id
       0,
       1,
       2,

 // properties: revision
       0,
       0,
       4,

       0        // eod
};

void QQuickCheckBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickCheckBox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->tristateChanged(); break;
        case 1: _t->checkStateChanged(); break;
        case 2: _t->nextCheckStateChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickCheckBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickCheckBox::tristateChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickCheckBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickCheckBox::checkStateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickCheckBox::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickCheckBox::nextCheckStateChanged)) {
                *result = 2;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickCheckBox *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->isTristate(); break;
        case 1: *reinterpret_cast< Qt::CheckState*>(_v) = _t->checkState(); break;
        case 2: *reinterpret_cast< QJSValue*>(_v) = _t->QQuickCheckBox::d_func()->nextCheckState; break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickCheckBox *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTristate(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setCheckState(*reinterpret_cast< Qt::CheckState*>(_v)); break;
        case 2: _t->QQuickCheckBox::d_func()->setNextCheckState(*reinterpret_cast< QJSValue*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickCheckBox::staticMetaObject = { {
    &QQuickAbstractButton::staticMetaObject,
    qt_meta_stringdata_QQuickCheckBox.data,
    qt_meta_data_QQuickCheckBox,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickCheckBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickCheckBox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickCheckBox.stringdata0))
        return static_cast<void*>(this);
    return QQuickAbstractButton::qt_metacast(_clname);
}

int QQuickCheckBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 3)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 3;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickCheckBox::tristateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickCheckBox::checkStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickCheckBox::nextCheckStateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
