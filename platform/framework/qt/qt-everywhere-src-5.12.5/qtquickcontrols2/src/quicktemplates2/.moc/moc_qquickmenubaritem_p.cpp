/****************************************************************************
** Meta object code from reading C++ file 'qquickmenubaritem_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickmenubaritem_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickmenubaritem_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickMenuBarItem_t {
    QByteArrayData data[11];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickMenuBarItem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickMenuBarItem_t qt_meta_stringdata_QQuickMenuBarItem = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QQuickMenuBarItem"
QT_MOC_LITERAL(1, 18, 9), // "triggered"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 14), // "menuBarChanged"
QT_MOC_LITERAL(4, 44, 11), // "menuChanged"
QT_MOC_LITERAL(5, 56, 18), // "highlightedChanged"
QT_MOC_LITERAL(6, 75, 7), // "menuBar"
QT_MOC_LITERAL(7, 83, 14), // "QQuickMenuBar*"
QT_MOC_LITERAL(8, 98, 4), // "menu"
QT_MOC_LITERAL(9, 103, 11), // "QQuickMenu*"
QT_MOC_LITERAL(10, 115, 11) // "highlighted"

    },
    "QQuickMenuBarItem\0triggered\0\0"
    "menuBarChanged\0menuChanged\0"
    "highlightedChanged\0menuBar\0QQuickMenuBar*\0"
    "menu\0QQuickMenu*\0highlighted"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickMenuBarItem[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       3,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x06 /* Public */,
       3,    0,   35,    2, 0x06 /* Public */,
       4,    0,   36,    2, 0x06 /* Public */,
       5,    0,   37,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       6, 0x80000000 | 7, 0x00495809,
       8, 0x80000000 | 9, 0x0049590b,
      10, QMetaType::Bool, 0x00495903,

 // properties: notify_signal_id
       1,
       2,
       3,

       0        // eod
};

void QQuickMenuBarItem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickMenuBarItem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->triggered(); break;
        case 1: _t->menuBarChanged(); break;
        case 2: _t->menuChanged(); break;
        case 3: _t->highlightedChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickMenuBarItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBarItem::triggered)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickMenuBarItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBarItem::menuBarChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickMenuBarItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBarItem::menuChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickMenuBarItem::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickMenuBarItem::highlightedChanged)) {
                *result = 3;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickMenuBarItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickMenuBar**>(_v) = _t->menuBar(); break;
        case 1: *reinterpret_cast< QQuickMenu**>(_v) = _t->menu(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isHighlighted(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickMenuBarItem *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setMenu(*reinterpret_cast< QQuickMenu**>(_v)); break;
        case 2: _t->setHighlighted(*reinterpret_cast< bool*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QQuickMenuBarItem::staticMetaObject = { {
    &QQuickAbstractButton::staticMetaObject,
    qt_meta_stringdata_QQuickMenuBarItem.data,
    qt_meta_data_QQuickMenuBarItem,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickMenuBarItem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickMenuBarItem::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickMenuBarItem.stringdata0))
        return static_cast<void*>(this);
    return QQuickAbstractButton::qt_metacast(_clname);
}

int QQuickMenuBarItem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractButton::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickMenuBarItem::triggered()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickMenuBarItem::menuBarChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickMenuBarItem::menuChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickMenuBarItem::highlightedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
