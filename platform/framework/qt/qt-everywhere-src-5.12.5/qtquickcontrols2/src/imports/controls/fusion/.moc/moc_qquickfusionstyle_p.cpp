/****************************************************************************
** Meta object code from reading C++ file 'qquickfusionstyle_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickfusionstyle_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickfusionstyle_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickFusionStyle_t {
    QByteArrayData data[26];
    char stringdata0[285];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickFusionStyle_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickFusionStyle_t qt_meta_stringdata_QQuickFusionStyle = {
    {
QT_MOC_LITERAL(0, 0, 17), // "QQuickFusionStyle"
QT_MOC_LITERAL(1, 18, 9), // "highlight"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 7), // "palette"
QT_MOC_LITERAL(4, 37, 15), // "highlightedText"
QT_MOC_LITERAL(5, 53, 7), // "outline"
QT_MOC_LITERAL(6, 61, 18), // "highlightedOutline"
QT_MOC_LITERAL(7, 80, 13), // "tabFrameColor"
QT_MOC_LITERAL(8, 94, 11), // "buttonColor"
QT_MOC_LITERAL(9, 106, 11), // "highlighted"
QT_MOC_LITERAL(10, 118, 4), // "down"
QT_MOC_LITERAL(11, 123, 7), // "hovered"
QT_MOC_LITERAL(12, 131, 13), // "buttonOutline"
QT_MOC_LITERAL(13, 145, 7), // "enabled"
QT_MOC_LITERAL(14, 153, 13), // "gradientStart"
QT_MOC_LITERAL(15, 167, 9), // "baseColor"
QT_MOC_LITERAL(16, 177, 12), // "gradientStop"
QT_MOC_LITERAL(17, 190, 12), // "mergedColors"
QT_MOC_LITERAL(18, 203, 6), // "colorA"
QT_MOC_LITERAL(19, 210, 6), // "colorB"
QT_MOC_LITERAL(20, 217, 6), // "factor"
QT_MOC_LITERAL(21, 224, 11), // "grooveColor"
QT_MOC_LITERAL(22, 236, 10), // "lightShade"
QT_MOC_LITERAL(23, 247, 9), // "darkShade"
QT_MOC_LITERAL(24, 257, 9), // "topShadow"
QT_MOC_LITERAL(25, 267, 17) // "innerContrastLine"

    },
    "QQuickFusionStyle\0highlight\0\0palette\0"
    "highlightedText\0outline\0highlightedOutline\0"
    "tabFrameColor\0buttonColor\0highlighted\0"
    "down\0hovered\0buttonOutline\0enabled\0"
    "gradientStart\0baseColor\0gradientStop\0"
    "mergedColors\0colorA\0colorB\0factor\0"
    "grooveColor\0lightShade\0darkShade\0"
    "topShadow\0innerContrastLine"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickFusionStyle[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       4,  174, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x02 /* Public */,
       4,    1,  102,    2, 0x02 /* Public */,
       5,    1,  105,    2, 0x02 /* Public */,
       6,    1,  108,    2, 0x02 /* Public */,
       7,    1,  111,    2, 0x02 /* Public */,
       8,    4,  114,    2, 0x02 /* Public */,
       8,    3,  123,    2, 0x22 /* Public | MethodCloned */,
       8,    2,  130,    2, 0x22 /* Public | MethodCloned */,
       8,    1,  135,    2, 0x22 /* Public | MethodCloned */,
      12,    3,  138,    2, 0x02 /* Public */,
      12,    2,  145,    2, 0x22 /* Public | MethodCloned */,
      12,    1,  150,    2, 0x22 /* Public | MethodCloned */,
      14,    1,  153,    2, 0x02 /* Public */,
      16,    1,  156,    2, 0x02 /* Public */,
      17,    3,  159,    2, 0x02 /* Public */,
      17,    2,  166,    2, 0x22 /* Public | MethodCloned */,
      21,    1,  171,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette, QMetaType::Bool, QMetaType::Bool, QMetaType::Bool,    3,    9,   10,   11,
    QMetaType::QColor, QMetaType::QPalette, QMetaType::Bool, QMetaType::Bool,    3,    9,   10,
    QMetaType::QColor, QMetaType::QPalette, QMetaType::Bool,    3,    9,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QPalette, QMetaType::Bool, QMetaType::Bool,    3,    9,   13,
    QMetaType::QColor, QMetaType::QPalette, QMetaType::Bool,    3,    9,
    QMetaType::QColor, QMetaType::QPalette,    3,
    QMetaType::QColor, QMetaType::QColor,   15,
    QMetaType::QColor, QMetaType::QColor,   15,
    QMetaType::QColor, QMetaType::QColor, QMetaType::QColor, QMetaType::Int,   18,   19,   20,
    QMetaType::QColor, QMetaType::QColor, QMetaType::QColor,   18,   19,
    QMetaType::QColor, QMetaType::QPalette,    3,

 // properties: name, type, flags
      22, QMetaType::QColor, 0x00095401,
      23, QMetaType::QColor, 0x00095401,
      24, QMetaType::QColor, 0x00095401,
      25, QMetaType::QColor, 0x00095401,

       0        // eod
};

void QQuickFusionStyle::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickFusionStyle *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QColor _r = _t->highlight((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 1: { QColor _r = _t->highlightedText((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 2: { QColor _r = _t->outline((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 3: { QColor _r = _t->highlightedOutline((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 4: { QColor _r = _t->tabFrameColor((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 5: { QColor _r = _t->buttonColor((*reinterpret_cast< const QPalette(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])),(*reinterpret_cast< bool(*)>(_a[4])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 6: { QColor _r = _t->buttonColor((*reinterpret_cast< const QPalette(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 7: { QColor _r = _t->buttonColor((*reinterpret_cast< const QPalette(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 8: { QColor _r = _t->buttonColor((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 9: { QColor _r = _t->buttonOutline((*reinterpret_cast< const QPalette(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])),(*reinterpret_cast< bool(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 10: { QColor _r = _t->buttonOutline((*reinterpret_cast< const QPalette(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 11: { QColor _r = _t->buttonOutline((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 12: { QColor _r = _t->gradientStart((*reinterpret_cast< const QColor(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 13: { QColor _r = _t->gradientStop((*reinterpret_cast< const QColor(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 14: { QColor _r = _t->mergedColors((*reinterpret_cast< const QColor(*)>(_a[1])),(*reinterpret_cast< const QColor(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 15: { QColor _r = _t->mergedColors((*reinterpret_cast< const QColor(*)>(_a[1])),(*reinterpret_cast< const QColor(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        case 16: { QColor _r = _t->grooveColor((*reinterpret_cast< const QPalette(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QColor*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickFusionStyle *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = _t->lightShade(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = _t->darkShade(); break;
        case 2: *reinterpret_cast< QColor*>(_v) = _t->topShadow(); break;
        case 3: *reinterpret_cast< QColor*>(_v) = _t->innerContrastLine(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickFusionStyle::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QQuickFusionStyle.data,
    qt_meta_data_QQuickFusionStyle,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickFusionStyle::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickFusionStyle::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickFusionStyle.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QQuickFusionStyle::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
