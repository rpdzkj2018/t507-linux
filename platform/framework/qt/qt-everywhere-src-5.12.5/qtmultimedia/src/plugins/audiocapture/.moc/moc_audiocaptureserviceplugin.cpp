/****************************************************************************
** Meta object code from reading C++ file 'audiocaptureserviceplugin.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../audiocaptureserviceplugin.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'audiocaptureserviceplugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_AudioCaptureServicePlugin_t {
    QByteArrayData data[1];
    char stringdata0[26];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_AudioCaptureServicePlugin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_AudioCaptureServicePlugin_t qt_meta_stringdata_AudioCaptureServicePlugin = {
    {
QT_MOC_LITERAL(0, 0, 25) // "AudioCaptureServicePlugin"

    },
    "AudioCaptureServicePlugin"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_AudioCaptureServicePlugin[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void AudioCaptureServicePlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject AudioCaptureServicePlugin::staticMetaObject = { {
    &QMediaServiceProviderPlugin::staticMetaObject,
    qt_meta_stringdata_AudioCaptureServicePlugin.data,
    qt_meta_data_AudioCaptureServicePlugin,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *AudioCaptureServicePlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *AudioCaptureServicePlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_AudioCaptureServicePlugin.stringdata0))
        return static_cast<void*>(this);
    return QMediaServiceProviderPlugin::qt_metacast(_clname);
}

int AudioCaptureServicePlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMediaServiceProviderPlugin::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x31,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  'm',  'e',  'd', 
    'i',  'a',  's',  'e',  'r',  'v',  'i',  'c', 
    'e',  'p',  'r',  'o',  'v',  'i',  'd',  'e', 
    'r',  'f',  'a',  'c',  't',  'o',  'r',  'y', 
    '/',  '5',  '.',  '0', 
    // "className"
    0x03,  0x78,  0x19,  'A',  'u',  'd',  'i',  'o', 
    'C',  'a',  'p',  't',  'u',  'r',  'e',  'S', 
    'e',  'r',  'v',  'i',  'c',  'e',  'P',  'l', 
    'u',  'g',  'i',  'n', 
    // "MetaData"
    0x04,  0xa2,  0x64,  'K',  'e',  'y',  's',  0x81, 
    0x6c,  'a',  'u',  'd',  'i',  'o',  'c',  'a', 
    'p',  't',  'u',  'r',  'e',  0x68,  'S',  'e', 
    'r',  'v',  'i',  'c',  'e',  's',  0x81,  0x78, 
    0x1d,  'o',  'r',  'g',  '.',  'q',  't',  '-', 
    'p',  'r',  'o',  'j',  'e',  'c',  't',  '.', 
    'q',  't',  '.',  'a',  'u',  'd',  'i',  'o', 
    's',  'o',  'u',  'r',  'c',  'e', 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(AudioCaptureServicePlugin, AudioCaptureServicePlugin)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
