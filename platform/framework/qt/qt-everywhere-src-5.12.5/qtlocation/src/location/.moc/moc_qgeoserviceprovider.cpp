/****************************************************************************
** Meta object code from reading C++ file 'qgeoserviceprovider.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../maps/qgeoserviceprovider.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeoserviceprovider.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoServiceProvider_t {
    QByteArrayData data[54];
    char stringdata0[1077];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoServiceProvider_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoServiceProvider_t qt_meta_stringdata_QGeoServiceProvider = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QGeoServiceProvider"
QT_MOC_LITERAL(1, 20, 5), // "Error"
QT_MOC_LITERAL(2, 26, 7), // "NoError"
QT_MOC_LITERAL(3, 34, 17), // "NotSupportedError"
QT_MOC_LITERAL(4, 52, 21), // "UnknownParameterError"
QT_MOC_LITERAL(5, 74, 29), // "MissingRequiredParameterError"
QT_MOC_LITERAL(6, 104, 15), // "ConnectionError"
QT_MOC_LITERAL(7, 120, 11), // "LoaderError"
QT_MOC_LITERAL(8, 132, 15), // "RoutingFeatures"
QT_MOC_LITERAL(9, 148, 14), // "RoutingFeature"
QT_MOC_LITERAL(10, 163, 17), // "NoRoutingFeatures"
QT_MOC_LITERAL(11, 181, 20), // "OnlineRoutingFeature"
QT_MOC_LITERAL(12, 202, 21), // "OfflineRoutingFeature"
QT_MOC_LITERAL(13, 224, 23), // "LocalizedRoutingFeature"
QT_MOC_LITERAL(14, 248, 19), // "RouteUpdatesFeature"
QT_MOC_LITERAL(15, 268, 24), // "AlternativeRoutesFeature"
QT_MOC_LITERAL(16, 293, 26), // "ExcludeAreasRoutingFeature"
QT_MOC_LITERAL(17, 320, 18), // "AnyRoutingFeatures"
QT_MOC_LITERAL(18, 339, 17), // "GeocodingFeatures"
QT_MOC_LITERAL(19, 357, 16), // "GeocodingFeature"
QT_MOC_LITERAL(20, 374, 19), // "NoGeocodingFeatures"
QT_MOC_LITERAL(21, 394, 22), // "OnlineGeocodingFeature"
QT_MOC_LITERAL(22, 417, 23), // "OfflineGeocodingFeature"
QT_MOC_LITERAL(23, 441, 23), // "ReverseGeocodingFeature"
QT_MOC_LITERAL(24, 465, 25), // "LocalizedGeocodingFeature"
QT_MOC_LITERAL(25, 491, 20), // "AnyGeocodingFeatures"
QT_MOC_LITERAL(26, 512, 15), // "MappingFeatures"
QT_MOC_LITERAL(27, 528, 14), // "MappingFeature"
QT_MOC_LITERAL(28, 543, 17), // "NoMappingFeatures"
QT_MOC_LITERAL(29, 561, 20), // "OnlineMappingFeature"
QT_MOC_LITERAL(30, 582, 21), // "OfflineMappingFeature"
QT_MOC_LITERAL(31, 604, 23), // "LocalizedMappingFeature"
QT_MOC_LITERAL(32, 628, 18), // "AnyMappingFeatures"
QT_MOC_LITERAL(33, 647, 14), // "PlacesFeatures"
QT_MOC_LITERAL(34, 662, 13), // "PlacesFeature"
QT_MOC_LITERAL(35, 676, 16), // "NoPlacesFeatures"
QT_MOC_LITERAL(36, 693, 19), // "OnlinePlacesFeature"
QT_MOC_LITERAL(37, 713, 20), // "OfflinePlacesFeature"
QT_MOC_LITERAL(38, 734, 16), // "SavePlaceFeature"
QT_MOC_LITERAL(39, 751, 18), // "RemovePlaceFeature"
QT_MOC_LITERAL(40, 770, 19), // "SaveCategoryFeature"
QT_MOC_LITERAL(41, 790, 21), // "RemoveCategoryFeature"
QT_MOC_LITERAL(42, 812, 27), // "PlaceRecommendationsFeature"
QT_MOC_LITERAL(43, 840, 24), // "SearchSuggestionsFeature"
QT_MOC_LITERAL(44, 865, 22), // "LocalizedPlacesFeature"
QT_MOC_LITERAL(45, 888, 20), // "NotificationsFeature"
QT_MOC_LITERAL(46, 909, 20), // "PlaceMatchingFeature"
QT_MOC_LITERAL(47, 930, 17), // "AnyPlacesFeatures"
QT_MOC_LITERAL(48, 948, 18), // "NavigationFeatures"
QT_MOC_LITERAL(49, 967, 17), // "NavigationFeature"
QT_MOC_LITERAL(50, 985, 20), // "NoNavigationFeatures"
QT_MOC_LITERAL(51, 1006, 23), // "OnlineNavigationFeature"
QT_MOC_LITERAL(52, 1030, 24), // "OfflineNavigationFeature"
QT_MOC_LITERAL(53, 1055, 21) // "AnyNavigationFeatures"

    },
    "QGeoServiceProvider\0Error\0NoError\0"
    "NotSupportedError\0UnknownParameterError\0"
    "MissingRequiredParameterError\0"
    "ConnectionError\0LoaderError\0RoutingFeatures\0"
    "RoutingFeature\0NoRoutingFeatures\0"
    "OnlineRoutingFeature\0OfflineRoutingFeature\0"
    "LocalizedRoutingFeature\0RouteUpdatesFeature\0"
    "AlternativeRoutesFeature\0"
    "ExcludeAreasRoutingFeature\0"
    "AnyRoutingFeatures\0GeocodingFeatures\0"
    "GeocodingFeature\0NoGeocodingFeatures\0"
    "OnlineGeocodingFeature\0OfflineGeocodingFeature\0"
    "ReverseGeocodingFeature\0"
    "LocalizedGeocodingFeature\0"
    "AnyGeocodingFeatures\0MappingFeatures\0"
    "MappingFeature\0NoMappingFeatures\0"
    "OnlineMappingFeature\0OfflineMappingFeature\0"
    "LocalizedMappingFeature\0AnyMappingFeatures\0"
    "PlacesFeatures\0PlacesFeature\0"
    "NoPlacesFeatures\0OnlinePlacesFeature\0"
    "OfflinePlacesFeature\0SavePlaceFeature\0"
    "RemovePlaceFeature\0SaveCategoryFeature\0"
    "RemoveCategoryFeature\0PlaceRecommendationsFeature\0"
    "SearchSuggestionsFeature\0"
    "LocalizedPlacesFeature\0NotificationsFeature\0"
    "PlaceMatchingFeature\0AnyPlacesFeatures\0"
    "NavigationFeatures\0NavigationFeature\0"
    "NoNavigationFeatures\0OnlineNavigationFeature\0"
    "OfflineNavigationFeature\0AnyNavigationFeatures"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoServiceProvider[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       6,   14, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // enums: name, alias, flags, count, data
       1,    1, 0x0,    6,   44,
       8,    9, 0x1,    8,   56,
      18,   19, 0x1,    6,   72,
      26,   27, 0x1,    5,   84,
      33,   34, 0x1,   13,   94,
      48,   49, 0x1,    4,  120,

 // enum data: key, value
       2, uint(QGeoServiceProvider::NoError),
       3, uint(QGeoServiceProvider::NotSupportedError),
       4, uint(QGeoServiceProvider::UnknownParameterError),
       5, uint(QGeoServiceProvider::MissingRequiredParameterError),
       6, uint(QGeoServiceProvider::ConnectionError),
       7, uint(QGeoServiceProvider::LoaderError),
      10, uint(QGeoServiceProvider::NoRoutingFeatures),
      11, uint(QGeoServiceProvider::OnlineRoutingFeature),
      12, uint(QGeoServiceProvider::OfflineRoutingFeature),
      13, uint(QGeoServiceProvider::LocalizedRoutingFeature),
      14, uint(QGeoServiceProvider::RouteUpdatesFeature),
      15, uint(QGeoServiceProvider::AlternativeRoutesFeature),
      16, uint(QGeoServiceProvider::ExcludeAreasRoutingFeature),
      17, uint(QGeoServiceProvider::AnyRoutingFeatures),
      20, uint(QGeoServiceProvider::NoGeocodingFeatures),
      21, uint(QGeoServiceProvider::OnlineGeocodingFeature),
      22, uint(QGeoServiceProvider::OfflineGeocodingFeature),
      23, uint(QGeoServiceProvider::ReverseGeocodingFeature),
      24, uint(QGeoServiceProvider::LocalizedGeocodingFeature),
      25, uint(QGeoServiceProvider::AnyGeocodingFeatures),
      28, uint(QGeoServiceProvider::NoMappingFeatures),
      29, uint(QGeoServiceProvider::OnlineMappingFeature),
      30, uint(QGeoServiceProvider::OfflineMappingFeature),
      31, uint(QGeoServiceProvider::LocalizedMappingFeature),
      32, uint(QGeoServiceProvider::AnyMappingFeatures),
      35, uint(QGeoServiceProvider::NoPlacesFeatures),
      36, uint(QGeoServiceProvider::OnlinePlacesFeature),
      37, uint(QGeoServiceProvider::OfflinePlacesFeature),
      38, uint(QGeoServiceProvider::SavePlaceFeature),
      39, uint(QGeoServiceProvider::RemovePlaceFeature),
      40, uint(QGeoServiceProvider::SaveCategoryFeature),
      41, uint(QGeoServiceProvider::RemoveCategoryFeature),
      42, uint(QGeoServiceProvider::PlaceRecommendationsFeature),
      43, uint(QGeoServiceProvider::SearchSuggestionsFeature),
      44, uint(QGeoServiceProvider::LocalizedPlacesFeature),
      45, uint(QGeoServiceProvider::NotificationsFeature),
      46, uint(QGeoServiceProvider::PlaceMatchingFeature),
      47, uint(QGeoServiceProvider::AnyPlacesFeatures),
      50, uint(QGeoServiceProvider::NoNavigationFeatures),
      51, uint(QGeoServiceProvider::OnlineNavigationFeature),
      52, uint(QGeoServiceProvider::OfflineNavigationFeature),
      53, uint(QGeoServiceProvider::AnyNavigationFeatures),

       0        // eod
};

void QGeoServiceProvider::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QGeoServiceProvider::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QGeoServiceProvider.data,
    qt_meta_data_QGeoServiceProvider,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGeoServiceProvider::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoServiceProvider::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoServiceProvider.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QGeoServiceProvider::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
