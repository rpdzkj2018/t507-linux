/****************************************************************************
** Meta object code from reading C++ file 'qgeoserviceproviderpluginitemsoverlay.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qgeoserviceproviderpluginitemsoverlay.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeoserviceproviderpluginitemsoverlay.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay_t {
    QByteArrayData data[1];
    char stringdata0[39];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay_t qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay = {
    {
QT_MOC_LITERAL(0, 0, 38) // "QGeoServiceProviderFactoryIte..."

    },
    "QGeoServiceProviderFactoryItemsOverlay"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoServiceProviderFactoryItemsOverlay[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QGeoServiceProviderFactoryItemsOverlay::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QGeoServiceProviderFactoryItemsOverlay::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay.data,
    qt_meta_data_QGeoServiceProviderFactoryItemsOverlay,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGeoServiceProviderFactoryItemsOverlay::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoServiceProviderFactoryItemsOverlay::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoServiceProviderFactoryItemsOverlay.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QGeoServiceProviderFactory"))
        return static_cast< QGeoServiceProviderFactory*>(this);
    if (!strcmp(_clname, "org.qt-project.qt.geoservice.serviceproviderfactory/5.0"))
        return static_cast< QGeoServiceProviderFactory*>(this);
    return QObject::qt_metacast(_clname);
}

int QGeoServiceProviderFactoryItemsOverlay::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x37,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  'g',  'e',  'o', 
    's',  'e',  'r',  'v',  'i',  'c',  'e',  '.', 
    's',  'e',  'r',  'v',  'i',  'c',  'e',  'p', 
    'r',  'o',  'v',  'i',  'd',  'e',  'r',  'f', 
    'a',  'c',  't',  'o',  'r',  'y',  '/',  '5', 
    '.',  '0', 
    // "className"
    0x03,  0x78,  0x26,  'Q',  'G',  'e',  'o',  'S', 
    'e',  'r',  'v',  'i',  'c',  'e',  'P',  'r', 
    'o',  'v',  'i',  'd',  'e',  'r',  'F',  'a', 
    'c',  't',  'o',  'r',  'y',  'I',  't',  'e', 
    'm',  's',  'O',  'v',  'e',  'r',  'l',  'a', 
    'y', 
    // "MetaData"
    0x04,  0xa5,  0x6c,  'E',  'x',  'p',  'e',  'r', 
    'i',  'm',  'e',  'n',  't',  'a',  'l',  0xf4, 
    0x68,  'F',  'e',  'a',  't',  'u',  'r',  'e', 
    's',  0x81,  0x75,  'O',  'f',  'f',  'l',  'i', 
    'n',  'e',  'M',  'a',  'p',  'p',  'i',  'n', 
    'g',  'F',  'e',  'a',  't',  'u',  'r',  'e', 
    0x64,  'K',  'e',  'y',  's',  0x81,  0x6c,  'i', 
    't',  'e',  'm',  's',  'o',  'v',  'e',  'r', 
    'l',  'a',  'y',  0x68,  'P',  'r',  'o',  'v', 
    'i',  'd',  'e',  'r',  0x6c,  'i',  't',  'e', 
    'm',  's',  'o',  'v',  'e',  'r',  'l',  'a', 
    'y',  0x67,  'V',  'e',  'r',  's',  'i',  'o', 
    'n',  0x18,  0x64, 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(QGeoServiceProviderFactoryItemsOverlay, QGeoServiceProviderFactoryItemsOverlay)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
