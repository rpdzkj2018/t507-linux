/****************************************************************************
** Meta object code from reading C++ file 'sensorproxy_interface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../sensorproxy_interface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'sensorproxy_interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NetHadessSensorProxyInterface_t {
    QByteArrayData data[12];
    char stringdata0[199];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NetHadessSensorProxyInterface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NetHadessSensorProxyInterface_t qt_meta_stringdata_NetHadessSensorProxyInterface = {
    {
QT_MOC_LITERAL(0, 0, 29), // "NetHadessSensorProxyInterface"
QT_MOC_LITERAL(1, 30, 18), // "ClaimAccelerometer"
QT_MOC_LITERAL(2, 49, 19), // "QDBusPendingReply<>"
QT_MOC_LITERAL(3, 69, 0), // ""
QT_MOC_LITERAL(4, 70, 10), // "ClaimLight"
QT_MOC_LITERAL(5, 81, 20), // "ReleaseAccelerometer"
QT_MOC_LITERAL(6, 102, 12), // "ReleaseLight"
QT_MOC_LITERAL(7, 115, 24), // "AccelerometerOrientation"
QT_MOC_LITERAL(8, 140, 16), // "HasAccelerometer"
QT_MOC_LITERAL(9, 157, 15), // "HasAmbientLight"
QT_MOC_LITERAL(10, 173, 10), // "LightLevel"
QT_MOC_LITERAL(11, 184, 14) // "LightLevelUnit"

    },
    "NetHadessSensorProxyInterface\0"
    "ClaimAccelerometer\0QDBusPendingReply<>\0"
    "\0ClaimLight\0ReleaseAccelerometer\0"
    "ReleaseLight\0AccelerometerOrientation\0"
    "HasAccelerometer\0HasAmbientLight\0"
    "LightLevel\0LightLevelUnit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NetHadessSensorProxyInterface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       5,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    3, 0x0a /* Public */,
       4,    0,   35,    3, 0x0a /* Public */,
       5,    0,   36,    3, 0x0a /* Public */,
       6,    0,   37,    3, 0x0a /* Public */,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,
    0x80000000 | 2,
    0x80000000 | 2,

 // properties: name, type, flags
       7, QMetaType::QString, 0x00095001,
       8, QMetaType::Bool, 0x00095001,
       9, QMetaType::Bool, 0x00095001,
      10, QMetaType::Double, 0x00095001,
      11, QMetaType::QString, 0x00095001,

       0        // eod
};

void NetHadessSensorProxyInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<NetHadessSensorProxyInterface *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QDBusPendingReply<> _r = _t->ClaimAccelerometer();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 1: { QDBusPendingReply<> _r = _t->ClaimLight();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 2: { QDBusPendingReply<> _r = _t->ReleaseAccelerometer();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        case 3: { QDBusPendingReply<> _r = _t->ReleaseLight();
            if (_a[0]) *reinterpret_cast< QDBusPendingReply<>*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<NetHadessSensorProxyInterface *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->accelerometerOrientation(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->hasAccelerometer(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->hasAmbientLight(); break;
        case 3: *reinterpret_cast< double*>(_v) = _t->lightLevel(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->lightLevelUnit(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject NetHadessSensorProxyInterface::staticMetaObject = { {
    &QDBusAbstractInterface::staticMetaObject,
    qt_meta_stringdata_NetHadessSensorProxyInterface.data,
    qt_meta_data_NetHadessSensorProxyInterface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *NetHadessSensorProxyInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NetHadessSensorProxyInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NetHadessSensorProxyInterface.stringdata0))
        return static_cast<void*>(this);
    return QDBusAbstractInterface::qt_metacast(_clname);
}

int NetHadessSensorProxyInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDBusAbstractInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 5;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 5;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
