/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeomap_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qdeclarativegeomap_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeomap_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeGeoMap_t {
    QByteArrayData data[105];
    char stringdata0[1704];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoMap_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoMap_t qt_meta_stringdata_QDeclarativeGeoMap = {
    {
QT_MOC_LITERAL(0, 0, 18), // "QDeclarativeGeoMap"
QT_MOC_LITERAL(1, 19, 13), // "pluginChanged"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 31), // "QDeclarativeGeoServiceProvider*"
QT_MOC_LITERAL(4, 66, 6), // "plugin"
QT_MOC_LITERAL(5, 73, 16), // "zoomLevelChanged"
QT_MOC_LITERAL(6, 90, 9), // "zoomLevel"
QT_MOC_LITERAL(7, 100, 13), // "centerChanged"
QT_MOC_LITERAL(8, 114, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(9, 129, 10), // "coordinate"
QT_MOC_LITERAL(10, 140, 20), // "activeMapTypeChanged"
QT_MOC_LITERAL(11, 161, 24), // "supportedMapTypesChanged"
QT_MOC_LITERAL(12, 186, 23), // "minimumZoomLevelChanged"
QT_MOC_LITERAL(13, 210, 23), // "maximumZoomLevelChanged"
QT_MOC_LITERAL(14, 234, 15), // "mapItemsChanged"
QT_MOC_LITERAL(15, 250, 12), // "errorChanged"
QT_MOC_LITERAL(16, 263, 22), // "copyrightLinkActivated"
QT_MOC_LITERAL(17, 286, 4), // "link"
QT_MOC_LITERAL(18, 291, 24), // "copyrightsVisibleChanged"
QT_MOC_LITERAL(19, 316, 7), // "visible"
QT_MOC_LITERAL(20, 324, 12), // "colorChanged"
QT_MOC_LITERAL(21, 337, 5), // "color"
QT_MOC_LITERAL(22, 343, 14), // "bearingChanged"
QT_MOC_LITERAL(23, 358, 7), // "bearing"
QT_MOC_LITERAL(24, 366, 11), // "tiltChanged"
QT_MOC_LITERAL(25, 378, 4), // "tilt"
QT_MOC_LITERAL(26, 383, 18), // "fieldOfViewChanged"
QT_MOC_LITERAL(27, 402, 11), // "fieldOfView"
QT_MOC_LITERAL(28, 414, 18), // "minimumTiltChanged"
QT_MOC_LITERAL(29, 433, 11), // "minimumTilt"
QT_MOC_LITERAL(30, 445, 18), // "maximumTiltChanged"
QT_MOC_LITERAL(31, 464, 11), // "maximumTilt"
QT_MOC_LITERAL(32, 476, 25), // "minimumFieldOfViewChanged"
QT_MOC_LITERAL(33, 502, 18), // "minimumFieldOfView"
QT_MOC_LITERAL(34, 521, 25), // "maximumFieldOfViewChanged"
QT_MOC_LITERAL(35, 547, 18), // "maximumFieldOfView"
QT_MOC_LITERAL(36, 566, 17), // "copyrightsChanged"
QT_MOC_LITERAL(37, 584, 15), // "copyrightsImage"
QT_MOC_LITERAL(38, 600, 14), // "copyrightsHtml"
QT_MOC_LITERAL(39, 615, 15), // "mapReadyChanged"
QT_MOC_LITERAL(40, 631, 5), // "ready"
QT_MOC_LITERAL(41, 637, 17), // "mapObjectsChanged"
QT_MOC_LITERAL(42, 655, 18), // "visibleAreaChanged"
QT_MOC_LITERAL(43, 674, 25), // "mappingManagerInitialized"
QT_MOC_LITERAL(44, 700, 11), // "pluginReady"
QT_MOC_LITERAL(45, 712, 26), // "onSupportedMapTypesChanged"
QT_MOC_LITERAL(46, 739, 27), // "onCameraCapabilitiesChanged"
QT_MOC_LITERAL(47, 767, 22), // "QGeoCameraCapabilities"
QT_MOC_LITERAL(48, 790, 21), // "oldCameraCapabilities"
QT_MOC_LITERAL(49, 812, 42), // "onAttachedCopyrightNoticeVisi..."
QT_MOC_LITERAL(50, 855, 19), // "onCameraDataChanged"
QT_MOC_LITERAL(51, 875, 14), // "QGeoCameraData"
QT_MOC_LITERAL(52, 890, 10), // "cameraData"
QT_MOC_LITERAL(53, 901, 10), // "setBearing"
QT_MOC_LITERAL(54, 912, 22), // "alignCoordinateToPoint"
QT_MOC_LITERAL(55, 935, 5), // "point"
QT_MOC_LITERAL(56, 941, 13), // "removeMapItem"
QT_MOC_LITERAL(57, 955, 27), // "QDeclarativeGeoMapItemBase*"
QT_MOC_LITERAL(58, 983, 4), // "item"
QT_MOC_LITERAL(59, 988, 10), // "addMapItem"
QT_MOC_LITERAL(60, 999, 15), // "addMapItemGroup"
QT_MOC_LITERAL(61, 1015, 28), // "QDeclarativeGeoMapItemGroup*"
QT_MOC_LITERAL(62, 1044, 9), // "itemGroup"
QT_MOC_LITERAL(63, 1054, 18), // "removeMapItemGroup"
QT_MOC_LITERAL(64, 1073, 17), // "removeMapItemView"
QT_MOC_LITERAL(65, 1091, 27), // "QDeclarativeGeoMapItemView*"
QT_MOC_LITERAL(66, 1119, 8), // "itemView"
QT_MOC_LITERAL(67, 1128, 14), // "addMapItemView"
QT_MOC_LITERAL(68, 1143, 13), // "clearMapItems"
QT_MOC_LITERAL(69, 1157, 15), // "addMapParameter"
QT_MOC_LITERAL(70, 1173, 28), // "QDeclarativeGeoMapParameter*"
QT_MOC_LITERAL(71, 1202, 9), // "parameter"
QT_MOC_LITERAL(72, 1212, 18), // "removeMapParameter"
QT_MOC_LITERAL(73, 1231, 18), // "clearMapParameters"
QT_MOC_LITERAL(74, 1250, 12), // "toCoordinate"
QT_MOC_LITERAL(75, 1263, 8), // "position"
QT_MOC_LITERAL(76, 1272, 14), // "clipToViewPort"
QT_MOC_LITERAL(77, 1287, 14), // "fromCoordinate"
QT_MOC_LITERAL(78, 1302, 21), // "fitViewportToMapItems"
QT_MOC_LITERAL(79, 1324, 28), // "fitViewportToVisibleMapItems"
QT_MOC_LITERAL(80, 1353, 3), // "pan"
QT_MOC_LITERAL(81, 1357, 2), // "dx"
QT_MOC_LITERAL(82, 1360, 2), // "dy"
QT_MOC_LITERAL(83, 1363, 12), // "prefetchData"
QT_MOC_LITERAL(84, 1376, 9), // "clearData"
QT_MOC_LITERAL(85, 1386, 7), // "gesture"
QT_MOC_LITERAL(86, 1394, 24), // "QQuickGeoMapGestureArea*"
QT_MOC_LITERAL(87, 1419, 16), // "minimumZoomLevel"
QT_MOC_LITERAL(88, 1436, 16), // "maximumZoomLevel"
QT_MOC_LITERAL(89, 1453, 13), // "activeMapType"
QT_MOC_LITERAL(90, 1467, 23), // "QDeclarativeGeoMapType*"
QT_MOC_LITERAL(91, 1491, 17), // "supportedMapTypes"
QT_MOC_LITERAL(92, 1509, 40), // "QQmlListProperty<QDeclarative..."
QT_MOC_LITERAL(93, 1550, 6), // "center"
QT_MOC_LITERAL(94, 1557, 8), // "mapItems"
QT_MOC_LITERAL(95, 1566, 15), // "QList<QObject*>"
QT_MOC_LITERAL(96, 1582, 13), // "mapParameters"
QT_MOC_LITERAL(97, 1596, 5), // "error"
QT_MOC_LITERAL(98, 1602, 26), // "QGeoServiceProvider::Error"
QT_MOC_LITERAL(99, 1629, 11), // "errorString"
QT_MOC_LITERAL(100, 1641, 13), // "visibleRegion"
QT_MOC_LITERAL(101, 1655, 9), // "QGeoShape"
QT_MOC_LITERAL(102, 1665, 17), // "copyrightsVisible"
QT_MOC_LITERAL(103, 1683, 8), // "mapReady"
QT_MOC_LITERAL(104, 1692, 11) // "visibleArea"

    },
    "QDeclarativeGeoMap\0pluginChanged\0\0"
    "QDeclarativeGeoServiceProvider*\0plugin\0"
    "zoomLevelChanged\0zoomLevel\0centerChanged\0"
    "QGeoCoordinate\0coordinate\0"
    "activeMapTypeChanged\0supportedMapTypesChanged\0"
    "minimumZoomLevelChanged\0maximumZoomLevelChanged\0"
    "mapItemsChanged\0errorChanged\0"
    "copyrightLinkActivated\0link\0"
    "copyrightsVisibleChanged\0visible\0"
    "colorChanged\0color\0bearingChanged\0"
    "bearing\0tiltChanged\0tilt\0fieldOfViewChanged\0"
    "fieldOfView\0minimumTiltChanged\0"
    "minimumTilt\0maximumTiltChanged\0"
    "maximumTilt\0minimumFieldOfViewChanged\0"
    "minimumFieldOfView\0maximumFieldOfViewChanged\0"
    "maximumFieldOfView\0copyrightsChanged\0"
    "copyrightsImage\0copyrightsHtml\0"
    "mapReadyChanged\0ready\0mapObjectsChanged\0"
    "visibleAreaChanged\0mappingManagerInitialized\0"
    "pluginReady\0onSupportedMapTypesChanged\0"
    "onCameraCapabilitiesChanged\0"
    "QGeoCameraCapabilities\0oldCameraCapabilities\0"
    "onAttachedCopyrightNoticeVisibilityChanged\0"
    "onCameraDataChanged\0QGeoCameraData\0"
    "cameraData\0setBearing\0alignCoordinateToPoint\0"
    "point\0removeMapItem\0QDeclarativeGeoMapItemBase*\0"
    "item\0addMapItem\0addMapItemGroup\0"
    "QDeclarativeGeoMapItemGroup*\0itemGroup\0"
    "removeMapItemGroup\0removeMapItemView\0"
    "QDeclarativeGeoMapItemView*\0itemView\0"
    "addMapItemView\0clearMapItems\0"
    "addMapParameter\0QDeclarativeGeoMapParameter*\0"
    "parameter\0removeMapParameter\0"
    "clearMapParameters\0toCoordinate\0"
    "position\0clipToViewPort\0fromCoordinate\0"
    "fitViewportToMapItems\0"
    "fitViewportToVisibleMapItems\0pan\0dx\0"
    "dy\0prefetchData\0clearData\0gesture\0"
    "QQuickGeoMapGestureArea*\0minimumZoomLevel\0"
    "maximumZoomLevel\0activeMapType\0"
    "QDeclarativeGeoMapType*\0supportedMapTypes\0"
    "QQmlListProperty<QDeclarativeGeoMapType>\0"
    "center\0mapItems\0QList<QObject*>\0"
    "mapParameters\0error\0QGeoServiceProvider::Error\0"
    "errorString\0visibleRegion\0QGeoShape\0"
    "copyrightsVisible\0mapReady\0visibleArea"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoMap[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      51,   14, // methods
      24,  447, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      24,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  320,    2, 0x06 /* Public */,
       5,    1,  323,    2, 0x06 /* Public */,
       7,    1,  326,    2, 0x06 /* Public */,
      10,    0,  329,    2, 0x06 /* Public */,
      11,    0,  330,    2, 0x06 /* Public */,
      12,    0,  331,    2, 0x06 /* Public */,
      13,    0,  332,    2, 0x06 /* Public */,
      14,    0,  333,    2, 0x06 /* Public */,
      15,    0,  334,    2, 0x06 /* Public */,
      16,    1,  335,    2, 0x06 /* Public */,
      18,    1,  338,    2, 0x06 /* Public */,
      20,    1,  341,    2, 0x06 /* Public */,
      22,    1,  344,    2, 0x06 /* Public */,
      24,    1,  347,    2, 0x06 /* Public */,
      26,    1,  350,    2, 0x06 /* Public */,
      28,    1,  353,    2, 0x06 /* Public */,
      30,    1,  356,    2, 0x06 /* Public */,
      32,    1,  359,    2, 0x06 /* Public */,
      34,    1,  362,    2, 0x06 /* Public */,
      36,    1,  365,    2, 0x06 /* Public */,
      36,    1,  368,    2, 0x06 /* Public */,
      39,    1,  371,    2, 0x06 /* Public */,
      41,    0,  374,    2, 0x86 /* Public | MethodRevisioned */,
      42,    0,  375,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      43,    0,  376,    2, 0x08 /* Private */,
      44,    0,  377,    2, 0x08 /* Private */,
      45,    0,  378,    2, 0x08 /* Private */,
      46,    1,  379,    2, 0x08 /* Private */,
      49,    0,  382,    2, 0x08 /* Private */,
      50,    1,  383,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      53,    2,  386,    2, 0x02 /* Public */,
      54,    2,  391,    2, 0x02 /* Public */,
      56,    1,  396,    2, 0x02 /* Public */,
      59,    1,  399,    2, 0x02 /* Public */,
      60,    1,  402,    2, 0x02 /* Public */,
      63,    1,  405,    2, 0x02 /* Public */,
      64,    1,  408,    2, 0x02 /* Public */,
      67,    1,  411,    2, 0x02 /* Public */,
      68,    0,  414,    2, 0x02 /* Public */,
      69,    1,  415,    2, 0x02 /* Public */,
      72,    1,  418,    2, 0x02 /* Public */,
      73,    0,  421,    2, 0x02 /* Public */,
      74,    2,  422,    2, 0x02 /* Public */,
      74,    1,  427,    2, 0x22 /* Public | MethodCloned */,
      77,    2,  430,    2, 0x02 /* Public */,
      77,    1,  435,    2, 0x22 /* Public | MethodCloned */,
      78,    0,  438,    2, 0x02 /* Public */,
      79,    0,  439,    2, 0x02 /* Public */,
      80,    2,  440,    2, 0x02 /* Public */,
      83,    0,  445,    2, 0x02 /* Public */,
      84,    0,  446,    2, 0x02 /* Public */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      11,
       0,

 // slots: revision
       0,
       0,
       0,
       0,
       0,
       0,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QReal,    6,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void, QMetaType::QColor,   21,
    QMetaType::Void, QMetaType::QReal,   23,
    QMetaType::Void, QMetaType::QReal,   25,
    QMetaType::Void, QMetaType::QReal,   27,
    QMetaType::Void, QMetaType::QReal,   29,
    QMetaType::Void, QMetaType::QReal,   31,
    QMetaType::Void, QMetaType::QReal,   33,
    QMetaType::Void, QMetaType::QReal,   35,
    QMetaType::Void, QMetaType::QImage,   37,
    QMetaType::Void, QMetaType::QString,   38,
    QMetaType::Void, QMetaType::Bool,   40,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 47,   48,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 51,   52,

 // methods: parameters
    QMetaType::Void, QMetaType::QReal, 0x80000000 | 8,   23,    9,
    QMetaType::Void, 0x80000000 | 8, QMetaType::QPointF,    9,   55,
    QMetaType::Void, 0x80000000 | 57,   58,
    QMetaType::Void, 0x80000000 | 57,   58,
    QMetaType::Void, 0x80000000 | 61,   62,
    QMetaType::Void, 0x80000000 | 61,   62,
    QMetaType::Void, 0x80000000 | 65,   66,
    QMetaType::Void, 0x80000000 | 65,   66,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 70,   71,
    QMetaType::Void, 0x80000000 | 70,   71,
    QMetaType::Void,
    0x80000000 | 8, QMetaType::QPointF, QMetaType::Bool,   75,   76,
    0x80000000 | 8, QMetaType::QPointF,   75,
    QMetaType::QPointF, 0x80000000 | 8, QMetaType::Bool,    9,   76,
    QMetaType::QPointF, 0x80000000 | 8,    9,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   81,   82,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      85, 0x80000000 | 86, 0x00095409,
       4, 0x80000000 | 3, 0x0049510b,
      87, QMetaType::QReal, 0x00495103,
      88, QMetaType::QReal, 0x00495103,
       6, QMetaType::QReal, 0x00495103,
      25, QMetaType::QReal, 0x00495103,
      29, QMetaType::QReal, 0x00495103,
      31, QMetaType::QReal, 0x00495103,
      23, QMetaType::QReal, 0x00495103,
      27, QMetaType::QReal, 0x00495103,
      33, QMetaType::QReal, 0x00495103,
      35, QMetaType::QReal, 0x00495103,
      89, 0x80000000 | 90, 0x0049510b,
      91, 0x80000000 | 92, 0x00495009,
      93, 0x80000000 | 8, 0x0049510b,
      94, 0x80000000 | 95, 0x00495009,
      96, 0x80000000 | 95, 0x00095009,
      97, 0x80000000 | 98, 0x00495009,
      99, QMetaType::QString, 0x00495001,
     100, 0x80000000 | 101, 0x0009510b,
     102, QMetaType::Bool, 0x00495103,
      21, QMetaType::QColor, 0x00495103,
     103, QMetaType::Bool, 0x00495001,
     104, QMetaType::QRectF, 0x00c95103,

 // properties: notify_signal_id
       0,
       0,
       5,
       6,
       1,
      13,
      15,
      16,
      12,
      14,
      17,
      17,
       3,
       4,
       2,
       7,
       0,
       8,
       8,
       0,
      10,
      11,
      21,
      23,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      12,

 // enums: name, alias, flags, count, data

 // enum data: key, value

       0        // eod
};

void QDeclarativeGeoMap::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->pluginChanged((*reinterpret_cast< QDeclarativeGeoServiceProvider*(*)>(_a[1]))); break;
        case 1: _t->zoomLevelChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 2: _t->centerChanged((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 3: _t->activeMapTypeChanged(); break;
        case 4: _t->supportedMapTypesChanged(); break;
        case 5: _t->minimumZoomLevelChanged(); break;
        case 6: _t->maximumZoomLevelChanged(); break;
        case 7: _t->mapItemsChanged(); break;
        case 8: _t->errorChanged(); break;
        case 9: _t->copyrightLinkActivated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->copyrightsVisibleChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->colorChanged((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 12: _t->bearingChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 13: _t->tiltChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 14: _t->fieldOfViewChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 15: _t->minimumTiltChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 16: _t->maximumTiltChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 17: _t->minimumFieldOfViewChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 18: _t->maximumFieldOfViewChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 19: _t->copyrightsChanged((*reinterpret_cast< const QImage(*)>(_a[1]))); break;
        case 20: _t->copyrightsChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 21: _t->mapReadyChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: _t->mapObjectsChanged(); break;
        case 23: _t->visibleAreaChanged(); break;
        case 24: _t->mappingManagerInitialized(); break;
        case 25: _t->pluginReady(); break;
        case 26: _t->onSupportedMapTypesChanged(); break;
        case 27: _t->onCameraCapabilitiesChanged((*reinterpret_cast< const QGeoCameraCapabilities(*)>(_a[1]))); break;
        case 28: _t->onAttachedCopyrightNoticeVisibilityChanged(); break;
        case 29: _t->onCameraDataChanged((*reinterpret_cast< const QGeoCameraData(*)>(_a[1]))); break;
        case 30: _t->setBearing((*reinterpret_cast< qreal(*)>(_a[1])),(*reinterpret_cast< const QGeoCoordinate(*)>(_a[2]))); break;
        case 31: _t->alignCoordinateToPoint((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< const QPointF(*)>(_a[2]))); break;
        case 32: _t->removeMapItem((*reinterpret_cast< QDeclarativeGeoMapItemBase*(*)>(_a[1]))); break;
        case 33: _t->addMapItem((*reinterpret_cast< QDeclarativeGeoMapItemBase*(*)>(_a[1]))); break;
        case 34: _t->addMapItemGroup((*reinterpret_cast< QDeclarativeGeoMapItemGroup*(*)>(_a[1]))); break;
        case 35: _t->removeMapItemGroup((*reinterpret_cast< QDeclarativeGeoMapItemGroup*(*)>(_a[1]))); break;
        case 36: _t->removeMapItemView((*reinterpret_cast< QDeclarativeGeoMapItemView*(*)>(_a[1]))); break;
        case 37: _t->addMapItemView((*reinterpret_cast< QDeclarativeGeoMapItemView*(*)>(_a[1]))); break;
        case 38: _t->clearMapItems(); break;
        case 39: _t->addMapParameter((*reinterpret_cast< QDeclarativeGeoMapParameter*(*)>(_a[1]))); break;
        case 40: _t->removeMapParameter((*reinterpret_cast< QDeclarativeGeoMapParameter*(*)>(_a[1]))); break;
        case 41: _t->clearMapParameters(); break;
        case 42: { QGeoCoordinate _r = _t->toCoordinate((*reinterpret_cast< const QPointF(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 43: { QGeoCoordinate _r = _t->toCoordinate((*reinterpret_cast< const QPointF(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 44: { QPointF _r = _t->fromCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = std::move(_r); }  break;
        case 45: { QPointF _r = _t->fromCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = std::move(_r); }  break;
        case 46: _t->fitViewportToMapItems(); break;
        case 47: _t->fitViewportToVisibleMapItems(); break;
        case 48: _t->pan((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 49: _t->prefetchData(); break;
        case 50: _t->clearData(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 29:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCameraData >(); break;
            }
            break;
        case 30:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 31:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 34:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoMapItemGroup* >(); break;
            }
            break;
        case 35:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoMapItemGroup* >(); break;
            }
            break;
        case 36:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoMapItemView* >(); break;
            }
            break;
        case 37:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoMapItemView* >(); break;
            }
            break;
        case 44:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 45:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoMap::*)(QDeclarativeGeoServiceProvider * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::pluginChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::zoomLevelChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(const QGeoCoordinate & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::centerChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::activeMapTypeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::supportedMapTypesChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::minimumZoomLevelChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::maximumZoomLevelChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::mapItemsChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::errorChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::copyrightLinkActivated)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::copyrightsVisibleChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(const QColor & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::colorChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::bearingChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::tiltChanged)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::fieldOfViewChanged)) {
                *result = 14;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::minimumTiltChanged)) {
                *result = 15;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::maximumTiltChanged)) {
                *result = 16;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::minimumFieldOfViewChanged)) {
                *result = 17;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(qreal );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::maximumFieldOfViewChanged)) {
                *result = 18;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(const QImage & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::copyrightsChanged)) {
                *result = 19;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::copyrightsChanged)) {
                *result = 20;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::mapReadyChanged)) {
                *result = 21;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::mapObjectsChanged)) {
                *result = 22;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoMap::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoMap::visibleAreaChanged)) {
                *result = 23;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 14:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
        case 19:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoShape >(); break;
        case 16:
        case 15:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QObject*> >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickGeoMapGestureArea* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickGeoMapGestureArea**>(_v) = _t->gesture(); break;
        case 1: *reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v) = _t->plugin(); break;
        case 2: *reinterpret_cast< qreal*>(_v) = _t->minimumZoomLevel(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->maximumZoomLevel(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->zoomLevel(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->tilt(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->minimumTilt(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->maximumTilt(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->bearing(); break;
        case 9: *reinterpret_cast< qreal*>(_v) = _t->fieldOfView(); break;
        case 10: *reinterpret_cast< qreal*>(_v) = _t->minimumFieldOfView(); break;
        case 11: *reinterpret_cast< qreal*>(_v) = _t->maximumFieldOfView(); break;
        case 12: *reinterpret_cast< QDeclarativeGeoMapType**>(_v) = _t->activeMapType(); break;
        case 13: *reinterpret_cast< QQmlListProperty<QDeclarativeGeoMapType>*>(_v) = _t->supportedMapTypes(); break;
        case 14: *reinterpret_cast< QGeoCoordinate*>(_v) = _t->center(); break;
        case 15: *reinterpret_cast< QList<QObject*>*>(_v) = _t->mapItems(); break;
        case 16: *reinterpret_cast< QList<QObject*>*>(_v) = _t->mapParameters(); break;
        case 17: *reinterpret_cast< QGeoServiceProvider::Error*>(_v) = _t->error(); break;
        case 18: *reinterpret_cast< QString*>(_v) = _t->errorString(); break;
        case 19: *reinterpret_cast< QGeoShape*>(_v) = _t->visibleRegion(); break;
        case 20: *reinterpret_cast< bool*>(_v) = _t->copyrightsVisible(); break;
        case 21: *reinterpret_cast< QColor*>(_v) = _t->color(); break;
        case 22: *reinterpret_cast< bool*>(_v) = _t->mapReady(); break;
        case 23: *reinterpret_cast< QRectF*>(_v) = _t->visibleArea(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoMap *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 1: _t->setPlugin(*reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v)); break;
        case 2: _t->setMinimumZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 3: _t->setMaximumZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setZoomLevel(*reinterpret_cast< qreal*>(_v)); break;
        case 5: _t->setTilt(*reinterpret_cast< qreal*>(_v)); break;
        case 6: _t->setMinimumTilt(*reinterpret_cast< qreal*>(_v)); break;
        case 7: _t->setMaximumTilt(*reinterpret_cast< qreal*>(_v)); break;
        case 8: _t->setBearing(*reinterpret_cast< qreal*>(_v)); break;
        case 9: _t->setFieldOfView(*reinterpret_cast< qreal*>(_v)); break;
        case 10: _t->setMinimumFieldOfView(*reinterpret_cast< qreal*>(_v)); break;
        case 11: _t->setMaximumFieldOfView(*reinterpret_cast< qreal*>(_v)); break;
        case 12: _t->setActiveMapType(*reinterpret_cast< QDeclarativeGeoMapType**>(_v)); break;
        case 14: _t->setCenter(*reinterpret_cast< QGeoCoordinate*>(_v)); break;
        case 19: _t->setVisibleRegion(*reinterpret_cast< QGeoShape*>(_v)); break;
        case 20: _t->setCopyrightsVisible(*reinterpret_cast< bool*>(_v)); break;
        case 21: _t->setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 23: _t->setVisibleArea(*reinterpret_cast< QRectF*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QDeclarativeGeoMap[] = {
        &QGeoServiceProvider::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoMap::staticMetaObject = { {
    &QQuickItem::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoMap.data,
    qt_meta_data_QDeclarativeGeoMap,
    qt_static_metacall,
    qt_meta_extradata_QDeclarativeGeoMap,
    nullptr
} };


const QMetaObject *QDeclarativeGeoMap::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoMap::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoMap.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QQuickItem::qt_metacast(_clname);
}

int QDeclarativeGeoMap::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickItem::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 51)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 51;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 24;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 24;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoMap::pluginChanged(QDeclarativeGeoServiceProvider * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDeclarativeGeoMap::zoomLevelChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QDeclarativeGeoMap::centerChanged(const QGeoCoordinate & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QDeclarativeGeoMap::activeMapTypeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QDeclarativeGeoMap::supportedMapTypesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeGeoMap::minimumZoomLevelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeGeoMap::maximumZoomLevelChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeGeoMap::mapItemsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeGeoMap::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeGeoMap::copyrightLinkActivated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QDeclarativeGeoMap::copyrightsVisibleChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void QDeclarativeGeoMap::colorChanged(const QColor & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QDeclarativeGeoMap::bearingChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void QDeclarativeGeoMap::tiltChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void QDeclarativeGeoMap::fieldOfViewChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void QDeclarativeGeoMap::minimumTiltChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void QDeclarativeGeoMap::maximumTiltChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void QDeclarativeGeoMap::minimumFieldOfViewChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}

// SIGNAL 18
void QDeclarativeGeoMap::maximumFieldOfViewChanged(qreal _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 18, _a);
}

// SIGNAL 19
void QDeclarativeGeoMap::copyrightsChanged(const QImage & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 19, _a);
}

// SIGNAL 20
void QDeclarativeGeoMap::copyrightsChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 20, _a);
}

// SIGNAL 21
void QDeclarativeGeoMap::mapReadyChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 21, _a);
}

// SIGNAL 22
void QDeclarativeGeoMap::mapObjectsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 22, nullptr);
}

// SIGNAL 23
void QDeclarativeGeoMap::visibleAreaChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 23, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
