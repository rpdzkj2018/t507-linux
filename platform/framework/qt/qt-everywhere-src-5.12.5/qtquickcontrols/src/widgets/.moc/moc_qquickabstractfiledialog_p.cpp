/****************************************************************************
** Meta object code from reading C++ file 'qquickabstractfiledialog_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../dialogs/qquickabstractfiledialog_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickabstractfiledialog_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickAbstractFileDialog_t {
    QByteArrayData data[43];
    char stringdata0[582];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickAbstractFileDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickAbstractFileDialog_t qt_meta_stringdata_QQuickAbstractFileDialog = {
    {
QT_MOC_LITERAL(0, 0, 24), // "QQuickAbstractFileDialog"
QT_MOC_LITERAL(1, 25, 13), // "folderChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 18), // "nameFiltersChanged"
QT_MOC_LITERAL(4, 59, 14), // "filterSelected"
QT_MOC_LITERAL(5, 74, 15), // "fileModeChanged"
QT_MOC_LITERAL(6, 90, 17), // "selectionAccepted"
QT_MOC_LITERAL(7, 108, 21), // "sidebarVisibleChanged"
QT_MOC_LITERAL(8, 130, 20), // "defaultSuffixChanged"
QT_MOC_LITERAL(9, 151, 10), // "setVisible"
QT_MOC_LITERAL(10, 162, 1), // "v"
QT_MOC_LITERAL(11, 164, 8), // "setTitle"
QT_MOC_LITERAL(12, 173, 1), // "t"
QT_MOC_LITERAL(13, 175, 17), // "setSelectExisting"
QT_MOC_LITERAL(14, 193, 1), // "s"
QT_MOC_LITERAL(15, 195, 17), // "setSelectMultiple"
QT_MOC_LITERAL(16, 213, 15), // "setSelectFolder"
QT_MOC_LITERAL(17, 229, 9), // "setFolder"
QT_MOC_LITERAL(18, 239, 1), // "f"
QT_MOC_LITERAL(19, 241, 14), // "setNameFilters"
QT_MOC_LITERAL(20, 256, 16), // "selectNameFilter"
QT_MOC_LITERAL(21, 273, 26), // "setSelectedNameFilterIndex"
QT_MOC_LITERAL(22, 300, 3), // "idx"
QT_MOC_LITERAL(23, 304, 17), // "setSidebarVisible"
QT_MOC_LITERAL(24, 322, 16), // "setDefaultSuffix"
QT_MOC_LITERAL(25, 339, 6), // "suffix"
QT_MOC_LITERAL(26, 346, 12), // "updateFolder"
QT_MOC_LITERAL(27, 359, 14), // "selectExisting"
QT_MOC_LITERAL(28, 374, 14), // "selectMultiple"
QT_MOC_LITERAL(29, 389, 12), // "selectFolder"
QT_MOC_LITERAL(30, 402, 6), // "folder"
QT_MOC_LITERAL(31, 409, 11), // "nameFilters"
QT_MOC_LITERAL(32, 421, 18), // "selectedNameFilter"
QT_MOC_LITERAL(33, 440, 28), // "selectedNameFilterExtensions"
QT_MOC_LITERAL(34, 469, 23), // "selectedNameFilterIndex"
QT_MOC_LITERAL(35, 493, 7), // "fileUrl"
QT_MOC_LITERAL(36, 501, 8), // "fileUrls"
QT_MOC_LITERAL(37, 510, 11), // "QList<QUrl>"
QT_MOC_LITERAL(38, 522, 14), // "sidebarVisible"
QT_MOC_LITERAL(39, 537, 13), // "defaultSuffix"
QT_MOC_LITERAL(40, 551, 9), // "shortcuts"
QT_MOC_LITERAL(41, 561, 8), // "QJSValue"
QT_MOC_LITERAL(42, 570, 11) // "__shortcuts"

    },
    "QQuickAbstractFileDialog\0folderChanged\0"
    "\0nameFiltersChanged\0filterSelected\0"
    "fileModeChanged\0selectionAccepted\0"
    "sidebarVisibleChanged\0defaultSuffixChanged\0"
    "setVisible\0v\0setTitle\0t\0setSelectExisting\0"
    "s\0setSelectMultiple\0setSelectFolder\0"
    "setFolder\0f\0setNameFilters\0selectNameFilter\0"
    "setSelectedNameFilterIndex\0idx\0"
    "setSidebarVisible\0setDefaultSuffix\0"
    "suffix\0updateFolder\0selectExisting\0"
    "selectMultiple\0selectFolder\0folder\0"
    "nameFilters\0selectedNameFilter\0"
    "selectedNameFilterExtensions\0"
    "selectedNameFilterIndex\0fileUrl\0"
    "fileUrls\0QList<QUrl>\0sidebarVisible\0"
    "defaultSuffix\0shortcuts\0QJSValue\0"
    "__shortcuts"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickAbstractFileDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
      14,  152, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  109,    2, 0x06 /* Public */,
       3,    0,  110,    2, 0x06 /* Public */,
       4,    0,  111,    2, 0x06 /* Public */,
       5,    0,  112,    2, 0x06 /* Public */,
       6,    0,  113,    2, 0x06 /* Public */,
       7,    0,  114,    2, 0x06 /* Public */,
       8,    0,  115,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,  116,    2, 0x0a /* Public */,
      11,    1,  119,    2, 0x0a /* Public */,
      13,    1,  122,    2, 0x0a /* Public */,
      15,    1,  125,    2, 0x0a /* Public */,
      16,    1,  128,    2, 0x0a /* Public */,
      17,    1,  131,    2, 0x0a /* Public */,
      19,    1,  134,    2, 0x0a /* Public */,
      20,    1,  137,    2, 0x0a /* Public */,
      21,    1,  140,    2, 0x0a /* Public */,
      23,    1,  143,    2, 0x0a /* Public */,
      24,    1,  146,    2, 0x0a /* Public */,
      26,    1,  149,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::QUrl,   18,
    QMetaType::Void, QMetaType::QStringList,   18,
    QMetaType::Void, QMetaType::QString,   18,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void, QMetaType::Bool,   14,
    QMetaType::Void, QMetaType::QString,   25,
    QMetaType::Void, QMetaType::QUrl,   18,

 // properties: name, type, flags
      27, QMetaType::Bool, 0x00495103,
      28, QMetaType::Bool, 0x00495103,
      29, QMetaType::Bool, 0x00495103,
      30, QMetaType::QUrl, 0x00495103,
      31, QMetaType::QStringList, 0x00495103,
      32, QMetaType::QString, 0x00495003,
      33, QMetaType::QStringList, 0x00495001,
      34, QMetaType::Int, 0x00495103,
      35, QMetaType::QUrl, 0x00495001,
      36, 0x80000000 | 37, 0x00495009,
      38, QMetaType::Bool, 0x00495103,
      39, QMetaType::QString, 0x00495103,
      40, 0x80000000 | 41, 0x00095409,
      42, 0x80000000 | 41, 0x00095409,

 // properties: notify_signal_id
       3,
       3,
       3,
       0,
       1,
       2,
       2,
       2,
       4,
       4,
       5,
       6,
       0,
       0,

       0        // eod
};

void QQuickAbstractFileDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->folderChanged(); break;
        case 1: _t->nameFiltersChanged(); break;
        case 2: _t->filterSelected(); break;
        case 3: _t->fileModeChanged(); break;
        case 4: _t->selectionAccepted(); break;
        case 5: _t->sidebarVisibleChanged(); break;
        case 6: _t->defaultSuffixChanged(); break;
        case 7: _t->setVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->setTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->setSelectExisting((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->setSelectMultiple((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: _t->setSelectFolder((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->setFolder((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 13: _t->setNameFilters((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 14: _t->selectNameFilter((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->setSelectedNameFilterIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->setSidebarVisible((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: _t->setDefaultSuffix((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 18: _t->updateFolder((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::folderChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::nameFiltersChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::filterSelected)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::fileModeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::selectionAccepted)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::sidebarVisibleChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickAbstractFileDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickAbstractFileDialog::defaultSuffixChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 13:
        case 12:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 9:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<QUrl> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->selectExisting(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->selectMultiple(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->selectFolder(); break;
        case 3: *reinterpret_cast< QUrl*>(_v) = _t->folder(); break;
        case 4: *reinterpret_cast< QStringList*>(_v) = _t->nameFilters(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->selectedNameFilter(); break;
        case 6: *reinterpret_cast< QStringList*>(_v) = _t->selectedNameFilterExtensions(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->selectedNameFilterIndex(); break;
        case 8: *reinterpret_cast< QUrl*>(_v) = _t->fileUrl(); break;
        case 9: *reinterpret_cast< QList<QUrl>*>(_v) = _t->fileUrls(); break;
        case 10: *reinterpret_cast< bool*>(_v) = _t->sidebarVisible(); break;
        case 11: *reinterpret_cast< QString*>(_v) = _t->defaultSuffix(); break;
        case 12: *reinterpret_cast< QJSValue*>(_v) = _t->shortcuts(); break;
        case 13: *reinterpret_cast< QJSValue*>(_v) = _t->__shortcuts(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickAbstractFileDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setSelectExisting(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setSelectMultiple(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->setSelectFolder(*reinterpret_cast< bool*>(_v)); break;
        case 3: _t->setFolder(*reinterpret_cast< QUrl*>(_v)); break;
        case 4: _t->setNameFilters(*reinterpret_cast< QStringList*>(_v)); break;
        case 5: _t->selectNameFilter(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->setSelectedNameFilterIndex(*reinterpret_cast< int*>(_v)); break;
        case 10: _t->setSidebarVisible(*reinterpret_cast< bool*>(_v)); break;
        case 11: _t->setDefaultSuffix(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickAbstractFileDialog::staticMetaObject = { {
    &QQuickAbstractDialog::staticMetaObject,
    qt_meta_stringdata_QQuickAbstractFileDialog.data,
    qt_meta_data_QQuickAbstractFileDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickAbstractFileDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickAbstractFileDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickAbstractFileDialog.stringdata0))
        return static_cast<void*>(this);
    return QQuickAbstractDialog::qt_metacast(_clname);
}

int QQuickAbstractFileDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickAbstractDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 19)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 19;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 14;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 14;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickAbstractFileDialog::folderChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickAbstractFileDialog::nameFiltersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickAbstractFileDialog::filterSelected()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickAbstractFileDialog::fileModeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickAbstractFileDialog::selectionAccepted()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickAbstractFileDialog::sidebarVisibleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickAbstractFileDialog::defaultSuffixChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
