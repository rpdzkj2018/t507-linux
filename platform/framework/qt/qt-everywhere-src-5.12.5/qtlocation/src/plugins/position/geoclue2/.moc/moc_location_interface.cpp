/****************************************************************************
** Meta object code from reading C++ file 'location_interface.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../location_interface.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'location_interface.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface_t {
    QByteArrayData data[9];
    char stringdata0[113];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface_t qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface = {
    {
QT_MOC_LITERAL(0, 0, 39), // "OrgFreedesktopGeoClue2Locatio..."
QT_MOC_LITERAL(1, 40, 8), // "Accuracy"
QT_MOC_LITERAL(2, 49, 8), // "Altitude"
QT_MOC_LITERAL(3, 58, 11), // "Description"
QT_MOC_LITERAL(4, 70, 7), // "Heading"
QT_MOC_LITERAL(5, 78, 8), // "Latitude"
QT_MOC_LITERAL(6, 87, 9), // "Longitude"
QT_MOC_LITERAL(7, 97, 5), // "Speed"
QT_MOC_LITERAL(8, 103, 9) // "Timestamp"

    },
    "OrgFreedesktopGeoClue2LocationInterface\0"
    "Accuracy\0Altitude\0Description\0Heading\0"
    "Latitude\0Longitude\0Speed\0Timestamp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OrgFreedesktopGeoClue2LocationInterface[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       8,   14, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // properties: name, type, flags
       1, QMetaType::Double, 0x00095001,
       2, QMetaType::Double, 0x00095001,
       3, QMetaType::QString, 0x00095001,
       4, QMetaType::Double, 0x00095001,
       5, QMetaType::Double, 0x00095001,
       6, QMetaType::Double, 0x00095001,
       7, QMetaType::Double, 0x00095001,
       8, 0x80000000 | 8, 0x00095009,

       0        // eod
};

void OrgFreedesktopGeoClue2LocationInterface::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Timestamp >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<OrgFreedesktopGeoClue2LocationInterface *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = _t->accuracy(); break;
        case 1: *reinterpret_cast< double*>(_v) = _t->altitude(); break;
        case 2: *reinterpret_cast< QString*>(_v) = _t->description(); break;
        case 3: *reinterpret_cast< double*>(_v) = _t->heading(); break;
        case 4: *reinterpret_cast< double*>(_v) = _t->latitude(); break;
        case 5: *reinterpret_cast< double*>(_v) = _t->longitude(); break;
        case 6: *reinterpret_cast< double*>(_v) = _t->speed(); break;
        case 7: *reinterpret_cast< Timestamp*>(_v) = _t->timestamp(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_o);
}

QT_INIT_METAOBJECT const QMetaObject OrgFreedesktopGeoClue2LocationInterface::staticMetaObject = { {
    &QDBusAbstractInterface::staticMetaObject,
    qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface.data,
    qt_meta_data_OrgFreedesktopGeoClue2LocationInterface,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OrgFreedesktopGeoClue2LocationInterface::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OrgFreedesktopGeoClue2LocationInterface::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OrgFreedesktopGeoClue2LocationInterface.stringdata0))
        return static_cast<void*>(this);
    return QDBusAbstractInterface::qt_metacast(_clname);
}

int OrgFreedesktopGeoClue2LocationInterface::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDBusAbstractInterface::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    
#ifndef QT_NO_PROPERTIES
    if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
