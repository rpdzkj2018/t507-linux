/****************************************************************************
** Meta object code from reading C++ file 'qdeclarativegeoroutemodel_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../declarativemaps/qdeclarativegeoroutemodel_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QList>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdeclarativegeoroutemodel_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QDeclarativeGeoRouteModel_t {
    QByteArrayData data[49];
    char stringdata0[669];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoRouteModel_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoRouteModel_t qt_meta_stringdata_QDeclarativeGeoRouteModel = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QDeclarativeGeoRouteModel"
QT_MOC_LITERAL(1, 26, 12), // "countChanged"
QT_MOC_LITERAL(2, 39, 0), // ""
QT_MOC_LITERAL(3, 40, 13), // "pluginChanged"
QT_MOC_LITERAL(4, 54, 12), // "queryChanged"
QT_MOC_LITERAL(5, 67, 17), // "autoUpdateChanged"
QT_MOC_LITERAL(6, 85, 13), // "statusChanged"
QT_MOC_LITERAL(7, 99, 12), // "errorChanged"
QT_MOC_LITERAL(8, 112, 13), // "routesChanged"
QT_MOC_LITERAL(9, 126, 24), // "measurementSystemChanged"
QT_MOC_LITERAL(10, 151, 14), // "abortRequested"
QT_MOC_LITERAL(11, 166, 6), // "update"
QT_MOC_LITERAL(12, 173, 15), // "routingFinished"
QT_MOC_LITERAL(13, 189, 15), // "QGeoRouteReply*"
QT_MOC_LITERAL(14, 205, 5), // "reply"
QT_MOC_LITERAL(15, 211, 12), // "routingError"
QT_MOC_LITERAL(16, 224, 21), // "QGeoRouteReply::Error"
QT_MOC_LITERAL(17, 246, 5), // "error"
QT_MOC_LITERAL(18, 252, 11), // "errorString"
QT_MOC_LITERAL(19, 264, 19), // "queryDetailsChanged"
QT_MOC_LITERAL(20, 284, 11), // "pluginReady"
QT_MOC_LITERAL(21, 296, 3), // "get"
QT_MOC_LITERAL(22, 300, 21), // "QDeclarativeGeoRoute*"
QT_MOC_LITERAL(23, 322, 5), // "index"
QT_MOC_LITERAL(24, 328, 5), // "reset"
QT_MOC_LITERAL(25, 334, 6), // "cancel"
QT_MOC_LITERAL(26, 341, 6), // "plugin"
QT_MOC_LITERAL(27, 348, 31), // "QDeclarativeGeoServiceProvider*"
QT_MOC_LITERAL(28, 380, 5), // "query"
QT_MOC_LITERAL(29, 386, 26), // "QDeclarativeGeoRouteQuery*"
QT_MOC_LITERAL(30, 413, 5), // "count"
QT_MOC_LITERAL(31, 419, 10), // "autoUpdate"
QT_MOC_LITERAL(32, 430, 6), // "status"
QT_MOC_LITERAL(33, 437, 6), // "Status"
QT_MOC_LITERAL(34, 444, 10), // "RouteError"
QT_MOC_LITERAL(35, 455, 17), // "measurementSystem"
QT_MOC_LITERAL(36, 473, 26), // "QLocale::MeasurementSystem"
QT_MOC_LITERAL(37, 500, 4), // "Null"
QT_MOC_LITERAL(38, 505, 5), // "Ready"
QT_MOC_LITERAL(39, 511, 7), // "Loading"
QT_MOC_LITERAL(40, 519, 5), // "Error"
QT_MOC_LITERAL(41, 525, 7), // "NoError"
QT_MOC_LITERAL(42, 533, 17), // "EngineNotSetError"
QT_MOC_LITERAL(43, 551, 18), // "CommunicationError"
QT_MOC_LITERAL(44, 570, 10), // "ParseError"
QT_MOC_LITERAL(45, 581, 22), // "UnsupportedOptionError"
QT_MOC_LITERAL(46, 604, 12), // "UnknownError"
QT_MOC_LITERAL(47, 617, 21), // "UnknownParameterError"
QT_MOC_LITERAL(48, 639, 29) // "MissingRequiredParameterError"

    },
    "QDeclarativeGeoRouteModel\0countChanged\0"
    "\0pluginChanged\0queryChanged\0"
    "autoUpdateChanged\0statusChanged\0"
    "errorChanged\0routesChanged\0"
    "measurementSystemChanged\0abortRequested\0"
    "update\0routingFinished\0QGeoRouteReply*\0"
    "reply\0routingError\0QGeoRouteReply::Error\0"
    "error\0errorString\0queryDetailsChanged\0"
    "pluginReady\0get\0QDeclarativeGeoRoute*\0"
    "index\0reset\0cancel\0plugin\0"
    "QDeclarativeGeoServiceProvider*\0query\0"
    "QDeclarativeGeoRouteQuery*\0count\0"
    "autoUpdate\0status\0Status\0RouteError\0"
    "measurementSystem\0QLocale::MeasurementSystem\0"
    "Null\0Ready\0Loading\0Error\0NoError\0"
    "EngineNotSetError\0CommunicationError\0"
    "ParseError\0UnsupportedOptionError\0"
    "UnknownError\0UnknownParameterError\0"
    "MissingRequiredParameterError"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoRouteModel[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       8,  126, // properties
       2,  158, // enums/sets
       0,    0, // constructors
       0,       // flags
       9,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x06 /* Public */,
       3,    0,  100,    2, 0x06 /* Public */,
       4,    0,  101,    2, 0x06 /* Public */,
       5,    0,  102,    2, 0x06 /* Public */,
       6,    0,  103,    2, 0x06 /* Public */,
       7,    0,  104,    2, 0x06 /* Public */,
       8,    0,  105,    2, 0x06 /* Public */,
       9,    0,  106,    2, 0x06 /* Public */,
      10,    0,  107,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,  108,    2, 0x0a /* Public */,
      12,    1,  109,    2, 0x08 /* Private */,
      15,    3,  112,    2, 0x08 /* Private */,
      19,    0,  119,    2, 0x08 /* Private */,
      20,    0,  120,    2, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      21,    1,  121,    2, 0x02 /* Public */,
      24,    0,  124,    2, 0x02 /* Public */,
      25,    0,  125,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 13, 0x80000000 | 16, QMetaType::QString,   14,   17,   18,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    0x80000000 | 22, QMetaType::Int,   23,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      26, 0x80000000 | 27, 0x0049510b,
      28, 0x80000000 | 29, 0x0049510b,
      30, QMetaType::Int, 0x00495001,
      31, QMetaType::Bool, 0x00495103,
      32, 0x80000000 | 33, 0x00495009,
      18, QMetaType::QString, 0x00495001,
      17, 0x80000000 | 34, 0x00495009,
      35, 0x80000000 | 36, 0x0049510b,

 // properties: notify_signal_id
       1,
       2,
       0,
       3,
       4,
       5,
       5,
       7,

 // enums: name, alias, flags, count, data
      33,   33, 0x0,    4,  168,
      34,   34, 0x0,    8,  176,

 // enum data: key, value
      37, uint(QDeclarativeGeoRouteModel::Null),
      38, uint(QDeclarativeGeoRouteModel::Ready),
      39, uint(QDeclarativeGeoRouteModel::Loading),
      40, uint(QDeclarativeGeoRouteModel::Error),
      41, uint(QDeclarativeGeoRouteModel::NoError),
      42, uint(QDeclarativeGeoRouteModel::EngineNotSetError),
      43, uint(QDeclarativeGeoRouteModel::CommunicationError),
      44, uint(QDeclarativeGeoRouteModel::ParseError),
      45, uint(QDeclarativeGeoRouteModel::UnsupportedOptionError),
      46, uint(QDeclarativeGeoRouteModel::UnknownError),
      47, uint(QDeclarativeGeoRouteModel::UnknownParameterError),
      48, uint(QDeclarativeGeoRouteModel::MissingRequiredParameterError),

       0        // eod
};

void QDeclarativeGeoRouteModel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoRouteModel *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->countChanged(); break;
        case 1: _t->pluginChanged(); break;
        case 2: _t->queryChanged(); break;
        case 3: _t->autoUpdateChanged(); break;
        case 4: _t->statusChanged(); break;
        case 5: _t->errorChanged(); break;
        case 6: _t->routesChanged(); break;
        case 7: _t->measurementSystemChanged(); break;
        case 8: _t->abortRequested(); break;
        case 9: _t->update(); break;
        case 10: _t->routingFinished((*reinterpret_cast< QGeoRouteReply*(*)>(_a[1]))); break;
        case 11: _t->routingError((*reinterpret_cast< QGeoRouteReply*(*)>(_a[1])),(*reinterpret_cast< QGeoRouteReply::Error(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 12: _t->queryDetailsChanged(); break;
        case 13: _t->pluginReady(); break;
        case 14: { QDeclarativeGeoRoute* _r = _t->get((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QDeclarativeGeoRoute**>(_a[0]) = std::move(_r); }  break;
        case 15: _t->reset(); break;
        case 16: _t->cancel(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoRouteReply* >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoRouteReply* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::countChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::pluginChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::queryChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::autoUpdateChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::statusChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::errorChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::routesChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::measurementSystemChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteModel::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteModel::abortRequested)) {
                *result = 8;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoRouteQuery* >(); break;
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QDeclarativeGeoServiceProvider* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoRouteModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v) = _t->plugin(); break;
        case 1: *reinterpret_cast< QDeclarativeGeoRouteQuery**>(_v) = _t->query(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->count(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->autoUpdate(); break;
        case 4: *reinterpret_cast< Status*>(_v) = _t->status(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->errorString(); break;
        case 6: *reinterpret_cast< RouteError*>(_v) = _t->error(); break;
        case 7: *reinterpret_cast< QLocale::MeasurementSystem*>(_v) = _t->measurementSystem(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoRouteModel *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPlugin(*reinterpret_cast< QDeclarativeGeoServiceProvider**>(_v)); break;
        case 1: _t->setQuery(*reinterpret_cast< QDeclarativeGeoRouteQuery**>(_v)); break;
        case 3: _t->setAutoUpdate(*reinterpret_cast< bool*>(_v)); break;
        case 7: _t->setMeasurementSystem(*reinterpret_cast< QLocale::MeasurementSystem*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

static const QMetaObject * const qt_meta_extradata_QDeclarativeGeoRouteModel[] = {
        &QLocale::staticMetaObject,
    nullptr
};

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoRouteModel::staticMetaObject = { {
    &QAbstractListModel::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoRouteModel.data,
    qt_meta_data_QDeclarativeGeoRouteModel,
    qt_static_metacall,
    qt_meta_extradata_QDeclarativeGeoRouteModel,
    nullptr
} };


const QMetaObject *QDeclarativeGeoRouteModel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoRouteModel::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoRouteModel.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QAbstractListModel::qt_metacast(_clname);
}

int QDeclarativeGeoRouteModel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QAbstractListModel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoRouteModel::countChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeGeoRouteModel::pluginChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeGeoRouteModel::queryChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeGeoRouteModel::autoUpdateChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QDeclarativeGeoRouteModel::statusChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeGeoRouteModel::errorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeGeoRouteModel::routesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeGeoRouteModel::measurementSystemChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeGeoRouteModel::abortRequested()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}
struct qt_meta_stringdata_QDeclarativeGeoWaypoint_t {
    QByteArrayData data[16];
    char stringdata0[227];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoWaypoint_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoWaypoint_t qt_meta_stringdata_QDeclarativeGeoWaypoint = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QDeclarativeGeoWaypoint"
QT_MOC_LITERAL(1, 24, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 40, 13), // "quickChildren"
QT_MOC_LITERAL(3, 54, 9), // "completed"
QT_MOC_LITERAL(4, 64, 0), // ""
QT_MOC_LITERAL(5, 65, 22), // "waypointDetailsChanged"
QT_MOC_LITERAL(6, 88, 14), // "bearingChanged"
QT_MOC_LITERAL(7, 103, 22), // "extraParametersChanged"
QT_MOC_LITERAL(8, 126, 21), // "extraParameterChanged"
QT_MOC_LITERAL(9, 148, 8), // "latitude"
QT_MOC_LITERAL(10, 157, 9), // "longitude"
QT_MOC_LITERAL(11, 167, 8), // "altitude"
QT_MOC_LITERAL(12, 176, 7), // "isValid"
QT_MOC_LITERAL(13, 184, 7), // "bearing"
QT_MOC_LITERAL(14, 192, 8), // "metadata"
QT_MOC_LITERAL(15, 201, 25) // "QQmlListProperty<QObject>"

    },
    "QDeclarativeGeoWaypoint\0DefaultProperty\0"
    "quickChildren\0completed\0\0"
    "waypointDetailsChanged\0bearingChanged\0"
    "extraParametersChanged\0extraParameterChanged\0"
    "latitude\0longitude\0altitude\0isValid\0"
    "bearing\0metadata\0QQmlListProperty<QObject>"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoWaypoint[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
       5,   16, // methods
       7,   46, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,   41,    4, 0x06 /* Public */,
       5,    0,   42,    4, 0x06 /* Public */,
       6,    0,   43,    4, 0x06 /* Public */,
       7,    0,   44,    4, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    0,   45,    4, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,

 // properties: name, type, flags
       9, QMetaType::Double, 0x00085103,
      10, QMetaType::Double, 0x00085103,
      11, QMetaType::Double, 0x00085103,
      12, QMetaType::Bool, 0x00085001,
      13, QMetaType::QReal, 0x00495103,
      14, QMetaType::QVariantMap, 0x00095001,
       2, 0x80000000 | 15, 0x00094009,

 // properties: notify_signal_id
       0,
       0,
       0,
       0,
       2,
       0,
       0,

       0        // eod
};

void QDeclarativeGeoWaypoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoWaypoint *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->completed(); break;
        case 1: _t->waypointDetailsChanged(); break;
        case 2: _t->bearingChanged(); break;
        case 3: _t->extraParametersChanged(); break;
        case 4: _t->extraParameterChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoWaypoint::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoWaypoint::completed)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoWaypoint::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoWaypoint::waypointDetailsChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoWaypoint::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoWaypoint::bearingChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoWaypoint::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoWaypoint::extraParametersChanged)) {
                *result = 3;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoWaypoint *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< double*>(_v) = _t->latitude(); break;
        case 1: *reinterpret_cast< double*>(_v) = _t->longitude(); break;
        case 2: *reinterpret_cast< double*>(_v) = _t->altitude(); break;
        case 3: *reinterpret_cast< bool*>(_v) = _t->isValid(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->bearing(); break;
        case 5: *reinterpret_cast< QVariantMap*>(_v) = _t->metadata(); break;
        case 6: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->declarativeChildren(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoWaypoint *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setLatitude(*reinterpret_cast< double*>(_v)); break;
        case 1: _t->setLongitude(*reinterpret_cast< double*>(_v)); break;
        case 2: _t->setAltitude(*reinterpret_cast< double*>(_v)); break;
        case 4: _t->setBearing(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoWaypoint::staticMetaObject = { {
    &QGeoCoordinateObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoWaypoint.data,
    qt_meta_data_QDeclarativeGeoWaypoint,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoWaypoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoWaypoint::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoWaypoint.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QGeoCoordinateObject::qt_metacast(_clname);
}

int QDeclarativeGeoWaypoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGeoCoordinateObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 7;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 7;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoWaypoint::completed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeGeoWaypoint::waypointDetailsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeGeoWaypoint::bearingChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeGeoWaypoint::extraParametersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}
struct qt_meta_stringdata_QDeclarativeGeoRouteQuery_t {
    QByteArrayData data[83];
    char stringdata0[1344];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QDeclarativeGeoRouteQuery_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QDeclarativeGeoRouteQuery_t qt_meta_stringdata_QDeclarativeGeoRouteQuery = {
    {
QT_MOC_LITERAL(0, 0, 25), // "QDeclarativeGeoRouteQuery"
QT_MOC_LITERAL(1, 26, 15), // "DefaultProperty"
QT_MOC_LITERAL(2, 42, 13), // "quickChildren"
QT_MOC_LITERAL(3, 56, 30), // "numberAlternativeRoutesChanged"
QT_MOC_LITERAL(4, 87, 0), // ""
QT_MOC_LITERAL(5, 88, 18), // "travelModesChanged"
QT_MOC_LITERAL(6, 107, 25), // "routeOptimizationsChanged"
QT_MOC_LITERAL(7, 133, 16), // "waypointsChanged"
QT_MOC_LITERAL(8, 150, 20), // "excludedAreasChanged"
QT_MOC_LITERAL(9, 171, 19), // "featureTypesChanged"
QT_MOC_LITERAL(10, 191, 21), // "maneuverDetailChanged"
QT_MOC_LITERAL(11, 213, 20), // "segmentDetailChanged"
QT_MOC_LITERAL(12, 234, 19), // "queryDetailsChanged"
QT_MOC_LITERAL(13, 254, 22), // "extraParametersChanged"
QT_MOC_LITERAL(14, 277, 29), // "excludedAreaCoordinateChanged"
QT_MOC_LITERAL(15, 307, 21), // "extraParameterChanged"
QT_MOC_LITERAL(16, 329, 15), // "waypointChanged"
QT_MOC_LITERAL(17, 345, 15), // "waypointObjects"
QT_MOC_LITERAL(18, 361, 11), // "addWaypoint"
QT_MOC_LITERAL(19, 373, 1), // "w"
QT_MOC_LITERAL(20, 375, 14), // "removeWaypoint"
QT_MOC_LITERAL(21, 390, 8), // "waypoint"
QT_MOC_LITERAL(22, 399, 14), // "clearWaypoints"
QT_MOC_LITERAL(23, 414, 15), // "addExcludedArea"
QT_MOC_LITERAL(24, 430, 13), // "QGeoRectangle"
QT_MOC_LITERAL(25, 444, 4), // "area"
QT_MOC_LITERAL(26, 449, 18), // "removeExcludedArea"
QT_MOC_LITERAL(27, 468, 18), // "clearExcludedAreas"
QT_MOC_LITERAL(28, 487, 16), // "setFeatureWeight"
QT_MOC_LITERAL(29, 504, 11), // "FeatureType"
QT_MOC_LITERAL(30, 516, 11), // "featureType"
QT_MOC_LITERAL(31, 528, 13), // "FeatureWeight"
QT_MOC_LITERAL(32, 542, 13), // "featureWeight"
QT_MOC_LITERAL(33, 556, 19), // "resetFeatureWeights"
QT_MOC_LITERAL(34, 576, 19), // "doCoordinateChanged"
QT_MOC_LITERAL(35, 596, 23), // "numberAlternativeRoutes"
QT_MOC_LITERAL(36, 620, 11), // "travelModes"
QT_MOC_LITERAL(37, 632, 11), // "TravelModes"
QT_MOC_LITERAL(38, 644, 18), // "routeOptimizations"
QT_MOC_LITERAL(39, 663, 18), // "RouteOptimizations"
QT_MOC_LITERAL(40, 682, 13), // "segmentDetail"
QT_MOC_LITERAL(41, 696, 13), // "SegmentDetail"
QT_MOC_LITERAL(42, 710, 14), // "maneuverDetail"
QT_MOC_LITERAL(43, 725, 14), // "ManeuverDetail"
QT_MOC_LITERAL(44, 740, 9), // "waypoints"
QT_MOC_LITERAL(45, 750, 13), // "excludedAreas"
QT_MOC_LITERAL(46, 764, 8), // "QJSValue"
QT_MOC_LITERAL(47, 773, 12), // "featureTypes"
QT_MOC_LITERAL(48, 786, 10), // "QList<int>"
QT_MOC_LITERAL(49, 797, 15), // "extraParameters"
QT_MOC_LITERAL(50, 813, 25), // "QQmlListProperty<QObject>"
QT_MOC_LITERAL(51, 839, 10), // "TravelMode"
QT_MOC_LITERAL(52, 850, 9), // "CarTravel"
QT_MOC_LITERAL(53, 860, 16), // "PedestrianTravel"
QT_MOC_LITERAL(54, 877, 13), // "BicycleTravel"
QT_MOC_LITERAL(55, 891, 19), // "PublicTransitTravel"
QT_MOC_LITERAL(56, 911, 11), // "TruckTravel"
QT_MOC_LITERAL(57, 923, 9), // "NoFeature"
QT_MOC_LITERAL(58, 933, 11), // "TollFeature"
QT_MOC_LITERAL(59, 945, 14), // "HighwayFeature"
QT_MOC_LITERAL(60, 960, 20), // "PublicTransitFeature"
QT_MOC_LITERAL(61, 981, 12), // "FerryFeature"
QT_MOC_LITERAL(62, 994, 13), // "TunnelFeature"
QT_MOC_LITERAL(63, 1008, 15), // "DirtRoadFeature"
QT_MOC_LITERAL(64, 1024, 12), // "ParksFeature"
QT_MOC_LITERAL(65, 1037, 20), // "MotorPoolLaneFeature"
QT_MOC_LITERAL(66, 1058, 14), // "TrafficFeature"
QT_MOC_LITERAL(67, 1073, 20), // "NeutralFeatureWeight"
QT_MOC_LITERAL(68, 1094, 19), // "PreferFeatureWeight"
QT_MOC_LITERAL(69, 1114, 20), // "RequireFeatureWeight"
QT_MOC_LITERAL(70, 1135, 18), // "AvoidFeatureWeight"
QT_MOC_LITERAL(71, 1154, 21), // "DisallowFeatureWeight"
QT_MOC_LITERAL(72, 1176, 17), // "RouteOptimization"
QT_MOC_LITERAL(73, 1194, 13), // "ShortestRoute"
QT_MOC_LITERAL(74, 1208, 12), // "FastestRoute"
QT_MOC_LITERAL(75, 1221, 17), // "MostEconomicRoute"
QT_MOC_LITERAL(76, 1239, 15), // "MostScenicRoute"
QT_MOC_LITERAL(77, 1255, 13), // "NoSegmentData"
QT_MOC_LITERAL(78, 1269, 16), // "BasicSegmentData"
QT_MOC_LITERAL(79, 1286, 14), // "SegmentDetails"
QT_MOC_LITERAL(80, 1301, 11), // "NoManeuvers"
QT_MOC_LITERAL(81, 1313, 14), // "BasicManeuvers"
QT_MOC_LITERAL(82, 1328, 15) // "ManeuverDetails"

    },
    "QDeclarativeGeoRouteQuery\0DefaultProperty\0"
    "quickChildren\0numberAlternativeRoutesChanged\0"
    "\0travelModesChanged\0routeOptimizationsChanged\0"
    "waypointsChanged\0excludedAreasChanged\0"
    "featureTypesChanged\0maneuverDetailChanged\0"
    "segmentDetailChanged\0queryDetailsChanged\0"
    "extraParametersChanged\0"
    "excludedAreaCoordinateChanged\0"
    "extraParameterChanged\0waypointChanged\0"
    "waypointObjects\0addWaypoint\0w\0"
    "removeWaypoint\0waypoint\0clearWaypoints\0"
    "addExcludedArea\0QGeoRectangle\0area\0"
    "removeExcludedArea\0clearExcludedAreas\0"
    "setFeatureWeight\0FeatureType\0featureType\0"
    "FeatureWeight\0featureWeight\0"
    "resetFeatureWeights\0doCoordinateChanged\0"
    "numberAlternativeRoutes\0travelModes\0"
    "TravelModes\0routeOptimizations\0"
    "RouteOptimizations\0segmentDetail\0"
    "SegmentDetail\0maneuverDetail\0"
    "ManeuverDetail\0waypoints\0excludedAreas\0"
    "QJSValue\0featureTypes\0QList<int>\0"
    "extraParameters\0QQmlListProperty<QObject>\0"
    "TravelMode\0CarTravel\0PedestrianTravel\0"
    "BicycleTravel\0PublicTransitTravel\0"
    "TruckTravel\0NoFeature\0TollFeature\0"
    "HighwayFeature\0PublicTransitFeature\0"
    "FerryFeature\0TunnelFeature\0DirtRoadFeature\0"
    "ParksFeature\0MotorPoolLaneFeature\0"
    "TrafficFeature\0NeutralFeatureWeight\0"
    "PreferFeatureWeight\0RequireFeatureWeight\0"
    "AvoidFeatureWeight\0DisallowFeatureWeight\0"
    "RouteOptimization\0ShortestRoute\0"
    "FastestRoute\0MostEconomicRoute\0"
    "MostScenicRoute\0NoSegmentData\0"
    "BasicSegmentData\0SegmentDetails\0"
    "NoManeuvers\0BasicManeuvers\0ManeuverDetails"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDeclarativeGeoRouteQuery[] = {

 // content:
       8,       // revision
       0,       // classname
       1,   14, // classinfo
      24,   16, // methods
      10,  198, // properties
      10,  248, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // classinfo: key, value
       1,    2,

 // signals: name, argc, parameters, tag, flags
       3,    0,  160,    4, 0x06 /* Public */,
       5,    0,  161,    4, 0x06 /* Public */,
       6,    0,  162,    4, 0x06 /* Public */,
       7,    0,  163,    4, 0x06 /* Public */,
       8,    0,  164,    4, 0x06 /* Public */,
       9,    0,  165,    4, 0x06 /* Public */,
      10,    0,  166,    4, 0x06 /* Public */,
      11,    0,  167,    4, 0x06 /* Public */,
      12,    0,  168,    4, 0x06 /* Public */,
      13,    0,  169,    4, 0x86 /* Public | MethodRevisioned */,

 // slots: name, argc, parameters, tag, flags
      14,    0,  170,    4, 0x08 /* Private */,
      15,    0,  171,    4, 0x08 /* Private */,
      16,    0,  172,    4, 0x08 /* Private */,

 // methods: name, argc, parameters, tag, flags
      17,    0,  173,    4, 0x02 /* Public */,
      18,    1,  174,    4, 0x02 /* Public */,
      20,    1,  177,    4, 0x02 /* Public */,
      22,    0,  180,    4, 0x02 /* Public */,
      23,    1,  181,    4, 0x02 /* Public */,
      26,    1,  184,    4, 0x02 /* Public */,
      27,    0,  187,    4, 0x02 /* Public */,
      28,    2,  188,    4, 0x02 /* Public */,
      32,    1,  193,    4, 0x02 /* Public */,
      33,    0,  196,    4, 0x02 /* Public */,
      34,    0,  197,    4, 0x00 /* Private */,

 // signals: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      11,

 // slots: revision
       0,
       0,
       0,

 // methods: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // methods: parameters
    QMetaType::QVariantList,
    QMetaType::Void, QMetaType::QVariant,   19,
    QMetaType::Void, QMetaType::QVariant,   21,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 29, 0x80000000 | 31,   30,   32,
    QMetaType::Int, 0x80000000 | 29,   30,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      35, QMetaType::Int, 0x00495103,
      36, 0x80000000 | 37, 0x0049510b,
      38, 0x80000000 | 39, 0x0049510b,
      40, 0x80000000 | 41, 0x0049510b,
      42, 0x80000000 | 43, 0x0049510b,
      44, QMetaType::QVariantList, 0x00495103,
      45, 0x80000000 | 46, 0x0049510b,
      47, 0x80000000 | 48, 0x00495009,
      49, QMetaType::QVariantMap, 0x00895001,
       2, 0x80000000 | 50, 0x00094009,

 // properties: notify_signal_id
       0,
       1,
       2,
       7,
       6,
       3,
       4,
       5,
       0,
       0,

 // properties: revision
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       0,
      11,
       0,

 // enums: name, alias, flags, count, data
      51,   51, 0x0,    5,  298,
      37,   51, 0x1,    5,  308,
      29,   29, 0x0,   10,  318,
      31,   31, 0x0,    5,  338,
      72,   72, 0x0,    4,  348,
      39,   72, 0x1,    4,  356,
      41,   41, 0x0,    2,  364,
      79,   41, 0x1,    2,  368,
      43,   43, 0x0,    2,  372,
      82,   43, 0x1,    2,  376,

 // enum data: key, value
      52, uint(QDeclarativeGeoRouteQuery::CarTravel),
      53, uint(QDeclarativeGeoRouteQuery::PedestrianTravel),
      54, uint(QDeclarativeGeoRouteQuery::BicycleTravel),
      55, uint(QDeclarativeGeoRouteQuery::PublicTransitTravel),
      56, uint(QDeclarativeGeoRouteQuery::TruckTravel),
      52, uint(QDeclarativeGeoRouteQuery::CarTravel),
      53, uint(QDeclarativeGeoRouteQuery::PedestrianTravel),
      54, uint(QDeclarativeGeoRouteQuery::BicycleTravel),
      55, uint(QDeclarativeGeoRouteQuery::PublicTransitTravel),
      56, uint(QDeclarativeGeoRouteQuery::TruckTravel),
      57, uint(QDeclarativeGeoRouteQuery::NoFeature),
      58, uint(QDeclarativeGeoRouteQuery::TollFeature),
      59, uint(QDeclarativeGeoRouteQuery::HighwayFeature),
      60, uint(QDeclarativeGeoRouteQuery::PublicTransitFeature),
      61, uint(QDeclarativeGeoRouteQuery::FerryFeature),
      62, uint(QDeclarativeGeoRouteQuery::TunnelFeature),
      63, uint(QDeclarativeGeoRouteQuery::DirtRoadFeature),
      64, uint(QDeclarativeGeoRouteQuery::ParksFeature),
      65, uint(QDeclarativeGeoRouteQuery::MotorPoolLaneFeature),
      66, uint(QDeclarativeGeoRouteQuery::TrafficFeature),
      67, uint(QDeclarativeGeoRouteQuery::NeutralFeatureWeight),
      68, uint(QDeclarativeGeoRouteQuery::PreferFeatureWeight),
      69, uint(QDeclarativeGeoRouteQuery::RequireFeatureWeight),
      70, uint(QDeclarativeGeoRouteQuery::AvoidFeatureWeight),
      71, uint(QDeclarativeGeoRouteQuery::DisallowFeatureWeight),
      73, uint(QDeclarativeGeoRouteQuery::ShortestRoute),
      74, uint(QDeclarativeGeoRouteQuery::FastestRoute),
      75, uint(QDeclarativeGeoRouteQuery::MostEconomicRoute),
      76, uint(QDeclarativeGeoRouteQuery::MostScenicRoute),
      73, uint(QDeclarativeGeoRouteQuery::ShortestRoute),
      74, uint(QDeclarativeGeoRouteQuery::FastestRoute),
      75, uint(QDeclarativeGeoRouteQuery::MostEconomicRoute),
      76, uint(QDeclarativeGeoRouteQuery::MostScenicRoute),
      77, uint(QDeclarativeGeoRouteQuery::NoSegmentData),
      78, uint(QDeclarativeGeoRouteQuery::BasicSegmentData),
      77, uint(QDeclarativeGeoRouteQuery::NoSegmentData),
      78, uint(QDeclarativeGeoRouteQuery::BasicSegmentData),
      80, uint(QDeclarativeGeoRouteQuery::NoManeuvers),
      81, uint(QDeclarativeGeoRouteQuery::BasicManeuvers),
      80, uint(QDeclarativeGeoRouteQuery::NoManeuvers),
      81, uint(QDeclarativeGeoRouteQuery::BasicManeuvers),

       0        // eod
};

void QDeclarativeGeoRouteQuery::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QDeclarativeGeoRouteQuery *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->numberAlternativeRoutesChanged(); break;
        case 1: _t->travelModesChanged(); break;
        case 2: _t->routeOptimizationsChanged(); break;
        case 3: _t->waypointsChanged(); break;
        case 4: _t->excludedAreasChanged(); break;
        case 5: _t->featureTypesChanged(); break;
        case 6: _t->maneuverDetailChanged(); break;
        case 7: _t->segmentDetailChanged(); break;
        case 8: _t->queryDetailsChanged(); break;
        case 9: _t->extraParametersChanged(); break;
        case 10: _t->excludedAreaCoordinateChanged(); break;
        case 11: _t->extraParameterChanged(); break;
        case 12: _t->waypointChanged(); break;
        case 13: { QVariantList _r = _t->waypointObjects();
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = std::move(_r); }  break;
        case 14: _t->addWaypoint((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 15: _t->removeWaypoint((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 16: _t->clearWaypoints(); break;
        case 17: _t->addExcludedArea((*reinterpret_cast< const QGeoRectangle(*)>(_a[1]))); break;
        case 18: _t->removeExcludedArea((*reinterpret_cast< const QGeoRectangle(*)>(_a[1]))); break;
        case 19: _t->clearExcludedAreas(); break;
        case 20: _t->setFeatureWeight((*reinterpret_cast< FeatureType(*)>(_a[1])),(*reinterpret_cast< FeatureWeight(*)>(_a[2]))); break;
        case 21: { int _r = _t->featureWeight((*reinterpret_cast< FeatureType(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 22: _t->resetFeatureWeights(); break;
        case 23: _t->doCoordinateChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 17:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoRectangle >(); break;
            }
            break;
        case 18:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoRectangle >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::numberAlternativeRoutesChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::travelModesChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::routeOptimizationsChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::waypointsChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::excludedAreasChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::featureTypesChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::maneuverDetailChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::segmentDetailChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::queryDetailsChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QDeclarativeGeoRouteQuery::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QDeclarativeGeoRouteQuery::extraParametersChanged)) {
                *result = 9;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 6:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QJSValue >(); break;
        case 7:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QList<int> >(); break;
        case 9:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQmlListProperty<QObject> >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QDeclarativeGeoRouteQuery *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->numberAlternativeRoutes(); break;
        case 1: *reinterpret_cast<int*>(_v) = QFlag(_t->travelModes()); break;
        case 2: *reinterpret_cast<int*>(_v) = QFlag(_t->routeOptimizations()); break;
        case 3: *reinterpret_cast< SegmentDetail*>(_v) = _t->segmentDetail(); break;
        case 4: *reinterpret_cast< ManeuverDetail*>(_v) = _t->maneuverDetail(); break;
        case 5: *reinterpret_cast< QVariantList*>(_v) = _t->waypoints(); break;
        case 6: *reinterpret_cast< QJSValue*>(_v) = _t->excludedAreas(); break;
        case 7: *reinterpret_cast< QList<int>*>(_v) = _t->featureTypes(); break;
        case 8: *reinterpret_cast< QVariantMap*>(_v) = _t->extraParameters(); break;
        case 9: *reinterpret_cast< QQmlListProperty<QObject>*>(_v) = _t->declarativeChildren(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QDeclarativeGeoRouteQuery *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setNumberAlternativeRoutes(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setTravelModes(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 2: _t->setRouteOptimizations(QFlag(*reinterpret_cast<int*>(_v))); break;
        case 3: _t->setSegmentDetail(*reinterpret_cast< SegmentDetail*>(_v)); break;
        case 4: _t->setManeuverDetail(*reinterpret_cast< ManeuverDetail*>(_v)); break;
        case 5: _t->setWaypoints(*reinterpret_cast< QVariantList*>(_v)); break;
        case 6: _t->setExcludedAreas(*reinterpret_cast< QJSValue*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QDeclarativeGeoRouteQuery::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QDeclarativeGeoRouteQuery.data,
    qt_meta_data_QDeclarativeGeoRouteQuery,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QDeclarativeGeoRouteQuery::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDeclarativeGeoRouteQuery::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QDeclarativeGeoRouteQuery.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    if (!strcmp(_clname, "org.qt-project.Qt.QQmlParserStatus"))
        return static_cast< QQmlParserStatus*>(this);
    return QObject::qt_metacast(_clname);
}

int QDeclarativeGeoRouteQuery::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 24)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 24;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 10;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 10;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QDeclarativeGeoRouteQuery::numberAlternativeRoutesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QDeclarativeGeoRouteQuery::travelModesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QDeclarativeGeoRouteQuery::routeOptimizationsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QDeclarativeGeoRouteQuery::waypointsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QDeclarativeGeoRouteQuery::excludedAreasChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QDeclarativeGeoRouteQuery::featureTypesChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QDeclarativeGeoRouteQuery::maneuverDetailChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void QDeclarativeGeoRouteQuery::segmentDetailChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void QDeclarativeGeoRouteQuery::queryDetailsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void QDeclarativeGeoRouteQuery::extraParametersChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
