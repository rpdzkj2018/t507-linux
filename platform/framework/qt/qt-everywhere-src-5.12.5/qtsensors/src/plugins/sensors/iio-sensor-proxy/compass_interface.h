/*
 * This file was generated by qdbusxml2cpp version 0.8
 * Command line was: qdbusxml2cpp -N -p compass_interface.h: net.hadess.SensorProxy.Compass.xml
 *
 * qdbusxml2cpp is Copyright (C) 2019 The Qt Company Ltd.
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#ifndef COMPASS_INTERFACE_H
#define COMPASS_INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

/*
 * Proxy class for interface net.hadess.SensorProxy.Compass
 */
class NetHadessSensorProxyCompassInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.hadess.SensorProxy.Compass"; }

public:
    NetHadessSensorProxyCompassInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = nullptr);

    ~NetHadessSensorProxyCompassInterface();

    Q_PROPERTY(double CompassHeading READ compassHeading)
    inline double compassHeading() const
    { return qvariant_cast< double >(property("CompassHeading")); }

    Q_PROPERTY(bool HasCompass READ hasCompass)
    inline bool hasCompass() const
    { return qvariant_cast< bool >(property("HasCompass")); }

public Q_SLOTS: // METHODS
    inline QDBusPendingReply<> ClaimCompass()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("ClaimCompass"), argumentList);
    }

    inline QDBusPendingReply<> ReleaseCompass()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("ReleaseCompass"), argumentList);
    }

Q_SIGNALS: // SIGNALS
};

#endif
