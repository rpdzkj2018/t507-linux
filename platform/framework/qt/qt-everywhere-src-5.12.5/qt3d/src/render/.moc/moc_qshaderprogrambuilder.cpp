/****************************************************************************
** Meta object code from reading C++ file 'qshaderprogrambuilder.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../materialsystem/qshaderprogrambuilder.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qshaderprogrambuilder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder_t {
    QByteArrayData data[29];
    char stringdata0[668];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder_t qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder = {
    {
QT_MOC_LITERAL(0, 0, 33), // "Qt3DRender::QShaderProgramBui..."
QT_MOC_LITERAL(1, 34, 20), // "shaderProgramChanged"
QT_MOC_LITERAL(2, 55, 0), // ""
QT_MOC_LITERAL(3, 56, 27), // "Qt3DRender::QShaderProgram*"
QT_MOC_LITERAL(4, 84, 13), // "shaderProgram"
QT_MOC_LITERAL(5, 98, 20), // "enabledLayersChanged"
QT_MOC_LITERAL(6, 119, 6), // "layers"
QT_MOC_LITERAL(7, 126, 24), // "vertexShaderGraphChanged"
QT_MOC_LITERAL(8, 151, 17), // "vertexShaderGraph"
QT_MOC_LITERAL(9, 169, 37), // "tessellationControlShaderGrap..."
QT_MOC_LITERAL(10, 207, 30), // "tessellationControlShaderGraph"
QT_MOC_LITERAL(11, 238, 40), // "tessellationEvaluationShaderG..."
QT_MOC_LITERAL(12, 279, 33), // "tessellationEvaluationShaderG..."
QT_MOC_LITERAL(13, 313, 26), // "geometryShaderGraphChanged"
QT_MOC_LITERAL(14, 340, 19), // "geometryShaderGraph"
QT_MOC_LITERAL(15, 360, 26), // "fragmentShaderGraphChanged"
QT_MOC_LITERAL(16, 387, 19), // "fragmentShaderGraph"
QT_MOC_LITERAL(17, 407, 25), // "computeShaderGraphChanged"
QT_MOC_LITERAL(18, 433, 18), // "computeShaderGraph"
QT_MOC_LITERAL(19, 452, 16), // "setShaderProgram"
QT_MOC_LITERAL(20, 469, 7), // "program"
QT_MOC_LITERAL(21, 477, 16), // "setEnabledLayers"
QT_MOC_LITERAL(22, 494, 20), // "setVertexShaderGraph"
QT_MOC_LITERAL(23, 515, 33), // "setTessellationControlShaderG..."
QT_MOC_LITERAL(24, 549, 36), // "setTessellationEvaluationShad..."
QT_MOC_LITERAL(25, 586, 22), // "setGeometryShaderGraph"
QT_MOC_LITERAL(26, 609, 22), // "setFragmentShaderGraph"
QT_MOC_LITERAL(27, 632, 21), // "setComputeShaderGraph"
QT_MOC_LITERAL(28, 654, 13) // "enabledLayers"

    },
    "Qt3DRender::QShaderProgramBuilder\0"
    "shaderProgramChanged\0\0Qt3DRender::QShaderProgram*\0"
    "shaderProgram\0enabledLayersChanged\0"
    "layers\0vertexShaderGraphChanged\0"
    "vertexShaderGraph\0"
    "tessellationControlShaderGraphChanged\0"
    "tessellationControlShaderGraph\0"
    "tessellationEvaluationShaderGraphChanged\0"
    "tessellationEvaluationShaderGraph\0"
    "geometryShaderGraphChanged\0"
    "geometryShaderGraph\0fragmentShaderGraphChanged\0"
    "fragmentShaderGraph\0computeShaderGraphChanged\0"
    "computeShaderGraph\0setShaderProgram\0"
    "program\0setEnabledLayers\0setVertexShaderGraph\0"
    "setTessellationControlShaderGraph\0"
    "setTessellationEvaluationShaderGraph\0"
    "setGeometryShaderGraph\0setFragmentShaderGraph\0"
    "setComputeShaderGraph\0enabledLayers"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QShaderProgramBuilder[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       8,  142, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   94,    2, 0x06 /* Public */,
       5,    1,   97,    2, 0x06 /* Public */,
       7,    1,  100,    2, 0x06 /* Public */,
       9,    1,  103,    2, 0x06 /* Public */,
      11,    1,  106,    2, 0x06 /* Public */,
      13,    1,  109,    2, 0x06 /* Public */,
      15,    1,  112,    2, 0x06 /* Public */,
      17,    1,  115,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,  118,    2, 0x0a /* Public */,
      21,    1,  121,    2, 0x0a /* Public */,
      22,    1,  124,    2, 0x0a /* Public */,
      23,    1,  127,    2, 0x0a /* Public */,
      24,    1,  130,    2, 0x0a /* Public */,
      25,    1,  133,    2, 0x0a /* Public */,
      26,    1,  136,    2, 0x0a /* Public */,
      27,    1,  139,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::QStringList,    6,
    QMetaType::Void, QMetaType::QUrl,    8,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void, QMetaType::QUrl,   12,
    QMetaType::Void, QMetaType::QUrl,   14,
    QMetaType::Void, QMetaType::QUrl,   16,
    QMetaType::Void, QMetaType::QUrl,   18,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,   20,
    QMetaType::Void, QMetaType::QStringList,    6,
    QMetaType::Void, QMetaType::QUrl,    8,
    QMetaType::Void, QMetaType::QUrl,   10,
    QMetaType::Void, QMetaType::QUrl,   12,
    QMetaType::Void, QMetaType::QUrl,   14,
    QMetaType::Void, QMetaType::QUrl,   16,
    QMetaType::Void, QMetaType::QUrl,   18,

 // properties: name, type, flags
       4, 0x80000000 | 3, 0x0049510b,
      28, QMetaType::QStringList, 0x00495103,
       8, QMetaType::QUrl, 0x00495103,
      10, QMetaType::QUrl, 0x00495103,
      12, QMetaType::QUrl, 0x00495103,
      14, QMetaType::QUrl, 0x00495103,
      16, QMetaType::QUrl, 0x00495103,
      18, QMetaType::QUrl, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,

       0        // eod
};

void Qt3DRender::QShaderProgramBuilder::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QShaderProgramBuilder *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->shaderProgramChanged((*reinterpret_cast< Qt3DRender::QShaderProgram*(*)>(_a[1]))); break;
        case 1: _t->enabledLayersChanged((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 2: _t->vertexShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 3: _t->tessellationControlShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 4: _t->tessellationEvaluationShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 5: _t->geometryShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 6: _t->fragmentShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 7: _t->computeShaderGraphChanged((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 8: _t->setShaderProgram((*reinterpret_cast< Qt3DRender::QShaderProgram*(*)>(_a[1]))); break;
        case 9: _t->setEnabledLayers((*reinterpret_cast< const QStringList(*)>(_a[1]))); break;
        case 10: _t->setVertexShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 11: _t->setTessellationControlShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 12: _t->setTessellationEvaluationShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 13: _t->setGeometryShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 14: _t->setFragmentShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 15: _t->setComputeShaderGraph((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QShaderProgramBuilder::*)(Qt3DRender::QShaderProgram * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::shaderProgramChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QStringList & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::enabledLayersChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::vertexShaderGraphChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::tessellationControlShaderGraphChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::tessellationEvaluationShaderGraphChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::geometryShaderGraphChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::fragmentShaderGraphChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QShaderProgramBuilder::*)(const QUrl & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QShaderProgramBuilder::computeShaderGraphChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QShaderProgramBuilder *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< Qt3DRender::QShaderProgram**>(_v) = _t->shaderProgram(); break;
        case 1: *reinterpret_cast< QStringList*>(_v) = _t->enabledLayers(); break;
        case 2: *reinterpret_cast< QUrl*>(_v) = _t->vertexShaderGraph(); break;
        case 3: *reinterpret_cast< QUrl*>(_v) = _t->tessellationControlShaderGraph(); break;
        case 4: *reinterpret_cast< QUrl*>(_v) = _t->tessellationEvaluationShaderGraph(); break;
        case 5: *reinterpret_cast< QUrl*>(_v) = _t->geometryShaderGraph(); break;
        case 6: *reinterpret_cast< QUrl*>(_v) = _t->fragmentShaderGraph(); break;
        case 7: *reinterpret_cast< QUrl*>(_v) = _t->computeShaderGraph(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QShaderProgramBuilder *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setShaderProgram(*reinterpret_cast< Qt3DRender::QShaderProgram**>(_v)); break;
        case 1: _t->setEnabledLayers(*reinterpret_cast< QStringList*>(_v)); break;
        case 2: _t->setVertexShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        case 3: _t->setTessellationControlShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        case 4: _t->setTessellationEvaluationShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        case 5: _t->setGeometryShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        case 6: _t->setFragmentShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        case 7: _t->setComputeShaderGraph(*reinterpret_cast< QUrl*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QShaderProgramBuilder::staticMetaObject = { {
    &Qt3DCore::QNode::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder.data,
    qt_meta_data_Qt3DRender__QShaderProgramBuilder,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QShaderProgramBuilder::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QShaderProgramBuilder::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QShaderProgramBuilder.stringdata0))
        return static_cast<void*>(this);
    return Qt3DCore::QNode::qt_metacast(_clname);
}

int Qt3DRender::QShaderProgramBuilder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QShaderProgramBuilder::shaderProgramChanged(Qt3DRender::QShaderProgram * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QShaderProgramBuilder::enabledLayersChanged(const QStringList & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QShaderProgramBuilder::vertexShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QShaderProgramBuilder::tessellationControlShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QShaderProgramBuilder::tessellationEvaluationShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QShaderProgramBuilder::geometryShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QShaderProgramBuilder::fragmentShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QShaderProgramBuilder::computeShaderGraphChanged(const QUrl & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
