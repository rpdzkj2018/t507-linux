/****************************************************************************
** Meta object code from reading C++ file 'openwnnplugin.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../openwnnplugin.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/qplugin.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'openwnnplugin.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin_t {
    QByteArrayData data[1];
    char stringdata0[31];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin_t qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin = {
    {
QT_MOC_LITERAL(0, 0, 30) // "QtVirtualKeyboardOpenWnnPlugin"

    },
    "QtVirtualKeyboardOpenWnnPlugin"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QtVirtualKeyboardOpenWnnPlugin[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

void QtVirtualKeyboardOpenWnnPlugin::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject QtVirtualKeyboardOpenWnnPlugin::staticMetaObject = { {
    &QVirtualKeyboardExtensionPlugin::staticMetaObject,
    qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin.data,
    qt_meta_data_QtVirtualKeyboardOpenWnnPlugin,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QtVirtualKeyboardOpenWnnPlugin::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QtVirtualKeyboardOpenWnnPlugin::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QtVirtualKeyboardOpenWnnPlugin.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "org.qt-project.qt.virtualkeyboard.plugin/5.12"))
        return static_cast< QVirtualKeyboardExtensionPlugin*>(this);
    return QVirtualKeyboardExtensionPlugin::qt_metacast(_clname);
}

int QtVirtualKeyboardOpenWnnPlugin::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QVirtualKeyboardExtensionPlugin::qt_metacall(_c, _id, _a);
    return _id;
}

QT_PLUGIN_METADATA_SECTION
static constexpr unsigned char qt_pluginMetaData[] = {
    'Q', 'T', 'M', 'E', 'T', 'A', 'D', 'A', 'T', 'A', ' ', '!',
    // metadata version, Qt version, architectural requirements
    0, QT_VERSION_MAJOR, QT_VERSION_MINOR, qPluginArchRequirements(),
    0xbf, 
    // "IID"
    0x02,  0x78,  0x2d,  'o',  'r',  'g',  '.',  'q', 
    't',  '-',  'p',  'r',  'o',  'j',  'e',  'c', 
    't',  '.',  'q',  't',  '.',  'v',  'i',  'r', 
    't',  'u',  'a',  'l',  'k',  'e',  'y',  'b', 
    'o',  'a',  'r',  'd',  '.',  'p',  'l',  'u', 
    'g',  'i',  'n',  '/',  '5',  '.',  '1',  '2', 
    // "className"
    0x03,  0x78,  0x1e,  'Q',  't',  'V',  'i',  'r', 
    't',  'u',  'a',  'l',  'K',  'e',  'y',  'b', 
    'o',  'a',  'r',  'd',  'O',  'p',  'e',  'n', 
    'W',  'n',  'n',  'P',  'l',  'u',  'g',  'i', 
    'n', 
    // "MetaData"
    0x04,  0xa4,  0x6b,  'I',  'n',  'p',  'u',  't', 
    'M',  'e',  't',  'h',  'o',  'd',  0x73,  'J', 
    'a',  'p',  'a',  'n',  'e',  's',  'e',  'I', 
    'n',  'p',  'u',  't',  'M',  'e',  't',  'h', 
    'o',  'd',  0x64,  'N',  'a',  'm',  'e',  0x68, 
    'j',  'a',  'p',  'a',  'n',  'e',  's',  'e', 
    0x68,  'P',  'r',  'o',  'v',  'i',  'd',  'e', 
    'r',  0x74,  'Q',  't',  ' ',  'O',  'p',  'e', 
    'n',  'W',  'N',  'N',  ' ',  'E',  'x',  't', 
    'e',  'n',  's',  'i',  'o',  'n',  0x67,  'V', 
    'e',  'r',  's',  'i',  'o',  'n',  0x18,  0x64, 
    0xff, 
};
QT_MOC_EXPORT_PLUGIN(QtVirtualKeyboardOpenWnnPlugin, QtVirtualKeyboardOpenWnnPlugin)

QT_WARNING_POP
QT_END_MOC_NAMESPACE
