/****************************************************************************
** Meta object code from reading C++ file 'qgeometryrenderer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../geometry/qgeometryrenderer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeometryrenderer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t {
    QByteArrayData data[53];
    char stringdata0[866];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DRender__QGeometryRenderer_t qt_meta_stringdata_Qt3DRender__QGeometryRenderer = {
    {
QT_MOC_LITERAL(0, 0, 29), // "Qt3DRender::QGeometryRenderer"
QT_MOC_LITERAL(1, 30, 20), // "instanceCountChanged"
QT_MOC_LITERAL(2, 51, 0), // ""
QT_MOC_LITERAL(3, 52, 13), // "instanceCount"
QT_MOC_LITERAL(4, 66, 18), // "vertexCountChanged"
QT_MOC_LITERAL(5, 85, 11), // "vertexCount"
QT_MOC_LITERAL(6, 97, 18), // "indexOffsetChanged"
QT_MOC_LITERAL(7, 116, 11), // "indexOffset"
QT_MOC_LITERAL(8, 128, 20), // "firstInstanceChanged"
QT_MOC_LITERAL(9, 149, 13), // "firstInstance"
QT_MOC_LITERAL(10, 163, 18), // "firstVertexChanged"
QT_MOC_LITERAL(11, 182, 11), // "firstVertex"
QT_MOC_LITERAL(12, 194, 28), // "indexBufferByteOffsetChanged"
QT_MOC_LITERAL(13, 223, 6), // "offset"
QT_MOC_LITERAL(14, 230, 24), // "restartIndexValueChanged"
QT_MOC_LITERAL(15, 255, 17), // "restartIndexValue"
QT_MOC_LITERAL(16, 273, 23), // "verticesPerPatchChanged"
QT_MOC_LITERAL(17, 297, 16), // "verticesPerPatch"
QT_MOC_LITERAL(18, 314, 30), // "primitiveRestartEnabledChanged"
QT_MOC_LITERAL(19, 345, 23), // "primitiveRestartEnabled"
QT_MOC_LITERAL(20, 369, 15), // "geometryChanged"
QT_MOC_LITERAL(21, 385, 10), // "QGeometry*"
QT_MOC_LITERAL(22, 396, 8), // "geometry"
QT_MOC_LITERAL(23, 405, 20), // "primitiveTypeChanged"
QT_MOC_LITERAL(24, 426, 13), // "PrimitiveType"
QT_MOC_LITERAL(25, 440, 13), // "primitiveType"
QT_MOC_LITERAL(26, 454, 16), // "setInstanceCount"
QT_MOC_LITERAL(27, 471, 14), // "setVertexCount"
QT_MOC_LITERAL(28, 486, 14), // "setIndexOffset"
QT_MOC_LITERAL(29, 501, 16), // "setFirstInstance"
QT_MOC_LITERAL(30, 518, 14), // "setFirstVertex"
QT_MOC_LITERAL(31, 533, 24), // "setIndexBufferByteOffset"
QT_MOC_LITERAL(32, 558, 20), // "setRestartIndexValue"
QT_MOC_LITERAL(33, 579, 5), // "index"
QT_MOC_LITERAL(34, 585, 19), // "setVerticesPerPatch"
QT_MOC_LITERAL(35, 605, 26), // "setPrimitiveRestartEnabled"
QT_MOC_LITERAL(36, 632, 7), // "enabled"
QT_MOC_LITERAL(37, 640, 11), // "setGeometry"
QT_MOC_LITERAL(38, 652, 16), // "setPrimitiveType"
QT_MOC_LITERAL(39, 669, 21), // "indexBufferByteOffset"
QT_MOC_LITERAL(40, 691, 22), // "Qt3DRender::QGeometry*"
QT_MOC_LITERAL(41, 714, 6), // "Points"
QT_MOC_LITERAL(42, 721, 5), // "Lines"
QT_MOC_LITERAL(43, 727, 8), // "LineLoop"
QT_MOC_LITERAL(44, 736, 9), // "LineStrip"
QT_MOC_LITERAL(45, 746, 9), // "Triangles"
QT_MOC_LITERAL(46, 756, 13), // "TriangleStrip"
QT_MOC_LITERAL(47, 770, 11), // "TriangleFan"
QT_MOC_LITERAL(48, 782, 14), // "LinesAdjacency"
QT_MOC_LITERAL(49, 797, 18), // "TrianglesAdjacency"
QT_MOC_LITERAL(50, 816, 18), // "LineStripAdjacency"
QT_MOC_LITERAL(51, 835, 22), // "TriangleStripAdjacency"
QT_MOC_LITERAL(52, 858, 7) // "Patches"

    },
    "Qt3DRender::QGeometryRenderer\0"
    "instanceCountChanged\0\0instanceCount\0"
    "vertexCountChanged\0vertexCount\0"
    "indexOffsetChanged\0indexOffset\0"
    "firstInstanceChanged\0firstInstance\0"
    "firstVertexChanged\0firstVertex\0"
    "indexBufferByteOffsetChanged\0offset\0"
    "restartIndexValueChanged\0restartIndexValue\0"
    "verticesPerPatchChanged\0verticesPerPatch\0"
    "primitiveRestartEnabledChanged\0"
    "primitiveRestartEnabled\0geometryChanged\0"
    "QGeometry*\0geometry\0primitiveTypeChanged\0"
    "PrimitiveType\0primitiveType\0"
    "setInstanceCount\0setVertexCount\0"
    "setIndexOffset\0setFirstInstance\0"
    "setFirstVertex\0setIndexBufferByteOffset\0"
    "setRestartIndexValue\0index\0"
    "setVerticesPerPatch\0setPrimitiveRestartEnabled\0"
    "enabled\0setGeometry\0setPrimitiveType\0"
    "indexBufferByteOffset\0Qt3DRender::QGeometry*\0"
    "Points\0Lines\0LineLoop\0LineStrip\0"
    "Triangles\0TriangleStrip\0TriangleFan\0"
    "LinesAdjacency\0TrianglesAdjacency\0"
    "LineStripAdjacency\0TriangleStripAdjacency\0"
    "Patches"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DRender__QGeometryRenderer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
      11,  190, // properties
       1,  234, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  124,    2, 0x06 /* Public */,
       4,    1,  127,    2, 0x06 /* Public */,
       6,    1,  130,    2, 0x06 /* Public */,
       8,    1,  133,    2, 0x06 /* Public */,
      10,    1,  136,    2, 0x06 /* Public */,
      12,    1,  139,    2, 0x06 /* Public */,
      14,    1,  142,    2, 0x06 /* Public */,
      16,    1,  145,    2, 0x06 /* Public */,
      18,    1,  148,    2, 0x06 /* Public */,
      20,    1,  151,    2, 0x06 /* Public */,
      23,    1,  154,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      26,    1,  157,    2, 0x0a /* Public */,
      27,    1,  160,    2, 0x0a /* Public */,
      28,    1,  163,    2, 0x0a /* Public */,
      29,    1,  166,    2, 0x0a /* Public */,
      30,    1,  169,    2, 0x0a /* Public */,
      31,    1,  172,    2, 0x0a /* Public */,
      32,    1,  175,    2, 0x0a /* Public */,
      34,    1,  178,    2, 0x0a /* Public */,
      35,    1,  181,    2, 0x0a /* Public */,
      37,    1,  184,    2, 0x0a /* Public */,
      38,    1,  187,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   15,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::Bool,   19,
    QMetaType::Void, 0x80000000 | 21,   22,
    QMetaType::Void, 0x80000000 | 24,   25,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,
    QMetaType::Void, QMetaType::Int,   33,
    QMetaType::Void, QMetaType::Int,   17,
    QMetaType::Void, QMetaType::Bool,   36,
    QMetaType::Void, 0x80000000 | 21,   22,
    QMetaType::Void, 0x80000000 | 24,   25,

 // properties: name, type, flags
       3, QMetaType::Int, 0x00495103,
       5, QMetaType::Int, 0x00495103,
       7, QMetaType::Int, 0x00495103,
       9, QMetaType::Int, 0x00495103,
      11, QMetaType::Int, 0x00495103,
      39, QMetaType::Int, 0x00495103,
      15, QMetaType::Int, 0x00495103,
      17, QMetaType::Int, 0x00495103,
      19, QMetaType::Bool, 0x00495103,
      22, 0x80000000 | 40, 0x0049510b,
      25, 0x80000000 | 24, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,
       8,
       9,
      10,

 // enums: name, alias, flags, count, data
      24,   24, 0x0,   12,  239,

 // enum data: key, value
      41, uint(Qt3DRender::QGeometryRenderer::Points),
      42, uint(Qt3DRender::QGeometryRenderer::Lines),
      43, uint(Qt3DRender::QGeometryRenderer::LineLoop),
      44, uint(Qt3DRender::QGeometryRenderer::LineStrip),
      45, uint(Qt3DRender::QGeometryRenderer::Triangles),
      46, uint(Qt3DRender::QGeometryRenderer::TriangleStrip),
      47, uint(Qt3DRender::QGeometryRenderer::TriangleFan),
      48, uint(Qt3DRender::QGeometryRenderer::LinesAdjacency),
      49, uint(Qt3DRender::QGeometryRenderer::TrianglesAdjacency),
      50, uint(Qt3DRender::QGeometryRenderer::LineStripAdjacency),
      51, uint(Qt3DRender::QGeometryRenderer::TriangleStripAdjacency),
      52, uint(Qt3DRender::QGeometryRenderer::Patches),

       0        // eod
};

void Qt3DRender::QGeometryRenderer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->instanceCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->vertexCountChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->indexOffsetChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->firstInstanceChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->firstVertexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->indexBufferByteOffsetChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->restartIndexValueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->verticesPerPatchChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->primitiveRestartEnabledChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->geometryChanged((*reinterpret_cast< QGeometry*(*)>(_a[1]))); break;
        case 10: _t->primitiveTypeChanged((*reinterpret_cast< PrimitiveType(*)>(_a[1]))); break;
        case 11: _t->setInstanceCount((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: _t->setVertexCount((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->setIndexOffset((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: _t->setFirstInstance((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: _t->setFirstVertex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->setIndexBufferByteOffset((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->setRestartIndexValue((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 18: _t->setVerticesPerPatch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->setPrimitiveRestartEnabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: _t->setGeometry((*reinterpret_cast< QGeometry*(*)>(_a[1]))); break;
        case 21: _t->setPrimitiveType((*reinterpret_cast< PrimitiveType(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeometry* >(); break;
            }
            break;
        case 20:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeometry* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::instanceCountChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::vertexCountChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::indexOffsetChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::firstInstanceChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::firstVertexChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::indexBufferByteOffsetChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::restartIndexValueChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::verticesPerPatchChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::primitiveRestartEnabledChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(QGeometry * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::geometryChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (QGeometryRenderer::*)(PrimitiveType );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QGeometryRenderer::primitiveTypeChanged)) {
                *result = 10;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 9:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< Qt3DRender::QGeometry* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< int*>(_v) = _t->instanceCount(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->vertexCount(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->indexOffset(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->firstInstance(); break;
        case 4: *reinterpret_cast< int*>(_v) = _t->firstVertex(); break;
        case 5: *reinterpret_cast< int*>(_v) = _t->indexBufferByteOffset(); break;
        case 6: *reinterpret_cast< int*>(_v) = _t->restartIndexValue(); break;
        case 7: *reinterpret_cast< int*>(_v) = _t->verticesPerPatch(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->primitiveRestartEnabled(); break;
        case 9: *reinterpret_cast< Qt3DRender::QGeometry**>(_v) = _t->geometry(); break;
        case 10: *reinterpret_cast< PrimitiveType*>(_v) = _t->primitiveType(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QGeometryRenderer *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setInstanceCount(*reinterpret_cast< int*>(_v)); break;
        case 1: _t->setVertexCount(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setIndexOffset(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setFirstInstance(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setFirstVertex(*reinterpret_cast< int*>(_v)); break;
        case 5: _t->setIndexBufferByteOffset(*reinterpret_cast< int*>(_v)); break;
        case 6: _t->setRestartIndexValue(*reinterpret_cast< int*>(_v)); break;
        case 7: _t->setVerticesPerPatch(*reinterpret_cast< int*>(_v)); break;
        case 8: _t->setPrimitiveRestartEnabled(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setGeometry(*reinterpret_cast< Qt3DRender::QGeometry**>(_v)); break;
        case 10: _t->setPrimitiveType(*reinterpret_cast< PrimitiveType*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DRender::QGeometryRenderer::staticMetaObject = { {
    &Qt3DCore::QComponent::staticMetaObject,
    qt_meta_stringdata_Qt3DRender__QGeometryRenderer.data,
    qt_meta_data_Qt3DRender__QGeometryRenderer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DRender::QGeometryRenderer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DRender::QGeometryRenderer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DRender__QGeometryRenderer.stringdata0))
        return static_cast<void*>(this);
    return Qt3DCore::QComponent::qt_metacast(_clname);
}

int Qt3DRender::QGeometryRenderer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Qt3DCore::QComponent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DRender::QGeometryRenderer::instanceCountChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DRender::QGeometryRenderer::vertexCountChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DRender::QGeometryRenderer::indexOffsetChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DRender::QGeometryRenderer::firstInstanceChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DRender::QGeometryRenderer::firstVertexChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DRender::QGeometryRenderer::indexBufferByteOffsetChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DRender::QGeometryRenderer::restartIndexValueChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DRender::QGeometryRenderer::verticesPerPatchChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void Qt3DRender::QGeometryRenderer::primitiveRestartEnabledChanged(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Qt3DRender::QGeometryRenderer::geometryChanged(QGeometry * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Qt3DRender::QGeometryRenderer::primitiveTypeChanged(PrimitiveType _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
