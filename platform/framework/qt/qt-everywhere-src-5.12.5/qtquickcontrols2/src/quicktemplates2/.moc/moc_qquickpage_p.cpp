/****************************************************************************
** Meta object code from reading C++ file 'qquickpage_p.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qquickpage_p.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qquickpage_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QQuickPage_t {
    QByteArrayData data[21];
    char stringdata0[345];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QQuickPage_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QQuickPage_t qt_meta_stringdata_QQuickPage = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QQuickPage"
QT_MOC_LITERAL(1, 11, 12), // "titleChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 13), // "headerChanged"
QT_MOC_LITERAL(4, 39, 13), // "footerChanged"
QT_MOC_LITERAL(5, 53, 26), // "implicitHeaderWidthChanged"
QT_MOC_LITERAL(6, 80, 27), // "implicitHeaderHeightChanged"
QT_MOC_LITERAL(7, 108, 26), // "implicitFooterWidthChanged"
QT_MOC_LITERAL(8, 135, 27), // "implicitFooterHeightChanged"
QT_MOC_LITERAL(9, 163, 19), // "contentWidthChanged"
QT_MOC_LITERAL(10, 183, 20), // "contentHeightChanged"
QT_MOC_LITERAL(11, 204, 5), // "title"
QT_MOC_LITERAL(12, 210, 6), // "header"
QT_MOC_LITERAL(13, 217, 11), // "QQuickItem*"
QT_MOC_LITERAL(14, 229, 6), // "footer"
QT_MOC_LITERAL(15, 236, 12), // "contentWidth"
QT_MOC_LITERAL(16, 249, 13), // "contentHeight"
QT_MOC_LITERAL(17, 263, 19), // "implicitHeaderWidth"
QT_MOC_LITERAL(18, 283, 20), // "implicitHeaderHeight"
QT_MOC_LITERAL(19, 304, 19), // "implicitFooterWidth"
QT_MOC_LITERAL(20, 324, 20) // "implicitFooterHeight"

    },
    "QQuickPage\0titleChanged\0\0headerChanged\0"
    "footerChanged\0implicitHeaderWidthChanged\0"
    "implicitHeaderHeightChanged\0"
    "implicitFooterWidthChanged\0"
    "implicitFooterHeightChanged\0"
    "contentWidthChanged\0contentHeightChanged\0"
    "title\0header\0QQuickItem*\0footer\0"
    "contentWidth\0contentHeight\0"
    "implicitHeaderWidth\0implicitHeaderHeight\0"
    "implicitFooterWidth\0implicitFooterHeight"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QQuickPage[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       9,   56, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x06 /* Public */,
       3,    0,   50,    2, 0x06 /* Public */,
       4,    0,   51,    2, 0x06 /* Public */,
       5,    0,   52,    2, 0x06 /* Public */,
       6,    0,   53,    2, 0x06 /* Public */,
       7,    0,   54,    2, 0x06 /* Public */,
       8,    0,   55,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
      11, QMetaType::QString, 0x00495903,
      12, 0x80000000 | 13, 0x0049590b,
      14, 0x80000000 | 13, 0x0049590b,
      15, QMetaType::QReal, 0x00c95903,
      16, QMetaType::QReal, 0x00c95903,
      17, QMetaType::QReal, 0x00c95801,
      18, QMetaType::QReal, 0x00c95801,
      19, QMetaType::QReal, 0x00c95801,
      20, QMetaType::QReal, 0x00c95801,

 // properties: notify_signal_id
       0,
       1,
       2,
    1879048201,
    1879048202,
       3,
       4,
       5,
       6,

 // properties: revision
       0,
       0,
       0,
       1,
       1,
       5,
       5,
       5,
       5,

       0        // eod
};

void QQuickPage::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QQuickPage *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->titleChanged(); break;
        case 1: _t->headerChanged(); break;
        case 2: _t->footerChanged(); break;
        case 3: _t->implicitHeaderWidthChanged(); break;
        case 4: _t->implicitHeaderHeightChanged(); break;
        case 5: _t->implicitFooterWidthChanged(); break;
        case 6: _t->implicitFooterHeightChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::titleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::headerChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::footerChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::implicitHeaderWidthChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::implicitHeaderHeightChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::implicitFooterWidthChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QQuickPage::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QQuickPage::implicitFooterHeightChanged)) {
                *result = 6;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
        case 1:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QQuickItem* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QQuickPage *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->title(); break;
        case 1: *reinterpret_cast< QQuickItem**>(_v) = _t->header(); break;
        case 2: *reinterpret_cast< QQuickItem**>(_v) = _t->footer(); break;
        case 3: *reinterpret_cast< qreal*>(_v) = _t->contentWidth(); break;
        case 4: *reinterpret_cast< qreal*>(_v) = _t->contentHeight(); break;
        case 5: *reinterpret_cast< qreal*>(_v) = _t->implicitHeaderWidth(); break;
        case 6: *reinterpret_cast< qreal*>(_v) = _t->implicitHeaderHeight(); break;
        case 7: *reinterpret_cast< qreal*>(_v) = _t->implicitFooterWidth(); break;
        case 8: *reinterpret_cast< qreal*>(_v) = _t->implicitFooterHeight(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QQuickPage *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setTitle(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setHeader(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 2: _t->setFooter(*reinterpret_cast< QQuickItem**>(_v)); break;
        case 3: _t->setContentWidth(*reinterpret_cast< qreal*>(_v)); break;
        case 4: _t->setContentHeight(*reinterpret_cast< qreal*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QQuickPage::staticMetaObject = { {
    &QQuickPane::staticMetaObject,
    qt_meta_stringdata_QQuickPage.data,
    qt_meta_data_QQuickPage,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QQuickPage::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QQuickPage::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QQuickPage.stringdata0))
        return static_cast<void*>(this);
    return QQuickPane::qt_metacast(_clname);
}

int QQuickPage::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QQuickPane::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 9;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 9;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void QQuickPage::titleChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QQuickPage::headerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QQuickPage::footerChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QQuickPage::implicitHeaderWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QQuickPage::implicitHeaderHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void QQuickPage::implicitFooterWidthChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void QQuickPage::implicitFooterHeightChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
// If you get a compile error in this function it can be because either
//     a) You are using a NOTIFY signal that does not exist. Fix it.
//     b) You are using a NOTIFY signal that does exist (in a parent class) but has a non-empty parameter list. This is a moc limitation.
Q_DECL_UNUSED static void checkNotifySignalValidity_QQuickPage(QQuickPage *t) {
    t->contentWidthChanged();
    t->contentHeightChanged();
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
