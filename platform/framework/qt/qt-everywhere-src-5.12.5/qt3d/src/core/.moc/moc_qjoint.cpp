/****************************************************************************
** Meta object code from reading C++ file 'qjoint.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../transforms/qjoint.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qjoint.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qt3DCore__QJoint_t {
    QByteArrayData data[27];
    char stringdata0[352];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qt3DCore__QJoint_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qt3DCore__QJoint_t qt_meta_stringdata_Qt3DCore__QJoint = {
    {
QT_MOC_LITERAL(0, 0, 16), // "Qt3DCore::QJoint"
QT_MOC_LITERAL(1, 17, 12), // "scaleChanged"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 5), // "scale"
QT_MOC_LITERAL(4, 37, 15), // "rotationChanged"
QT_MOC_LITERAL(5, 53, 8), // "rotation"
QT_MOC_LITERAL(6, 62, 18), // "translationChanged"
QT_MOC_LITERAL(7, 81, 11), // "translation"
QT_MOC_LITERAL(8, 93, 24), // "inverseBindMatrixChanged"
QT_MOC_LITERAL(9, 118, 17), // "inverseBindMatrix"
QT_MOC_LITERAL(10, 136, 16), // "rotationXChanged"
QT_MOC_LITERAL(11, 153, 9), // "rotationX"
QT_MOC_LITERAL(12, 163, 16), // "rotationYChanged"
QT_MOC_LITERAL(13, 180, 9), // "rotationY"
QT_MOC_LITERAL(14, 190, 16), // "rotationZChanged"
QT_MOC_LITERAL(15, 207, 9), // "rotationZ"
QT_MOC_LITERAL(16, 217, 11), // "nameChanged"
QT_MOC_LITERAL(17, 229, 4), // "name"
QT_MOC_LITERAL(18, 234, 8), // "setScale"
QT_MOC_LITERAL(19, 243, 11), // "setRotation"
QT_MOC_LITERAL(20, 255, 14), // "setTranslation"
QT_MOC_LITERAL(21, 270, 20), // "setInverseBindMatrix"
QT_MOC_LITERAL(22, 291, 12), // "setRotationX"
QT_MOC_LITERAL(23, 304, 12), // "setRotationY"
QT_MOC_LITERAL(24, 317, 12), // "setRotationZ"
QT_MOC_LITERAL(25, 330, 7), // "setName"
QT_MOC_LITERAL(26, 338, 13) // "setToIdentity"

    },
    "Qt3DCore::QJoint\0scaleChanged\0\0scale\0"
    "rotationChanged\0rotation\0translationChanged\0"
    "translation\0inverseBindMatrixChanged\0"
    "inverseBindMatrix\0rotationXChanged\0"
    "rotationX\0rotationYChanged\0rotationY\0"
    "rotationZChanged\0rotationZ\0nameChanged\0"
    "name\0setScale\0setRotation\0setTranslation\0"
    "setInverseBindMatrix\0setRotationX\0"
    "setRotationY\0setRotationZ\0setName\0"
    "setToIdentity"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qt3DCore__QJoint[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       8,  148, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x06 /* Public */,
       4,    1,  102,    2, 0x06 /* Public */,
       6,    1,  105,    2, 0x06 /* Public */,
       8,    1,  108,    2, 0x06 /* Public */,
      10,    1,  111,    2, 0x06 /* Public */,
      12,    1,  114,    2, 0x06 /* Public */,
      14,    1,  117,    2, 0x06 /* Public */,
      16,    1,  120,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      18,    1,  123,    2, 0x0a /* Public */,
      19,    1,  126,    2, 0x0a /* Public */,
      20,    1,  129,    2, 0x0a /* Public */,
      21,    1,  132,    2, 0x0a /* Public */,
      22,    1,  135,    2, 0x0a /* Public */,
      23,    1,  138,    2, 0x0a /* Public */,
      24,    1,  141,    2, 0x0a /* Public */,
      25,    1,  144,    2, 0x0a /* Public */,
      26,    0,  147,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QQuaternion,    5,
    QMetaType::Void, QMetaType::QVector3D,    7,
    QMetaType::Void, QMetaType::QMatrix4x4,    9,
    QMetaType::Void, QMetaType::Float,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Float,   15,
    QMetaType::Void, QMetaType::QString,   17,

 // slots: parameters
    QMetaType::Void, QMetaType::QVector3D,    3,
    QMetaType::Void, QMetaType::QQuaternion,    5,
    QMetaType::Void, QMetaType::QVector3D,    7,
    QMetaType::Void, QMetaType::QMatrix4x4,    9,
    QMetaType::Void, QMetaType::Float,   11,
    QMetaType::Void, QMetaType::Float,   13,
    QMetaType::Void, QMetaType::Float,   15,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void,

 // properties: name, type, flags
       3, QMetaType::QVector3D, 0x00495103,
       5, QMetaType::QQuaternion, 0x00495103,
       7, QMetaType::QVector3D, 0x00495103,
       9, QMetaType::QMatrix4x4, 0x00495103,
      11, QMetaType::Float, 0x00495103,
      13, QMetaType::Float, 0x00495103,
      15, QMetaType::Float, 0x00495103,
      17, QMetaType::QString, 0x00495103,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       4,
       5,
       6,
       7,

       0        // eod
};

void Qt3DCore::QJoint::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QJoint *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->scaleChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 1: _t->rotationChanged((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        case 2: _t->translationChanged((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 3: _t->inverseBindMatrixChanged((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 4: _t->rotationXChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 5: _t->rotationYChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 6: _t->rotationZChanged((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 7: _t->nameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->setScale((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 9: _t->setRotation((*reinterpret_cast< const QQuaternion(*)>(_a[1]))); break;
        case 10: _t->setTranslation((*reinterpret_cast< const QVector3D(*)>(_a[1]))); break;
        case 11: _t->setInverseBindMatrix((*reinterpret_cast< const QMatrix4x4(*)>(_a[1]))); break;
        case 12: _t->setRotationX((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 13: _t->setRotationY((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 14: _t->setRotationZ((*reinterpret_cast< float(*)>(_a[1]))); break;
        case 15: _t->setName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->setToIdentity(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QJoint::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::scaleChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(const QQuaternion & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::rotationChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(const QVector3D & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::translationChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(const QMatrix4x4 & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::inverseBindMatrixChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::rotationXChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::rotationYChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(float );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::rotationZChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (QJoint::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QJoint::nameChanged)) {
                *result = 7;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<QJoint *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVector3D*>(_v) = _t->scale(); break;
        case 1: *reinterpret_cast< QQuaternion*>(_v) = _t->rotation(); break;
        case 2: *reinterpret_cast< QVector3D*>(_v) = _t->translation(); break;
        case 3: *reinterpret_cast< QMatrix4x4*>(_v) = _t->inverseBindMatrix(); break;
        case 4: *reinterpret_cast< float*>(_v) = _t->rotationX(); break;
        case 5: *reinterpret_cast< float*>(_v) = _t->rotationY(); break;
        case 6: *reinterpret_cast< float*>(_v) = _t->rotationZ(); break;
        case 7: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<QJoint *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setScale(*reinterpret_cast< QVector3D*>(_v)); break;
        case 1: _t->setRotation(*reinterpret_cast< QQuaternion*>(_v)); break;
        case 2: _t->setTranslation(*reinterpret_cast< QVector3D*>(_v)); break;
        case 3: _t->setInverseBindMatrix(*reinterpret_cast< QMatrix4x4*>(_v)); break;
        case 4: _t->setRotationX(*reinterpret_cast< float*>(_v)); break;
        case 5: _t->setRotationY(*reinterpret_cast< float*>(_v)); break;
        case 6: _t->setRotationZ(*reinterpret_cast< float*>(_v)); break;
        case 7: _t->setName(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Qt3DCore::QJoint::staticMetaObject = { {
    &QNode::staticMetaObject,
    qt_meta_stringdata_Qt3DCore__QJoint.data,
    qt_meta_data_Qt3DCore__QJoint,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Qt3DCore::QJoint::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qt3DCore::QJoint::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Qt3DCore__QJoint.stringdata0))
        return static_cast<void*>(this);
    return QNode::qt_metacast(_clname);
}

int Qt3DCore::QJoint::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QNode::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 8;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 8;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Qt3DCore::QJoint::scaleChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Qt3DCore::QJoint::rotationChanged(const QQuaternion & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Qt3DCore::QJoint::translationChanged(const QVector3D & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Qt3DCore::QJoint::inverseBindMatrixChanged(const QMatrix4x4 & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Qt3DCore::QJoint::rotationXChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Qt3DCore::QJoint::rotationYChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Qt3DCore::QJoint::rotationZChanged(float _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Qt3DCore::QJoint::nameChanged(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
