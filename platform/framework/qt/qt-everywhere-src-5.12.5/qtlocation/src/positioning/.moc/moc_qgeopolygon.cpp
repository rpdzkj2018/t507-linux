/****************************************************************************
** Meta object code from reading C++ file 'qgeopolygon.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qgeopolygon.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeopolygon.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoPolygon_t {
    QByteArrayData data[26];
    char stringdata0[290];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoPolygon_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoPolygon_t qt_meta_stringdata_QGeoPolygon = {
    {
QT_MOC_LITERAL(0, 0, 11), // "QGeoPolygon"
QT_MOC_LITERAL(1, 12, 7), // "addHole"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 8), // "holePath"
QT_MOC_LITERAL(4, 30, 4), // "hole"
QT_MOC_LITERAL(5, 35, 5), // "index"
QT_MOC_LITERAL(6, 41, 10), // "removeHole"
QT_MOC_LITERAL(7, 52, 10), // "holesCount"
QT_MOC_LITERAL(8, 63, 9), // "translate"
QT_MOC_LITERAL(9, 73, 15), // "degreesLatitude"
QT_MOC_LITERAL(10, 89, 16), // "degreesLongitude"
QT_MOC_LITERAL(11, 106, 10), // "translated"
QT_MOC_LITERAL(12, 117, 6), // "length"
QT_MOC_LITERAL(13, 124, 9), // "indexFrom"
QT_MOC_LITERAL(14, 134, 7), // "indexTo"
QT_MOC_LITERAL(15, 142, 4), // "size"
QT_MOC_LITERAL(16, 147, 13), // "addCoordinate"
QT_MOC_LITERAL(17, 161, 14), // "QGeoCoordinate"
QT_MOC_LITERAL(18, 176, 10), // "coordinate"
QT_MOC_LITERAL(19, 187, 16), // "insertCoordinate"
QT_MOC_LITERAL(20, 204, 17), // "replaceCoordinate"
QT_MOC_LITERAL(21, 222, 12), // "coordinateAt"
QT_MOC_LITERAL(22, 235, 18), // "containsCoordinate"
QT_MOC_LITERAL(23, 254, 16), // "removeCoordinate"
QT_MOC_LITERAL(24, 271, 8), // "toString"
QT_MOC_LITERAL(25, 280, 9) // "perimeter"

    },
    "QGeoPolygon\0addHole\0\0holePath\0hole\0"
    "index\0removeHole\0holesCount\0translate\0"
    "degreesLatitude\0degreesLongitude\0"
    "translated\0length\0indexFrom\0indexTo\0"
    "size\0addCoordinate\0QGeoCoordinate\0"
    "coordinate\0insertCoordinate\0"
    "replaceCoordinate\0coordinateAt\0"
    "containsCoordinate\0removeCoordinate\0"
    "toString\0perimeter"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoPolygon[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       1,  160, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       4,       // flags
       0,       // signalCount

 // methods: name, argc, parameters, tag, flags
       1,    1,  104,    2, 0x02 /* Public */,
       4,    1,  107,    2, 0x02 /* Public */,
       6,    1,  110,    2, 0x02 /* Public */,
       7,    0,  113,    2, 0x02 /* Public */,
       8,    2,  114,    2, 0x02 /* Public */,
      11,    2,  119,    2, 0x02 /* Public */,
      12,    2,  124,    2, 0x02 /* Public */,
      12,    1,  129,    2, 0x22 /* Public | MethodCloned */,
      12,    0,  132,    2, 0x22 /* Public | MethodCloned */,
      15,    0,  133,    2, 0x02 /* Public */,
      16,    1,  134,    2, 0x02 /* Public */,
      19,    2,  137,    2, 0x02 /* Public */,
      20,    2,  142,    2, 0x02 /* Public */,
      21,    1,  147,    2, 0x02 /* Public */,
      22,    1,  150,    2, 0x02 /* Public */,
      23,    1,  153,    2, 0x02 /* Public */,
      23,    1,  156,    2, 0x02 /* Public */,
      24,    0,  159,    2, 0x02 /* Public */,

 // methods: parameters
    QMetaType::Void, QMetaType::QVariant,    3,
    QMetaType::QVariantList, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Int,
    QMetaType::Void, QMetaType::Double, QMetaType::Double,    9,   10,
    0x80000000 | 0, QMetaType::Double, QMetaType::Double,    9,   10,
    QMetaType::Double, QMetaType::Int, QMetaType::Int,   13,   14,
    QMetaType::Double, QMetaType::Int,   13,
    QMetaType::Double,
    QMetaType::Int,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 17,    5,   18,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 17,    5,   18,
    0x80000000 | 17, QMetaType::Int,    5,
    QMetaType::Bool, 0x80000000 | 17,   18,
    QMetaType::Void, 0x80000000 | 17,   18,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::QString,

 // properties: name, type, flags
      25, QMetaType::QVariantList, 0x00895103,

 // properties: revision
      12,

       0        // eod
};

void QGeoPolygon::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = reinterpret_cast<QGeoPolygon *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->addHole((*reinterpret_cast< const QVariant(*)>(_a[1]))); break;
        case 1: { QVariantList _r = _t->hole((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QVariantList*>(_a[0]) = std::move(_r); }  break;
        case 2: _t->removeHole((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: { int _r = _t->holesCount();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->translate((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 5: { QGeoPolygon _r = _t->translated((*reinterpret_cast< double(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< QGeoPolygon*>(_a[0]) = std::move(_r); }  break;
        case 6: { double _r = _t->length((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = std::move(_r); }  break;
        case 7: { double _r = _t->length((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = std::move(_r); }  break;
        case 8: { double _r = _t->length();
            if (_a[0]) *reinterpret_cast< double*>(_a[0]) = std::move(_r); }  break;
        case 9: { int _r = _t->size();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = std::move(_r); }  break;
        case 10: _t->addCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 11: _t->insertCoordinate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QGeoCoordinate(*)>(_a[2]))); break;
        case 12: _t->replaceCoordinate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QGeoCoordinate(*)>(_a[2]))); break;
        case 13: { QGeoCoordinate _r = _t->coordinateAt((*reinterpret_cast< int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QGeoCoordinate*>(_a[0]) = std::move(_r); }  break;
        case 14: { bool _r = _t->containsCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 15: _t->removeCoordinate((*reinterpret_cast< const QGeoCoordinate(*)>(_a[1]))); break;
        case 16: _t->removeCoordinate((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: { QString _r = _t->toString();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 10:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 14:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        case 15:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QGeoCoordinate >(); break;
            }
            break;
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = reinterpret_cast<QGeoPolygon *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QVariantList*>(_v) = _t->perimeter(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = reinterpret_cast<QGeoPolygon *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setPerimeter(*reinterpret_cast< QVariantList*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject QGeoPolygon::staticMetaObject = { {
    &QGeoShape::staticMetaObject,
    qt_meta_stringdata_QGeoPolygon.data,
    qt_meta_data_QGeoPolygon,
    qt_static_metacall,
    nullptr,
    nullptr
} };

QT_WARNING_POP
QT_END_MOC_NAMESPACE
