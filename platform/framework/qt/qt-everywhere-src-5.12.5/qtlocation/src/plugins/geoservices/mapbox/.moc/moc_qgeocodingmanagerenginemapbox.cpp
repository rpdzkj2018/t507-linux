/****************************************************************************
** Meta object code from reading C++ file 'qgeocodingmanagerenginemapbox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qgeocodingmanagerenginemapbox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qgeocodingmanagerenginemapbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QGeoCodingManagerEngineMapbox_t {
    QByteArrayData data[7];
    char stringdata0[103];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QGeoCodingManagerEngineMapbox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QGeoCodingManagerEngineMapbox_t qt_meta_stringdata_QGeoCodingManagerEngineMapbox = {
    {
QT_MOC_LITERAL(0, 0, 29), // "QGeoCodingManagerEngineMapbox"
QT_MOC_LITERAL(1, 30, 15), // "onReplyFinished"
QT_MOC_LITERAL(2, 46, 0), // ""
QT_MOC_LITERAL(3, 47, 12), // "onReplyError"
QT_MOC_LITERAL(4, 60, 20), // "QGeoCodeReply::Error"
QT_MOC_LITERAL(5, 81, 9), // "errorCode"
QT_MOC_LITERAL(6, 91, 11) // "errorString"

    },
    "QGeoCodingManagerEngineMapbox\0"
    "onReplyFinished\0\0onReplyError\0"
    "QGeoCodeReply::Error\0errorCode\0"
    "errorString"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QGeoCodingManagerEngineMapbox[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x08 /* Private */,
       3,    2,   25,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4, QMetaType::QString,    5,    6,

       0        // eod
};

void QGeoCodingManagerEngineMapbox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QGeoCodingManagerEngineMapbox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->onReplyFinished(); break;
        case 1: _t->onReplyError((*reinterpret_cast< QGeoCodeReply::Error(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QGeoCodingManagerEngineMapbox::staticMetaObject = { {
    &QGeoCodingManagerEngine::staticMetaObject,
    qt_meta_stringdata_QGeoCodingManagerEngineMapbox.data,
    qt_meta_data_QGeoCodingManagerEngineMapbox,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QGeoCodingManagerEngineMapbox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QGeoCodingManagerEngineMapbox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QGeoCodingManagerEngineMapbox.stringdata0))
        return static_cast<void*>(this);
    return QGeoCodingManagerEngine::qt_metacast(_clname);
}

int QGeoCodingManagerEngineMapbox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGeoCodingManagerEngine::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
