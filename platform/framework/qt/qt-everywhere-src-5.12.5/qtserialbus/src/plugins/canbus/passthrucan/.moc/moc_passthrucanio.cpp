/****************************************************************************
** Meta object code from reading C++ file 'passthrucanio.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../passthrucanio.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'passthrucanio.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PassThruCanIO_t {
    QByteArrayData data[23];
    char stringdata0[237];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PassThruCanIO_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PassThruCanIO_t qt_meta_stringdata_PassThruCanIO = {
    {
QT_MOC_LITERAL(0, 0, 13), // "PassThruCanIO"
QT_MOC_LITERAL(1, 14, 13), // "errorOccurred"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 11), // "description"
QT_MOC_LITERAL(4, 41, 26), // "QCanBusDevice::CanBusError"
QT_MOC_LITERAL(5, 68, 5), // "error"
QT_MOC_LITERAL(6, 74, 16), // "messagesReceived"
QT_MOC_LITERAL(7, 91, 21), // "QVector<QCanBusFrame>"
QT_MOC_LITERAL(8, 113, 6), // "frames"
QT_MOC_LITERAL(9, 120, 12), // "messagesSent"
QT_MOC_LITERAL(10, 133, 5), // "count"
QT_MOC_LITERAL(11, 139, 12), // "openFinished"
QT_MOC_LITERAL(12, 152, 7), // "success"
QT_MOC_LITERAL(13, 160, 13), // "closeFinished"
QT_MOC_LITERAL(14, 174, 4), // "open"
QT_MOC_LITERAL(15, 179, 7), // "library"
QT_MOC_LITERAL(16, 187, 6), // "subDev"
QT_MOC_LITERAL(17, 194, 7), // "bitRate"
QT_MOC_LITERAL(18, 202, 5), // "close"
QT_MOC_LITERAL(19, 208, 11), // "applyConfig"
QT_MOC_LITERAL(20, 220, 3), // "key"
QT_MOC_LITERAL(21, 224, 5), // "value"
QT_MOC_LITERAL(22, 230, 6) // "listen"

    },
    "PassThruCanIO\0errorOccurred\0\0description\0"
    "QCanBusDevice::CanBusError\0error\0"
    "messagesReceived\0QVector<QCanBusFrame>\0"
    "frames\0messagesSent\0count\0openFinished\0"
    "success\0closeFinished\0open\0library\0"
    "subDev\0bitRate\0close\0applyConfig\0key\0"
    "value\0listen"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PassThruCanIO[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,   59,    2, 0x06 /* Public */,
       6,    1,   64,    2, 0x06 /* Public */,
       9,    1,   67,    2, 0x06 /* Public */,
      11,    1,   70,    2, 0x06 /* Public */,
      13,    0,   73,    2, 0x06 /* Public */,

 // methods: name, argc, parameters, tag, flags
      14,    3,   74,    2, 0x02 /* Public */,
      18,    0,   81,    2, 0x02 /* Public */,
      19,    2,   82,    2, 0x02 /* Public */,
      22,    0,   87,    2, 0x02 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, 0x80000000 | 4,    3,    5,
    QMetaType::Void, 0x80000000 | 7,    8,
    QMetaType::Void, QMetaType::LongLong,   10,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void,

 // methods: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::QByteArray, QMetaType::UInt,   15,   16,   17,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::QVariant,   20,   21,
    QMetaType::Void,

       0        // eod
};

void PassThruCanIO::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<PassThruCanIO *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->errorOccurred((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< QCanBusDevice::CanBusError(*)>(_a[2]))); break;
        case 1: _t->messagesReceived((*reinterpret_cast< QVector<QCanBusFrame>(*)>(_a[1]))); break;
        case 2: _t->messagesSent((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 3: _t->openFinished((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->closeFinished(); break;
        case 5: _t->open((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QByteArray(*)>(_a[2])),(*reinterpret_cast< uint(*)>(_a[3]))); break;
        case 6: _t->close(); break;
        case 7: _t->applyConfig((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< const QVariant(*)>(_a[2]))); break;
        case 8: _t->listen(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (PassThruCanIO::*)(const QString & , QCanBusDevice::CanBusError );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PassThruCanIO::errorOccurred)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (PassThruCanIO::*)(QVector<QCanBusFrame> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PassThruCanIO::messagesReceived)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (PassThruCanIO::*)(qint64 );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PassThruCanIO::messagesSent)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (PassThruCanIO::*)(bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PassThruCanIO::openFinished)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (PassThruCanIO::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PassThruCanIO::closeFinished)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject PassThruCanIO::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_PassThruCanIO.data,
    qt_meta_data_PassThruCanIO,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *PassThruCanIO::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PassThruCanIO::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PassThruCanIO.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int PassThruCanIO::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void PassThruCanIO::errorOccurred(const QString & _t1, QCanBusDevice::CanBusError _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void PassThruCanIO::messagesReceived(QVector<QCanBusFrame> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void PassThruCanIO::messagesSent(qint64 _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PassThruCanIO::openFinished(bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void PassThruCanIO::closeFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
