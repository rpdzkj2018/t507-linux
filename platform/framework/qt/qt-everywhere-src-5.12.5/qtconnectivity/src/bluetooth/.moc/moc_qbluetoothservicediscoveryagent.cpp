/****************************************************************************
** Meta object code from reading C++ file 'qbluetoothservicediscoveryagent.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../qbluetoothservicediscoveryagent.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qbluetoothservicediscoveryagent.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QBluetoothServiceDiscoveryAgent_t {
    QByteArrayData data[22];
    char stringdata0[297];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QBluetoothServiceDiscoveryAgent_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QBluetoothServiceDiscoveryAgent_t qt_meta_stringdata_QBluetoothServiceDiscoveryAgent = {
    {
QT_MOC_LITERAL(0, 0, 31), // "QBluetoothServiceDiscoveryAgent"
QT_MOC_LITERAL(1, 32, 17), // "serviceDiscovered"
QT_MOC_LITERAL(2, 50, 0), // ""
QT_MOC_LITERAL(3, 51, 21), // "QBluetoothServiceInfo"
QT_MOC_LITERAL(4, 73, 4), // "info"
QT_MOC_LITERAL(5, 78, 8), // "finished"
QT_MOC_LITERAL(6, 87, 8), // "canceled"
QT_MOC_LITERAL(7, 96, 5), // "error"
QT_MOC_LITERAL(8, 102, 38), // "QBluetoothServiceDiscoveryAge..."
QT_MOC_LITERAL(9, 141, 5), // "start"
QT_MOC_LITERAL(10, 147, 13), // "DiscoveryMode"
QT_MOC_LITERAL(11, 161, 4), // "mode"
QT_MOC_LITERAL(12, 166, 4), // "stop"
QT_MOC_LITERAL(13, 171, 5), // "clear"
QT_MOC_LITERAL(14, 177, 5), // "Error"
QT_MOC_LITERAL(15, 183, 7), // "NoError"
QT_MOC_LITERAL(16, 191, 16), // "InputOutputError"
QT_MOC_LITERAL(17, 208, 15), // "PoweredOffError"
QT_MOC_LITERAL(18, 224, 28), // "InvalidBluetoothAdapterError"
QT_MOC_LITERAL(19, 253, 12), // "UnknownError"
QT_MOC_LITERAL(20, 266, 16), // "MinimalDiscovery"
QT_MOC_LITERAL(21, 283, 13) // "FullDiscovery"

    },
    "QBluetoothServiceDiscoveryAgent\0"
    "serviceDiscovered\0\0QBluetoothServiceInfo\0"
    "info\0finished\0canceled\0error\0"
    "QBluetoothServiceDiscoveryAgent::Error\0"
    "start\0DiscoveryMode\0mode\0stop\0clear\0"
    "Error\0NoError\0InputOutputError\0"
    "PoweredOffError\0InvalidBluetoothAdapterError\0"
    "UnknownError\0MinimalDiscovery\0"
    "FullDiscovery"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QBluetoothServiceDiscoveryAgent[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       2,   68, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       5,    0,   57,    2, 0x06 /* Public */,
       6,    0,   58,    2, 0x06 /* Public */,
       7,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   62,    2, 0x0a /* Public */,
       9,    0,   65,    2, 0x2a /* Public | MethodCloned */,
      12,    0,   66,    2, 0x0a /* Public */,
      13,    0,   67,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    7,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 10,   11,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // enums: name, alias, flags, count, data
      14,   14, 0x0,    5,   78,
      10,   10, 0x0,    2,   88,

 // enum data: key, value
      15, uint(QBluetoothServiceDiscoveryAgent::NoError),
      16, uint(QBluetoothServiceDiscoveryAgent::InputOutputError),
      17, uint(QBluetoothServiceDiscoveryAgent::PoweredOffError),
      18, uint(QBluetoothServiceDiscoveryAgent::InvalidBluetoothAdapterError),
      19, uint(QBluetoothServiceDiscoveryAgent::UnknownError),
      20, uint(QBluetoothServiceDiscoveryAgent::MinimalDiscovery),
      21, uint(QBluetoothServiceDiscoveryAgent::FullDiscovery),

       0        // eod
};

void QBluetoothServiceDiscoveryAgent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<QBluetoothServiceDiscoveryAgent *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->serviceDiscovered((*reinterpret_cast< const QBluetoothServiceInfo(*)>(_a[1]))); break;
        case 1: _t->finished(); break;
        case 2: _t->canceled(); break;
        case 3: _t->error((*reinterpret_cast< QBluetoothServiceDiscoveryAgent::Error(*)>(_a[1]))); break;
        case 4: _t->start((*reinterpret_cast< DiscoveryMode(*)>(_a[1]))); break;
        case 5: _t->start(); break;
        case 6: _t->stop(); break;
        case 7: _t->clear(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QBluetoothServiceInfo >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (QBluetoothServiceDiscoveryAgent::*)(const QBluetoothServiceInfo & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothServiceDiscoveryAgent::serviceDiscovered)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (QBluetoothServiceDiscoveryAgent::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothServiceDiscoveryAgent::finished)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (QBluetoothServiceDiscoveryAgent::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothServiceDiscoveryAgent::canceled)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (QBluetoothServiceDiscoveryAgent::*)(QBluetoothServiceDiscoveryAgent::Error );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QBluetoothServiceDiscoveryAgent::error)) {
                *result = 3;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QBluetoothServiceDiscoveryAgent::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_QBluetoothServiceDiscoveryAgent.data,
    qt_meta_data_QBluetoothServiceDiscoveryAgent,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *QBluetoothServiceDiscoveryAgent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QBluetoothServiceDiscoveryAgent::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QBluetoothServiceDiscoveryAgent.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QBluetoothServiceDiscoveryAgent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void QBluetoothServiceDiscoveryAgent::serviceDiscovered(const QBluetoothServiceInfo & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QBluetoothServiceDiscoveryAgent::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void QBluetoothServiceDiscoveryAgent::canceled()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QBluetoothServiceDiscoveryAgent::error(QBluetoothServiceDiscoveryAgent::Error _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
